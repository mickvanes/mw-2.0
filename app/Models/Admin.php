<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    protected $table = 'user_admin_adminrang';

    protected $fillable = [
        'userID',
        'admin',
        'adminRangID'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function role()
    {
        return $this->belongsTo(AdminRoles::class, 'adminRangID');
    }
}
