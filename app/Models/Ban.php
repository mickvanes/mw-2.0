<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ban extends Model
{
    use HasFactory;

    protected $table = 'bans';

    protected $fillable = [
        'userID', // the user who was banned
        'type', // 0 = message/reaction ban , 1 = account ban
        'reason', // the reason for the ban
        'note', // a note from the admin who issued the ban
        'bannedBy', // the admin who issued the ban
        'active', // 0 = inactive, 1 = active
        'permanent', // 0 = temporary, 1 = permanent
        'days', // permanent = 0, this is the number of days the ban will last
        'since', // the date the ban was issued
        'expires', // the date the ban will expire
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function bannedBy()
    {
        return $this->belongsTo(User::class, 'bannedBy');
    }
}
