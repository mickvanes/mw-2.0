@extends('layouts.default')

@section('header')
BANK
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1 md:grid-cols-3">
                        <div><b>Cash:</b> &euro;{{ number_format($userCash, 0, ',', '.') }}</div>
                        <div><b>Bank:</b> &euro;{{ number_format($userBank, 0, ',', '.') }}</div>
                        <div><b>Safe:</b> &euro;{{ number_format($userSafe, 0, ',', '.') }}</div>
                    </div>
                    <div class="mt-4">Your total amount of money:
                        &euro; {{number_format($userTotal, 0, ',', '.')}}</div>
                    <hr class="my-4">
                    <div class="grid lg:grid-cols-2 sm:grid-cols-1">
                        <form method="post" action="{{ route('bank.store') }}">
                            @csrf
                            <div class="grid grid-cols-3">
                            </div>
                            <div></div>
                            <div class="grid grid-cols-3 mt-4 mb-4">
                                <div><label for="safe">Transaction</label></div>
                                <div><input type="radio" id="withdraw"
                                            name="transaction_type"
                                            value="0"><label for="withdraw"> Withdraw</label></div>
                                <div><input type="radio" id="deposit"
                                            name="transaction_type"
                                            value="1" checked><label for="deposit"> Deposit</label></div>
                            </div>
                            <div></div>
                            <div class="grid grid-cols-3">
                                <div class="py-2"><label for="transaction">Cash &euro;</label></div>
                                <div class="col-span-2 text-black"><input class="text-black w-full md:w-3/4"
                                                                          type="number" id="transaction"
                                                                          name="transaction" min="0" placeholder="Cash"
                                                                          value="{{ $userCash }}"></div>
                            </div>
                            <input type="hidden" name="action" value="transaction">
                            <input type="submit" value="Submit"
                                   class="bg-darkButton hover:bg-darkHover dark:bg-buttonBlue mt-4 font-bold py-2 px-4">
                        </form>
                        <form method="post" action="{{ route('bank.store') }}" class="mt-4 lg:mt-0"> {{--  --}}
                            @csrf
                            <div class="grid grid-cols-3">
                                <div><label for="player">Type</label></div>
                                <div class="col-span-2 text-black">
                                    <select name="trans_type" class="text-black" id="type"
                                            onchange="cash(document.getElementById('trans').value,this.value)">
                                        <option value="player">Player</option>
                                        @if($hasFamily)
                                            <option value="family">Family</option>
                                        @endif
                                    </select>
                                </div>

                            </div>
                            <div class="grid grid-cols-3 mt-2">

                                <div><label id="label" for="player">Player</label></div>
                                <div class="col-span-2"><input id="input" class="text-black w-full md:w-3/4" type="text"
                                                               list="friends" name="player" autocomplete="off"
                                                               placeholder="Name" aria-label="player"></div>
                                <datalist id="friends">
                                    @foreach($friends as $friend)
                                        @if($friend->user->id == Auth::id())
                                            <option
                                                value="{{ $friend->friend->username }}">{{ ucfirst($friend->friend->username) }}</option>
                                        @else
                                            <option
                                                value="{{ $friend->user->username }}">{{ ucfirst($friend->user->username) }}</option>
                                        @endif
                                    @endforeach
                                </datalist>
                            </div>
                            <div class="grid grid-cols-3 mt-2">
                                <div><label for="message">Message</label></div>
                                <div class="col-span-2"><input type="text" class="text-black w-full md:w-3/4" id="text"
                                                               name="text" placeholder="Message" maxlength="20"></div>
                            </div>
                            <div class="grid grid-cols-3 mt-2">
                                <div class="py-2"><label for="trans">Cash &euro; <a
                                            onclick="transAll(<?=$userCash?>,document.getElementById('type').value)">[all]</a></label>
                                </div>
                                <div class="col-span-2"><input type="number" id="trans"
                                                               name="trans" class="text-black w-full md:w-3/4" min="100"
                                                               placeholder="Cash"
                                                               onchange="cash(this.value,document.getElementById('type').value)">
                                </div>
                            </div>
                            <div class="grid grid-cols-3 mt-2">
                                <div class="py-2"><label for="trans">Netto: &euro;</label></div>
                                <label class="py-2" id="netto"></label>
                            </div>
                            <div>
                                <div x-data={trans:false}>
                                    <p class="flex">
                                        <button @click="trans=!trans"
                                                class="bg-gray-500 hover:bg-gray-700 mt-4 text-white font-bold py-2 px-4"
                                                type="button">
                                            Transaction Costs
                                        </button>
                                    </p>
                                    <div x-show="trans">
                                        <table class="mt-4">
                                            <tr>
                                                <td></td>
                                                <td class="pl-4 w-36">Transaction Costs</td>
                                                <td class="pl-4">Player</td>
                                                <td class="pl-4">Family</td>
                                            </tr>
                                            <tr>
                                                <td>>=</td>
                                                <td class="pl-4 w-36">
                                                    &euro; {{ number_format(($rules->transMax * 0.5), 0, ',', '.') }}</td>
                                                <td class="pl-4">5%</td>
                                                <td class="pl-4">2.5%</td>
                                            </tr>
                                            <tr>
                                                <td> <</td>
                                                <td class="pl-4 w-36">
                                                    &euro; {{ number_format(($rules->transMax * 0.5), 0, ',', '.') }}</td>
                                                <td class="pl-4">10%</td>
                                                <td class="pl-4">5%</td>
                                            </tr>
                                            <tr>
                                                <td>MAX</td>
                                                <td class="pl-4 w-36">
                                                    &euro; {{ number_format(($rules->transMax), 0, ',', '.') }}</td>
                                                <td class="pl-4">
                                                    &euro; {{ number_format(($rules->transMax), 0, ',', '.') }}</td>
                                                <td class="pl-4"> &infin;</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="action" value="send">
                            <input type="submit" value="Submit"
                                   class="bg-darkButton hover:bg-darkHover mt-4 text-white font-bold py-2 px-4">
                        </form>
                    </div>
                    <div>
                        <hr class="mt-4">
                        <div x-data={show:false}>
                            <p class="flex">
                                <button @click="show=!show"
                                        class="bg-gray-500 hover:bg-gray-700 mt-4 text-white font-bold py-2 px-4"
                                        type="button">
                                    Show latest 5 transactions
                                </button>
                            </p>
                            <div x-show="show">
                                <div class="grid grid-cols-1 md:grid-cols-3 mt-2">
                                    <hr class="md:hidden mb-4">
                                    <div class="w-11/12 mb-4 md:mb-0">
                                        <h3>Sent Transactions <a href="{{ route('player.transactions') }}">[all]</a>
                                        </h3>
                                        <table class="table-fixed m-auto w-full">
                                            <thead>
                                            <tr>
                                                <th class="text-left">To Player</th>
                                                <th class="text-center">Cash</th>
                                                <th class="text-right">Time</th>
                                            </tr>
                                            </thead>
                                            @foreach($lastTransactionsToPlayer as $toPlayerItem)
                                                <tr>
                                                    <td class="text-left"><a
                                                            href="{{route("player.show", $toPlayerItem->receiver->username )}}">{{ $toPlayerItem->receiver->username }}</a>
                                                    </td>
                                                    <td class="text-center">
                                                        &euro;{{ number_format($toPlayerItem->nettoCash, 0, ',', '.') }}</td>
                                                    <td class="text-right">{{ $toPlayerItem->created_at }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    <hr class="md:hidden mb-4">
                                    <div class="w-11/12 mb-4 md:mb-0">
                                        <h3>Received Transactions <a href="{{ route('player.transactions') }}">[all]</a>
                                        </h3>
                                        <table class="table-fixed m-auto w-full">
                                            <thead>
                                            <tr>
                                                <th class="text-left">From Player</th>
                                                <th class="text-center">Cash</th>
                                                <th class="text-right">Time</th>
                                            </tr>
                                            </thead>

                                            @foreach($lastTransactionsFromPlayer as $fromPlayerItem)
                                                <tr>
                                                    <td class="text-left"><a
                                                            href="{{route("player.show", $fromPlayerItem->sender->username )}}">{{ $fromPlayerItem->sender->username }}</a>
                                                    </td>
                                                    <td class="text-center">
                                                        &euro;{{ number_format($fromPlayerItem->nettoCash, 0, ',', '.') }}</td>
                                                    <td class="text-right">{{ $fromPlayerItem->created_at }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    <hr class="md:hidden mb-4">
                                    <div class="w-11/12 mb-4 md:mb-0">
                                        <h3>Family Transactions <a href="{{ route('player.transactions') }}">[all]</a>
                                        </h3>
                                        <table class="table-fixed m-auto w-full">
                                            <thead>
                                            <tr>
                                                <th class="text-left">Family</th>
                                                <th class="text-center">Cash</th>
                                                <th class="text-right">Time</th>
                                            </tr>
                                            </thead>
                                            @foreach($lastTransactionsToFamily as $toFamilyItem)
                                                <tr>
                                                    <td class="text-left"><a
                                                            href="{{route("family", $toFamilyItem->family->name )}}">{{ $toFamilyItem->family->name }}</a>
                                                    </td>
                                                    <td class="text-center">
                                                        &euro;{{ number_format($toFamilyItem->nettoCash, 0, ',', '.') }}</td>
                                                    <td class="text-right">{{ $toFamilyItem->created_at }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    function transAll(money, type) {
        document.getElementById('trans').value = money;
        cash(money, type);
    }

    function cash(cash, type) {
        function formatMoney(number) {
            return number.toLocaleString('nl-NL', {
                style: 'currency', currency: 'EUR', minimumFractionDigits: 0,
                maximumFractionDigits: 0
            });
        }

        if (cash >= 100) {
            if (type === "player") {
                document.getElementById("label").innerHTML = "Player";
                document.getElementById("input").disabled = false;
                document.getElementById("input").value = "";

                if (cash <= <?= $rules->transMax ?>) {
                    if (cash >= <?= $rules->transMax * 0.5 ?>) {
                        const cashNetto = Math.round(cash * <?= $rules->transCostsPlayerGreater ?>);
                        document.getElementById("netto").innerHTML = formatMoney(cashNetto);
                    } else if (cash < <?= $rules->transMax * 0.5 ?>) {
                        const cashNetto = Math.round(cash * <?= $rules->transCostsPlayerLess ?>);
                        document.getElementById("netto").innerHTML = formatMoney(cashNetto);
                    }
                } else {
                    const max = formatMoney(<?= $rules->transMax ?>);
                    document.getElementById("netto").innerHTML = "Max. transaction is " + max;
                }
            } else if (type === "family") {
                document.getElementById("label").innerHTML = "Family";
                document.getElementById("input").disabled = true;

                const famName = <?PHP echo(!empty($familyName) ? json_encode($familyName) : '""'); ?>;

                if (famName) {
                    document.getElementById("input").value = "<?= $familyName ?>";
                }

                if (cash >= <?= $rules->transMax * 0.5 ?>) {
                    const cashNetto = Math.round(cash * <?= $rules->transCostsFamGreater ?>);
                    document.getElementById("netto").innerHTML = formatMoney(cashNetto);
                } else if (cash < <?= $rules->transMax * 0.5 ?>) {
                    const cashNetto = Math.round(cash * <?= $rules->transCostsFamLess ?>);
                    document.getElementById("netto").innerHTML = formatMoney(cashNetto);
                }
            }
        } else {
            const min = formatMoney(100);
            document.getElementById("netto").innerHTML = "Min. transaction is " + min;
        }
    }

</script>
<style>
    input[type="text"]:disabled {
        background: #c4c2c2;
    }
</style>
@stop
