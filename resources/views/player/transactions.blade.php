@extends('layouts.default')

@section('header')
    RACE
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div x-data="{ show: 1 }">
                        <button  @click="show=1" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                            All Received Transaction
                        </button>
                        <button  @click="show=2" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                            All Sent Transaction
                        </button>
                        <button  @click="show=3" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                            All Family Transactions
                        </button>

                        <div x-show="show === 1">
                            <p>Received transactions</p>
                            <table class="table-fixed m-auto w-full">
                                <thead>
                                <tr>
                                    <th class="text-left">From Player</th>
                                    <th class="text-left">Bruto Cash</th>
                                    <th class="text-left">Netto Cash</th>
                                    <th class="text-left">Message</th>
                                    <th class="text-left">Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($fromUserTransactions as $fromUserItem)
                                    <tr>
                                        <td class="text-left"><a href="{{route("player.show", $fromUserItem->sender->username )}}">{{ $fromUserItem->sender->username }}</a></td>
                                        <td class="text-left">&euro; {{ number_format($fromUserItem->brutoCash, 0, ',', '.') }}</td>
                                        <td class="text-left">&euro; {{ number_format($fromUserItem->nettoCash, 0, ',', '.') }}</td>
                                        <td class="text-left">{{ $fromUserItem->message  }}</td>
                                        <td class="text-left">{{ $fromUserItem->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>{{ $fromUserTransactions->links() }}</div>
                        </div>
                        <div x-show="show === 2">
                            <p>Sent transactions</p>
                            <table class="table-fixed m-auto w-full">
                                <thead>
                                <tr>
                                    <th class="text-left">To Player</th>
                                    <th class="text-left">Bruto Cash</th>
                                    <th class="text-left">Netto Cash</th>
                                    <th class="text-left">Message</th>
                                    <th class="text-left">Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($toUserTransactions as $toUserItem)
                                    <tr>
                                        <td class="text-left"><a href="{{route("player.show", $toUserItem->receiver->username )}}">{{ $toUserItem->receiver->username }}</a></td>
                                        <td class="text-left">&euro; {{ number_format($toUserItem->brutoCash, 0, ',', '.') }}</td>
                                        <td class="text-left">&euro; {{ number_format($toUserItem->nettoCash, 0, ',', '.') }}</td>
                                        <td class="text-left">{{ $toUserItem->message  }}</td>
                                        <td class="text-left">{{ $toUserItem->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>{{ $toUserTransactions->links() }}</div>
                        </div>
                        <div x-show="show === 3">
                            <p>Family transactions</p>
                            <table class="table-fixed m-auto w-full">
                                <thead>
                                <tr>
                                    <th class="text-left">To Family</th>
                                    <th class="text-left">Bruto Cash</th>
                                    <th class="text-left">Netto Cash</th>
                                    <th class="text-left">Message</th>
                                    <th class="text-left">Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($toFamilyTransactions as $toFamilyItem)
                                    <tr>
                                        <td class="text-left"><a href="{{route("family", $toFamilyItem->family->name )}}">{{ $toFamilyItem->family->name }}</a></td>
                                        <td class="text-left">&euro; {{ number_format($toFamilyItem->brutoCash, 0, ',', '.') }}</td>
                                        <td class="text-left">&euro; {{ number_format($toFamilyItem->nettoCash, 0, ',', '.') }}</td>
                                        <td class="text-left">{{ $toFamilyItem->message }}</td>
                                        <td class="text-left">{{ $toFamilyItem->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>{{ $toUserTransactions->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

