@extends('layouts.default')

@section('header')
    Family {{ $familyProfile->name }}
@stop

@section('content')
    <style>

        .image{
            height: 300px;
            width: 300px;
            max-height: 300px;
            max-width: 300px;
        }

        img {
            display: inline-block!important;
        }

        #content img{
            max-width: 100%;
            height: auto;
            width: auto\9; /* ie8 */
        }

    </style>

    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @include('modules.flash-messages')

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 relative"
                                 role="alert">
                                <strong class="font-bold">Family</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 relative"
                                 role="alert">
                                <strong class="font-bold">Family</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                    <div class="flex flex-row flex-wrap w-full">
                        <div class="basis-full md:basis-1/2">
                            @if(!$familyProfile->avatar == "")
                                <img src="{{ $familyProfile->avatar }}" class="max-w-300" style="height: 300px; width: 300px; max-height: 300px; max-width: 300px;" nonce="{{ csp_nonce() }}">
                            @else
                                <img src="{{ asset('images/stock.png') }}" class="image">
                            @endif
                        </div>
                        <div class="basis-full md:basis-1/2 mt-4 md:mt-0 md:ml-4">
                            <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                                <div class="grid grid-cols-2 gap-x-6">
                                    <div><b>Name: </b></div>
                                    <div>{{ $familyProfile->name }}</div>
                                    <div><b>Owner:</b></div>
                                    <div><a href="{{route('player.show', $owner->user->username)}}">{{$owner->user->username}}</a></div>
                                    <div><b>Shares:</b></div>
                                    <div> {{ number_format($familyProfile->shares, 0, ',', '.') }}</div>
                                    <div><b>Exit Costs:</b></div>
                                    <div> $ {{ number_format($familyProfile->exitCost, 0, ',', '.') }}</div>
                                    <div><b>Creation Date:</b></div>
                                    <div> {{ $familyProfile->creation_date }}</div>
                                    <div><b>Houses:</b></div>
                                    <div> {{ number_format($familyProfile->houses, 0, ',', '.') }}</div>
                                    <div><b>Energy:</b></div>
                                    <div> {{ number_format($familyProfile->energy, 0, ',', '.') }}</div>
                                    <div><b>Land:</b></div>
                                    <div> {{ number_format($familyProfile->land, 0, ',', '.') }} m2</div>
                                    <div><b>Walls:</b></div>
                                    <div> {{ number_format($familyProfile->walls, 0, ',', '.') }}</div>
                                    <div><b>Reputation:</b></div>
                                    <div>{{ number_format($famStats->rep + $familyProfile->famRep , 0, ',', '.') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="content" class="my-10">
                        {!! (new \App\Helpers\Helper)->makeUBB($familyProfile->profileText) !!}
                    </div>

                    <div class="my-10">
                        <button class="bg-gray-500 text-white font-bold py-2 px-2" style="font-size: 100%;">
                            <a href="{{route('family.click', $familyProfile->name)}}">Click</a>
                        </button>
                        <button onclick="showPromotion(0)" class="bg-gray-500 text-white font-bold py-2 px-2" style="font-size: 100%;">
                            Promotion
                        </button>
                        <button class="bg-gray-500 text-white font-bold py-2 px-4" style="font-size: 100%;">
                            Attack
                        </button>
                        <button class="bg-gray-500 text-white font-bold py-2 px-4 lg:mt-0 mt-1" style="font-size: 100%;">
                            Objects
                        </button>
                    </div>

                    <div class="my-10">
                        <div class="grid grid-cols-2 gap-4">
                            <div>
                                <div class="grid grid-cols-2 gap-4">
                                    <div>
                                        <div>&nbsp;</div>
                                        <div>Loan hourly:</div>
                                        <div>Loan daily:</div>
                                    </div>
                                    <div>
                                        <div>&nbsp;</div>
                                        <div>$ {{ number_format($familyProfile->shares * 200, 0, ',', '.') }}</div>
                                        <div>$ {{ number_format($familyProfile->shares * 200 * 24, 0, ',', '.') }}</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="grid grid-cols-3 gap-4">
                                    <div>
                                        <div>&nbsp;</div>
                                        <div>Attacks</div>
                                        <div>Defenses</div>
                                        <div>Total</div>
                                    </div>
                                    <div>
                                        <div>Won</div>
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                    </div>
                                    <div>
                                        <div>Lost</div>
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-10 md:p-8">
                        <table class="table-fixed mx-auto mt-4 ">
                            <thead>
                            <tr>
                                <th class="hidden sm:table-cell lg:table-cell"><b>ID</b></th>
                                <th><b>Name</b></th>
                                <th class="ml-10 lg:ml-0"><b>Rang</b></th>
                                <th class="ml-10 lg:ml-0"><b>Power</b></th>
                                <th class="ml-10 lg:ml-0"><b>Rank</b></th>
                                <th class="ml-10 lg:ml-0 text-center"><b>Online</b></th>
                            </tr>
                            </thead>

                            @foreach($familyMembers as $familyMember)
                                <tr onclick="window.location='{{route('player.show', $familyMember->user->username)}}';" class="hover:bg-lightSecondary">
                                    <td class="w-1/7 text-center hidden sm:table-cell lg:table-cell">{{$loop->index + 1}}</td>
                                    <td class="w-1/5 text-center"><a href="{{route('player.show', $familyMember->user->username)}}">{{$familyMember->user->username}}</a></td>
                                    <td class="w-1/5 ml-10 lg:ml-0 text-center">{{$familyMember->rang->name}}</td>
                                    <td class="w-1/5 break-words ml-10 lg:ml-0 text-center">{{ number_format($familyMember->user->profile->power, 0, ',', '.') }}</td>
                                    <td class="w-1/5 ml-10 lg:ml-0 text-center">{{$familyMember->user->getCurrentRank->ranks->rank}}</td>
                                    <td class="w-1/5 ml-10 lg:ml-0">
                                        <div class="w-full flex justify-content-center">
                                            @if(Cache::has('user-is-online-' . $familyMember->user->id))
                                                <svg width="15" height="15" class="m-auto">
                                                    <rect width="15" fill="green" height="15" rx="50"></rect>
                                                </svg>
                                            @else
                                                <svg width="15" height="15" class="m-auto">
                                                    <rect width="15" fill="red" height="15" rx="50"></rect>
                                                </svg>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                        @endforeach
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>

@stop













