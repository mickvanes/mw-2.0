<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detective extends Model
{
    use HasFactory;

    protected $table = 'user_dets';

    protected $fillable = [
        'userID',
        'playerID',
        'dets',
        'detsCountry',
        'maxTime',
        'endTime',
        'countryID',
        'found',
        'active'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function player()
    {
        return $this->belongsTo(User::class, 'playerID');
    }

    public function country()
    {
        return $this->belongsTo(Countries::class, 'countryID');
    }

    public function searchInCountry()
    {
        return $this->belongsTo(Countries::class, 'detsCountry');
    }

}
