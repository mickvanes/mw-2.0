<?php

namespace App\Http\Controllers;

use App\Models\CompletedRace;
use App\Models\Race;
use App\Models\Settings;
use App\Models\Stats;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RaceController extends Controller
{
    public function index(){
        $user = User::with('cash')->find(Auth::id());
        $races = Race::with('user')->where('hidden', '=', 0)->get();

        return view('race.index', compact('races', 'user'));
    }

    public function create(){
        $user = User::with('cash')->find(Auth::id());
        return view('race.create', compact('user'));
    }

    public function createForm(Request $request){
        $user = User::with('cash')->find(Auth::id());
        $races = Race::where('userID','=' , Auth::id())->count();

        if($races < 2){
            $this->validate($request, [
                'bet' => 'required|numeric|min:10000',
            ]);

            if($request->bet <= $user->cash->cash){
                DB::table('user_cash')
                    ->where('userID', Auth::id())
                    ->update(['cash' => $user->cash->cash - $request->bet]);

                $race = new Race;
                $race->userID = Auth::id();
                $race->bet = $request->bet;
                $race->save();

                return redirect()->route('race.create')->with('message', 'You created a race');
            }
            else{
                return redirect()->route('race.create')->with('error', "You don't have enough cash");
            }
        }
        else{
            return redirect()->route('race.create')->with('error', 'You have already 2 races');
        }
    }

    /**
     * @throws \Exception
     */

    public function take($id){
        $user = User::with('cash')->find(Auth::id());
        $race = Race::find($id);

        function sendMessages($data){
            $user1 = User::with('cash')->find($data[1]);
            $user2 = User::with('cash')->find($data[2]);

            $racerName1 = $user1->name;
            $racerName2 = $user2->name;

            if($data[5] == $data[1]){
                if(Settings::where('userID','=',$data[2])->get()->pluck('raceMessages')->first()) {
                    DB::table('system_messages')->insert([
                        'senderID' => 1,
                        'recipientID' => $data[2],
                        'subject' => "Race Result (id :" . $data[0] . ")",
                        'body' => "You LOST the race," . $racerName1 . " won €" . number_format($data[3], 0, ',', '.') . " !",
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'timestamp' => Carbon::now(),
                        'seen' => 0
                    ]);
                }

                if(Settings::where('userID','=',$data[1])->get()->pluck('raceMessages')->first()) {
                    DB::table('system_messages')->insert([
                        'senderID' => 1,
                        'recipientID' => $data[1],
                        'subject' => "Race Result (id :" . $data[0] . ")",
                        'body' => "You WON the race," . $racerName2 . " lost €" . number_format($data[3], 0, ',', '.') . " !",
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'timestamp' => Carbon::now(),
                        'seen' => 0
                    ]);
                }

                DB::table('user_cash')
                    ->where('userID', $data[1])
                    ->update(['cash' => $user1->cash->cash + $data[3]]);
            }

            elseif($data[5] == $data[2]){
                if(Settings::where('userID','=',$data[1])->get()->pluck('raceMessages')->first()){
                    DB::table('system_messages')->insert([
                        'senderID' => 1,
                        'recipientID' => $data[1],
                        'subject' => "Race Result (id :" . $data[0] . ")",
                        'body' => "You LOST the race," . $racerName2 . " won €" . number_format($data[3], 0, ',', '.') . " !",
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'timestamp' => Carbon::now(),
                        'seen' => 0
                    ]);
                }

                if(Settings::where('userID','=',$data[2])->get()->pluck('raceMessages')->first()){
                    DB::table('system_messages')->insert([
                        'senderID' => 1,
                        'recipientID' => $data[2],
                        'subject' => "Race Result (id :" . $data[0] . ")",
                        'body' => "You WON the race," . $racerName1 . " lost €" . number_format($data[3], 0, ',', '.') . " !",
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'timestamp' => Carbon::now(),
                        'seen' => 0
                    ]);
                }

                DB::table('user_cash')
                    ->where('userID', $data[2])
                    ->update(['cash' => $user2->cash->cash + $data[3]]);
            }
            else{
                return redirect()->action([RaceController::class, 'index'])->with('error', 'sendMessage Error');
            }
        }

        function insertDB($id, $winner){
            $race = Race::find($id);
            $winning = $race->bet * 2;
            $data = [$race->id,$race->userID,Auth::id(),$winning,Carbon::now('Europe/Amsterdam'),$winner];

            Race::destroy($id);

            $race_completed = new CompletedRace;
            $race_completed->racer1_ID = $race->userID;
            $race_completed->racer2_ID = Auth::id();
            $race_completed->bet = $winning;
            $race_completed->winner = $winner;
            $race_completed->save();

            if($data[1] == $winner){
                sendMessages($data);
                #$message = "You lost the Race";
                #return redirect()->action([RaceController::class, 'index'])->with('error', $message);
            }
            elseif($data[2] == $winner){
                sendMessages($data);
                #$message = "You won the Race";
                #return redirect()->action([RaceController::class, 'index'])->with('message', $message);
            }
            else{
                #$message = "FU";
                #return redirect()->action([RaceController::class, 'index'])->with('error', $message);
            }
        }

        if($race->hidden != 1){
            if(Auth::id() != $race->userID) {
                if ($user->cash->cash >= $race->bet) {
                    DB::table('races')
                        ->where('id', $id)
                        ->update(['hidden' => 1]);

                    DB::table('user_cash')
                        ->where('userID', Auth::id())
                        ->update(['cash' => $user->cash->cash - $race->bet]);

                    $randomInt = random_int(0, 1);

                    if ($randomInt == 0) {
                        $winner = $race->userID;
                        insertDB($id, $winner);
                        return redirect()->action([RaceController::class, 'index'])->with('error', "You lost the Race");
                    } elseif ($randomInt == 1) {
                        $winner = Auth::id();
                        insertDB($id, $winner);
                        return redirect()->action([RaceController::class, 'index'])->with('message', "You won the Race");
                    }
                } else {
                    return redirect()->action([RaceController::class, 'index'])->with('error', 'Not enough cash!');
                }
            }
            else{
                return redirect()->action([RaceController::class, 'index'])->with('error', "You can't race against yourself!");

            }
        }
        else{
            return redirect()->action([RaceController::class, 'index'])->with('error', 'This race is already been taken');
        }
    }
}
