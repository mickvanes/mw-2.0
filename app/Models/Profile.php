<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * Get the user that has this Profile
     */

    protected $table = 'profiles';

    protected $fillable = [
        'id',
        'rankAdv',
        'burstRank',
        'burstAdv',
        'bursts',
        'bullets',
        'employees',
        'clicks',
        'remClicks',
        'rep',
        'honor',
        'power',
        'health',
        'kills',
        'bfKills',
        'killPoints',
        'bfKillPoints',
        'coins',
        'profileText',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function rank()
    {
        return $this->belongsTo(BurstRank::class, 'burstRank');
    }


}
