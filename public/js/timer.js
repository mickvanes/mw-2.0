function countdown(date,func,id){
    tick(date,func,id);
    setInterval(tick, 1000, date,func,id);
}

function tick(date,func,id) {
    const now = new Date();

    const dateFull = date.split(" ");
    const time = dateFull[1].split(":");
    const dateSep = dateFull[0].split("/");

    const countdown = new Date(dateSep[2], dateSep[1] - 1, dateSep[0], time[0], time[1], time[2]);

    const distance = countdown - now;

    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    if(func === "crime"){
        document.getElementById(id).innerHTML = "You have to wait for: " + minutes + " minutes, " + seconds + " seconds ";
        if (days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0) {
            document.location.reload(true);
        }
    }

    else if(func === "hidden"){
        document.getElementById(id).innerHTML = "You have to wait for: " + hours + " hours, " + minutes + " minutes, " + seconds + " seconds ";
        if (days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0) {
            document.location.reload(true);
        }
    }

    else if(func === "prison"){
        document.getElementById(id).innerHTML = "You have to wait for: " + minutes + " minutes, " + seconds + " seconds ";

        setTimeout("document.location.reload(true);", 3000);

        if (days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0) {
            document.location.reload(true);
        }
    }

    else if(func === "dets"){
        document.getElementById(id).innerHTML = hours + "h " + minutes + "m"
    }

}
