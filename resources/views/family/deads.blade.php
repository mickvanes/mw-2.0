@extends('layouts.default')

@section('header')
    Family Deads
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-3 md:grid-cols-6">
                        @foreach($deads as $dead)
                                <div>
                                    {{ $dead->created_at }}
                                </div>
                                <div class="col-span-2 md:col-span-5 mb-1">
                                    @if($dead->witnesses->first())
                                        <span style="color: yellow;"><a href="{{ route('player.show', $dead->witnesses->first()->killer->username) }}"><b>{{ $dead->witnesses->first()->killer->username }}</b></a></span> killed
                                        <a href="{{ route('player.show', $dead->witnesses->first()->killer->username) }}"><b>{{ $dead->player->username }}</b></a> ({{ $dead->playerPercent }}%
                                        {{ $dead->playerRankName->rank }}), <b><a href="{{ route('player.show', $dead->player->username ) }}">{{ $dead->player->username }}</a></b> @if($dead->success) <span style="color: red;"><b>died</b></span> from the shots @else survived the shots @endif
                                    @else
                                        Anonymous killed <a href="{{ route('player.show', $dead->player->username ) }}"><b>{{ $dead->player->username }}</b></a> ({{ $dead->playerPercent }}%
                                        {{ $dead->playerRankName->rank }}), <b>{{ $dead->player->username }}</b> @if($dead->success) <span style="color: red;"><b>died</b></span> from the shots @else <span style="color: green;"><b>survived</b></span> the shots @endif
                                    @endif
                                </div>
                        @endforeach


                        </div>
                    <div class="w-full text-center">{{ $deadCountDay }} attack on the family in the last 24 hours.</div>
                    <div class="mt-4">{{ $deads->links()}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

