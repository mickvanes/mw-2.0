@extends('layouts.default')

@section('header')
    ATTACK RIP LOGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div>
                            <table class="table-fixed w-full mb-2">
                                Attack
                                <thead>
                                    <tr>
                                        <th class="text-left">Defender</th>
                                        <th class="text-left">Cash</th>
                                        <th class="text-left">Won/Loss</th>
                                        <th class="text-left">Time</th>
                                    </tr>
                                </thead>

                                @foreach($ripAttack as $ripAttackItem)
                                    <tr>
                                        <td><a href="{{ route('player.show',$ripAttackItem->defender->username) }}">{{ $ripAttackItem->defender->username }}</a></td>
                                        <td>&euro; {{ number_format($ripAttackItem->cash, 0, ',', '.') }}</td>
                                        <td>
                                            @if($ripAttackItem->result == 0)
                                                Loss
                                            @else
                                                Won
                                            @endif
                                        </td>
                                        <td>{{ $ripAttackItem->created_at }}</td>
                                    </tr>
                                @endforeach
                            </table>
                            {{ $ripAttack->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

