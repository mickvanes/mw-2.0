<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessBank extends Model
{
    use HasFactory;

    protected $table = 'business_bank';

    protected $fillable = [
        'bank',
        'ownerID'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'id', 'ownerID');
    }

    public function business()
    {
        return $this->belongsTo(Business::class, 'id');
    }
}
