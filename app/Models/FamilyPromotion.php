<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyPromotion extends Model
{
    use HasFactory;

    protected $table = 'family_promotion';

    protected $fillable = [
        'familyID',
        'emptysuit',
        'deliveryboy',
        'picciotto',
        'shoplifter',
        'pickpocket',
        'thief',
        'associate',
        'mobster',
        'soldier',
        'swindler',
        'assassin',
        'localchief',
        'chief',
        'bruglione',
        'godfather',
    ];

}
