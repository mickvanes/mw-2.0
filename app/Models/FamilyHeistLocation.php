<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyHeistLocation extends Model
{
    use HasFactory;

    protected $table = 'family_heists_locations';
}
