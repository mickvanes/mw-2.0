<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BurstRank extends Model
{
    use HasFactory;

    protected $table = 'burstRanks';

    protected $fillable = [
        'id',
        'name',
        'percent',
    ];

}
