  // tailwind.config.js
  const colors = require('tailwindcss/colors')

  module.exports = {
    purge: [
      './resources/**/*.blade.php',
      './resources/**/*.js',
      './resources/**/*.vue',
    ],
     darkMode: 'class', // or 'media' or 'class'
     theme: {
         colors: {
             transparent: 'transparent',
             current: 'currentColor',
             black: colors.black,
             white: colors.white,
             gray: colors.gray,
             green: colors.emerald,
             indigo: colors.indigo,
             yellow: colors.yellow,
             red: colors.red,
             lightDefault: '#262626',
             lightSecondary: '#3d3d3d',
             darkDefault: '#1e1e1e',
             darkSecondary: '#121212',
             darkCard:'#535c70',
             darkButton:'#3277b3',
             darkHover:'#2d6a9f',
             darkDisabled:'#464e5e',
             menu: '#171717',
             abc: '#3277b3',
             abcHover: '#2d6a9f',
             abcWhite:'#e2e4e9',
             abcWhiteHover:'#c4c9d4',
             abcTextDark:'#686d76',
             abcPlaceholder:'#a4acbd',
             abcDisabled:'#e2e4e9',
             alertText: '#B91C1C',
             alertDiv: '#FEE2E2',
             alertBorder: '#F87171',
         },
     },
     variants: {
        extend: {
            opacity: ['disabled'],
            colors: {
                red: {
                    50: '#FEF2F2',
                    100: '#FEE2E2',
                    200: '#FECACA',
                    300: '#FCA5A5',
                    400: '#F87171',
                    500: '#EF4444',
                    600: '#DC2626',
                    700: '#B91C1C',
                    800: '#991B1B',
                    900: '#7F1D1D',
                },
            },
        },
     },
     plugins: [require('@tailwindcss/forms')],
   }
