@extends('layouts.default')

@section('header')
    DASHBOARD
@stop

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success mb-4">
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                 role="alert">
                <strong class="font-bold">Dashboard</strong>
                <span class="block sm:inline">{{ session()->pull('message') }}</span>
            </div>
        </div><br>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger mb-4">
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                 role="alert">
                <strong class="font-bold">Dashboard</strong>
                <span class="block sm:inline">{{ session()->pull('error') }}</span>
            </div>
        </div><br>
    @endif

    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1 md:grid-cols-2">
                        <div>
                            <b>Name:</b> <a href="{{ route('player.show', $user->username) }}">{{ $user->username }}</a> <br>
                            @if($user->family)
                                <b>Family :</b> <a href="{{route('family', $user->family->familyDetails->name )}}">{{ $user->family->familyDetails->name }}</a> <span class="font-bold">({{ $user->getFamilyRang() }})</span><br>
                            @elseif($user->isRecruit)
                                <b>Family :</b> <a href="{{route('family', $user->isRecruit->family->name)}}">{{ $user->isRecruit->family->name }}-recruit</a><br>
                            @else
                                <b>Family :</b> None<br>
                            @endif

                            <b>Rank:</b> {{ $user->getCurrentRank->ranks->rank }} ({{ $user->getCurrentRank->adv }}%)<br>
                            <b>Country:</b> {{ $user->country->countryDetails->name }} <br>
                            @if($user->isMarriedPlayer1 || $user->isMarriedPlayer2)
                                @if($user->isMarriedPlayer1)
                                    <b>Married with:</b> <a href="{{ route('player.show', $user->isMarriedPlayer1->player2Details->username) }}">{{$user->isMarriedPlayer1->player2Details->username}}</a><br>
                                @else
                                    <b>Married with:</b> <a href="{{ route('player.show', $user->isMarriedPlayer2->player1Details->username) }}">{{$user->isMarriedPlayer2->player1Details->username}}</a><br>
                                @endif
                            @else
                                <b>Marital status: Single</b><br>
                            @endif<br>
                            <b>Clicks:</b> {{ number_format($user->profile->clicks, 0, ',', '.') }} <br>
                            <b>Power:</b> {{ number_format($user->profile->power, 0, ',', '.') }} <br>

                            <b>Reputation:</b> {{ number_format($user->profile->rep, 0, ',', '.') }} <br>
                            <b>Honor:</b> {{ number_format($user->profile->honor, 0, ',', '.') }} <br><br>

                            <div class="grid grid-cols-1 md:grid-cols-2 w-1/2">
                                <div><b>Health:</b></div>
                                <div class="grid grid-cols-4">
                                    <div class="col-span-3 border border-1 border-black">
                                        @if($user->profile->health <= 100)
                                        <div class="bg-green-900 float-left text-white text-center font-bold" style="width: {{ $user->profile->health }}%">
                                            @if($user->profile->health > 50)
                                                <span class="font-bold">{{ $user->profile->health }}%</span>
                                            @else
                                                &nbsp;
                                            @endif</div>
                                        <div class="bg-red-900 float-left text-white text-center font-bold" style="width: {{ 100 - $user->profile->health }}%">
                                            @if($user->profile->health != 100 && $user->profile->health <= 50)
                                                <span class="font-bold">{{ $user->profile->health }}%</span>
                                            @else
                                                &nbsp;
                                            @endif</div>
                                        @else
                                            <div class="bg-green-900 float-left text-white text-center font-bold" style="width: 100%">
                                                @if($user->profile->health > 50)
                                                    <span class="font-bold">{{ $user->profile->health }}%</span>
                                                @else
                                                    &nbsp;
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div><br>
                        </div>

                        <div class="grid grid-cols-1">
                            <div>
                                <div><b>Bullets:</b> {{ number_format($user->profile->bullets, 0, ',', '.') }}</div>
                                <div><b>Kills (Kill points):</b> {{ number_format($killCount, 0, ',', '.') }} ({{ $killPoints }})</div>
                                <div><b>Backfire Kills (BF points):</b> {{ number_format($backfireKillCount, 0, ',', '.') }} ({{ $bfKillPoints }})</div>
                            </div>
                            <div>
                                <div><b>Crime:</b> {{ number_format($user->stats->crime, 0, ',', '.') }}</div>
                                <div><b>Car:</b> {{ number_format($user->stats->car, 0, ',', '.') }}</div>
                                <div><b>Hostage:</b> {{ number_format($user->stats->hostage, 0, ',', '.') }} </div>
                                <div><b>Employees:</b> {{ number_format($user->stats->employees, 0, ',', '.') }}</div>
                                <div><b>Races:</b> {{ number_format($user->stats->races, 0, ',', '.') }} </div>
                            </div>

                            <div>
                                <div><b>Coins:</b> {{ number_format($user->profile->coins, 0, ',', '.') }} </div>
                                <div><a>Buy coins here</a></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop

