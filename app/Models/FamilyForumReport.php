<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyForumReport extends Model
{
    use HasFactory;

    protected $table = 'familyforum_report';

    protected $fillable = [
        'type',
        'itemID',
        'content',
        'user',
        'family_id',
        'repUser',
        'count',
        'treatedBy',
        'active',
        'approved',
        'declined',
    ];

    public function item()
    {
        if($this->type == "famComment"){
            return $this->belongsTo(FamilyForumReaction::class, 'id','itemID');
        }
        elseif($this->type == "famTopic"){
            return $this->belongsTo(FamilyForumTopic::class, 'id','itemID');
        }
        else{
            return [];
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id','user');
    }

    public function treatedBy()
    {
        return $this->belongsTo(User::class, 'id','treatedBy');
    }

    public function family()
    {
        return $this->belongsTo(Family::class, 'id','family_id');
    }

    public function repUser()
    {
        return $this->belongsTo(User::class, 'id','repUser');
    }


}
