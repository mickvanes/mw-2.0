<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'userID',
        'bet'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

}
