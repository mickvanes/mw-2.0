<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.css') }}">
{{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">--}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

<x-app-layout>
    <style>
        #outbox_length > label:nth-child(1) > select:nth-child(1){
            padding-right: 20px;
        }
    </style>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table id="outbox" class="mx-auto">
                        <thead>
                        <tr>
                            <th class="hidden sm:table-cell lg:table-cell"><b>Read/Unread</b></th>
                            <th class="lg:pl-24 sm:table-cell lg:table-cell "><b>Recipient</b></th>
                            <th class="pl-10 lg:pl-24 sm:table-cell lg:table-cell "><b>Subject</b></th>
                            <th class="pl-10 lg:pl-24 sm:table-cell lg:table-cell "><b>Received on</b></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $message)
                            <tr class="@if($message->seen == false) text-bold @endif">
                                <td class="hidden sm:table-cell lg:table-cell  text-center">
                                    <div class="w-full flex justify-content-center">
                                    @if($message->seen == true)
                                        <svg width="15" height="15" class="m-auto">
                                            <rect width="15" fill="green" height="15" rx="50"></rect>
                                        </svg>
                                    @else
                                        <svg width="15" height="15" class="m-auto">
                                            <rect width="15" fill="red" height="15" rx="50"></rect>
                                        </svg>
                                    @endif
                                    </div>
                                </td>
                                <td class="sm:table-cell lg:table-cell  lg:pl-24 text-center"><a
                                        href="{{route('player.show', $message->recipient->username)}}">{{$message->recipient->username}}</a>
                                </td>
                                <td class="sm:table-cell lg:table-cell  pl-10 lg:pl-24 text-center"><a
                                        href="{{route('message.show', $message->id)}}">{{$message->subject}}</a>
                                </td>
                                <td class="sm:table-cell lg:table-cell  pl-10 lg:pl-24 text-center">{{$message->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready( function () {
            $('#outbox').DataTable(
                {
                    "order": [[ 3, "desc" ]]
                }
            );
        } );
    </script>
</x-app-layout>

