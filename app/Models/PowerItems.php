<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PowerItems extends Model
{
    use HasFactory;

    protected $table = 'powerItems';

    protected $fillable = [
        'id',
        'name',
        'slug',
        'power',
        'needClicks',
        'image',
        'costs',
    ];

}
