
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="grid grid-cols-1 gap-2">
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg" style="max-width: 904px;">
                    <div class="mx-auto" style="max-width: 904px; width: 100%">
                        <div class="w-3/4 mx-auto text-center mb-4 mt-4 grid grid-cols-1">
                        </div>
                    </div>
                </div>
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg" style="max-width: 904px;">
                    <div class="p-6 bg-white border-b border-gray-200">

                        @if(session()->has('message'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Prison</strong>
                                    <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                </div>
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Prison</strong>
                                    <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                </div>
                            </div>
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
