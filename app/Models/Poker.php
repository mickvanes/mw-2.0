<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poker extends Model
{
    use HasFactory;

    protected $table = 'pokers';

    protected $fillable = [
        'jackpot',
        'raiseLimit',
        'timeOut',
        'player1',
        'player2',
        'player3',
        'player4',
        'playerCount',
        'inProgress',
        'playing',
        'completed'
    ];

    public function player1()
    {
        return $this->belongsTo(User::class, 'player1');
    }

    public function player2()
    {
        return $this->belongsTo(User::class, 'player2');
    }

    public function player3()
    {
        return $this->belongsTo(User::class, 'player3');
    }

    public function player4()
    {
        return $this->belongsTo(User::class, 'player4');
    }

}
