<?php

namespace App\Http\Controllers;

use App\Models\FamilyForumReaction;
use App\Models\FamilyForumReport;
use App\Models\FamilyForumTopic;
use App\Models\ForumCategory;
use App\Models\ForumReaction;
use App\Models\ForumTopic;
use App\Models\Message;
use App\Models\Mute;
use App\Models\ReactionReport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;
use function Sodium\increment;

class ForumController extends Controller
{
    public function categories(){
        $forumCategories = ForumCategory::paginate(15);
        $comments = ForumReaction::paginate(15);
        $topics = ForumTopic::paginate(15);

        return view('forum.categories',compact('forumCategories','comments','topics'));
    }

    public function category($slug){
        $forumCategory = ForumCategory::with('topics')->where('slug',$slug)->paginate(5);

        return view('forum.category',compact('forumCategory'));
    }

    public function topic($slug,$id){
        $forumTopic = ForumTopic::with('user')->where('id',$id)->first();
        $category = ForumCategory::where("slug", "=", $slug)->first()->id;
        $comments = ForumReaction::with('user')->where([
            'categoryID' => $category,
            'topicID' => $id,
        ])->paginate(15);

        $mute = Mute::where([
            ['player', Auth::id()],
            ['beginDate', '<=', Carbon::now()],
            ['endDate', '>=', Carbon::now()],
        ])->first();

        return view('forum.topic',compact('forumTopic','comments','mute'));
    }

    public function postReaction($slug,$id,Request $request){
        $this->validate($request, [
            'message' => 'required',
        ]);

        $message = htmlspecialchars($request->input('message'));

        $mute = Mute::where([
            ['player', Auth::id()],
            ['beginDate', '<=', Carbon::now()],
            ['endDate', '>=', Carbon::now()],
        ])->first();

        $category = ForumCategory::where("slug", "=", $slug)->first()->id;

        if(!$mute){
            if($request->input('comment')){
                $level = ForumReaction::where("id","=",$request->input('comment'))->firstOrFail()->level + 1;
            }
            else{
                $level = 0;
            }

            ForumReaction::create([
                'userID' => Auth::id(),
                'topicID' => $id,
                'categoryID' => $category,
                'content' => $message,
                'level' => $level,
                'reactionID' => $request->input('comment'),
            ]);

            ForumTopic::where('id','=',$id)->update([
                'updated_at' => Carbon::now()
            ]);

            return back()->with('message', "You have posted a comment");
        }
        else{
            return back()->with('error', "You have a mute, you can't post a message until you are unmuted");
        }

    }

    public function report($type,$id){
        if($type == "forumTopic"){
            $data = ForumTopic::where([
                ['id','=',$id],
            ])->firstOrFail();

            $check = ReactionReport::where([
                ['type','=', $type],
                ['itemID','=',$id],
                ['user','=',Auth::id()],
                ['repUser','=',$data->userID]
            ])->first();

            if(!$check){
                ReactionReport::create([
                    'type' => $type,
                    'itemID' => $id,
                    'content' => $data->content,
                    'user' => Auth::id(),
                    'repUser' => $data->userID
                ]);

                return back()->with('message', "You have reported this topic");
            }
            else{
                return back()->with('error', "You have already reported this topic");
            }
        }
        elseif ($type == "forumComment"){
            $data = ForumReaction::where([
                ['id','=',$id],
            ])->firstOrFail();

            $check = ReactionReport::where([
                ['type','=', $type],
                ['itemID','=',$id],
                ['user','=',Auth::id()],
                ['repUser','=',$data->userID]
            ])->first();

            if(!$check){
                ReactionReport::create([
                    'type' => $type,
                    'itemID' => $id,
                    'content' => $data->content,
                    'user' => Auth::id(),
                    'repUser' => $data->userID
                ]);

                return back()->with('message', "You have reported this comment");
            }
            else{
                return back()->with('error', "You have already reported this comment");
            }

        }
        elseif ($type == "familyTopic"){
            $data = FamilyForumTopic::findOrFail($id);

            $check = FamilyForumReport::where([
                ['type','=', $type],
                ['itemID','=',$id],
                ['content','=',$data->content],
                ['repUser','=',$data->userID]
            ])->first();

            if(!$check){
                FamilyForumReport::create([
                    'type' => $type,
                    'itemID' => $id,
                    'family_id' => $data->familyID,
                    'content' => $data->content,
                    'user' => Auth::id(),
                    'repUser' => $data->userID,
                    'count' => 0,
                ]);
                return back()->with('message', "You have reported this topic");
            }
            else{
                $checkUser = FamilyForumReport::where([
                    ['type','=', $type],
                    ['itemID','=',$id],
                    ['content','=',$data->content],
                    ['user','=',Auth::id()],
                    ['repUser','=',$data->userID]
                ])->first();
                if($checkUser){
                    return back()->with('error', "You have already reported this topic");
                }
                else{
                    $check->increment('count');
                    return back()->with('message', "You have reported this topic");
                }
            }
        }
        elseif ($type == "familyComment"){
            $data = FamilyForumReaction::findOrFail($id);

            $check = FamilyForumReport::where([
                ['type','=', $type],
                ['itemID','=',$id],
                ['content','=',$data->content],
                ['user','=',Auth::id()],
                ['repUser','=',$data->userID]
            ])->first();

            if(!$check){
                FamilyForumReport::create([
                    'type' => $type,
                    'family_id' => $data->familyID,
                    'itemID' => $id,
                    'content' => $data->content,
                    'user' => Auth::id(),
                    'repUser' => $data->userID,
                    'count' => 0,
                ]);
                return back()->with('message', "You have reported this comment");
            }
            else{
                $checkUser = FamilyForumReport::where([
                    ['type','=', $type],
                    ['itemID','=',$id],
                    ['content','=',$data->content],
                    ['user','=',Auth::id()],
                    ['repUser','=',$data->userID]
                ])->first();
                if($checkUser){
                    return back()->with('error', "You have already reported this comment");
                }
                else{
                    $check->increment('count');
                    return back()->with('message', "You have reported this comment");
                }
            }

        }
        else{
            return back()->with('error', "Link not found");
        }
    }
}
