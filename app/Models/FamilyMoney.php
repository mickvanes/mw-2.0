<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyMoney extends Model
{
    use HasFactory;

    protected $table = 'family_cash';

    protected $fillable = [
        'family_id',
        'cash',
        'bank',
    ];

    public function family()
    {
        return $this->belongsTo(Family::class, 'id','family_id');
    }
}
