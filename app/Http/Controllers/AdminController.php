<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function index(){
        $user = User::with('admin')
            ->findOrFail(Auth::id());

        if($user->admin){
            if($user->admin->adminRangID == 1){
                return view('admin.developer');
            }
            elseif($user->admin->adminRangID == 2){
                return view('admin.moderator');
            }
        }
        else{
            return abort(404);
        }
    }
}
