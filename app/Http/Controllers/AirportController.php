<?php

namespace App\Http\Controllers;

use App\Models\Airplane;
use App\Models\Cash;
use App\Models\Countries;
use App\Models\Country;
use App\Models\Defense;
use App\Models\Profile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class AirportController extends Controller
{
    public function index() {
        $lastFlight = Country::find(Auth::id())->updated_at;
        $airplane = Defense::where('userID',Auth::id())->first()->airplane;

        if($airplane){
            $data = Airplane::find($airplane);
            $boolean = Carbon::createFromDate($lastFlight)->addMinutes($data->time)->isPast();

            if($boolean) {
                $countries = Countries::where('active', '=', 1)->get();
                $users = Profile::where('last_online', '>=', Carbon::now()->subMinutes(5))->get('id');
                $onlineUsersCountries = [];

                foreach ($users as $user) {
                    $userCountry = Country::with('country')->where('userID', '=', $user->id)->pluck('countryID')->first();
                    array_push($onlineUsersCountries, $userCountry);
                }

                $userCountry = Country::with('country')->where('userID', '=', Auth::id());

                $countryOnlineUsers = array_count_values($onlineUsersCountries);

                return view('airport', compact('countries', 'countryOnlineUsers', 'userCountry','boolean'));
            }

            $dateNew = Carbon::createFromDate($lastFlight)->addMinutes($data->time)->format('d/m/Y H:i:s');

            return view('airport',compact('boolean','dateNew'));
        }
        else{
            return redirect()->route('shop.airplanes')->with('error', "You don't have an airplane, buy one in the shop");
        }
    }

    public function airportForm(Request $request){
        $lastFlight = Country::find(Auth::id())->updated_at;
        $airplane = Defense::where('userID',Auth::id())->first()->airplane;

        $data = Airplane::find($airplane);

        $this->validate($request, [
            'countryID' => 'required',
        ]);
        if($airplane){
            if(Carbon::createFromDate($lastFlight)->addMinutes($data->time)->isPast()){
                if(Auth::user()->cash->cash >= $data->flightCosts){
                    Cash::where('userID', Auth::id())->update([
                        'cash' => Auth::user()->cash->cash - $data->flightCosts
                    ]);

                    Country::with('user')
                        ->where('userID', Auth::id())
                        ->update(['countryID' => $request->input('countryID')]);

                    $flew = Countries::find($request->input('countryID'));

                    return redirect()->route('airport')->with('message', 'You flew to '. $flew->name . ' ' . $airplane);
                }
                else{
                    return redirect()->route('airport')->with('error', 'Not enough cash');
                }
            }
            else{
                return redirect()->route('airport')->with('message', 'Nog ff wachte maat' . Carbon::createFromDate($lastFlight)->addMinutes($data->time)->diffInRealSeconds());
            }
        }
        else{
            return redirect()->route('shop.airplanes')->with('error', "You don't have an airplane, buy one in the shop");
        }


    }
}
