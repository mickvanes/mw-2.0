<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table = 'user_country';

    protected $primaryKey = "userID";

    protected $fillable = [
        'userID',
        'countryID',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function countryDetails()
    {
        return $this->belongsTo(Countries::class, 'countryID');
    }
}
