<?php

namespace App\Http\Middleware;

use App\Http\Controllers\BusinessController;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CodeBusiness
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            $business = DB::table("businesses")
                ->where([
                    ['id','=',$request->id],
                    ['ownerID','=', Auth::id()]
                ])->first();

            if($business){
                $now = Carbon::now();
                if($now >= $business->codeExpire || $business->codeExpire == null){
                    return redirect()->route('business.pincode',$business->id);
                }
                else{
                    return $next($request);
                }
            }
            else{
                return redirect()->route('dashboard');
            }
        }


    }
}
