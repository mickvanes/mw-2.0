
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200">

                        @if(session()->has('message'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Hospital</strong>
                                    <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                </div>
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Hospital</strong>
                                    <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                </div>
                            </div>
                        @endif

                            <div class="grid grid-cols-1 gap-4">
                                <div>
                                    Revenue last week: &euro;{{ number_format($revenueLastWeek , 0, ',', '.') }} <br>
                                    @if($revenue >= 0)
                                        Revenue this week: <span class="text-green-600">&euro;{{ number_format($revenue , 0, ',', '.') }} </span>
                                    @else
                                        Revenue this week: <span class="text-red-600">&euro;{{ number_format($revenue , 0, ',', '.') }} </span>
                                    @endif
                                    <hr class="border-1 border-gray-500 mt-4">
                                </div>
                                <div>
                                    <span class="text-red-600"><b>Every night at 00:00 the blood bags are reset</b></span>
                                    <div class="grid grid-cols-1 mb-4">
                                        <span>Remaining blood bags: {{ number_format($remainingBlood , 0, ',', '.') }}</span>
                                        <span>Purchase price per blood bag: &euro;{{ number_format($hospitalBloodCosts , 0, ',', '.') }}</span>
                                        <span>Selling price per blood bag: &euro;{{ number_format($sellingPrice , 0, ',', '.') }}
                                            @if($sellingPrice-$hospitalBloodCosts > 0)
                                                <span class="text-green-600">Profit: &euro;{{ number_format($sellingPrice-$hospitalBloodCosts , 0, ',', '.') }}</span>
                                            @else
                                                <span class="text-red-600">Loss: &euro;{{ number_format($sellingPrice-$hospitalBloodCosts , 0, ',', '.') }}</span>
                                            @endif
                                        </span>
                                    </div>
                                    <form method="post" action="{{ route('hospital.buyBlood') }}">
                                        @csrf
                                        <div class="mb-4">
                                            <input type="number" id="bloodBags"
                                                    name="bloodBags" placeholder="Blood bags" min="300" max="1000">

                                            <input type="submit" value="Buy"
                                                   class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4">
                                        </div>
                                    </form>

                                    <form method="post" action="{{ route('hospital.changePrice') }}">
                                        @csrf
                                        <div class="mb-4">
                                            <input type="number" id="price"
                                                   name="price" placeholder="Price" min="{{ $minPrice }}" max="{{ $maxPrice }}">

                                            <input type="submit" value="Change Price"
                                                   class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4">
                                        </div>
                                    </form>
                                    <hr class="border-1 border-gray-500 mt-4">
                                </div>
                                <div>
                                    <div class="grid grid-cols-1 md:grid-cols-2">
                                        <div>Player Cash: &euro;{{ number_format($userCash , 0, ',', '.') }}<br>
                                            Player Bank: &euro;{{ number_format($userBank , 0, ',', '.') }}<br><br></div>
                                        <div>Business Bank: &euro;{{ number_format($bank , 0, ',', '.') }}<br><br></div>
                                    </div>
                                    <form method="post" action="{{ route('businessBank.store',$businessID) }}">
                                        @csrf
                                            <div class="grid grid-cols-1 md:grid-cols-2 mb-4">
                                                <div><input type="radio" id="withdraw"
                                                            name="transaction_type"
                                                            value="0">
                                                    <label for="withdraw"> Withdraw</label></div>
                                                <div>
                                                    <input type="radio" id="deposit"
                                                           name="transaction_type"
                                                           value="1" checked>
                                                    <label for="deposit"> Deposit</label>
                                                    <a onclick="transAll()">[all]</a>
                                                </div>
                                            </div>

                                        <div class="grid grid-cols-3">
                                            <div><input type="number" id="transaction"
                                                        name="transaction" placeholder="Cash &euro;" min="0" value=""></div>
                                        </div>

                                        <input type="submit" value="Submit"
                                               class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4">
                                        <button class="bg-gray-500 hover:bg-gray-700 mt-4 text-white font-bold py-2 px-4" type="button">
                                            <a href="{{ route('businessBank.logs',$businessID) }}">Bank Logs</a>
                                        </button>
                                        <script>
                                            function transAll(){
                                                var type = document.querySelector('input[name="transaction_type"]:checked').value

                                                if(type == 0){
                                                    document.getElementById('transaction').value=<?= $bank ?>;
                                                }
                                                else if(type == 1) {
                                                    document.getElementById('transaction').value=<?= $userCash ?>;
                                                }
                                                else{
                                                    console.log('error');
                                                }
                                            }
                                        </script>
                                    </form>
                                </div>
                            </div>











                    </div>
                </div>
        </div>
    </div>
</x-app-layout>

