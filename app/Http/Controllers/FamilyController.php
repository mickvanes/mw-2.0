<?php

namespace App\Http\Controllers;

use App\Models\BackfireKill;
use App\Models\Cash;
use App\Models\FamilyHeist;
use App\Models\FamilyHeistLocation;
use App\Models\FamilyHeistMessages;
use App\Models\Family;
use App\Models\FamilyForumCategory;
use App\Models\FamilyForumReaction;
use App\Models\FamilyForumReport;
use App\Models\FamilyForumTopic;
use App\Models\FamilyHeistMoneyLocations;
use App\Models\FamilyHeistRole;
use App\Models\FamilyMoney;
use App\Models\FamilyUserClicks;
use App\Models\FamilyUserRang;
use App\Models\Kill;
use App\Models\KillPoints;
use App\Models\Message;
use App\Models\Mute;
use App\Models\Profile;
use App\Models\Rank;
use App\Models\Ranks;
use App\Models\Recruit;
use App\Models\Rule;
use App\Models\SystemMessage;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FamilyController extends Controller
{
    public function joinFamily($name)
    {

        $user = auth()->user();

        $famId = DB::table('families')
            ->select('families.id')
            ->where('families.name', '=', $name)
            ->first();

        $maxFamilyCount = DB::table('rules')
            ->select('maxFamMembers')
            ->first();

        if ($famId) {
            $famUsersCount = DB::table('family_user_rang')
                ->join('users', 'users.id', '=', 'family_user_rang.userID')
                ->where('family_user_rang.familyID', '=', $famId->id)
                ->select('family_user_rang.*', 'users.name')
                ->count();

            $checkAlreadyFam = DB::table('family_user_rang')
                ->where('family_user_rang.userID', '=', Auth::id())
                ->select('family_user_rang.userID')
                ->first();

            $checkAlreadyRecruit = DB::table('family_recruiting')
                ->where('family_recruiting.userID', '=', Auth::id())
                ->select('family_recruiting.userID')
                ->first();

            if (!$checkAlreadyFam) {
                if (!$checkAlreadyRecruit) {
                    Recruit::create([
                        'familyID' => $famId->id,
                        'userID' => Auth::id()
                    ]);

                    if ($famUsersCount < $maxFamilyCount->maxFamMembers) {
                        return redirect()->route('families')->with('message', 'Aangemeld voor familie');
                    } else {
                        return redirect()->route('families')->with('message', 'Aangemeld voor familie maar familie heeft al 50 leden, het kan even duren voordat je aangenomen word');
                    }
                } else {
                    $checkAlreadyRecruitFam = DB::table('family_recruiting')
                        ->where('family_recruiting.userID', '=', Auth::id())
                        ->where('family_recruiting.familyID', '=', $famId->id)
                        ->select('family_recruiting.userID')
                        ->first();

                    if ($checkAlreadyRecruitFam) {
                        return redirect()->route('families')->with('message', 'Je bent al recruit van deze familie');
                    } else {
                        $joinOtherFamName = DB::table('family_recruiting')
                            ->join('families', 'families.id', '=', 'family_recruiting.familyID')
                            ->where('family_recruiting.userID', '=', Auth::id())
                            ->select('families.name as name')
                            ->first();

                        DB::table('family_recruiting')
                            ->where('userID', Auth::id())
                            ->update(['familyID' => $famId->id, 'timestamp' => Carbon::now()]);

                        $joinFamName = DB::table('family_recruiting')
                            ->join('families', 'families.id', '=', 'family_recruiting.familyID')
                            ->where('family_recruiting.userID', '=', Auth::id())
                            ->where('family_recruiting.familyID', '=', $famId->id)
                            ->select('families.name as name')
                            ->first();

                        return redirect()->route('families')->with('message', 'Je hebt je afgemeld bij ' . $joinOtherFamName->name . ' en aangemeld bij ' . $joinFamName->name);
                    }
                }
            } else {
                return redirect()->route('families')->with('message', 'Je zit al in een familie');
            }
        }

    }

    public function makeFamily()
    {
        $userID = Auth::id();
        $userName = Auth::user()->username;

        $userCash = DB::table('user_cash')
            ->select('cash')
            ->where('user_cash.userID', '=', $userID)
            ->pluck('cash')
            ->first();

        $userBank = DB::table('user_cash')
            ->select('bank')
            ->where('user_cash.userID', '=', $userID)
            ->pluck('bank')
            ->first();

        $minRankFam = DB::table('rules')
            ->select('minRankCreateFam')
            ->pluck('minRankCreateFam')
            ->first();

        $userRank = DB::table('user_rank')
            ->where('user_rank.userID', '=' , $userID)
            ->select('rankID')
            ->pluck('rankID')
            ->first();

        $checkAlreadyFam = DB::table('family_user_rang')
            ->where('family_user_rang.userID', '=', $userID)
            ->select('family_user_rang.userID')
            ->first();

        $checkAlreadyRecruit = DB::table('family_recruiting')
            ->where('family_recruiting.userID', '=', $userID)
            ->select('family_recruiting.userID')
            ->first();

        if(!$checkAlreadyFam){
            if(!$checkAlreadyRecruit){
                return view('family.create', compact('userCash', 'userBank', 'userName','minRankFam','userRank','checkAlreadyFam','checkAlreadyRecruit'));
            }
            else{
                return view('family.create', compact('userCash', 'userBank', 'userName','minRankFam','userRank','checkAlreadyFam','checkAlreadyRecruit'))->with('message','Je staat nog aangemeld voor een familie, weet je zeker dat je kiest voor een eigen familie?');
            }
        }
        else{
            return view('family.create', compact('userCash', 'userBank', 'userName','minRankFam','userRank','checkAlreadyFam','checkAlreadyRecruit'))->with('error','Je zit nog in een familie, je kan geen familie aanmaken');
        }
    }

    public function makeFamilyForm(Request $request){
        $this->validate($request, [
            'family_name' => 'required|regex:/[a-zA-Z0-9-]/|max:15',
        ]);

        $userID = Auth::id();

        $ownerPower = DB::table('profiles')
            ->select('profiles.power')
            ->where('profiles.id', '=', $userID)
            ->pluck('profiles.power')
            ->first();

        $familyNameNew = $request->input('family_name');

        $familyNameCheck = DB::table('families')
            ->select('families.name')
            ->where('families.name', '=', $familyNameNew)
            ->pluck('families.name')
            ->first();

        $ownerRep = DB::table('profiles')
            ->select('profiles.rep')
            ->where('profiles.id', '=', $userID)
            ->pluck('profiles.rep')
            ->first();

        if(!$familyNameCheck){
            DB::table('families')->insert([
                'name' => $request->input('family_name'),
                'description' => '',
                'famClicks' => 0,
                'famPower' => $ownerPower,
                'shares' => 0,
                'exitCost' => 0,
                'creation_date' => Carbon::now()->toDateString(),
                'houses' => 0,
                'energy' => 0,
                'land' => 0,
                'walls' => 0,
                'profileText' => '',
                'famRep' => $ownerRep,
            ]);

            $famID = DB::table('families')
                ->select('families.id')
                ->where('families.name', '=', $request->input('family_name'))
                ->pluck('families.id')
                ->first();

            DB::table('family_user_rang')->insert([
                'familyID' => $famID,
                'userID' => $userID,
                'rangID' => 1,
            ]);

            DB::table('family_cash')->insert([
                'familyID' => $famID,
                'cash' => 0,
                'bank' => 0,
            ]);

            DB::table('family_promotion')->insert([
                'familyID' => $famID,
                'emptysuit' => 0,
                'deliveryboy' => 0,
                'picciotto' => 0,
                'shoplifter' => 0,
                'pickpocket' => 0,
                'thief' => 0,
                'associate' => 0,
                'mobster' => 0,
                'soldier' => 0,
                'swindler' => 0,
                'assassin' => 0,
                'localchief' => 0,
                'chief' => 0,
                'bruglione' => 0,
                'godfather' => 0,
            ]);

            return redirect()->route('dashboard')->with('message', "Your family is successfully created!");
        }
        else{
            return redirect()->route('family.create')->with('error', "This family name is already taken!");
        }
    }

    public function family($name)
    {
        $familyProfile = Family::where('name',$name)->firstOrFail();
        $familyMembers = FamilyUserRang::with('user','familyDetails','rang')->where('familyID', $familyProfile->id)->orderBy('rangID')->get();
        $owner = $familyMembers->firstWhere('rangID',1);
        $famStats = $this->calcFamStats($familyProfile->id);

        return view('family.show',compact('familyProfile','familyMembers','owner','famStats'));
    }

    private function calcFamStats($id) {
        $famStats = Family::selectRaw('SUM(profiles.power) as power, SUM(profiles.rep) as rep')
            ->join('family_user_rang', 'family_user_rang.familyID', '=', 'families.id')
            ->join('profiles', 'profiles.id', '=', 'family_user_rang.userID')
            ->where('families.id',$id)
            ->firstOrFail();

        return $famStats;
    }

    public function families()
    {
        $families =  Family::with('owner','members')->get();
        $familiesStats = new Collection();

        foreach ($families as $family){
            $familyMembers = FamilyUserRang::with('user','familyDetails','rang')->where('familyID', $family->id)->get();
            $rep = 0;

            foreach ($familyMembers as $member){
                $rep = $rep + $member->user->profile->rep;
            }

            $familiesStats->push(
                (object)
                [
                    'name' => $family->name,
                    'famRep' => $rep + $family->famRep,
                    'owner' => $family->owner->user->username,
                    'description' => $family->description,
                    'loan' => $family->shares * 200 * 24,
                ]
            );
        }

        $sorted = $familiesStats->sortByDesc('famRep');
        $data = $this->paginate($sorted);

        return view('families',compact('data'));
    }

    public function paginate($items, $perPage = 30, $page = null, $options = [])

    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

    }

    public function clickFam($name)
    {
        $now = Carbon::now();
        $hour = Carbon::now()->addHour();

        $family_ID = Family::where('name',$name)->firstOrFail()->id;

        $checkTime = FamilyUserClicks::where('userID', '=', Auth::id())
            ->where('familyID', $family_ID)
            ->pluck('timestamp')
            ->first();

        if (!$checkTime) {
            FamilyUserClicks::where('userID', Auth::id())
                ->where('familyID', $family_ID)
                ->insert([
                    'userID' => Auth::id(),
                    'familyID' => $family_ID,
                    'timestamp' => $hour
                ]);

            Family::where('id', $family_ID)
                ->firstOrFail()
                ->increment('famClicks', 1)
                ->increment('famRep', 1);

            return redirect()->route('family', $name)->with('message', 'You clicked this family, you can do this again on ' . $hour);
        }
        else{
            if ($now->isAfter($checkTime)) {
                FamilyUserClicks::where('userID', Auth::id())
                    ->where('familyID', $family_ID)
                    ->update(['timestamp' => $hour]);

                Family::where('id', $family_ID)
                    ->firstOrFail()
                    ->increment('famClicks', 1)
                    ->increment('famRep', 1);

                    return redirect()->route('family', $name)->with('message', 'You clicked this family, you can do this again on ' . $hour);
                }
            else {
                return redirect()->route('family', $name)->with('error', 'You need to wait, you can click this family on ' . $checkTime . ' again');
            }
        }
    }

    public function promotion($name){
        $family_ID = Family::where('name',$name)->firstOrFail()->id;

        $promotions = DB::table('family_promotion')
            ->where('family_promotion.familyID', '=', $family_ID)
            ->first();

        dd($promotions);
    }

    public function recruitment(){
        $userRang = FamilyUserRang::with('user')->where('userID', Auth::id())->first();
        if($userRang->rangID == 1 || $userRang->rangID == 2  || $userRang->rangID == 5  ){
            $userFam = User::with('family')->findOrFail(Auth::id());
            $recruitMembers = Recruit::where('familyID','=', $userFam->family->familyID)->get();

            return view('family.manage.recruitment',compact('recruitMembers'));
        }
        else{
            return redirect()->route('dashboard')->with('error',"You don't have the right permissions to see this page");
        }
    }

    public function killsToday()
    {
        $kills = Kill::with('user','player')->where([
            ['user_familyID', Auth::user()->family->familyID],
            ['created_at', '>=', Carbon::now()->startOfDay()],
            ['created_at', '<=', Carbon::now()->endOfDay()]
        ])->orderByDesc('created_at')->paginate(15);

        $killCountDay = Kill::where([
                ['user_familyID', Auth::user()->family->familyID],
                ['created_at', '>=', Carbon::now()->startOfDay()],
                ['created_at', '<=', Carbon::now()->endOfDay()]
            ])->count();

        return view('family.killsToday', compact('kills','killCountDay'));
    }

    public function kills(){
        $kills = Kill::with('user','player')->where('user_familyID', Auth::user()->family->familyID)->orderByDesc('created_at')->paginate(15);

        $killCountDay = Kill::where('user_familyID', Auth::user()->family->familyID)
            ->where([
                ['created_at', '>=', Carbon::now()->startOfDay()],
                ['created_at', '<=', Carbon::now()->endOfDay()]
            ])
            ->count();

        return view('family.kills', compact('kills', 'killCountDay'));
    }

    public function deads()
    {
        $deads = Kill::with('user','player','witnesses')->where('player_familyID', Auth::user()->family->familyID)->orderByDesc('created_at')->paginate(15);

        $deadCountDay = Kill::where('player_familyID', Auth::user()->family->familyID)
            ->where('created_at', '>=', Carbon::now()->subDay())
            ->count();

        return view('family.deads', compact('deads','deadCountDay'));



    }

    public function familyForumReports()
    {
        $userRang = FamilyUserRang::with('user')->where('userID', Auth::id())->first();

        if($userRang->rangID == 1 || $userRang->rangID == 2  || $userRang->rangID == 5  ){
            $reports = FamilyForumReport::where(
                'familyID', Auth::user()->family->familyID
            )->get();
            return view('family.manage.forumReports', compact('reports'));
        }
        else{
            return redirect()->route('dashboard')->with('error',"You don't have the right permissions to see this page");
        }
    }
    public function stats(){
        if(Auth::user()->family){
            $familyID = Auth::user()->family->familyID;
            $familyMembers = FamilyUserRang::with('user')->where('familyID', $familyID)->get();

            $stats = new Collection();
            $rankStats = new Collection();

            foreach ($familyMembers as $familyMember){
                $kills = Kill::where('userID',$familyMember->user->id)->where('success',1)->get();
                $bfKills = BackfireKill::where('userID',$familyMember->user->id)->where('success',1)->get();

                $killPoints = 0;
                $bfKillPoints = 0;

                foreach ($kills as $kill){
                    $points = Ranks::find($kill->playerRank)->killPoints;
                    $killPoints = $killPoints + $points;
                }

                foreach ($bfKills as $bfKill){
                    $points = Ranks::find($bfKill->playerRank)->killPoints / 2;
                    $bfKillPoints = $bfKillPoints + $points;
                }

                $rankStats->push(
                    (object)
                    [
                        'name' => $familyMember->user->username,
                        'rank' => $familyMember->user->getCurrentRank->ranks->rank,
                        'adv' => $familyMember->user->profile->rankAdv
                        ]
                );

                $stats->push(
                    (object)
                    ['name' => $familyMember->user->username,
                    'cash' => $familyMember->user->cash->cash + $familyMember->user->cash->bank,
                    'clicks' => $familyMember->user->profile->clicks,
                    'power' => $familyMember->user->profile->power,
                    'rep' => $familyMember->user->profile->rep,
                    'honor' => $familyMember->user->profile->honor,
                    'coins' => $familyMember->user->profile->coins,
                    'rank' => $familyMember->user->getCurrentRank->ranks->rank,
                    'rankAdv' => $familyMember->user->profile->rankAdv,
                    'kills' => Kill::where('userID',$familyMember->user->id)->where('success',1)->count(),
                    'killsPoints' => $killPoints,
                    'backfire' => BackfireKill::where('userID',$familyMember->user->id)->where('success',1)->count(),
                    'bfKillsPoints' => $bfKillPoints,
                        ]
                );
            }
            return view('family.stats',compact('stats','rankStats'));
        }
        return redirect()->route('dashboard')->with('error',"You don't have a family, first join a family to see stats of the family");

    }

    public function famStats(){
        $families = Family::all();
        $familiesStats = new Collection();

        foreach ($families as $family){
            $familyMembers = FamilyUserRang::with('user','familyDetails','rang')->where('familyID', $family->id)->get();

            $power = 0;
            $clicks = 0;
            $rep = 0;
            $honor = 0;
            $coins = 0;
            $cash = 0;
            $rankArray = [];
            $killPoints = 0;
            $killCount = 0;
            $bfKillPoints = 0;
            $bfKillCount = 0;

            foreach($familyMembers as $familyMember){
                $kills = Kill::where('userID',$familyMember->user->id)->where('success',1)->get();
                $killsCount = Kill::where('userID',$familyMember->user->id)->where('success',1)->count();

                $bfKills = BackfireKill::where('userID',$familyMember->user->id)->where('success',1)->get();
                $bfKillsCount = BackfireKill::where('userID',$familyMember->user->id)->where('success',1)->count();

                $killCount = $killCount + $killsCount;
                $bfKillCount = $bfKillCount + $bfKillsCount;

                foreach ($kills as $kill){
                    $points = Ranks::find($kill->playerRank)->killPoints;
                    $killPoints = $killPoints + $points;
                }

                foreach ($bfKills as $bfKill){
                    $bfPoints = Ranks::find($bfKill->playerRank)->killPoints / 2;
                    $bfKillPoints = $bfKillPoints + $bfPoints;
                }

            }

            foreach ($familyMembers as $member){
                $power = $power + $member->user->profile->power;
                $clicks = $clicks + $member->user->profile->clicks;
                $cash = $cash + $member->user->cash->cash + $member->user->cash->bank;
                $rep = $rep + $member->user->profile->rep;
                array_push($rankArray,$member->user->getCurrentRank->rankID);
                $honor = $honor + $member->user->profile->honor;
                $coins = $coins + $member->user->profile->coins;
            }

            $rankAvg = floor(array_sum($rankArray) / count($rankArray));
            $rankPer = array_sum($rankArray) / count($rankArray);
            $rankName = Ranks::find($rankAvg);
            $percent = $rankPer;

                $familiesStats->push(
                (object)
                [
                    'name' => $family->name,
                    'power' => $power,
                    'clicks' => $clicks,
                    'rep' => $rep,
                    'honor' => $honor,
                    'coins' => $coins,
                    'killCount' => $killCount,
                    'killPoints' => $killPoints,
                    'bfKillCount' => $bfKillCount,
                    'bfKillPoints' => $bfKillPoints,
                    'rankDown' => $rankName,
                    'percent' => number_format((float)$percent, 2, '.', ''),
                    'loan' => $family->shares * 200 * 24,
                    'cash' => $cash,
                ]
            );

            ## https://cdn.discordapp.com/attachments/908004621535019041/972960625510719538/Screenshot_20220508-223756_Chrome.jpg
        }
        #dd($familiesStats);
        return view('famStats',compact('familiesStats'));

    }

    public function forum(){
        if(Auth::user()->family){
            $famCategory = FamilyForumCategory::where('familyID',Auth::user()->family->familyID)->get();
            $count = FamilyForumCategory::where('familyID',Auth::user()->family->familyID)->count();

            if($famCategory){
                if($count >= 3)
                    $categoryCount = 3;
                else{
                    $categoryCount = 15 - $count;
                }

                $famTopics = FamilyForumTopic::where([
                    'familyID' => Auth::user()->family->familyID,
                    'categoryID' => NULL,
                ])->paginate($categoryCount);
                return view('family.forum.categories',compact('famTopics','famCategory'));
            }
            else{
                $famTopics = FamilyForumTopic::where('familyID',Auth::user()->family->familyID)->paginate(15);
                return view('family.forum.categories',compact('famTopics'));
            }
        }
        else{
            return redirect()->route('dashboard')->with('error',"You don't have a family");
        }
    }

    public function topic($id){
        $forumTopic = FamilyForumTopic::with('user')->where('id',$id)->first();
        if(Auth::user()->family){
            if(Auth::user()->family->familyID == $forumTopic->familyID){
                $comments = FamilyForumReaction::with('user')->where('topicID', $id)->paginate(15);

                $mute = Mute::where([
                    ['player', Auth::id()],
                    ['beginDate', '<=', Carbon::now()],
                    ['endDate', '>=', Carbon::now()],
                ])->first();

                return view('family.forum.topic',compact('forumTopic','comments','mute'));
            }
            else{
                return redirect()->route('dashboard')->with('error',"You are not permitted to see this topic.");
            }
        }
        else{
            return redirect()->route('dashboard')->with('error',"You are not permitted to see this topic.");
        }


    }

    public function category($id){
        $forumCategory = FamilyForumCategory::with('topics')->where('id',$id)->paginate(15);

        return view('family.forum.category',compact('forumCategory'));
    }

    public function shop(){

        return view('family.manage.shop');
    }

    public function famHeistCreate()
    {
        $checkPlayer1 = FamilyHeist::where([
            'active' => 1,
            'player_1' => Auth::id()
        ])->first();

        $checkPlayer2 = FamilyHeist::where([
            'active' => 1,
            'player_2' => Auth::id()
        ])->first();

        $checkPlayer3 = FamilyHeist::where([
            'active' => 1,
            'player_3' => Auth::id()
        ])->first();

        $checkPlayer4 = FamilyHeist::where([
            'active' => 1,
            'player_4' => Auth::id()
        ])->first();

        $checkPlayer5 = FamilyHeist::where([
            'active' => 1,
            'player_5' => Auth::id()
        ])->first();

        if($checkPlayer1 || $checkPlayer2 || $checkPlayer3 || $checkPlayer4 || $checkPlayer5){
            return redirect()->route('family.heists')->with('error','You are already in a heist');
        }

        $locations = FamilyHeistLocation::all();
        $roles = FamilyHeistRole::all();
        $moneyLocations = FamilyHeistMoneyLocations::all();

        return view('family.heist.create',compact('locations','roles','moneyLocations'));
    }

    public function famHeistCreateForm(Request $request){
        $checkPlayer1 = FamilyHeist::where([
            'active' => 1,
            'player_1' => Auth::id()
        ])->first();

        $checkPlayer2 = FamilyHeist::where([
            'active' => 1,
            'player_2' => Auth::id()
        ])->first();

        $checkPlayer3 = FamilyHeist::where([
            'active' => 1,
            'player_3' => Auth::id()
        ])->first();

        $checkPlayer4 = FamilyHeist::where([
            'active' => 1,
            'player_4' => Auth::id()
        ])->first();

        $checkPlayer5 = FamilyHeist::where([
            'active' => 1,
            'player_5' => Auth::id()
        ])->first();

        if($checkPlayer1 || $checkPlayer2 || $checkPlayer3 || $checkPlayer4 || $checkPlayer5){
            return redirect()->route('family.heists')->with('error','You are already in a heist');
        }

        $request->validate([
            'location' => 'required|exists:family_heists_locations,id',
            'role' => 'required|exists:family_heists_roles,id',
        ]);

        $countHeists = FamilyHeist::where([
            'familyID' => Auth::user()->family->familyID,
            'completed' => 0,
            'active' => 1
        ])->count();

        $ratio = FamilyHeistRole::find($request->input('role'))->ratio;

        if($countHeists >= 5){
            return redirect()->route('family.heists')->with('error','There can be only 5 active family heists');
        }
        else{
            FamilyHeist::create([
                'heist_id' => substr(Str::orderedUuid(),-10),
                'location' => $request->input('location'),
                'familyID' => Auth::user()->family->familyID,
                'player_1' => Auth::id(),
                'player_1_role' => $request->input('role'),
                'moneyLocation' => $request->input('moneyLocation'),
                'ratio' => $ratio,
                'active' => 1,
                'completed' => 0,
            ]);

            return redirect()->route('family.heists')->with('message','Heist created');
        }
    }

    public function famHeistShow($id){
        $heist = FamilyHeist::where('heist_id',$id)->firstOrFail();

        if(!$heist){
            return redirect()->route('family.heists')->with('error','Heist not found');
        }

        $checkPlayer1 = FamilyHeist::where([
            'active' => 1,
            'player_1' => Auth::id()
        ])->first();

        $checkPlayer2 = FamilyHeist::where([
            'active' => 1,
            'player_2' => Auth::id()
        ])->first();

        $checkPlayer3 = FamilyHeist::where([
            'active' => 1,
            'player_3' => Auth::id()
        ])->first();

        $checkPlayer4 = FamilyHeist::where([
            'active' => 1,
            'player_4' => Auth::id()
        ])->first();

        $checkPlayer5 = FamilyHeist::where([
            'active' => 1,
            'player_5' => Auth::id()
        ])->first();

        if($checkPlayer1 || $checkPlayer2 || $checkPlayer3 || $checkPlayer4 || $checkPlayer5){
            return redirect()->route('family.heists')->with('error','You are already in a heist');
        }

        $heist = FamilyHeist::where('heist_id',$id)->firstOrFail();
        $roles = FamilyHeistRole::all();

        return view('family.heist.show',compact('heist','roles'));
    }

    public function famHeistsHistory()
    {
        $heists = FamilyHeist::where('familyID', Auth::user()->family->familyID)
            ->where('completed', 1)
            ->paginate(15);

        return view('family.heist.history',compact('heists'));
    }
    public function famHeistAll(){
        $checkPlayer1 = FamilyHeist::where([
            'active' => 1,
            'player_1' => Auth::id()
        ])->first();

        $checkPlayer2 = FamilyHeist::where([
            'active' => 1,
            'player_2' => Auth::id()
        ])->first();

        $checkPlayer3 = FamilyHeist::where([
            'active' => 1,
            'player_3' => Auth::id()
        ])->first();

        $checkPlayer4 = FamilyHeist::where([
            'active' => 1,
            'player_4' => Auth::id()
        ])->first();

        $checkPlayer5 = FamilyHeist::where([
            'active' => 1,
            'player_5' => Auth::id()
        ])->first();

        if($checkPlayer1 || $checkPlayer2 || $checkPlayer3 || $checkPlayer4 || $checkPlayer5){
            $check = true;
        }
        else{
            $check = false;
        }

        $heists = FamilyHeist::where([
            'familyID' => Auth::user()->family->familyID,
            'completed' => 0,
            'active' => 1
        ])->get();

        return view('family.heist.all',compact('heists', 'check'));
    }
    public function famHeistJoin(Request $request)
    {
        $heistFind = FamilyHeist::where('heist_id',$request->input('id'))->firstOrFail();

        if(!$heistFind){
            return redirect()->route('family.heists')->with('error','Heist not found');
        }

        $request->validate([
            'id' => 'required|exists:family_heists,heist_id',
            'role' => 'required|exists:family_heists_roles,id',
        ]);

        $heist = FamilyHeist::where('heist_id',$request->input('id'))->firstOrFail();
        $ratioRole = FamilyHeistRole::find($request->input('role'))->ratio;
        $newRatio = $heist->ratio + $ratioRole;

        if($heist->player_1 == Auth::id()){
            return redirect()->route('family.heists')->with('error','You can\'t join your own heist');
        }

        if($heist->player_2 == NULL){
            $heist->update([
                'player_2' => Auth::id(),
                'player_2_role' => $request->input('role'),
                'ratio' => $newRatio,
            ]);

            return redirect()->route('family.heists')->with('message','You joined the heist, wait for the heist to be full');
        }
        elseif($heist->player_3 == NULL){
            $heist->update([
                'player_3' => Auth::id(),
                'player_3_role' => $request->input('role'),
                'ratio' => $newRatio,
            ]);

            return redirect()->route('family.heists')->with('message','You joined the heist, wait for the heist to be full');
        }
        elseif($heist->player_4 == NULL){
            $heist->update([
                'player_4' => Auth::id(),
                'player_4_role' => $request->input('role'),
                'ratio' => $newRatio,
            ]);

            return redirect()->route('family.heists')->with('message','You joined the heist, wait for the heist to be full');
        }
        elseif($heist->player_5 == NULL){
            $heist->update([
                'player_5' => Auth::id(),
                'player_5_role' => $request->input('role'),
                'ratio' => $newRatio,
                'active' => 0,
                'completed' => 1,
            ]);

            $rules = DB::table('rules')
                ->select('heistMoneyMin','heistMoneyMax')
                ->first();

            $players = [$heist->player_1,$heist->player_2,$heist->player_3,$heist->player_4,$heist->player_5];
            $ratio = $heist->ratio;
            $actualRatio = $ratio / 100;

            if($heist->moneyLocation == 1){
                // To Family Cash
                $money = rand($rules->heistMoneyMin,$rules->heistMoneyMax);
                $money = $money * $actualRatio;
                FamilyMoney::where('familyID',$heist->familyID)->increment('cash',$money);
                $heist->update([
                    'money' => $money,
                ]);
            }
            elseif($heist->moneyLocation == 2){
                // To Player
                $random = rand($rules->heistMoneyMin,$rules->heistMoneyMax);
                $money = $random / 5;
                $money = $money * $actualRatio;
                foreach ($players as $player){
                    Cash::where('userID',$player)->increment('cash',$money);
                }

                $heist->update([
                    'money' => $money * 5,
                ]);
            }
            elseif ($heist->moneyLocation == 3){
                // To Family and Player
                $moneyR = rand($rules->heistMoneyMin,$rules->heistMoneyMax);
                $moneyTotal = $moneyR;
                $money = $moneyTotal * $actualRatio;
                $moneyFamily = $money / 2;
                $moneyPlayers = $money / 5;

                foreach ($players as $player){
                    Cash::where('userID',$player)->increment('cash',$moneyPlayers);
                }

                FamilyMoney::where('familyID',$heist->familyID)->increment('cash',$moneyFamily);

                $heist->update([
                    'money' => $money,
                ]);
            }

            $random = rand(1,10);
            $message = FamilyHeistMessages::where('location', $heist->location)->where('id',$random)->first();

            $players = [$heist->player_1,$heist->player_2,$heist->player_3,$heist->player_4,$heist->player_5];

            foreach ($players as $player){
                Message::create([
                    'systemID' => 1,
                    'recipientID' => $player,
                    'systemMessage' => 1,
                    'subject' => "Family heist completed",
                    'body' => $message->body,
                    'seen' => 0,
                ]);

                Profile::find($player)->increment('rep',1);
                Family::where('id',$heist->familyID)->increment('famRep',1);
            }
            return redirect()->route('family.heists')->with('message','You joined the heist, the family heist is completed');
        }
        else{
            return redirect()->route('family.heists')->with('error',"This heist can't be joined anymore, it's full");
        }
    }
}
