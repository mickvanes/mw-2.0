<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyForumCategory extends Model
{
    use HasFactory;

    protected $table = 'familyforum_category';

    protected $fillable = [
        'title',
        'subtitle',
        'familyID',
        'userID',
        'onlyCrew'
    ];

    public function topics()
    {
        return $this->hasMany(FamilyForumTopic::class, 'categoryID');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'userID');
    }

    public function family()
    {
        return $this->belongsTo(Family::class, 'familyID');
    }
}
