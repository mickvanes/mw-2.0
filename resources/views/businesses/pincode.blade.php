
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="grid grid-cols-1 gap-2">
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg" style="max-width: 904px;">
                    <div class="mx-auto" style="max-width: 904px; width: 100%">
                        <div class="w-3/4 mx-auto text-center mb-4 mt-4 grid grid-cols-1">
                        </div>
                    </div>
                </div>
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg" style="max-width: 904px;">
                    <div class="p-6 bg-white border-b border-gray-200">
                        {{ $business->type->name }} {{ $country->country->name }}
                    </div>
                </div>
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg" style="max-width: 904px;">
                    <div class="p-6 bg-white border-b border-gray-200">

                        @if(session()->has('message'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Pincode</strong>
                                    <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                </div>
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Pincode</strong>
                                    <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                </div>
                            </div>
                        @endif
                            <form method="post"  action="{{ route('business.pincheck', $business->id ) }}" name="form" id="pincode">
                                @csrf
                                <div>
                                    <input class="text-center" id="pincode" type="password" name="pincode" pattern="[0-9]*" maxlength="4" autocomplete="off" autofocus="on" inputmode="numeric">
                                    <input type="hidden" id="postId" name="postId" value="34657">
                                </div>
                                <div class="text-center mt-2 grid grid-cols-3">
                                    <div onclick="addText(1)" class="keypad-item bg-gray-100">1</div>
                                    <div onclick="addText(2)" class="keypad-item bg-gray-100">2</div>
                                    <div onclick="addText(3)" class="keypad-item bg-gray-100">3</div>
                                    <div onclick="addText(4)" class="keypad-item bg-gray-100">4</div>
                                    <div onclick="addText(5)" class="keypad-item bg-gray-100">5</div>
                                    <div onclick="addText(6)" class="keypad-item bg-gray-100">6</div>
                                    <div onclick="addText(7)" class="keypad-item bg-gray-100">7</div>
                                    <div onclick="addText(8)" class="keypad-item bg-gray-100">8</div>
                                    <div onclick="addText(9)" class="keypad-item bg-gray-100">9</div>
                                    <div class="keypad-item bg-gray-100"></div>
                                    <div onclick="addText(0)" class="keypad-item bg-gray-100">0</div>
                                    <div class="keypad-item bg-blue-700"><input type="submit" name="send" value="SUB"></div>
                                </div>

                            </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .keypad-item{
            padding: 10px;
            margin-top: 5px;
            margin-right: 5px;
            border-radius: 5px;
        }

        input[type=submit]{
            background-color: rgba(29, 78, 216, var(--tw-bg-opacity));
            color: white;
        }
    </style>
    <script>
        function addText(value) {
            if(document.form.pincode.value.length <= 3){
                document.form.pincode.value += value;
            }
        }

        document
            .getElementById("pincode")
            .addEventListener("keypress", function(evt) {
                if (evt.which < 48 || evt.which > 57) {
                    evt.preventDefault();
                }
            });
    </script>

</x-app-layout>
