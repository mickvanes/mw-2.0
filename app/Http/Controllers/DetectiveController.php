<?php

namespace App\Http\Controllers;

use App\Jobs\DeactivateDets;
use App\Jobs\ProcessDets;
use App\Models\Admin;
use App\Models\Cash;
use App\Models\Countries;
use App\Models\Country;
use App\Models\Detective;
use App\Models\Settings;
use App\Models\SystemMessage;
use App\Models\User;
use App\Models\Witness;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Auth;

class DetectiveController extends Controller
{
    public function show(){
        $user = User::with('cash')->find(Auth::id());

        $userName = $user->username;
        $userCash = $user->cash->cash;
        $userBank = $user->cash->bank;

        $dets = Detective::where([
            ['userID','=', Auth::id()],
            ['active', '=', 1]
        ])->get();

        $allCountries = Countries::all()->count() + 1;
        $countries = Countries::where('active',1)->get();

        $settings = Settings::where('userID', Auth::id())->first();

        return view('player.detective', compact('userName','userCash','userBank','dets','allCountries','countries','settings'));
    }

    public function detsForm(Request $request){
        $countCountries = Country::all()->count() + 1;

        $request->validate([
            'player' => 'required|exists:users,username',
            'dets' => 'required|numeric|between:1,500',
            'detsCountry' => 'required|numeric|between:1,15',
            'maxTime' => 'required|numeric|between:1,4',
            'message' => 'required|boolean'
        ]);

        $player = User::where('username','=',$request->player)->first();

        $detsAll = Detective::where([
            ['userID', '=' , Auth::id()],
            ['playerID', '=', $player->id],
            ['active', '=', 1],
            ['detsCountry', '=' , $countCountries]
        ])->first();

        $detsCountry = Detective::where([
            ['userID', '=' , Auth::id()],
            ['playerID', '=', $player->id],
            ['active', '=', 1],
            ['detsCountry', '=' , $request->detsCountry]
        ])->first();

        $detsFound = Detective::where([
            ['userID', '=' , Auth::id()],
            ['playerID', '=', $player->id],
            ['active', '=', 1],
            ['found', '=', 1]
        ])->first();

        if(!$detsAll && !$detsCountry){
            if(!$detsFound){
                $countDets = $request->dets;

                if($request->detsCountry === $countCountries){
                    $costs = ($request->dets * 30) * $countCountries;
                }
                else{
                    $costs = ($request->dets * 30) * 1;
                }

                $cash = Cash::where('userID', '=', Auth::id())->first();

                if($cash->cash >= $costs){
                    $cash->cash = $cash->cash - $costs;
                    $cash->save();

                    if($countDets >= 1 && $countDets < 100){
                        $time = 45;
                    }
                    elseif ($countDets >= 100 && $countDets < 200){
                        $time = 40;
                    }
                    elseif ($countDets >= 200 && $countDets < 300){
                        $time = 35;
                    }
                    elseif ($countDets >= 300 && $countDets < 400){
                        $time = 30;
                    }
                    elseif ($countDets >= 400 && $countDets < 500){
                        $time = 25;
                    }
                    elseif ($countDets == 500){
                        $time = 20;
                    }else{
                        return redirect()->route('detectives')->with('error', "Dets number must be between 1-500");
                    }

                    $now = Carbon::now('Europe/Amsterdam');
                    $endTime = $now->addMinutes(($request->maxTime * 60) + $time);

                    $createdDets = Detective::create([
                        'userID' => Auth::id(),
                        'playerID' => $player->id,
                        'endTime' => $endTime,
                        'maxTime' => $request->maxTime,
                        'detsCountry' => $request->detsCountry,
                        'dets' => $request->dets,
                    ]);

//                    $jobProcess = new ProcessDets($createdDets)->afterCommit()->delay(now()->addMinutes($time));
//                    $jobProcessId = custom_dispatch($jobProcess);
//
//                    $jobDeactivate = new DeactivateDets($createdDets)->afterCommit()->delay($request->input('maxTime') * 60 + $time);
//                    $jobDeactivateId = custom_dispatch($jobDeactivate);

                    $deactivateTime = ($request->maxTime * 60) + $time;

                    ProcessDets::dispatch($createdDets)->delay(now()->addMinutes($time));
                    DeactivateDets::dispatch($createdDets)->delay(now()->addMinutes($deactivateTime));

                    return redirect()->route('detectives')->with('message', "Succesfully hired ". $request->dets ." Detectives for " . ucfirst($request->player));
                }
                else{
                    return back()->with('error', "Not enough cash");
                }
            }
            else{
                return redirect()->route('detectives')->with('error', "You already found " . ucfirst($request->player));
            }
        }else{
            return back()->with('error', "You have already active dets on " . ucfirst($request->player) ." in this country or countries");
        }
    }

    public function restartDets($id){
        $detective = Detective::findOrFail($id);
        $countCountries = Country::all()->count() + 1;
        $player = User::where('id','=',$detective->playerID)->firstOrFail();

        $detsAll = Detective::where([
            ['id', '=', $detective->id],
            ['userID', '=' , Auth::id()],
            ['playerID', '=', $player->id],
            ['active', '=', 1],
            ['detsCountry', '=' , $countCountries]
        ])->first();

        $detsCountry = Detective::where([
            ['id', '=', $detective->id],
            ['userID', '=' , Auth::id()],
            ['playerID', '=', $player->id],
            ['active', '=', 1],
            ['detsCountry', '=' , $detective->detsCountry]
        ])->first();

        if($detsAll || $detsCountry){

            $setInactive = Detective::find($id);
            $setInactive->active = 0;
            $setInactive->save();

            $countDets = $setInactive->dets;

            if($setInactive->detsCountry === $countCountries){
                $costs = ($setInactive->dets * 30) * $countCountries;
            }
            else{
                $costs = ($setInactive->dets * 30) * 1;
            }

            $cash = Cash::where('userID', '=', Auth::id())->first();

            if($cash->cash >= $costs){
                $cash->cash = $cash->cash - $costs;
                $cash->save();

                if($countDets >= 1 && $countDets < 100){
                    $time = 45;
                }elseif ($countDets >= 100 && $countDets < 200){
                    $time = 40;
                }elseif ($countDets >= 200 && $countDets < 300){
                    $time = 35;
                }elseif ($countDets >= 300 && $countDets < 400){
                    $time = 30;
                }elseif ($countDets >= 400 && $countDets < 500){
                    $time = 25;
                }elseif ($countDets == 500){
                    $time = 20;
                }else{
                    return redirect()->route('detectives')->with('error', "Dets number must be between 1-500");
                }

                $now = Carbon::now('Europe/Amsterdam');
                $endTime = $now->addMinutes(($setInactive->maxTime * 60) + $time);

                $createdDets = Detective::create([
                    'userID' => Auth::id(),
                    'playerID' => $player->id,
                    'endTime' => $endTime,
                    'maxTime' => $setInactive->maxTime,
                    'detsCountry' => $setInactive->detsCountry,
                    'dets' => $setInactive->dets,
                ]);

                ProcessDets::dispatch($createdDets)->afterCommit()->delay(now()->addMinutes($time));
                DeactivateDets::dispatch($createdDets)->afterCommit()->delay(now()->addMinutes(($setInactive->maxTime * 60) + $time));

                return redirect()->route('detectives')->with('message', "Succesfully hired ". $setInactive->dets ." Detectives for " . ucfirst($player->name) );
            }
            else{
                return back()->with('error', "Not enough cash");
            }
        }
        else{
            return back()->with('error', "You don't have active detectives on " . ucfirst($player->name) ." in this country or countries");
        }
    }

    public function deleteDets($id){
        $detective = Detective::find($id);

        if($detective->userID == Auth::id()){
            $delete = $detective->delete();

            if($delete == 1){
                return redirect()->route('detectives')->with('message', "Succesfully deleted" );
            }
            elseif ($delete == 0){
                return redirect()->route('detectives')->with('error', "Failed deletion");
            }
            else{
                return redirect()->route('detectives')->with('error', "Unknown Error by deletion");
            }
        }
        else{
            return redirect()->route('detectives')->with('error', "You can't delete something that doesn't belong to you");
        }
    }

    public function test(){

    }

}
