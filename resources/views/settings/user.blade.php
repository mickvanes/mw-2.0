@extends('layouts.default')

@section('header')
    USER SETTINGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <form action="" id="form" method="post">
                        <!-- CROSS Site Request Forgery Protection -->
                        @csrf
                        <div class="grid grid-cols-2 ">
                            <span class="mb-8"><h1 class="font-bold text-xl">Settings</h1></span>
                            <div></div>
                        </div>
                        <h1 class="font-bold text-md mb-1">Murder Options</h1>
                        <div class="grid grid-cols-1 md:grid-cols-2 w-full md:w-8/12 mb-4 gap-4">
                            <div class="grid grid-cols-2 mb-4">
                                <label>Backfire</label>
                                <select class="text-black" id="backfire" name="backfire">
                                    <option>Choose an option</option>
                                    <option value="0">Don't fire back</option>
                                    <option value="1">Fire the half bullets of the killer back</option>
                                    <option value="2">Fire the same amount bullets back to the killer</option>
                                    <option value="3">Fire the double amount of bullets to the killer</option>
                                </select>
                                <label>Testament</label>
                                <input type="text" class="text-black" value="{{ $settings->user->username }}" list="players" autocomplete="off">
                                <datalist id="players">
                                    @foreach($users as $user)
                                        <option value="{{ $user->username }}">{{ ucfirst($user->username) }}</option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>

                        <h1 class="font-bold text-md mb-1">Messages Options</h1>
                        <div class="grid grid-cols-1 md:grid-cols-2 w-full md:w-8/12 mb-4 gap-4">
                            <div class="grid grid-cols-2 mb-4">
                                <label>Race Messages</label>
                                <select class="text-black" id="raceMessages" name="raceMessages">
                                    <option value="0" @if($settings->raceMessages == 0) selected='selected' @endif>Off</option>
                                    <option value="1" @if($settings->raceMessages == 1) selected='selected' @endif>On</option>
                                </select>
                                <label>Buy out Messages</label>
                                <select class="text-black" id="buyOutMessages" name="buyOutMessages">
                                    <option value="0" @if($settings->buyOutJail == 0) selected='selected' @endif>Off</option>
                                    <option value="1" @if($settings->buyOutJail == 1) selected='selected' @endif>On</option>
                                </select>
                            </div>
                        </div>

                        <h1 class="font-bold text-md mb-1">Detective Options</h1>
                        <div class="grid grid-cols-1 md:grid-cols-2 w-full md:w-8/12 mb-4 gap-4">
                            <div class="grid grid-cols-2 mb-4">
                                <label>Default Dets Hours</label>
                                <select class="text-black" id="detsHours" name="detsHours">
                                    <option value="1">1 Hour</option>
                                    <option value="2">2 Hours</option>
                                    <option value="3">3 Hours</option>
                                    <option value="4">4 Hours</option>
                                </select>
                                <label>Default Dets Message</label>
                                <select class="text-black" id="detsMessages" name="detsMessages">
                                    <option value="0" @if($settings->detsMessage == 0) selected='selected' @endif>Off</option>
                                    <option value="1" @if($settings->detsMessage == 0) selected='selected' @endif>On</option>
                                </select>
                            </div>

                            <div class="grid grid-cols-2">
                                <label>Default Dets</label>
                                <select class="text-black" id="dets" name="dets">
                                    <option value="NULL" default>Empty</option>
                                    <option value="50" @if($settings->defaultDets == 50) selected='selected' @endif>50 detectives</option>
                                    <option value="100" @if($settings->defaultDets == 100) selected='selected' @endif>100 detectives</option>
                                    <option value="200" @if($settings->defaultDets == 200) selected='selected' @endif>200 detectives</option>
                                    <option value="300" @if($settings->defaultDets == 300) selected='selected' @endif>300 detectives</option>
                                    <option value="400" @if($settings->defaultDets == 400) selected='selected' @endif>400 detectives</option>
                                    <option value="500" @if($settings->defaultDets == 500) selected='selected' @endif>500 detectives</option>
                                </select>
                                <label>Default Dets Countries</label>
                                <select class="text-black" id="detsCountries" name="detsCountries">
                                    <option value="{{ $allCountries }}">All Countries</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="grid grid-cols-1 md:grid-cols-2 w-full md:w-8/12 mb-4 gap-4">
                            <div class="grid grid-cols-2">
                                <label>Theme</label>
                                <select class="text-black" id="theme" name="theme">
                                    <option value="0" @if($settings->theme == "null") selected='selected' @endif>Light</option>
                                    <option value="1" @if($settings->theme == "dark") selected='selected' @endif>Dark</option>
                                </select>

                            </div>
                        </div>



                        <input type="hidden" id="comment" name="comment" value="">
                        <input type="submit" name="send" value="Submit" class="bg-gray-500 hover:bg-gray-700 mt-4 text-white font-bold py-2 px-4">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

