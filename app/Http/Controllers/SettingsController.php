<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Countries;
use App\Models\Friends;
use App\Models\Profile;
use App\Models\Settings;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    public function index()
    {
        return view('settings.menu');
    }

    public function user()
    {
        $settings = Settings::where('userID', Auth::id())->first();
        $admins = Admin::where('adminRangID',1)->pluck('userID')->all();
        $users = User::select('username')->whereNotIn('id', $admins)->where('id', '!=', Auth::id())->get();
        $allCountries = Countries::select('id')->count() + 1;
        $countries = Countries::where('active', 1)->get();;

        return view('settings.user', compact('settings', 'users', 'allCountries', 'countries'));
    }

    public function profile()
    {
        $profile = Profile::find(Auth::id())->first();

        return view('settings.profile', compact('profile'));
    }

    public function friends()
    {
        $friends = Friends::with('user', 'friend')->where('accepted',1)->where(function ($query) {
            $query->where('userID', '=', Auth::id())
                ->orWhere('friendID', '=', Auth::id());
        })->paginate(15);

        $incoming = Friends::with('user', 'friend')->where([
            'friendID' => Auth::id(),
            'accepted' => 0
        ])->paginate(5, ['*'], 'incoming');

        $outgoing = Friends::with('user', 'friend')->where([
            'userID' => Auth::id(),
            'accepted' => 0
        ])->paginate(5, ['*'], 'outgoing');

        return view('settings.friends', compact('friends','incoming','outgoing'));
    }

    public function friendsForm($action,$id){
        $data = Friends::findOrFail($id);

        if($action == "accept"){
            $data->accepted = 1;
            $data->save();
            return back()->with('message', "Friend request accepted");
        }
        elseif($action == "decline"){
            $data->accepted = 0;
            $data->save();
            return back()->with('message', "Friend request declined");
        }
        else{
            return back()->with('error', "ERROR");
        }
    }

    public function friendsAdd(Request $request){
        $this->validate($request, [
            'name' => 'required|exists:users,username',
        ]);

        $button = $request->input('submit');

        $player = User::where('username', '=', ucfirst($request->input('name')))->first();

        $outgoing = Friends::where([
            'userID' => $player->id,
            'friendID' =>  Auth::id()
        ])->first();

        $incoming = Friends::where([
            'friendID' => $player->id,
            'userID' =>  Auth::id()
        ])->first();

        if($button == "Add"){
            if(!$outgoing && !$incoming) {
                Friends::create([
                    'userID' => Auth::id(),
                    'friendID' => $player->id
                ]);

                return back()->with('message', "Friend request send");
            }
            return back()->with('error', "You have already him/her add as friend");
        }
        elseif($button == "Del"){
            if($outgoing){
                Friends::destroy($outgoing->id);
            }
            elseif($incoming){
                Friends::destroy($incoming->id);
            }
            else{
                return back()->with('error', "You don't have him/her as a friend");
            }
            return back()->with('message', "Friend removed");
        }
        else{
            return back()->with('error', "ERROR");
        }

        }
}
