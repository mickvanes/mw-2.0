<?php

namespace App\Http\Middleware;

use App\Models\BackfireKill;
use App\Models\Jail;
use App\Models\Kill;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckIfAction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $getLastKill = Kill::where([
            ['playerID', '=', Auth::id()],
            ['active', '=', 1]
        ])->first();

        $getLastBackfireKill = BackfireKill::where([
            ['playerID', '=', Auth::id()],
            ['active', '=', 1]
        ])->first();

        $getJail = Jail::orderBy('endTime', 'desc')->where('userID','=', Auth::id())->first();

        if($getJail){
            $dateParse = Carbon::parse($getJail->endTime)->format('Y/m/d H:i:s');
            $dateNew = Carbon::createFromFormat('Y/m/d H:i:s',  $dateParse);
            $jailed = $dateNew->isPast();

            if(!$jailed){
                return redirect()->route('jailed');
            }
        }

        if($getLastKill || $getLastBackfireKill){
            return redirect('/dead');
        }

        return $next($request);

    }
}
