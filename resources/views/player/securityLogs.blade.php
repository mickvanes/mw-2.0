@extends('layouts.default')

@section('header')
    SECURITY LOGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                            <div class="w-full">
                                <table class="table-fixed w-full">
                                    <thead>
                                    <tr>
                                        <th class="text-left">Minutes</th>
                                        <th class="text-left">Begin Time</th>
                                        <th class="text-left">End Time</th>
                                    </tr>
                                    </thead>

                                @foreach($securityLogs->reverse() as $item)
                                    <tr>
                                        <td>{{ $item->minutes }}m</td>
                                        <td>{{ $item->beginTime }}</td>
                                        <td>{{ $item->endTime }}</td>
                                    </tr>
                                @endforeach
                                </table>
                            </div>
                        </div>
                </div>
            </div>
    </div>
@stop
