<?php

namespace App\Http\Controllers;

use App\Models\Cash;
use App\Models\FamilyPromotion;
use App\Models\Profile;
use App\Models\Rank;
use App\Models\Rule;
use App\Models\Stats;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Rules\ValidHCaptcha;

class CrimeController extends Controller
{

    public function crime(Request $request)
    {
        $user = auth()->user();

        $userName = $user->username;
        $userCash = $user->cash->cash;
        $userBank = $user->cash->bank;

        $rankID = $user->getCurrentRank->rankID;

        $crimeChance = DB::table('crime_chance')
            ->select('*')
            ->where('rankID', '=', $rankID)
            ->where('crimeID', '>=', 0)
            ->where('crimeID', '<=', 4)
            ->get();

        $crimes = DB::table('crimes')
            ->select('*')
            ->where('id', '>=', 0)
            ->where('id', '<=', 4)
            ->get();

        $dateNew1 = DB::table('profiles')
            ->select('profiles.last_crime')
            ->where('id', '=', Auth::id())
            ->pluck('profiles.last_crime')
            ->first();

        $dateParse = Carbon::parse($dateNew1)->format('d/m/Y H:i:s');
        $dateNew = Carbon::createFromFormat('d/m/Y H:i:s',  $dateParse);
        $diff = $dateNew->isPast();
        $dateNew = Carbon::createFromFormat('d/m/Y H:i:s',  $dateParse)->format('d/m/Y H:i:s');

        $crimeTimer = $request->session()->get('crimeTimer');
        $secs = $request->session()->get('secs');

        $hcaptcha = uniqid();

        return view('crime', compact('crimeChance', 'userName', 'userCash', 'userBank', 'crimeTimer', 'secs', 'crimes', 'dateNew','diff','hcaptcha'));
    }

    public function crimeForm(Request $request)
    {
        $this->validate($request, [
            'crimeID' => 'required',
            'cf-turnstile-response' => ['required', new ValidHCaptcha()],
        ]);

        $user = Auth::user();

        $rankID = $user->getCurrentRank->rankID;

        $crime = DB::table('crimes')
            ->select('crimes.name')
            ->where('crimes.id', '=', $request->input('crimeID'))
            ->pluck("crimes.name")[0];

        $crimeChance = DB::table('crime_chance')
            ->select('*')
            ->where('rankID', '=', $rankID)
            ->where('crimeID', '=', $request->input('crimeID'))
            ->first();

        $crimeAdv = DB::table('rank_adv')
            ->select('rankAdv')
            ->where('rankID', '=', $rankID)
            ->where('crimeID', '=', $request->input('crimeID'))
            ->pluck('rankAdv')
            ->first();

        $statsCrime = $user->stats()->pluck('crime')->first();

        $now = new DateTime();
        $date = $now->format('Y-m-d H:i:s');

        $lastCrime = DB::table('profiles')
            ->select('last_crime')
            ->where('id', '=', Auth::id())
            ->pluck('last_crime')
            ->first();

        $timer = $request->session()->get('crimeTimer');

        if ($date > $lastCrime) {
            $data = Carbon::now()->addSeconds(90);

            DB::table('profiles')
                ->where('id', Auth::id())
                ->update(['last_crime' => $data]);

            DB::table('user_stats')
                ->where('userID', Auth::id())
                ->orderBy('created_at','desc')
                ->take(1)
                ->update(['crime' => $statsCrime + 1]);

            if ($rankID < 15) {
                $rankAdv = Rank::where([
                    'userID' => Auth::id(),
                    'active' => 1
                ])->firstOrFail();

                if($rankAdv->adv < 100){
                    Rank::where([
                            'userID' => Auth::id(),
                            'active' => 1
                        ])->update(['adv' => $rankAdv->adv + $crimeAdv]);
                }

                $finalRank = Rank::where([
                    'userID' => Auth::id(),
                    'active' => 1
                ])->first();

                if ($finalRank->adv >= 100) {
                    Rank::where([
                        'userID' => Auth::id(),
                        'active' => 1
                    ])->update(['active' => 0]);

                    Rank::create([
                        'userID' => Auth::id(),
                        'rankID' => $rankID + 1,
                        'adv' => 0,
                        'active' => 1,
                        'created_at' => Carbon::now('Europe/Amsterdam'),
                        'updated_at' => Carbon::now('Europe/Amsterdam'),
                    ]);

                    if($user->family){
                        $rank = Rank::where([
                            'userID' => Auth::id(),
                            'active' => 1
                        ])->first();

                        $promotion = FamilyPromotion::where([
                            'familyID' => $user->family->familyID,
                            'rank' => $rank->rankID])->first('cash');

                        $cash = Cash::where('userID', Auth::id())->first('cash');

                        Cash::where('userID', Auth::id())->update([
                            'cash' => $cash->cash + $promotion->cash
                        ]);
                    }
                }
            }
            $chance = $request->session()->get($request->input('crimeID'));
            $crimeChanceFinal = rand(0, 100);
            if ($crimeChanceFinal < $chance) {
                $crimeMoney = (bool)random_int(0, 1);

                if ($crimeMoney) {
                    $minCrimeCash = Rule::select('minCrimeCash')->first();
                    $maxCrimeCash = Rule::select('maxCrimeCash')->first();
                    $randomCash = rand($minCrimeCash, $maxCrimeCash);
                    $userCash = Auth::user()->cash->cash;
                    Cash::where('userID', Auth::id())->update(['cash' => $userCash + $randomCash]);
                }

                $request->session()->put('timer', 90);
                $request->session()->put('crimeTimer', $data);
                return redirect()->route('crime.show')->with('message', 'You did' . $crime . ' succesfully' . $statsCrime + 1);

            } else {
                $request->session()->put('timer', 90);
                $request->session()->put('crimeTimer', $data);
                return redirect()->route('crime.show')->with('message', 'You failed to do ' . $crime . ' ' . $statsCrime + 1);
            }
        }
    }

    public function route(){

        $lastR66Player = DB::table('routeLogs')
            ->select('*')
            ->where([
                ['player1', '=', Auth::id()]
            ])
            ->orWhere([
                ['player2', '=', Auth::id()]
            ])
            ->get();

        if($lastR66Player){

        }
    }

    public function routeForm(Request $request)
    {
        $this->validate($request, [
            'player' => 'required',
            'weapon' => 'required',
            'bullets' => 'required|integer|between:1,50'
        ]);


        $playerCheck = DB::table('users')
            ->select('name')
            ->where('name', '=', $request->input('player'))
            ->count();

        $weaponCheck = DB::table('routeRules')
            ->select('*')
            ->where('weapon', '=', $request->input('weapon'))
            ->count();

        $lastR66Player1 = DB::table('routeLogs')
            ->select('time')
            ->where('name', '=', Auth::id())
            ->get();

        $lastR66Player2 = DB::table('routeLogs')
            ->select('time')
            ->where('name', '=', $request->input('player'))
            ->get();

        $routePlayerID = DB::table('users')
            ->select('id')
            ->where('name', '=', $request->input('player'))
            ->get();

        $dateTimePlayer1 = Carbon::createFromFormat('Y-m-d H:i:s', $lastR66Player1, 'Europe/Amsterdam');
        $dateTimeReadyPlayer1 = $dateTimePlayer1->add('hour', 2);

        $dateTimePlayer2 = Carbon::createFromFormat('Y-m-d H:i:s', $lastR66Player2, 'Europe/Amsterdam');
        $dateTimeReadyPlayer2 = $dateTimePlayer2->add('hour', 2);
        $dateTimeNow = Carbon::now();

        $userCash = DB::table('user_cash')
            ->select('cash')
            ->where('user_cash.userID', '=', Auth::id())
            ->pluck('cash')
            ->first();

        if ($playerCheck == 1) {
            if ($weaponCheck == 1) {
                $weaponCost = DB::table('routeRules')
                    ->select('costs')
                    ->where('weapon', '==', $request->input('weapon'))
                    ->pluck('costs')
                    ->first();

                if ($dateTimeNow > $dateTimeReadyPlayer1) {
                    if($dateTimeNow > $dateTimeReadyPlayer2){
                    if ($weaponCost <= $userCash) {
                        $now = new DateTime();
                        $date = $now->format('Y-m-d H:i:s');

                        DB::table('routeLogs')->insert([
                            'player1' => Auth::id(),
                            'player2' => $routePlayerID,
                            'accepted' => 0,
                            'time' => $date,
                        ]);

                        DB::table('user_cash')
                            ->where('userID', Auth::id())
                            ->update(['cash' => $userCash - $weaponCost]);

                        DB::table('system_messages')->insert([
                            'senderID' => 1,
                            'recipientID' => $routePlayerID,
                            'subject' => "Route 66",
                            'body' => "You invited to do a Route 66 from " . Auth::user()->name . ", Go quickly to the route 66 page to accept or decline",
                            'timestamp' => $date,
                            'seen' => 0
                        ]);
                    }
                    else {
                        return "You don't have enough cash to buy this weapon";
                    }
                }else {
                        return "This player isn't ready yet";
                    }
                }
                else {
                    return "You are not ready yet";
                }
            }
            else {
                return "This weapon doesn't exists in the database";
            }
        }
        else {
            return "This player doesn't exists in the database";
        }
    }
}

