<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CollectionCards extends Model
{
    use HasFactory;

    protected $table = 'cards';

    protected $fillable = [
        'cardName',
        'cardImage',
        'cardRarity',
        'cardMinCosts',
        'cardMaxCosts',
    ];

    public function collections()
    {
        return $this->hasMany(Collection::class, 'cardID','id');
    }
}
