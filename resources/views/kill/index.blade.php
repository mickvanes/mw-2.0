@extends('layouts.default')

@section('header')
    Kill
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if($errors)
                        @foreach ($errors->all() as $message)
                            <div class="alert alert-danger">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Error</strong>
                                    <span class="block sm:inline">{{$message}}</span>
                                </div>
                            </div><br>
                        @endforeach
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Error</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-danger">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    <div x-data="{ open: false }">
                        <button class="mb-4" @click="open = ! open"> Tips & Tricks</button>

                        <div x-show="open" class="mt-4 mb-4">
                            <div class="grid grid-rows-1 md:grid-rows-2 gap-2 bg-gray-50 rounded p-4">

                            </div>
                        </div>
                    </div>

                    <form method="post" name="form" action="{{ route('kill.store') }}" id="killForm" autocomplete="off">

                        <!-- CROSS Site Request Forgery Protection -->
                        @csrf

                        <div class="form-group">
                            <div class="grid grid-cols-5">
                                <div>
                                    <label for="recipient">Player</label>
                                </div>
                                <div><input id="player" onkeyup="checkDets()" class="text-black" type="text" list="dets" name="player" autocomplete="off" placeholder="player" aria-label="player" required></div>
                                <datalist id="dets">
                                    @foreach($players as $player)
                                        <option value="{{ $player->player->username }}">{{ ucfirst($player->player->username) }}</option>
                                    @endforeach
                                </datalist>
                            </div>
                            <div class="grid grid-cols-5 mt-2">
                                <div>
                                    <label for="Bullets">Bullets:</label>
                                </div>
                                <div>
                                    <input type="number" min="1" max="{{ $maxBullets }}" class="text-black" name="bullets" required>
                                </div>
                            </div>
                            <div></div>

                            <div class="grid grid-cols-5 mt-2">
                                <div>
                                    <label for="Subject">Message:</label>
                                </div>
                                <div>
                                    <label for="Bullets">boolMessage:</label><input type="checkbox" id="boolMessage" name="boolMessage">
                                    <label for="Bullets">boolCheck:</label><input type="checkbox" id="boolCheck" name="boolCheck">
                                    <textarea class="text-black" cols="25" rows="5" style="resize: both;" name="message" form="messageForm"></textarea>
                                </div>

                            </div>



                            <div class="grid grid-cols-5 mt-2">
                                <input type="submit" class="text-black" name="submit">
                                <div>
                                    <button data-action='submit'>Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script src='https://js.hcaptcha.com/1/api.js?onload=onloadCallback&render=explicit' async defer></script>
    <script>
        function addText(value) {
            document.form.message.value += value;
        }

        function checkDets(){
            const player = document.getElementsByName('player')[0].value;
            const list = document.getElementById('dets').options;
            const arr = [].slice.call(list);

            function checkArr(arr) {
                for (let i of arr) {
                    if (i.value.toLowerCase() === player.toLowerCase())
                        document.getElementById("player").style.outline = '2px solid green';
                    else{
                        document.getElementById("player").style.outline = '2px solid red';
                    }
                }
            }

            checkArr(arr)

        }

        function onloadCallback() {
            hcaptcha.render('<?= $hcaptcha ?>', {
                sitekey: 'fbf538f9-45bb-4730-9850-9a6e2d68bae7'
            });
        }

        setInterval(function() {
            hcaptcha.reset();
        }, 25 * 1000);
    </script>
    <script>
        function onSubmit(token) {
            document.getElementById("killForm").submit();
        }
    </script>

@stop
