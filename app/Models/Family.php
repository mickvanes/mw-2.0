<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    use HasFactory;

    protected $table = 'families';

    protected $fillable = [
        'name',
        'description',
        'profileText',
        'famClicks',
        'famPower',
        'shares',
        'exitCosts',
        'creation_date',
        'houses',
        'energy',
        'land',
        'walls',
        'famRep'
    ];

    public function members()
    {
        return $this->hasMany(FamilyUserRang::class, 'familyID', 'id')->orderBy('rangID');
    }

    public function owner()
    {
        return $this->hasOne(FamilyUserRang::class, 'familyID', 'id')->where('rangID',1);
    }


}
