@extends('layouts.default')

@section('header')
    SHOP POWER
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                    <b>POWER</b>
                        <br><span>Remaining clicks: {{ number_format($remClicks, 0, ',', '.')  }}</span>

                        <div class="grid grid-cols-1 md:grid-cols-2">
                        @foreach($powerItems as $powerItem)
                        <form method="POST" action="{{ route("power.post") }}">
                            @csrf
                            <div class="grid grid-cols-2">
                                <div class="grid grid-cols-1 mt-2">
                                    <span>{{ $powerItem->name }}<br> {{ $powerItem->power }} power <br> Needed Clicks: {{ $powerItem->needClicks }} (<span style="color: red;">{{ floor($remClicks / $powerItem->needClicks) }}</span>) <br>Costs: &euro;{{ number_format($powerItem->costs , 0, ',', '.')  }} (<span style="color: red;">{{ number_format( floor(Auth::user()->cash->cash / $powerItem->costs ), 0, ',', '.') }}</span>)</span>
                                </div>
                                <div class="mt-2 mx-auto">
                                    <img width="100px" src="{{ $powerItem->image }}">
                                </div>
                                <div class="grid col-span-2 mt-2 text-black mr-2 mb-2">
                                    <input type="hidden" id="item" name="item" value="{{ $powerItem->id }}">
                                    <input type="number" id="amount" name="amount" min="1" max="{{ floor($remClicks / $powerItem->needClicks) }}">
                                    <input type="submit" name="send" value="Buy" class="bg-blue-500 hover:bg-blue-700 text-black font-bold mt-2">
                                </div>
                            </div>
                        </form>
                        @endforeach
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection


