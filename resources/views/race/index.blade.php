@extends('layouts.default')

@section('header')
    RACE
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Race</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Race</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                    <div class="grid grid-cols-1 w-1/2">
                        <div>Races</div>
                        <div class="mt-2">
                            <button class="cursor-pointer bg-darkButton hover:bg-darkHover dark:bg-buttonBlue font-bold py-2 px-4"><a href="{{ route('race.create') }}">Create Race</a></button>
                        </div>
                    </div>

                    <table class="table-fixed mx-auto mt-4 w-full">
                        <thead>
                            <tr>
                                <th class="w-1/3 pr-4 text-left">Player</th>
                                <th class="w-1/3 pr-4 text-left">Bet</th>
                                <th class="w-1/3 text-left">Race!</th>
                            </tr>
                        </thead>
                        @foreach($races as $race)
                            <tr class="hover:bg-gray-700">
                                <td class="w-1/3 pr-4">{{ $race->user->username }}</td>
                                <td class="w-1/3 pr-4"> &euro;{{ number_format($race->bet, 0, ',', '.') }} </td>
                                <td class="w-1/3"><a href="{{ route('race.take', $race->id) }}">Race!</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
