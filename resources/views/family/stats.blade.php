@extends('layouts.default')

@section('header')
    Family Stats
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                        <div class="md:col-span-2 text-center">
                            <a href="{{ route('families.stats') }}"> View all family stats</a>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Power</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Power</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($stats->sortByDesc('power')->take(5) as $statPower)
                                    <span class="border p-2"><a href="{{ route('player.show', $statPower->name) }}">{{ $statPower->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statPower->power, 0, ',', '.') }} Power</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Clicks</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Clicks</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($stats->sortByDesc('clicks')->take(5) as $statClicks)
                                    <span class="border p-2"><a href="{{ route('player.show', $statClicks->name) }}">{{ $statClicks->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statClicks->clicks, 0, ',', '.') }} Clicks</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Cash</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Cash</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($stats->sortByDesc('cash')->take(5) as $statCash)
                                    <span class="border p-2"><a href="{{ route('player.show', $statCash->name) }}">{{ $statCash->name}}</a></span>
                                    <span class="border p-2 break-all"><b>&euro;{{ number_format($statCash->cash, 0, ',', '.') }}</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Rank</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Rank</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($stats->groupBy('rank')->take(5) as $statRanks)
                                    @foreach($statRanks->sortByDesc('rankAdv') as $ranks)
                                        <span class="border p-2"><a href="{{ route('player.show', $ranks->name) }}"> {{ $ranks->name }}</a></span>
                                        <span class="border p-2 break-all"><b>{{ $ranks->rank }} ({{ $ranks->rankAdv }}%)</b></span>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Reputation</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Reputation</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($stats->sortByDesc('rep')->take(5) as $statRep)
                                    <span class="border p-2"><a href="{{ route('player.show', $statRep->name) }}">{{ $statRep->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statRep->rep, 0, ',', '.') }} Reputation</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Honor</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Honor</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($stats->sortByDesc('honor')->take(5) as $statHonor)
                                    <span class="border p-2"><a href="{{ route('player.show', $statHonor->name) }}">{{ $statHonor->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statHonor->honor, 0, ',', '.') }} Honor</b></span>
                                @endforeach
                            </div>

                        </div>

                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Kills</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Kills (Kill points)</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($stats->sortByDesc('kills')->take(5) as $statKills)
                                    <span class="border p-2"><a href="{{ route('player.show', $statKills->name) }}">{{ $statKills->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statKills->kills, 0, ',', '.') }} ({{ number_format($statKills->killsPoints, 0, ',', '.') }})</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Backfire Kills</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Backfire Kills</span>
                            </div>
                            <div class="grid grid-cols-2 border">
                                @foreach($stats->sortByDesc('backfire')->take(5) as $statBF)
                                    <span class="border p-2"><a href="{{ route('player.show', $statBF->name) }}">{{ $statBF->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statBF->backfire, 0, ',', '.') }} ({{ number_format($statBF->bfKillsPoints,0,',','.') }})</b></span>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@foreach($familyMembers as $familyMember)--}}
