@extends('layouts.default')

@section('header')
    Topic
@stop

@section('content')
    <div class="mx-auto w-full  mb-2">
        <div class="max-w-7xl mx-auto ">
            <div class="shadow-sm md:w-full">
                <div class="text-white">
                    <div class="mx-auto grid md:grid-cols-6 grid-cols-2 gap-1">
                        @if($forumTopic->categoryID)
                            <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('family.category', $forumTopic->categoryID) }}"> < Category</a></div>
                        @endif
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('family.forum') }}"> < Forum</a></div>
                    </div>
                    <div class="mx-auto grid grid-cols-1 md:grid-cols-4 md:gap-2 mt-2">
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault md:col-span-3"><b>{{ $forumTopic->name }}</b></div>

                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault">Created at: {{ $forumTopic->created_at }} </br> Latest reaction: {{ $forumTopic->updated_at }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Topic</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Topic</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>
                        <div class="grid grid-cols-1">
                            <div class="w-full">
                                <div class="text-center"></div>
                                <div class="flex">
                                    <div class="w-2/5 md:w-2/12">
                                        <div class="w-full py-4">
                                            <img class="mx-auto mb-2" src="{{ $forumTopic->user->avatar }}">
                                            <div class="text-center">{{ $forumTopic->user->username }}</div>
                                        </div>
                                    </div>
                                    <div class="w-3/5 md:w-10/12 py-4 break-words">
                                        <div class="ml-4">{{ $forumTopic->content }}</div>
                                    </div>
                                    <div class="mb-4 text-right float-end">
                                        <a href="{{ route('reportItem', ["familyTopic",$forumTopic->id]) }}" )><img src="https://www.mafiaway.nl/images/icons/error.png"></a>
                                    </div>
                                </div>
                            </div>

                            <div>
                                @foreach($comments as $comment)
                                    <div class="flex mt-4">
                                        <div class="w-3/5 md:w-2/12">
                                            <div class="w-full py-4">
                                                <div class="p-4"><img class="mx-auto mb-2 block" src="{{ $comment->user->avatar }}" style="max-width: 100px; max-height: 100px"></div>
                                                <div class="text-center break-words">{{ $comment->user->username }}</div>
                                            </div>
                                        </div>
                                        <div class="w-3/5 md:w-10/12 pt-4 bg-gray-600 text-white rounded-xl">
                                            <div id="message_{{ $comment->id }}" class="ml-4">{{ $comment->content }}</div>
                                        </div>
                                        <div class="mb-4 text-right float-end">
                                            <a id="comment_{{ $comment->id }}" onclick=comment(<?php echo $comment->id; ?>)><img src="https://www.mafiaway.nl/images/icons/comment.png"></a>
                                            <a href="{{ route('reportItem', ["familyComment",$comment->id]) }}" )><img src="https://www.mafiaway.nl/images/icons/error.png"></a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if($mute)
                                <span class="font-bold mb-4 text-center" style="color: #970000;">You are muted because "{{ $mute->reason }}", you have a {{ $mute->days }} days mute, it ends on {{ $mute->endDate }} </span>
                            @elseif($forumTopic->onlyCrew)
                                <span class="font-bold mb-4 text-center" style="color: #970000;">You can't react on this topic</span>
                            @else
                                <form action="" id="form" method="post">
                                    <!-- CROSS Site Request Forgery Protection -->
                                    @csrf
                                    <div class="flex mt-4">
                                        <div class="w-full py-4 bg-gray-800 text-white rounded-xl">
                                            <textarea class="bg-gray-800 mx-auto border-0 ml-4 mr-4 w-11/12 min-h-full " name="message" id="message" cols="70" rows="10" ></textarea>
                                        </div>
                                    </div>
                                    <div class="mb-4 text-right">
                                        <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
                                    </div>
                                </form>
                            @endif

                            <div class="w-full">
                                {{ $comments->links() }}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
