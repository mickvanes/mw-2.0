<div style='background-image: url("{{ asset('images/1970270.jpg') }}"); background-size: cover; background-repeat: no-repeat; background-position: center; backdrop-filter: grayscale(100)'>
    <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0">
        <div>
            {{ $logo }}
        </div>

        <div class="w-full sm:max-w-md mt-6 px-6 py-4 shadow-md overflow-hidden bg-white bg-opacity-10 sm:rounded-lg">
            {{ $slot }}
        </div>
    </div>
</div>
