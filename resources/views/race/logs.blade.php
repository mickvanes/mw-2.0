@extends('layouts.default')

@section('header')
    RACE
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div>
                            <table class="table-fixed w-1/3 mb-4">
                                Stats
                                <tr>
                                    <td>Page</td>
                                    <td><span style="color:green"><b>{{ $winsPage }}</b></span> / <span style="color:red"><b>{{ $lostsPage }}</b></span></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td><span style="color:green"><b>{{ $wins }}</b></span> / <span style="color:red"><b>{{ $losts }} </b></span>({{ $wins + $losts }} races)</td>
                                </tr>
                            </table>
                            <table class="table-fixed w-full">
                                Race Logs

                                @if((new \Jenssegers\Agent\Agent())->isDesktop())
                                    <thead>
                                    <tr>
                                        <th class="text-left">Racer 1</th>
                                        <th class="text-left">Racer 2</th>
                                        <th class="text-right md:text-left">Total (Bet)</th>
                                        <th class="text-right md:text-left">Won/Lost</th>
                                        <th class="text-left hidden md:block">Date</th>
                                    </tr>
                                    </thead>
                                    @foreach($races as $race)
                                            <tr>
                                                <td><a href="{{ route('player.show', $race->racer1->username) }}"> {{ $race->racer1->username  }} </a></td>
                                                <td><a href="{{ route('player.show', $race->racer2->username) }}"> {{ $race->racer2->username  }} </a></td>
                                                <td class="text-right md:text-left">&euro;{{ number_format($race->bet, 0, ',', '.') }} (&euro;{{ number_format($race->bet / 2 , 0, ',', '.') }})<br>
                                                </td>
                                                <td class="text-right md:text-left">
                                                    @if($race->winner == Auth::id())
                                                        <span style="color:green"><b>Won</b></span>
                                                    @else
                                                        <span style="color:red"><b>Lost</b></span>
                                                    @endif
                                                </td>
                                                <td class="hidden  md:block">{{ $race->created_at }}</td>
                                            </tr>
                                    @endforeach
                                @endif
                                @if((new \Jenssegers\Agent\Agent())->isMobile())
                                    <thead>
                                    <tr>
                                        <th class="text-left">Racer</th>
                                        <th class="text-right">Total</th>
                                        <th class="text-right">Won/Lost</th>
                                    </tr>
                                    </thead>

                                    @foreach($races as $race)
                                        @if($race->racer1->id != \Illuminate\Support\Facades\Auth::id())
                                            <tr>
                                                <td><a href="{{ route('player.show', $race->racer1->username) }}"> {{ $race->racer1->username  }} </a></td>
                                                <td class="text-right">&euro;{{ number_format($race->bet, 0, ',', '.') }} <br></td>
                                                @if($race->winner == Auth::id())
                                                    <td class="text-right"><span style="color:green"><b>Won</b></span></td>
                                                @else
                                                    <td class="text-right"><span style="color:red"><b>Lost</b></span></td>
                                                @endif
                                            </tr>

                                        @elseif($race->racer2->id != Auth::id())
                                            <tr>
                                                <td><a href="{{ route('player.show', $race->racer2->username) }}"> {{ $race->racer2->username  }} </a></td>
                                                <td class="text-right">&euro;{{ number_format($race->bet, 0, ',', '.') }} <br></td>
                                                @if($race->winner == Auth::id())
                                                    <td class="text-right"><span style="color:green"><b>Won</b></span></td>
                                                @else
                                                    <td class="text-right"><span style="color:red"><b>Lost</b></span></td>
                                                @endif
                                            </tr>

                                        @endif

                                    @endforeach
                                @endif


                            </table>
                            <div class="mt-4">{{ $races->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

