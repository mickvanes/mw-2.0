<?php

namespace App\Jobs;

use App\Models\Cash;
use App\Models\Family;
use App\Models\FamilyUserRang;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PayFamLoan implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $families = Family::where('shares', '>=' , 1000)->get();

        foreach ($families as $family){
            $familyShares = $family->shares;
            $familyMembers = FamilyUserRang::where('familyID', '=' , $family->id)->get();

            foreach($familyMembers as $familyMember){
                $bankMember = Cash::where('userID', '=', $familyMember->userID)->first();
                $bankMember->bank = $bankMember->bank + ($familyShares * 200);
                $bankMember->save();
            }
        }
    }
}
