@extends('layouts.default')

@section('header')
    BANK
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <h1 class="text-lg font-semibold">Friends</h1>
                    <div class="grid grid-cols-3 md:grid-cols-4 gap-4 mt-4">
                        @foreach($friends as $friend)
                            @if($friend->friend->id == $playerID)

                            @else
                                <div>
                                    <div class="grid grid-cols-1">
                                        <a href="{{ route('player.show', $friend->friend->name) }}">
                                            <img src="{{ $friend->friend->avatar }}" width="150px" height="150px"><br>
                                            <span class="mt-2">{{ $friend->friend->name }}</span><br>
                                            <span>{{ $friend->friend->family->familyDetails->name ?? "" }}</span>
                                        </a>
                                    </div>
                                </div>
                            @endif

                        @endforeach
                    </div>
                    <div class="mt-4">{{ $friends->links() }}</div>
                </div>
            </div>
        </div>
    </div>
@stop
