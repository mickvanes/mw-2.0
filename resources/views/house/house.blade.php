@extends('layouts.default')

@section('header')
    DASHBOARD
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                        House <br><br>

                    @if(!$currentCountryBoolean)
                        Your house is not in this country. If you want to go to your safe, go to the airport to fly to your home country<br><br>
                        <button class="bg-gray-500 text-white font-bold py-2 px-2" style="font-size: 100%;">
                            <a href="{{ route('airport') }}">Go to the airport!</a>
                        </button>
                    @else
                            <button class="bg-gray-500 text-white font-bold py-2 px-2" style="font-size: 100%;">
                                <a href="{{ route('safe') }}">Go inside</a>
                            </button>

                            <button class="bg-gray-500 text-white font-bold py-2 px-2" style="font-size: 100%;">
                                <a href="{{ route('house.sell') }}">Sell your house</a>
                            </button>
                        @endif


                </div>
            </div>
        </div>
@stop

