<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.3/r-2.2.9/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/r-2.2.9/datatables.min.js"></script>


<script src="{{ URL::asset('js/timer.js') }}"></script>

<x-app-layout>
    <x-slot name="header">
        <div class="grid grid-cols-6">
            <div>
                <label>
                    <a href="{{route('dashboard')}}">
                        {{ __('Player') }} {{ Auth::user()->name }}
                    </a>
                </label>
            </div>
            <a href="{{route('bank')}}">
                <div>Cash: &euro;{{ number_format(Auth::user()->cash->cash, 0, ',', '.') }}</div>
            </a>
            <a href="{{route('bank')}}" class="ml-10">
                <div>Bank: &euro;{{ number_format(Auth::user()->cash->bank, 0, ',', '.') }}</div>
            </a>
            <div></div>
            <div>
                <div class="float-right">
                </div>
            </div>

            <div>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <x-nav-link class="float-right" :href="route('logout')"
                                onclick="event.preventDefault();
                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-nav-link>
                </form>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Detectives</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Detectives</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>
                        <div class="grid grid-cols-1"><div class="m-auto">
                            <p class="text-center"> An error occurred while playing on Zaoway!</p>
                            <p class="text-center"> Rest in Peace {{ Auth::user()->name }}</p>
                            <img class="mt-4 max-h-44 m-auto" src="{{ asset('images\dead.png') }}">
                                @if($dead)
                                    {!! nl2br($dead->message) !!}
                                    <a href="{{ route('deadHospital') }}">Go fast make yourself to life</a>
                                @else
                                    Idunno
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>


