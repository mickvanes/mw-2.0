<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyUserClicks extends Model
{
    use HasFactory;

    protected $table = 'family_user_clicks';

    protected $fillable = [
        'familyID',
        'userID',
        'timestamp'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'userID');
    }

    public function family()
    {
        return $this->belongsTo(Family::class, 'id', 'familyID');
    }


}
