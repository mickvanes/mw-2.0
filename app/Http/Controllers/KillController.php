<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\BackfireKill;
use App\Models\BurstRank;
use App\Models\Cash;
use App\Models\Detective;
use App\Models\Hidden;
use App\Models\Jail;
use App\Models\Kill;
use App\Models\Defense;
use App\Models\Marriage;
use App\Models\Message;
use App\Models\Profile;
use App\Models\Rank;
use App\Models\Ranks;
use App\Models\Rule;
use App\Models\Settings;
use App\Models\Stats;
use App\Models\SystemMessage;
use App\Models\User;
use App\Models\Witness;

use App\Rules\ValidHCaptcha;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KillController extends Controller
{

    public function index(){
        $players = Detective::with('player')->where([
            ['userID','=', Auth::id()],
            ['found', '=', 1],
            ['active', '=', 1]
        ])->get();

        $maxBullets = Profile::where('id','=',Auth::id())->first()->bullets;
        $hcaptcha = uniqid();

        return view('kill.index',compact('players','maxBullets','hcaptcha'));
    }

    public function kill(Request $request){
        $data = $request->validate([
            'player' => 'required|exists:users,username',
            'bullets' => 'required|numeric|min:1',
            'message' => "string|max:6000",
        ]);

        $coinMode = 0; // (if paid with coin for faster killing) later feature.

        if(!$request->input('boolMessage')){
            $killMessage = (new \App\Helpers\Helper)->makeUBB($request->input('message')); // change message to UBB
        }else{
            $defaultMessage = Settings::where('userID','=',Auth::id())->firstOrFail();
            $killMessage = (new \App\Helpers\Helper)->makeUBB($defaultMessage->killMessage); // change message to UBB
        }

        $victim = User::with('country','defense','profile','getCurrentRank','settings','cash')->where('username', '=', $data['player'])->firstOrFail(); // Find victim
        $killer = User::with('country','defense','profile','getCurrentRank','cash')->where('username', '=', Auth::user()->username)->firstOrFail(); // Find killer

        if($coinMode){
            $minutes = 30;
        } else{
            $minutes = 60;
        }

        $lastUnsuccessVictim = Kill::where([
            ['playerID', '=' , $victim->id],
            ['success', '=' , 0],
            ['created_at', '>=', Carbon::now()->subMinutes(20)]
        ])->first(); // Check if victim is shot in the last 20 minutes
        $lastUnsuccessKiller = Kill::where([['playerID', '=' , Auth::id()],
            ['created_at', '>=', Carbon::now()->subMinutes($minutes)]])->first(); // Check if killer has shot in the last 60 min or coinMode 30min

        $hiddenVictim = Hidden::where('userID', '=', $victim->id)->orderBy('endTime', 'desc')->first(); // Check if victim is hidden
        $hiddenKiller = Hidden::where('userID', '=', Auth::id())->orderBy('endTime', 'desc')->first(); // Check if killer is hidden

        $victimRank = $victim->getCurrentRank->rankID;
        $killerRank = $killer->getCurrentRank->rankID;

        $halfDefense = 75;
        $halfDefenseBullet = 350;  // bullets per health (350 bullets per 1%) (17,5k > 50%, 35k 100%)
        $fullDefenseBullet = 500; // bullets per health (500 bullets per 1%) (25k > 50%, 50k 100%)
        $halfDefenseBulletBackfire = 700;  // bullets per health (700 bullets per 1%) (35k 50%, 70k 100%)
        $fullDefenseBulletBackfire = 1000; // bullets per health (1000 bullets per 1%) (50k 50%, 100k 100%)

        $dets = Detective::where([
            'userID' => Auth::id(),
            'playerID' => $victim->id,
            'found' => 1,
            'active' => 1
        ])->firstOrFail();

        function CheckBeforeKill($victimHealth, $victimDefense,$halfDefense,$fullDefenseBullet,$halfDefenseBullet){
            if($victimDefense > $halfDefense){
                $neededBullets = $victimHealth * $fullDefenseBullet; //needed bullets to kill 100%
            }
            else{
                $neededBullets = $victimHealth * $halfDefenseBullet; //needed bullets to kill 100%
            }
            return $neededBullets;
        } // Calc health * bullets per health

        function CheckBeforeKillBF($killerHealth,$killerDefense,$halfDefense,$fullDefenseBulletBackfire,$halfDefenseBulletBackfire){
            if($killerDefense > $halfDefense){
                $neededBullets = $killerHealth * $fullDefenseBulletBackfire; //needed bullets to kill 100%
            }
            else{
                $neededBullets = $killerHealth * $halfDefenseBulletBackfire; //needed bullets to kill 100%
            }
            return $neededBullets;
        }

        function killVictim($uniqID,$victim,$killer,$data,$backfire,$halfDefense,$halfDefenseBullet,$fullDefenseBullet,$killMessage){
            $victimHealth = $victim->profile->health; // get victim health
            $victimDefense = $victim->defense->totalDefense; // 75 defense 1/2 bullets , all above is full bullets
            $CheckNeededBullets = CheckBeforeKill($victimHealth,$victimDefense,$halfDefense,$fullDefenseBullet,$halfDefenseBullet);
            if($data['bullets'] >= $CheckNeededBullets){
                Kill::create([
                    'uniqID' => $uniqID,
                    'userID' => Auth::id(),
                    'playerID' => $victim->id,
                    'bullets' => $data['bullets'],
                    'backfire' => $backfire,
                    'player_familyID' => $victim->family->id,
                    'user_familyID' => $killer->family->id,
                    'userRank' => $killer->getCurrentRank->rankID,
                    'playerRank' => $victim->getCurrentRank->rankID,
                    'userPercent' => $killer->getCurrentRank->adv,
                    'playerPercent' => $victim->getCurrentRank->adv,
                    'playerCash' => $victim->cash->cash,
                    'playerBank' => $victim->cash->bank,
                    'active' => true,
                    'success' => true,
                    'weapon' => $killer->defense->weapon,
                    'userDef' => $killer->defense->totalDefense,
                    'playerDef' => $victim->defense->totalDefense,
                    'message' => $killMessage,
                ]); // Create Kill

                $getRep = Ranks::where('id','=',$victim->getCurrentRank->rankID)->first('rep')->rep; // Search Rep for kill
                $getPoints = Ranks::where('id','=',$victim->getCurrentRank->rankID)->first('killPoints')->killPoints; // Search killPoints for kill

                Profile::find(Auth::id())->update([
                    'kills' => $killer->profile->kills + 1,
                    'killPoints' => $killer->profile->killPoints + $getPoints,
                    'rep' => $killer->profile->rep + $getRep,
                    'bullets' => $killer->profile->bullets - $data['bullets'],
                ]); // Update Profile Killer

                Profile::find($victim->id)->update([
                    'health' => 0,
                ]); // Update Profile Victim

                Rank::where([
                    'userID' => $victim->id,
                    'active' => 1
                ])->update(['active' => 0]); // set old rank for Victim non-active

                Rank::Create([
                    'userID' => $victim->id,
                    'rankID' => 1,
                    'adv' => 0,
                    'active' => 1
                ]); // set new rank for Victim

                $races = Stats::where(['userID' => $victim->id, 'active' => 1])->first('races')->races;

                Stats::where([
                    'userID' => $victim->id,
                    'active' => 1
                ])->update(['active' => 0]); // set old stats for Victim non-active

                Stats::Create([
                    'userID' => $victim->id,
                    'crime' => 0,
                    'car' => 0,
                    'hostage' => 0,
                    'races' => $races,
                    'active' => 1
                ]); // set new stats for Victim


                // Check if victim is married
                $marriage = Marriage::with('player1Details', 'player2Details')
                    ->where('accepted','=', 1)
                    ->where(function ($query) use ($victim){
                        $query->orWhere([
                            'player1' => $victim->id,
                            'player2' => $victim->id
                        ]);
                    })->first();

                if($marriage->player1Details->id == $victim->id){
                    $cashHusband = Cash::where('userID','=',$marriage->player2Details->id)->first()->cash; // Cash Husband
                    $cashVictim = Cash::where('userID','=',$victim->id)->first()->bank; // Bank Victim
                    $cashPayoutHusband = floor(($cashVictim / 100) * 10);

                    Cash::where('userID','=',$victim->id)->first()->update(['bank' => $cashVictim - $cashPayoutHusband,]);// delete 10%
                    Cash::where('userID','=',$marriage->player2Details->id)->first()->update(['cash' => $cashHusband + $cashPayoutHusband,]);

                } elseif($marriage->player2Details->id == $victim->id){
                    $cashHusband = Cash::where('userID','=',$marriage->player1Details->id)->first()->cash; // Cash Husband
                    $cashVictim = Cash::where('userID','=',$victim->id)->first()->bank; // Bank Victim
                    $cashPayoutHusband = floor(($cashVictim / 100) * 10);

                    Cash::where('userID','=',$victim->id)->first()->update(['bank' => $cashVictim - $cashPayoutHusband,]);// delete 10%
                    Cash::where('userID','=',$marriage->player1Details->id)->first()->update(['cash' => $cashHusband + $cashPayoutHusband,]);

                } else{
                    $cashVictim = Cash::where('userID','=',$victim->id)->first()->bank; // Bank Victim
                    $cashPayoutHusband = floor(($cashVictim / 100) * 10);

                    Cash::where('userID','=',$victim->id)->first()->update(['bank' => $cashVictim - $cashPayoutHusband,]);// delete 10%
                }

                // Check if testament and delete money from victim and add to killer
                if($victim->settings->testament != NULL){
                    $cashTestament = Cash::where('userID','=',$victim->settings->testament)->first()->cash; // Cash Testament
                    $cashRefreshVictim = Cash::where('userID','=',$victim->id)->first()->bank; // Bank Victim
                    $cashPayoutHusband = floor(($cashRefreshVictim / 100) * 5);

                    Cash::where('userID','=',$victim->settings->testament)->first()->update(['cash' => $cashTestament + $cashPayoutHusband,]); // delete 5%
                    Cash::where('userID','=',$victim->id)->first()->update(['bank' => $cashRefreshVictim - $cashPayoutHusband,]); // delete 5%

                    $cashPayoutKillerBank = Cash::where('userID','=',$victim->id)->first()->bank; // Bank Victim
                    $cashPayoutKillerCash = Cash::where('userID','=',$victim->id)->first()->cash; // Cash Victim
                    $cashKiller = Cash::where('userID','=',Auth::id())->first()->cash;

                    Cash::where('userID','=',Auth::id())->update(['cash' => $cashKiller + ($cashPayoutKillerBank + $cashPayoutKillerCash) ]);
                    Cash::where('userID','=',$victim->id)->first()->update(['cash' => 0, 'bank' => 0]); // delete all money
                }
                else{
                    $cashPayoutKillerBank = Cash::where('userID','=',$victim->id)->first()->bank; // Bank Victim
                    $cashPayoutKillerCash = Cash::where('userID','=',$victim->id)->first()->cash; // Cash Victim
                    $cashKiller = Cash::where('userID','=',Auth::id())->first()->cash;

                    Cash::where('userID','=',Auth::id())->first()->update(['cash' => $cashKiller + ($cashPayoutKillerBank + $cashPayoutKillerCash)]);
                    Cash::where('userID','=',$victim->id)->first()->update(['cash' => 0, 'bank' => 0]); // delete all money
                }
            } // KILL
            else{
                Kill::create([
                    'uniqID' => $uniqID,
                    'userID' => Auth::id(),
                    'playerID' => $victim->id,
                    'bullets' => $data['bullets'],
                    'backfire' => $backfire,
                    'userRank' => $killer->getCurrentRank->rankID,
                    'playerRank' => $victim->getCurrentRank->rankID,
                    'user_familyID' => $killer->family->id,
                    'player_familyID' => $victim->family->id,
                    'userPercent' => $killer->getCurrentRank->adv,
                    'playerPercent' => $victim->getCurrentRank->adv,
                    'playerCash' => $victim->cash->cash,
                    'playerBank' => $victim->cash->bank,
                    'active' => false,
                    'success' => false,
                    'weapon' => $killer->defense->weapon,
                    'userDef' => $killer->defense->totalDefense,
                    'playerDef' => $victim->defense->totalDefense,
                    'message' => $killMessage,
                ]);
                Profile::find(Auth::id())->update([
                    'bullets' => $killer->profile->bullets - $data['bullets'],
                ]); // Update Profile Killer

                $RemainingHealth = floor($data['bullets'] / $CheckNeededBullets); // Calc remaining health victim

                if($RemainingHealth < 0){
                    Profile::find($victim->id)->update([
                        'health' => 0,
                    ]); // Update Health Victim SAFE FALLBACK
                } else {
                    Profile::find($victim->id)->update([
                        'health' => $RemainingHealth,
                    ]); // Update Health Victim
                }


            } // NO KILL

        }
        function backfireKiller($uniqID,$victim,$killer,$halfDefense,$halfDefenseBulletBackfire,$fullDefenseBulletBackfire,$backfireBullets){
            $killerHealth = $killer->profile->health; // get killer health
            $killerDefense = $killer->defense->totalDefense; // 75 defense 1/2 bullets , all above is full bullets
            $CheckNeededBulletsBF = CheckBeforeKillBF($killerHealth,$killerDefense,$halfDefense,$fullDefenseBulletBackfire,$halfDefenseBulletBackfire);

            if($victim->profile->bullets >= $CheckNeededBulletsBF ){
                if($backfireBullets >= $CheckNeededBulletsBF){
                    BackfireKill::create([
                        'uniqID' => $uniqID,
                        'userID' => $victim->id,
                        'playerID' => Auth::id(),
                        'bullets' => $backfireBullets,
                        'userRank' => $victim->getCurrentRank->rankID,
                        'playerRank' => $killer->getCurrentRank->rankID,
                        'userPercent' => $victim->getCurrentRank->adv,
                        'playerPercent' => $killer->getCurrentRank->adv,
                        'playerCash' => $killer->cash->cash,
                        'playerBank' => $killer->cash->bank,
                        'active' => true,
                        'success' => true,
                        'weapon' => $victim->defense->weapon,
                        'userDef' => $victim->defense->totalDefense,
                        'playerDef' => $killer->defense->totalDefense,
                    ]); // Create BFKill

                    $getRep = Ranks::where('id','=',$killer->getCurrentRank->rankID)->first('bfRep')->bfRep; // Search Rep for bfKill
                    $getPoints = Ranks::where('id','=',$killer->getCurrentRank->rankID)->first('killPoints')->killPoints; // Search killPoints for bfKill

                    Profile::find($victim->id)->update([
                        'bfKills' => $victim->profile->bfKills + 1,
                        'bfKillPoints' => $victim->profile->bfKillPoints + $getPoints,
                        'rep' => $victim->profile->rep + $getRep,
                        'bullets' => $victim->profile->bullets - $backfireBullets,
                    ]); // Update Profile Victim

                    $RemainingHealth = 100 - floor($backfireBullets / $CheckNeededBulletsBF * 100); // Calc remaining health Killer

                    if($RemainingHealth < 0){
                        Profile::find($killer->id)->update([
                            'health' => 0,
                        ]); // Update Profile Killer SAFE FALLBACK
                    } else{
                        Profile::find($killer->id)->update([
                            'health' => $RemainingHealth,
                        ]); // Update Profile Killer
                    }

                    Cash::where('userID','=',Auth::id())->first()->update([
                        'cash' => 0,
                        'bank' => 0
                    ]); // delete all money of killer

                    Rank::where([
                        'userID' => $killer->id,
                        'active' => 1
                    ])->update(['active' => 0]); // set old rank for Killer non-active

                    Rank::Create([
                        'userID' => $killer->id,
                        'rankID' => 1,
                        'adv' => 0,
                        'active' => 1
                    ]); // set new rank for Killer

                    $races = Stats::where(['userID' => $killer->id, 'active' => 1])->first('races')->races;

                    Stats::where([
                        'userID' => $killer->id,
                        'active' => 1
                    ])->update(['active' => 0]); // set old stats for Victim non-active

                    Stats::Create([
                        'userID' => $killer->id,
                        'crime' => 0,
                        'car' => 0,
                        'hostage' => 0,
                        'races' => $races,
                        'active' => 1
                    ]); // set new stats for Victim
                } // KILL
                else {
                    BackfireKill::create([
                        'uniqID' => $uniqID,
                        'userID' => $victim->id,
                        'playerID' => Auth::id(),
                        'bullets' => $backfireBullets,
                        'userRank' => $victim->getCurrentRank->rankID,
                        'playerRank' => $killer->getCurrentRank->rankID,
                        'userPercent' => $victim->getCurrentRank->adv,
                        'playerPercent' => $killer->getCurrentRank->adv,
                        'playerCash' => $killer->cash->cash,
                        'playerBank' => $killer->cash->bank,
                        'active' => false,
                        'success' => false,
                        'weapon' => $victim->defense->weapon,
                        'userDef' => $victim->defense->totalDefense,
                        'playerDef' => $killer->defense->totalDefense,
                    ]);

                    Profile::find($victim->id)->update([
                        'bullets' => $victim->profile->bullets - $backfireBullets,
                    ]); // Update Profile Victim

                    $RemainingHealth = 100 - floor($backfireBullets / $CheckNeededBulletsBF * 100); // Calc remaining health Killer

                    Profile::find(Auth::id())->update([
                        'health' => $RemainingHealth,
                    ]); // Update Health Killer
                } // NO KILL but fired back
            } // if victim has enough bullets to bf // KILL
            else{
                BackfireKill::create([
                    'uniqID' => $uniqID,
                    'userID' => $victim->id,
                    'playerID' => Auth::id(),
                    'bullets' => $backfireBullets,
                    'userRank' => $victim->getCurrentRank->rankID,
                    'playerRank' => $killer->getCurrentRank->rankID,
                    'userPercent' => $victim->getCurrentRank->adv,
                    'playerPercent' => $killer->getCurrentRank->adv,
                    'playerCash' => $killer->cash->cash,
                    'playerBank' => $killer->cash->bank,
                    'active' => false,
                    'success' => false,
                    'weapon' => $victim->defense->weapon,
                    'userDef' => $victim->defense->totalDefense,
                    'playerDef' => $killer->defense->totalDefense,
                ]);
            } // NO KILL
        }


        $checkAdmin = Admin::where([
            'userID' => $victim->id,
            'adminRangID' => 1
        ])->first();


        if(!$checkAdmin){
            if($dets){
                if($victimRank <= $killerRank + 1) {
                    if($hiddenVictim == null || Carbon::now('Europe/Amsterdam') > Carbon::parse($hiddenVictim->endTime)){
                        if($hiddenKiller == null || Carbon::now('Europe/Amsterdam') > Carbon::parse($hiddenKiller->endTime)){
                            if(!$lastUnsuccessKiller){
                                if(!$lastUnsuccessVictim){
                                    if(!$request->input('boolCheck')){
                                        if($dets->detsCountry == $victim->country->countryID || $dets->detsCountry == 15){
                                            if($victim->country->countryID == $killer->country->countryID){
                                                if($victim->profile->health != 0){
                                                    $uniqID = uniqid();
                                                    $killerBullets = $killer->profile->bullets; // get killer bullets
                                                    $victimBullets = $victim->profile->bullets; // get victim bullets

                                                    if($killerBullets >= $data['bullets']){
                                                        if($victim->settings->backfireOption == 1){
                                                            killVictim($uniqID,$victim,$killer,$data, false,$halfDefense,$halfDefenseBullet,$fullDefenseBullet,$killMessage);
                                                            return redirect()->route('kill')->with('message', "You killed " . $victim->username);
                                                        } // Don't fire back - NO BACKFIRE

                                                        elseif ($victim->settings->backfireOption == 2){
                                                            $shotBulletsVictim = floor($data['bullets'] / 2);

                                                            if($victimBullets >= $shotBulletsVictim){
                                                                $bfBullets = $shotBulletsVictim;
                                                            }
                                                            else{
                                                                $bfBullets = $victimBullets;
                                                            }

                                                            killVictim($uniqID,$victim,$killer,$data, true ,$halfDefense,$halfDefenseBullet,$fullDefenseBullet,$killMessage);
                                                            backfireKiller($uniqID,$victim,$killer,$halfDefense,$halfDefenseBulletBackfire,$fullDefenseBulletBackfire,$bfBullets);

                                                            #return redirect()->route('kill')->with('message', "2: You killed " . $victim->username);
                                                        } // Half fire back

                                                        elseif ($victim->settings->backfireOption == 3){
                                                            $shotBulletsVictim = $data['bullets'];

                                                            if($victimBullets >= $shotBulletsVictim){
                                                                $bfBullets = $shotBulletsVictim;
                                                            }
                                                            else{
                                                                $bfBullets = $victimBullets;
                                                            }

                                                            killVictim($uniqID,$victim,$killer,$data,true,$halfDefense,$halfDefenseBullet,$fullDefenseBullet,$killMessage);
                                                            backfireKiller($uniqID,$victim,$killer,$halfDefense,$halfDefenseBulletBackfire,$fullDefenseBulletBackfire,$bfBullets);

                                                            #return redirect()->route('kill')->with('message', "3: You killed " . $victim->username);

                                                        } // Same fire back

                                                        elseif ($victim->settings->backfireOption == 4){
                                                            $shotBulletsVictim = $data['bullets'] * 2;

                                                            if($victimBullets >= $shotBulletsVictim){
                                                                $bfBullets = $shotBulletsVictim;
                                                            }
                                                            else{
                                                                $bfBullets = $victimBullets;
                                                            }

                                                            killVictim($uniqID,$victim,$killer,$data,true,$halfDefense,$halfDefenseBullet,$fullDefenseBullet,$killMessage);
                                                            backfireKiller($uniqID,$victim,$killer,$halfDefense,$halfDefenseBulletBackfire,$fullDefenseBulletBackfire,$bfBullets);

                                                            #return redirect()->route('kill')->with('message', "4: You killed " . $victim->username);

                                                        } // Double fire back

                                                        else{
                                                            return redirect()->route('kill')->with('error', "ERROR: K3");
                                                        } // Error Fire back :(

                                                    } // owned bullets must be equal or more than expected shooting bullets
                                                    else{
                                                        return redirect()->route('kill')->with('error', "Not enough bullets in your pocket");
                                                    } // owned bullets is lower than input bullet
                                                }
                                                else{
                                                    return redirect()->route('kill')->with('error', "This player is already dead");
                                                }

                                            }
                                            else{
                                                return redirect()->route('kill')->with('error', "You are not in the same country");
                                            }
                                        }
                                        else{
                                            return redirect()->route('kill')->with('error', "Your Dets didn't searched this country");
                                        }
                                    }
                                    else{
                                        return redirect()->route('kill')->with('error', "You clicked only check mate");
                                    }

                                } // Victim isn't shot in the last 20 minutes
                                else{
                                    return redirect()->route('kill')->with('error', "Wait u fack victim is in the hospital");
                                } // Victim is already shot in the last 20 minutes
                            } // Killer hasn't shot in the last ... minutes
                            else{
                                return redirect()->route('kill')->with('error', "Wait u fack");
                            } // Already shot in the last ... minutes
                        } // if Killer is not hidden
                        else{
                            return redirect()->route('kill')->with('error', "You are hidden u fack");
                        } // Killer is hidden
                    } // if Victim is not hidden
                    else{
                        return redirect()->route('kill')->with('error', "Victim is hidden");
                    } // if Victim is hidden
                } // if Victimrank is not higher than Killerrank + 1 rank
                else{
                    return redirect()->route('kill')->with('error', "This Victim is out of your league");
                } // if Victimrank is higher than Killerrank + 1 ranks
            }
            else {
                return redirect()->route('kill')->with('error', "Where is Sherlock?");
            }
        }
        else{
            return redirect()->route('kill')->with('error', "You can't kill the god" );
        }

    }

    public function dead(){
        $getLastKill = Kill::where([
            ['playerID', '=', Auth::id()],
            ['active', '=', 1],
        ])->first();

        $getLastBackfireKill = BackfireKill::where([
            ['playerID', '=', Auth::id()],
            ['active', '=', 1],
        ])->first();

        if($getLastKill || $getLastBackfireKill){
            if($getLastKill){
                $dead = $getLastKill;
            }
            else{
                $dead = $getLastBackfireKill;
            }
            return view('kill.dead', compact('dead'));
        }
        else{
            return redirect('/')->with('error',"You aren't dead :) nice try");
        }
    }

    public function test(){


        $test = Marriage::with('player1Details', 'player2Details')
            ->where('accepted','=', 1)
            ->where(function ($query){
                $query->orWhere([
                    'player1' => Auth::id(),
                    'player2' => Auth::id()
                ]);
            })->first();

        ddd($test);
    }
}
