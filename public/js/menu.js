let leftBar = 0;
let rightBar = 0;

function openLeftNav() {
    const leftSideBar = document.getElementById("leftSideBar");
    leftSideBar.classList.add('w-full');
    leftSideBar.classList.add('transition')
    leftSideBar.classList.remove('hidden')

    document.getElementById("icon-left").classList.remove('fa-bars');
    document.getElementById("icon-left").classList.add('fa-times');

    document.getElementById("content").classList.add('hidden');
    document.getElementById("leftMenu").setAttribute('onclick','closeLeftNav()');

    leftBar = 1;

    if(rightBar === 1){
        const rightSideBar = document.getElementById("rightSideBar");
        rightSideBar.classList.remove("w-full")

        document.getElementById("icon-right").classList.remove('fa-times');
        document.getElementById("icon-right").classList.add('fa-bars');

        document.getElementById("rightSideBar").classList.add('hidden');
        document.getElementById("rightMenu").setAttribute('onclick','openRightNav()')
    }
}

function closeLeftNav() {
    const leftSideBar = document.getElementById("leftSideBar");

    leftSideBar.classList.remove("w-full")
    leftSideBar.classList.add('hidden')

    document.getElementById("icon-left").classList.remove('fa-times');
    document.getElementById("icon-left").classList.add('fa-bars');

    document.getElementById("content").classList.remove('hidden');
    document.getElementById("leftMenu").setAttribute('onclick','openLeftNav()')

    leftBar = 0;
}

function openRightNav() {
    const rightSideBar = document.getElementById("rightSideBar");

    rightSideBar.classList.add("w-full")
    rightSideBar.classList.remove('hidden')

    document.getElementById("icon-right").classList.remove('fa-bars');
    document.getElementById("icon-right").classList.add('fa-times');

    document.getElementById("content").classList.add('hidden');
    document.getElementById("rightMenu").setAttribute('onclick','closeRightNav()')

    rightBar = 1;

    if(leftBar === 1){
        const leftSideBar = document.getElementById("leftSideBar");

        leftSideBar.classList.remove("w-full")

        document.getElementById("icon-left").classList.remove('fa-times');
        document.getElementById("icon-left").classList.add('fa-bars');

        document.getElementById("leftSideBar").classList.add('hidden');
        document.getElementById("leftMenu").setAttribute('onclick','openLeftNav()')
    }
}

function closeRightNav() {
    const rightSideBar = document.getElementById("rightSideBar");
    rightSideBar.classList.remove("w-full")
    rightSideBar.classList.add('hidden')

    document.getElementById("icon-right").classList.remove('fa-times');
    document.getElementById("icon-right").classList.add('fa-bars');

    document.getElementById("content").classList.remove('hidden');
    document.getElementById("rightMenu").setAttribute('onclick','openRightNav()')

    rightBar = 0;

}
