<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompletedRace extends Model
{
    use HasFactory;

    protected $table = "races_completed";

    protected $fillable = [
        'id',
        'racer1_ID',
        'racer2_ID',
        'time',
        'bet',
        'winner'
    ];

    public function racer1()
    {
        return $this->belongsTo(User::class, 'racer1_ID');
    }

    public function racer2()
    {
        return $this->belongsTo(User::class, 'racer2_ID');
    }

}
