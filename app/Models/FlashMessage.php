<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlashMessage extends Model
{
    use HasFactory;

    protected $table = 'flashmessages';

    protected $fillable = [
        'userID',
        'message',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }
}
