<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FamilyHeist extends Model
{
    use HasFactory;

    protected $table = 'family_heists';

    protected $fillable = [
        'heist_id',
        'familyID',
        'player_1',
        'player_2',
        'player_3',
        'player_4',
        'player_5',
        'player_1_role',
        'player_2_role',
        'player_3_role',
        'player_4_role',
        'player_5_role',
        'location',
        'money',
        'ratio',
        'moneyLocation',
        'active',
        'completed',
    ];

    public function player_1_name()
    {
        return $this->belongsTo(User::class, 'player_1');
    }

    public function player_2_name()
    {
        return $this->belongsTo(User::class, 'player_2');
    }

    public function player_3_name()
    {
        return $this->belongsTo(User::class, 'player_3');
    }

    public function player_4_name()
    {
        return $this->belongsTo(User::class, 'player_4');
    }

    public function player_5_name()
    {
        return $this->belongsTo(User::class, 'player_5');
    }

    public function familyName()
    {
        return $this->belongsTo(Family::class, 'familyID');
    }

    public function heistRole1()
    {
        return $this->belongsTo(FamilyHeistRole::class, 'player_1_role');
    }

    public function heistRole2()
    {
        return $this->belongsTo(FamilyHeistRole::class, 'player_2_role');
    }

    public function heistRole3()
    {
        return $this->belongsTo(FamilyHeistRole::class, 'player_3_role');
    }

    public function heistRole4()
    {
        return $this->belongsTo(FamilyHeistRole::class, 'player_4_role');
    }

    public function heistRole5()
    {
        return $this->belongsTo(FamilyHeistRole::class, 'player_5_role');
    }

    public function heistLocation()
    {
        return $this->belongsTo(FamilyHeistLocation::class, 'location');
    }

    public function heistMoneyLocation()
    {
        return $this->belongsTo(FamilyHeistMoneyLocations::class, 'moneyLocation');
    }


}
