<?php

namespace App\Jobs;

use App\Models\Countries;
use App\Models\Country;
use App\Models\Detective;
use App\Models\SystemMessage;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessDets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $detective;

    public function __construct(Detective $detective)
    {
        $this->detective = $detective;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currentDets = Detective::find($this->detective->id);

        if($currentDets){
            if($currentDets->active == 1)
                $player = User::with('country')->findOrFail($this->detective->playerID);
                $countries = Countries::all()->count();
                $countCountries = $countries + 1;

                if($currentDets->detsCountry == $countCountries || $currentDets->detsCountry == $player->country->countryID ){
                    SystemMessage::create([
                        'senderID' => 1,
                        'recipientID' => $currentDets->userID,
                        'subject' => 'Detectives',
                        'body' => "The detectives found " . $player->name . " in " . $player->country->country->name ,
                    ]);
                    $currentDets->found = 1;
                    $currentDets->countryID = $player->country->countryID;
                    $currentDets->updated_at = Carbon::now('Europe/Amsterdam');
                    $currentDets->save();
                }
                else{
                    $countryName = Countries::find($currentDets->detsCountry);

                    SystemMessage::create([
                        'senderID' => 1,
                        'recipientID' => $currentDets->userID,
                        'subject' => 'Detectives',
                        'body' => "The detectives didn't found " . $player->name . " in " . $countryName->name ,
                    ]);

                    $currentDets->found = 0;
                    $currentDets->active = 0;
                    $currentDets->updated_at = Carbon::now('Europe/Amsterdam');
                    $currentDets->save();
                }
        }
    }
}
