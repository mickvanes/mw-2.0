<?php

namespace App\Http\Controllers;

use App\Models\Collection;
use App\Models\Collections;
use App\Models\CollectionCards;
use App\Models\UserCards;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class CardController extends Controller
{
    public function index(){
        // the card collection page to get random cards

        $userCards = UserCards::where([
            'userID' => Auth::id(),
            'turnedIn' => 0,
            'hasSold' => 0
        ])->with('collectionDetails','card')->get();

        ddd($userCards);
    }

    public function turnedCards(){
        // collection page to see your collection

        $allCards = CollectionCards::all();
        $userCollection = UserCards::where([
                'userID' => Auth::id(),
                'hasSold' => 0
        ])->with('collectionDetails','card')->get();

        $userCards = UserCards::all();

        $previousCardID = NULL;

        foreach($userCards as $card){
            $countCard = 0;
            if($card->cardID == $previousCardID){
                print($card->cardID);
            }
            $countCard++;
            $previousCardID = $card->cardID;
            $card->count = $countCard;
        }


        ddd($userCards);


////
//        foreach ($allCollections as $collections){
//             $data = UserCards::where([
//                'userID' => Auth::id(),
//                'cardID' => $collections->cardID,
//                'collection' => $collections->collectionID
//            ])->get();
//
//            if($data->turnedIn){
//                $collections->turnedIn = True;
//            }
//            else{
//                $collections->turnedIn = False;
//            }
//
//
//            if($data) {
//                if($data->turnedIn == 0){
//                    $collections->turnedIn = False;
//                    $collections->owned = True;
//                }
//                elseif($data->collection == NULL){
//                    $collections->owned = True;
//                }
//                else{
//                    $collections->turnedIn = True;
//                    $collections->owned = True;
//                }
//            }
//            else{
//                $collections->owned = False;
//                $collections->turnedIn = False;
//            }
//        }
//
//        return view('cards.turned', compact('allCollections'));
    }

    public function soldCards(){
        // sold cards

//        $userCards = UserCards::where([
//            'userID' => Auth::id(),
//            'turnedIn' => 0,
//            'hasSold' => 1
//        ])->with('collection','card')->get();
//
//        ddd($userCards);

        $data = md5(UserCards::with('collectionDetails','card')->where('userID',Auth::id())->get());
        return $data;
    }
}
