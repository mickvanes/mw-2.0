<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HospitalBlood extends Model
{
    use HasFactory;

    protected $table = 'hospital_blood';

    protected $fillable = [
        'businessID',
        'ownerID',
        'costs',
        'amount',
    ];
}
