@extends('layouts.default')

@section('header')
    LOGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP POWER</div>
                            <span><b>Player</b></span>
                            <span><b>Power</b></span>
                            @foreach ($stats->sortByDesc('power') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{ $player->username }}</span></a>
                                <span>{{ number_format($player->power) }}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP REPUTATION</div>
                            <span><b>Player</b></span>
                            <span><b>Rep</b></span>
                            @foreach($stats->sortByDesc('rep') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{ $player->username }}</span></a>
                                <span>{{ number_format($player->rep) }}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP CASH</div>
                            <span><b>Player</b></span>
                            <span><b>Cash</b></span>
                            @foreach($stats->sortByDesc('funds') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>&euro; {{number_format($player->funds)}}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP CLICKS</div>
                            <span><b>Player</b></span>
                            <span><b>Clicks</b></span>
                            @foreach($stats->sortByDesc('clicks') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>{{ number_format($player->clicks) }}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP KILLS</div>
                            <span><b>Player</b></span>
                            <span><b>Kills</b></span>
                            @foreach($stats->sortByDesc('kills') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>{{ number_format($player->kills) }}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP KILL POINTS</div>
                            <span><b>Player</b></span>
                            <span><b>Kill Points</b></span>
                            @foreach($stats->sortByDesc('killPoints') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>{{ number_format($player->killPoints) }}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP BACKFIRE KILLS</div>
                            <span><b>Player</b></span>
                            <span><b>Backfire Kills</b></span>
                            @foreach($stats->sortByDesc('bfKills') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>{{ number_format($player->bfKills) }}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP BACKFIRE KILL POINTS</div>
                            <span><b>Player</b></span>
                            <span><b>Backfire Kill Points</b></span>
                            @foreach($stats->sortByDesc('bfKillPoints') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>{{ number_format($player->bfKillPoints) }}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP BURSTS</div>
                            <span><b>Player</b></span>
                            <span><b>Bursts</b></span>
                            @foreach($stats->sortByDesc('bursts') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>{{ number_format($player->bursts) }}</span>
                            @endforeach
                        </div>

                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP HONOR</div>
                            <span><b>Player</b></span>
                            <span><b>Honor</b></span>
                            @foreach($stats->sortByDesc('honor') as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>{{ number_format($player->honor) }}</span>
                            @endforeach
                        </div>
                        <div class="grid grid-cols-2">
                            <div class="col-span-2 text-center">TOP RANKS</div>
                            <span><b>Player</b></span>
                            <span><b>Rank (%)</b></span>
                            @foreach($highestRanks as $player)
                                <a href="{{ route('player.show',$player->username) }}"><span>{{$player->username}}</span></a>
                                <span>{{ $player->rank }} ({{ $player->adv }}%)</span>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

