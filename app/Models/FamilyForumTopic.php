<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyForumTopic extends Model
{
    use HasFactory;

    protected $table = 'familyforum_topics';

    protected $fillable = [
        'userID',
        'familyID',
        'categoryID',
        'crew',
        'name',
        'content'
    ];

    public function category()
    {
        return $this->belongsTo(FamilyForumCategory::class, 'categoryID','id');
    }

    public function reactions()
    {
        return $this->hasMany(FamilyForumReaction::class, 'topicID')->orderBy('created_at');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function family()
    {
        return $this->belongsTo(Family::class, 'familyID');
    }
}
