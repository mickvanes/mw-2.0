<div class="grid grid-cols-5">
    <div>
        <label>
            <a href="{{route('dashboard')}}">
                {{ __('Player') }} {{$userName}}
            </a>
        </label>
    </div>
    <a href="{{route('bank')}}"><div>Cash: &euro;{{ number_format($userCash, 0, ',', '.') }}</div></a>
    <a href="{{route('bank')}}"><div>Bank: &euro;{{ number_format($userBank, 0, ',', '.') }}</div></a>
    <div></div>
    <div>
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <x-nav-link class="float-right" :href="route('logout')"
                        onclick="event.preventDefault();
                        this.closest('form').submit();">
                {{ __('Log Out') }}
            </x-nav-link>
        </form>
    </div>
</div>
