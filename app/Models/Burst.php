<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Burst extends Model
{
    use HasFactory;

    protected $table = 'user_burst';

    protected $fillable = [
        'id',
        'prisoner',
        'burster',
        'buyout',
        'buyOutCash',
    ];

    public function prisoner()
    {
        return $this->belongsTo(User::class, 'prisoner');
    }

    public function burster()
    {
        return $this->belongsTo(User::class, 'burster');
    }
}
