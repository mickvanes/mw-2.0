<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
    use HasFactory;

    protected $table = 'user_stats';

    protected $fillable = [
        'id',
        'userID',
        'crime',
        'car',
        'hostage',
        'races',
        'active',
        'updated_at',
        'created_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }
}
