@extends('layouts.default')

@section('header')
    SHOP - AIRPLANES
@stop

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success mb-4">
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                 role="alert">
                <strong class="font-bold">Shop</strong>
                <span class="block sm:inline">{{ session()->pull('message') }}</span>
            </div>
        </div><br>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger mb-4">
            <div class="alertBox px-4 py-3 rounded relative"
                 role="alert">
                <strong class="font-bold">Shop</strong>
                <span class="block sm:inline">{{ session()->pull('error') }}</span>
            </div>
        </div><br>
    @endif

    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
                                <strong class="font-bold">Fly</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger mb-4">
                            <div class="alertBox px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Airport</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif
                        @if(!$defenses->airplane)
                            <div class="grid grid-cols-1 md:grid-cols-2">
                            @foreach($airplanes as $airplane)
                                <div class="p-2">
                                    <div class="grid grid-cols-1 mb-4 w-75">
                                        <img src="{{ $airplane->image }}" class="w-75">
                                        <div class="grid grid-cols-2 mt-2 text-center">
                                            <span>{{ $airplane->name }}</span>
                                            <span>{{ $airplane->time }} minutes to recharge</span>
                                            <span>Costs: &euro;{{ number_format($airplane->costs, 0, ',', '.')  }}</span>
                                            <span>Per Flight: &euro;{{ number_format($airplane->flightCosts, 0, ',', '.') }}</span>
                                            <span class="col-span-2"><a href="{{ route('shop.buyAirplane', $airplane->id) }}">Buy this airplane</a></span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                    </div>
                        @else
                            <div class="p-2">
                                <div class="grid grid-cols-1 mb-4 md:w-6/12 md:w-9/12 mx-auto">
                                    <span class="text-center font-semibold text-xl my-2 ">Your airplane</span>
                                    <img src="{{ $defenses->plane->image }}" class="w-25">
                                    <div class="grid grid-cols-2 mt-2 text-center">
                                        <span>{{ $defenses->plane->name }}</span>
                                        <span>{{ $defenses->plane->time }} minutes to recharge</span>
                                        <span>Costs: &euro;{{ number_format($defenses->plane->costs, 0, ',', '.')  }}</span>
                                        <span>Per Flight: &euro;{{ number_format($defenses->plane->flightCosts, 0, ',', '.')  }}</span>
                                    </div>
                                    <span class="mt-4 text-center"><a href="{{ route('shop.sellAirplane') }}">Sell your plane for &euro;{{ number_format($defenses->plane->costs * 0.50, 0, ',', '.') }}</a></span>
                                </div>
                            </div>
                        @endif

                </div>
            </div>
        </div>
    </div>
@stop
