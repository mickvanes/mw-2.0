<?php

namespace App\Http\Controllers;

use App\Models\BackfireKill;
use App\Models\Business;
use App\Models\BusinessBank;
use App\Models\BusinessTransaction;
use App\Models\Cash;
use App\Models\Country;
use App\Models\Hospital;
use App\Models\HospitalBlood;
use App\Models\Kill;
use App\Models\Profile;
use App\Models\Rule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HospitalController extends Controller
{
    public function index(){
        $health = Profile::find(Auth::id())->health;
        $country = Country::where('userID','=', Auth::id())->first();
        $businessID = Business::where([
            ['countryID','=', $country->countryID],
            ['typeID','=',2]
        ])->first()->id;

        $bloodBags = Hospital::where('businessID','=',$businessID)->first()->bloodRemaining;

        if($health >= 1){
            $bloodUnitPrice = Hospital::where('businessID','=',$businessID)->first()->price;
        }
        elseif ($health == 0){
            return redirect()->route('deadHospital');
        }

        $title = "Hospital";

        return view('hospital.index', compact('health','title','bloodUnitPrice','bloodBags'));
    }

    public function buyHealth(Request $request){
        $health = Profile::find(Auth::id())->health;
        $healthMin = 100 - $health ;

        $request->validate([
            'units' => 'required|numeric',
        ]);

        $country = Country::where('userID','=', Auth::id())->first();
        $business = Business::where([
            ['countryID','=', $country->countryID],
            ['typeID','=',2]
            ])->first();

        $bloodBags = Hospital::where('businessID','=',$business->id)->first()->bloodRemaining;
        $bloodUnitPrice = Hospital::where('businessID','=',$business->id)->first()->price;

        if($request->units <= $healthMin){
            $price = $request->units * $bloodUnitPrice;

            if(Auth::user()->cash->cash >= $price && $health >= 1){
                if($bloodBags >= $request->units) {
                    $bank = BusinessBank::where('id', $business->id)->first()->bank;

                    Cash::where('userID', Auth::id())->update([
                        'cash' => Auth::user()->cash->cash - $price
                    ]);

                    BusinessBank::where('id', $business->id)
                        ->update(['bank' => $bank + $price]);

                    Hospital::where('businessID', $business->id)->update([
                        'bloodRemaining' => $bloodBags - $request->units
                    ]);

                    BusinessTransaction::create([
                        'businessID' => $business->id,
                        'transactionType' => 2,
                        'cash' => $price,
                        'owner' => $business->ownerID
                    ]);

                    Profile::where('id', Auth::id())->update([
                        'health' => Auth::user()->profile->health + $request->units,
                    ]);

                    if($request->units > 1){
                        return redirect()->route('hospital')->with('message', "Bought ". $request->units ." health units");
                    }
                    else{
                        return redirect()->route('hospital')->with('message', "Bought ". $request->units ." health unit");
                    }
                }
                else{
                    return redirect()->route('hospital')->with('error', "This Hospital has no blood bags in , request the owner to buy blood bags or fly to another country");
                }
            }
            else{
                return redirect()->route('hospital')->with('error', "Not enough cash");
            }
        }
        else{
            return redirect()->route('hospital')->with('error', "You can't buy more health than you needed");
        }



    }

    public function owner(){
        $country = Country::where('userID','=', Auth::id())->first()->countryID;
        $business = Business::where([ ['ownerID', '=', Auth::id()] , ['countryID','=', $country] , ['typeID','=', 2]])->first();

        if($business) {
            $businessID = $business->id;

            $now = Carbon::now();

            $start = $now->copy()->startOfWeek();
            $end = $now->copy()->endOfWeek();

            $startLastWeek = $now->copy()->startOfWeek()->subweek();
            $endLastWeek = $now->copy()->endOfWeek()->subweek();

            $bank = BusinessBank::where([
                ['id','=',$businessID],
                ['ownerID','=', Auth::id()]
            ])->first()->bank;

            $revenue = BusinessTransaction::where([
                ['transactionType','=',2],
                ['owner','=',Auth::id()],
                ['businessID','=',$businessID],
            ])->whereBetween('created_at', [$start, $end])->sum('cash');

            $revenueLastWeek = BusinessTransaction::where([
                ['transactionType','=',2],
                ['owner','=',Auth::id()],
                ['businessID','=',$businessID],
            ])->whereBetween('created_at', [$startLastWeek, $endLastWeek])->sum('cash');

            $hospitalBloodCosts = Rule::latest()->first()->hospitalBloodCosts;

            $blood = Hospital::where('businessID','=',$businessID)->first()->blood;
            $remainingBlood = Hospital::where('businessID','=',$businessID)->first()->bloodRemaining;

            $userCash = Auth::user()->cash->cash;
            $userBank = Auth::user()->cash->bank;

            $minPrice = Rule::latest()->first()->minBloodPrice;
            $maxPrice = Rule::latest()->first()->maxBloodPrice;

            $sellingPrice = Hospital::where('businessID','=',$businessID)->first()->price;

            return view('hospital.manage',compact('businessID','bank','revenue','revenueLastWeek',
                'userCash','userBank','blood','remainingBlood','hospitalBloodCosts','minPrice','maxPrice','sellingPrice'));
        }

    }

    public function changePrice(Request $request){
        $request->validate([
            'price' => 'required|numeric',
        ]);

        $country = Country::where('userID','=', Auth::id())->first()->countryID;
        $business = Business::where([ ['ownerID', '=', Auth::id()] , ['countryID','=', $country] , ['typeID','=', 2]])->first();

        $minPrice = Rule::latest()->first()->minBloodPrice;
        $maxPrice = Rule::latest()->first()->maxBloodPrice;

        if($request->price >= $minPrice && $request->price <= $maxPrice){
            Hospital::where('businessID',$business->id)->update([
                'price' => $request->price
            ]);

            return redirect()->route('hospital.owner', $business->id)->with('message','The blood bags price has been updated to €'
                . number_format($request->price , 0, ',', '.'));
        }
        else{
            return redirect()->route('hospital.owner', $business->id)->with('error', "The blood bags price is not between €"
                . number_format($minPrice , 0, ',', '.') . " and €"
                . number_format($maxPrice , 0, ',', '.') );
        }


    }

    public function buyBlood(Request $request){
        $request->validate([
            'bloodBags' => 'required|numeric|min:300|max:1000',
        ]);

        $bloodPrice = Rule::latest()->first()->hospitalBloodCosts;
        $costs = $request->bloodBags * $bloodPrice;

        $country = Country::where('userID','=', Auth::id())->first()->countryID;
        $business = Business::where([ ['ownerID', '=', Auth::id()] , ['countryID','=', $country] , ['typeID','=', 2]])->first();

        if($business){
            $bank = BusinessBank::where([
                ['id','=',$business->id],
                ['ownerID','=', Auth::id()]
            ])->first()->bank;

            if($bank >= $costs){
                BusinessBank::where([
                    ['id','=',$business->id],
                    ['ownerID','=', Auth::id()]
                ])->update([
                    'bank' => $bank - $costs,
                    'updated_at' => Carbon::now()
                ]);

                BusinessTransaction::create([
                    'businessID' => $business->id,
                    'transactionType' => 4,
                    'cash' => $costs,
                    'owner' => Auth::id()
                ]);

                $bloodRemaining = Hospital::where([
                    ['businessID','=',$business->id],
                    ['ownerID','=', Auth::id()]
                ])->first()->bloodRemaining;

                DB::table('hospital')->where([
                    ['businessID','=',$business->id],
                    ['ownerID','=', Auth::id()]
                ])->update([
                    'bloodRemaining' => $bloodRemaining + $request->bloodBags,
                    'updated_at' => Carbon::now()
                ]);

                HospitalBlood::create([
                    'businessID' => $business->id,
                    'ownerID' => Auth::id(),
                    'costs' => $costs,
                    'amount' => $request->bloodBags
                ]);

                return redirect()->route('hospital.owner', $business->id)->with('message', "Bought ". $request->bloodBags ." blood bags.");
            }
            else{
                return redirect()->route('hospital.owner', $business->id)->with('error', "Not enough money on your business bank");
            }
        }
        return redirect()->route('dashboard')->with('error','You are not the owner of this Hospital or an error occurred');
    }

    public function dead(){
        $checkIfShooted = Kill::where(['playerID' => Auth::id(), 'active' => 1])->first();
        $checkIfBfShooted = BackfireKill::where(['playerID' => Auth::id(), 'active' => 1])->first();
        $health = Profile::where('id','=',Auth::id())->first()->health;

        if($checkIfShooted){
            Profile::where('id','=',Auth::id())->first()->update(['health' => 100]);
            $checkIfShooted->update(['active' => 0]);
            return redirect()->route('hospital')->with('message', "The sexy nurse made you alive, have fun baby");
        }
        if($checkIfBfShooted){
            Profile::where('id','=',Auth::id())->first()->update(['health' => 100]);
            $checkIfBfShooted->update(['active' => 0]);
            return redirect()->route('hospital')->with('message', "The ugly nurse made you alive, have fun baby");
        }
        if($health == 0){
            Profile::where('id','=',Auth::id())->first()->update(['health' => 100]);
            return redirect()->route('hospital')->with('message', "Don't bug around u facker");
        }
    }

}
