
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="grid grid-cols-1 gap-2">
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg" style="max-width: 904px;">
                    <div class="mx-auto" style="max-width: 904px; width: 100%">
                        <img  src="{{ asset('images/jail.jpg') }}">
                        <div class="w-3/4 mx-auto text-center mb-4 mt-4 grid grid-cols-1">
                            <span>Prison Country: {{ $country }} , Prison Owner: {{ $prisonOwner }}<br><br></span>

                            @if($owner)
                                <a href="{{ route('prison.owner', $businessID ) }}">Go to your business</a>
                            @endif

                            <span class="w-full">Buying someone out costs &euro;{{ number_format( $costsPerSec , 0, ',', '.') }} times the number of seconds remaining.</span>
                            <span>Burst Rank: {{ $burstRank->rank->name }} ({{ $burstRank->burstAdv }}%)</span>
                        </div>
                    </div>
                  </div>
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg" style="max-width: 904px;">
                    <div class="p-6 bg-white border-b border-gray-200">

                        @if(session()->has('message'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Prison</strong>
                                    <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                </div>
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Prison</strong>
                                    <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                </div>
                            </div>
                        @endif
                        <script src="{{ URL::asset('js/jail.js') }}"></script>
                        <table class="table-fixed w-full mt-4">
                            <thead>
                            <tr>
                                <th class="text-left">Name</th>
                                <th class="text-left">Time</th>
                                <th class="hidden md:block text-right md:text-left">Reason</th>
                                <th class="text-left">Costs</th>
                                <th class="text-left">Burst out</th>
                            </tr>
                            </thead>
                        @foreach($jails as $jail)
                            <tr>
                                <td><a href="{{ route('player.show', $jail->user->name) }}">{{ $jail->user->name }}</a></td>
                                <td id="{{ strtolower($jail->user->name) }}"></td>
                                <td class="hidden md:block">{{ $jail->reason }}</td>
                                <td><a href="{{ route('buyOut', $jail->id) }}"><span id="{{ strtolower($jail->user->name) }}_costs"></span></a></td>
                                <td><a href="{{ route('burst', $jail->id) }}"> Burst </a></td>
                                <script type="text/javascript">countdown('<?php echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',  $jail->endTime)->format('d/m/Y H:i:s') ?>','{{ strtolower($jail->user->name) }}','{{ $costsPerSec }}');</script>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
