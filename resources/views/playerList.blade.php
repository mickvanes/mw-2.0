@extends('layouts.default')

@section('header')
    DASHBOARD
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="pl-2 pr-2 pt-6 pb-6 md:p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                        <table class="table-fixed w-full ml-6 mr-6 md:p-0">
                            <thead>
                            <tr>
                                <th class="text-left">Name</th>
                                <th class="text-left">Cash</th>
                                <th class="text-left">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($players as $player)
                                <tr>
                                    <td class="text-left"><a href="{{route('player.show', $player->username)  }}"> {{$player->username}}</a></td>
                                    <td class="text-left">&euro; {{ number_format($player->cash->cash, 0, ',', '.') }}</td>
                                    <td class="text-left">
                                        @if(Cache::has('user-is-online-' . $player->id))
                                            <svg width="15" height="15">
                                                <rect width="15" fill="green" height="15" rx="50"/>
                                            </svg>
                                        @else
                                            <svg width="15" height="15">
                                                <rect width="15" fill="red" height="15" rx="50"/>
                                            </svg>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                </div>
            </div>
        </div>
    </div>
@stop
