<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckFamilyRang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  \Array $familyRangs
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$familyRangs)
    {
        if ( ! in_array($request->user()->getFamilyRang(), $familyRangs)) {
            return redirect('dashboard')->with('error','You are not authorized to access this resource.');
        }

        return $next($request);
    }
}
