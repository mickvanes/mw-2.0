function countdown(date,player,costsPerSeconds){
    tick(date,player,costsPerSeconds);
    setInterval(tick, 1000, date,player,costsPerSeconds);
}

function tick(date,player,costsPerSeconds) {
    function formatMoney(number) {
        return number.toLocaleString('en-US');
    }

    const now = new Date();

    const dateFull = date.split(" ");
    const time = dateFull[1].split(":");
    const dateSep = dateFull[0].split("/");

    const countdown = new Date(dateSep[2], dateSep[1] - 1, dateSep[0], time[0], time[1], time[2]);

    const distance = countdown - now;

    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById(player).innerHTML = minutes + "m " + seconds + "s";
    document.getElementById(player + "_costs").innerHTML = "&euro;" + formatMoney(Math.round(distance / 1000) * costsPerSeconds);
    if (days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0) {
        document.location.reload(true);
    }

}
