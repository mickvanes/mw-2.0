<?php

//Controllers
use App\Http\Controllers\AirportController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\DefaultController;
use App\Http\Controllers\DetectiveController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\InboxController;
use App\Http\Controllers\JailController;
use App\Http\Controllers\KillController;
use App\Http\Controllers\LogsController;
use App\Http\Controllers\PokerController;
use App\Http\Controllers\RaceController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\PlayersController;
use App\Http\Controllers\CrimeController;
use App\Http\Controllers\FamilyController;
use App\Http\Controllers\HouseController;
use App\Http\Controllers\AdminController;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth', 'verified'])->group(function () {
    Route::middleware(['message'])->group(function() {
        Route::middleware(['action'])->group(function() {

            Route::middleware(['familyRang:Leader,Owner,Manager'])->group(function () {
                Route::get('/manage/family/shop', [FamilyController::class, 'shop'])->name('familyShop');
            });

            Route::middleware(['familyRang:Leader,Owner,Recruiter'])->group(function () {
                Route::get('/manage/family/recruitment', [FamilyController::class, 'recruitment'])->name('recruitment');
            });

            Route::get('/dashboard', [PlayersController::class, 'dashboard'])->name('dashboard');

            //player
            Route::get('/safe', [HouseController::class, 'safe'])->name('safe');
            Route::post('/safe', [HouseController::class, 'safeForm'])->name('safe.store');

            //Route::get('/poker', [PokerController::class, 'index'])->name('poker');

            Route::get('/house', [HouseController::class, 'house'])->name('house');
            Route::get('/house/buy', [HouseController::class, 'buyHouse'])->name('house.buy');
            Route::get('/house/buy/{name}', [HouseController::class, 'buyHouseForm'])->name('house.buyForm');
            Route::get('/house/sell', [HouseController::class, 'sellHouseForm'])->name('house.sell');

            Route::get('/hide', [PlayersController::class, 'hidden'])->name('hidden');
            Route::post('/hide', [PlayersController::class, 'hiddenForm'])->name('hidden.store');

            Route::get('/businesses', [BusinessController::class, 'index'])->name('businesses');

            //players
            Route::get('/player/{username}', [PlayersController::class, 'player'])->name('player.show');
            Route::get('/player/{username}/click', [PlayersController::class, 'click'])->name('player.click');
            Route::get('/player/{username}/friends', [PlayersController::class, 'friends'])->name('player.friends');
            Route::get('/players', [PlayersController::class, 'playerList'])->name('player.all');
            Route::get('/players/online', [PlayersController::class, 'onlinePlayerList'])->name('player.allOnline');

            //admin
            Route::get('/admin', [AdminController::class, 'index'])->name('admin');

            //family
            Route::get('/families', [FamilyController::class, 'families'])->name('families');
            Route::get('/family/profile/{name}', [FamilyController::class, 'family'])->name('family');
            #Route::get('/family/{name}/promotion', [FamilyController::class, 'promotion'])->name('family.promotion');
            Route::get('/family/profile/{name}/click', [FamilyController::class, 'clickFam'])->name('family.click');
            Route::get('/family/stats', [FamilyController::class, 'stats'])->name('family.stats');
            Route::get('/families/stats', [FamilyController::class, 'famStats'])->name('families.stats');

            Route::get('/family/heists', [FamilyController::class, 'famHeistAll'])->name('family.heists');
            Route::get('/family/heists/history', [FamilyController::class, 'famHeistsHistory'])->name('family.heist.history');
            Route::get('/family/heist/create', [FamilyController::class, 'famHeistCreate'])->name('family.heist.create');
            Route::post('/family/heist/create', [FamilyController::class, 'famHeistCreateForm'])->name('family.heist.store');
            Route::get('/family/heist/{id}', [FamilyController::class, 'famHeistShow'])->name('family.heist.show');
            Route::post('/family/heist/{id}/join', [FamilyController::class, 'famHeistJoin'])->name('family.heist.join');
            Route::get('/family/deads', [FamilyController::class, 'deads'])->name('family.deads');
            Route::get('/family/kills', [FamilyController::class, 'kills'])->name('family.kills');
            Route::get('/family/kills/today', [FamilyController::class, 'killsToday'])->name('family.killsToday');

            Route::get('/family/forum', [FamilyController::class, 'forum'])->name('family.forum');
            Route::get('/family/forum/topic/{topic}', [FamilyController::class, 'topic'])->name('family.topic');
            Route::get('/family/forum/category/{category}', [FamilyController::class, 'category'])->name('family.category');


            Route::get('/family/forum/crew', [FamilyController::class, 'crewForum'])->name('familyCrew.forum');
            Route::get('/family/forum/crew/topic/{topic}', [FamilyController::class, 'crewTopic'])->name('familyCrew.topic');

            Route::get('/family/crew/forum', [FamilyController::class, 'forum'])->name('forum');

            //cards
            Route::get('/cards', [CardController::class, 'index'])->name('cards.show');
            Route::get('/cards/collection', [CardController::class, 'turnedCards'])->name('cards.turnedin');
            Route::get('/cards/sold', [CardController::class, 'soldCards'])->name('cards.sold');

            //crimes
            Route::get('/crime', [CrimeController::class, 'crime'])->name('crime.show');
            Route::post('/crime', [CrimeController::class, 'crimeForm'])->name('crime.store');

            Route::get('/races', [RaceController::class, 'index'])->name('race.index');
            Route::get('/races/create', [RaceController::class, 'create'])->name('race.create');
            Route::get('/race/{id}', [RaceController::class, 'take'])->name('race.take');
            Route::post('/race/create', [RaceController::class, 'createForm'])->name('race.post');

            Route::get('/attack/{name}', [PlayersController::class, 'moneyAttack'])->name('player.attack');

            Route::get('/logs', [LogsController::class, 'index'])->name('logs');
            Route::get('/logs/transactions', [LogsController::class, 'transactions'])->name('player.transactions');
            Route::get('/logs/ranks', [LogsController::class, 'ranks'])->name('player.ranks');
            Route::get('/logs/races', [LogsController::class, 'races'])->name('race.logs');
            Route::get('/logs/hide', [LogsController::class, 'hide'])->name('player.hideLogs');
            Route::get('/logs/rip', [LogsController::class, 'rip'])->name('player.ripLogs');
            Route::get('/logs/rip/defenses', [LogsController::class, 'defenses'])->name('player.ripLogsDefenses');
            Route::get('/logs/rip/attacks', [LogsController::class, 'attacks'])->name('player.ripLogsAttacks');
            Route::get('/logs/security', [LogsController::class, 'security'])->name('security.logs');

            Route::get('/shop/', [ShopController::class, 'index'])->name('shop');

            Route::get('/shop/security', [ShopController::class, 'security'])->name('shop.security');
            Route::get('/shop/airplanes', [ShopController::class, 'airplanes'])->name('shop.airplanes');
            Route::get('/shop/weapons', [ShopController::class, 'weapons'])->name('shop.weapons');
            Route::get('/shop/power', [ShopController::class, 'power'])->name('shop.power');

            Route::get('/shop/sell/airplane', [ShopController::class, 'sellAirplane'])->name('shop.sellAirplane');
            Route::get('/shop/buy/airplane/{id}', [ShopController::class, 'buyAirplane'])->name('shop.buyAirplane');

            Route::post('/shop/security', [ShopController::class, 'securityForm'])->name('security.post');
            Route::post('/shop/weapons', [ShopController::class, 'weaponsForm'])->name('weapons.post');
            Route::post('/shop/power', [ShopController::class, 'buyPower'])->name('power.post');


            Route::get('/forum', [ForumController::class, 'categories'])->name('forum');
            Route::get('/forum/{categorySlug}',[ForumController::class,'category'])->name('category');
            Route::get('/forum/{categorySlug}/topic/{id}',[ForumController::class,'topic'])->name('topic');

            Route::post('/forum/{categorySlug}/topic/{id}',[ForumController::class,'postReaction'])->name('postReaction');

            Route::get('/reportItem/{type}/{id}',[ForumController::class,'report'])->name('reportItem');

            #Route::get('/comment/{id}',[ForumController::class,'comment'])->name('comment');

            //bank
            Route::get('/bank', [PlayersController::class, 'bank'])->name('bank');
            Route::post('/bank', [PlayersController::class, 'bankForm'])->name('bank.store');

            //actions
            Route::get('/airport', [AirportController::class, 'index'])->name('airport');
            Route::post('/airport', [AirportController::class, 'airportForm'])->name('airport.store');

            Route::get('/prison', [JailController::class, 'index'])->name('prison');
            Route::get('/prison/buyout/{id}', [JailController::class, 'buyOut'])->name('buyOut');

            Route::get('/prison/{id}', [JailController::class, 'burst'])->name('burst');

            Route::get('/business/pincode/{id}', [BusinessController::class, 'pincode'])->name('business.pincode');
            Route::post('/business/pincode/{id}', [BusinessController::class, 'pincodeForm'])->name('business.pincheck');
            Route::post('/hospital/buyblood', [HospitalController::class, 'buyBlood'])->name('hospital.buyBlood');
            Route::post('/hospital/changePrice', [HospitalController::class, 'changePrice'])->name('hospital.changePrice');

            Route::get('/kill', [KillController::class, 'index'])->name('kill');
            Route::post('/kill', [KillController::class, 'kill'])->name('kill.store');

            Route::get('/marriage', [PlayersController::class, 'marriage'])->name('marriage.show');
            Route::post('/marriage', [PlayersController::class, 'marriageForm'])->name('marriage.store');
            Route::get('/marriage/{action}', [PlayersController::class, 'marriageAction'])->name('marriage.action');

            Route::get('/detectives', [DetectiveController::class, 'show'])->name('detectives');
            Route::post('/detectives', [DetectiveController::class, 'detsForm'])->name('detectives.store');
            Route::get('/detective/restart/{id}', [DetectiveController::class, 'restartDets'])->name('detective.restart');
            Route::get('/detective/delete/{id}', [DetectiveController::class, 'deleteDets'])->name('detective.delete');

            //messages
            Route::get('/inbox', [InboxController::class, 'inbox'])->name('inbox');
            Route::get('/outbox', [InboxController::class, 'outbox'])->name('outbox');

            Route::get('/message', [InboxController::class, 'newMessage'])->name('message.create');
            Route::get('/message/{id}/reply', [InboxController::class, 'replyMessage'])->name('message.reply');
            Route::get('/message/{id}', [InboxController::class, 'userMessage'])->name('message.show');
            Route::get('/message/{id}/delete', [InboxController::class, 'deleteMessage'])->name('message.delete');
            Route::post('/message', [InboxController::class, 'formMessage'])->name('message.store');
            Route::post('/message/{id}/report', [InboxController::class, 'reportMessage'])->name('message.report');

            Route::get('/settings', [SettingsController::class, 'index'])->name('settings');

            Route::get('/stats', [DefaultController::class, 'allStats'])->name('stats');

            Route::get('/settings/user', [SettingsController::class, 'user'])->name('settings.user');
            Route::get('/settings/profile', [SettingsController::class, 'profile'])->name('settings.profile');
            Route::get('/settings/password', [SettingsController::class, 'index'])->name('settings.password');
            Route::get('/settings/referrals', [SettingsController::class, 'index'])->name('settings.referrals');
            Route::get('/settings/friends', [SettingsController::class, 'friends'])->name('settings.friends');
            Route::post('/settings/friends', [SettingsController::class, 'friendsAdd'])->name('friends.add');

            Route::get('/settings/friends/{action}/{id}', [SettingsController::class, 'friendsForm'])->name('settings.friendsForm');

            //family actions
            Route::get('/families/join/{name}', [FamilyController::class, 'joinFamily'])->name('family.join');
            Route::get('/families/create', [FamilyController::class, 'makeFamily'])->name('family.create');
            Route::post('/families/create', [FamilyController::class, 'makeFamilyForm'])->name('family.store');

            Route::middleware(['code'])->group(function () {
                Route::get('/prison/manage/{id}', [JailController::class, 'owner'])->name('prison.owner');
                Route::get('/hospital/manage/{id}', [HospitalController::class, 'owner'])->name('hospital.owner');

                Route::post('/business/{id}/bank', [BusinessController::class, 'businessBank'])->name('businessBank.store');
                Route::get('/business/{id}/logs', [LogsController::class, 'businessLogs'])->name('businessBank.logs');
            });
        });

    Route::get('/jailed', [JailController::class, 'jailed'])->name('jailed');
    Route::get('/jailed/{id}', [JailController::class, 'jailedForm'])->name('jailedBuyOut');

    Route::get('/hospital', [HospitalController::class, 'index'])->name('hospital');

    Route::post('/hospital', [HospitalController::class, 'buyHealth'])->name('hospital.store');

    Route::get('/dead', [KillController::class, 'dead'])->name('dead');
    Route::get('/dead/hospital', [HospitalController::class, 'dead'])->name('deadHospital');
    Route::get('/test/', [DefaultController::class, 'allStats'])->name('test');

    });
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
