<style>

    /* ––––––––––––––––––––––––––––––––––––––––––––––––––
      Based on: https://codepen.io/nickelse/pen/YGPJQG
      Influenced by: https://sproutsocial.com/
    –––––––––––––––––––––––––––––––––––––––––––––––––– */


    /* #Mega Menu Styles
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
    .mega-menu {
        display: none;
        left: 0;
        position: absolute;
        text-align: left;
        width: 100%;
    }



    /* #hoverable Class Styles
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
    .hoverable {
        position: static;
    }

    .hoverable > a:after {
        content: "\25BC";
        font-size: 10px;
        padding-left: 6px;
        position: relative;
        top: -1px;
    }

    .hoverable:hover .mega-menu {
        display: block;
    }


    /* #toggle Class Styles
    –––––––––––––––––––––––––––––––––––––––––––––––––– */

    .toggleable > label:after {
        content: "\25BC";
        font-size: 10px;
        padding-left: 6px;
        position: relative;
        top: -1px;
    }

    .toggle-input {
        display: none;
    }
    .toggle-input:not(checked) ~ .mega-menu {
        display: none;
    }

    .toggle-input:checked ~ .mega-menu {
        display: block;
    }

    .toggle-input:checked + label {
        color: white;
        background: #2c5282; /*@apply bg-blue-800 */
    }

    .toggle-input:checked ~ label:after {
        content: "\25B2";
        font-size: 10px;
        padding-left: 6px;
        position: relative;
        top: -1px;
    }

</style>

<x-app-layout>
    <x-slot name="header">
        <div class="grid grid-cols-6">
            <div>
                <label>
                    <a href="{{route('admin')}}">
                        {{ __('Admin Panel Developer') }}
                    </a>
                </label>
            </div>
            <div></div>
            <div></div>
            <div></div>
            <div>
                <div class="float-right">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <x-nav-link class="float-right" :href="route('logout')"
                                    onclick="event.preventDefault();
                        this.closest('form').submit();">
                            {{ __('Log Out') }}
                        </x-nav-link>
                    </form>

                </div>
            </div>

            <div>

            </div>
        </div>
    </x-slot>
    <nav class="relative bg-white border-b-2 border-gray-300 text-gray-900">
        <div class="container mx-auto flex justify-between">
            <ul class="flex">
                <li class="hoverable hover:bg-gray-800 hover:text-white">
                    <a href="#" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold hover:bg-gray-800 hover:text-white">Menu</a>
                    <div class="p-6 mega-menu mb-16 sm:mb-0 shadow-xl bg-gray-800">
                        <div class="container mx-auto w-full flex flex-wrap justify-between mx-2">
                            <ul class="px-4 w-full sm:w-1/2 lg:w-1/6 border-gray-600 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                                <div class="flex items-center">
                                    <h3 class="font-bold text-xl text-white text-bold mb-2">Bans</h3>
                                </div>
                                <p class="text-gray-100 text-sm">All bans</p>
                                <p class="text-gray-100 text-sm">Create a ban</p>
                                <p class="text-gray-100 text-sm">Remove a ban</p>
                                <div class="flex items-center py-3">
                                    <p class="text-gray-100 text-sm">Reports</p>
                                </div>
                            </ul>
                            <ul class="px-4 w-full sm:w-1/2 lg:w-1/6 border-gray-600 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                                <div class="flex items-center">
                                    <h3 class="font-bold text-xl text-white text-bold mb-2">Users & Families</h3>
                                </div>
                                <p class="text-gray-100 text-sm">All users</p>
                                <p class="text-gray-100 text-sm">All families</p>
                                <p class="text-gray-100 text-sm">Text</p>
                                <div class="flex items-center py-3">
                                    <p class="text-gray-100 text-sm">Text</p>
                                </div>
                            </ul>
                            <ul class="px-4 w-full sm:w-1/2 lg:w-1/6 border-gray-600 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                                <div class="flex items-center">
                                    <h3 class="font-bold text-xl text-white text-bold mb-2">Objects</h3>
                                </div>
                                <p class="text-gray-100 text-sm">All objects</p>
                                <p class="text-gray-100 text-sm">Change object</p>
                                <p class="text-gray-100 text-sm">Text</p>
                                <div class="flex items-center py-3">
                                    <p class="text-gray-100 text-sm">Text</p>
                                </div>
                            </ul>
                            <ul class="px-4 w-full sm:w-1/2 lg:w-1/6 border-gray-600 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                                <div class="flex items-center">
                                    <h3 class="font-bold text-xl text-white text-bold mb-2">Forums</h3>
                                </div>
                                <p class="text-gray-100 text-sm">Forums</p>
                                <p class="text-gray-100 text-sm">Family Forums</p>
                                <p class="text-gray-100 text-sm">Chat</p>
                                <div class="flex items-center py-3">
                                    <p class="text-gray-100 text-sm">Text</p>
                                </div>
                            </ul>
                            <ul class="px-4 w-full sm:w-1/2 lg:w-1/6 border-gray-600 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                                <div class="flex items-center">
                                    <h3 class="font-bold text-xl text-white text-bold mb-2">Settings</h3>
                                </div>
                                <p class="text-gray-100 text-sm">User Settings</p>
                                <p class="text-gray-100 text-sm">Family Settings</p>
                                <p class="text-gray-100 text-sm">Game Settings</p>
                                <div class="flex items-center py-3">
                                    <p class="text-gray-100 text-sm">Text</p>
                                </div>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

        <div class="py-12 max-w-full">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm">
                    <div class="p-6 bg-white border-b border-gray-200">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Message</strong>
                                    <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                </div>
                            </div><br>
                        @endif

                    </div>
                </div>
            </div>
    </div>



</x-app-layout>

