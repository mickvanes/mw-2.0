<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForumTopic extends Model
{
    use HasFactory;

    protected $table = 'forum_topics';

    protected $fillable = [
        'userID',
        'categoryID',
        'name',
        'content'
    ];

    public function category()
    {
        return $this->belongsTo(ForumCategory::class, 'categoryID','id');
    }

    public function reactions()
    {
        return $this->hasMany(ForumReaction::class, 'topicID')->orderBy('created_at');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }
}
