
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mx-auto" style="max-width: 612px;">
                <div class="p-6 bg-white border-b border-gray-200" style="max-width: 612px;">
                        @if(session()->has('message'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Prison</strong>
                                    <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                </div>
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Prison</strong>
                                    <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                </div>
                            </div>
                        @endif

                        <div class="text-center">
                            <div class="grid grid-cols-1">
                                <img class="mx-auto" src="{{ asset('images/bars.jpg') }}">
                                <span class="mt-4" id="{{ $user }}"></span>
                                <span>
                                    <a href="{{ route('jailedBuyOut',$id) }}" >
                                        Bribe the guards and get out of the prison for <span id="{{ $user }}_costs"></span>
                                    </a>
                                </span>
                            </div>
                            <script src="{{ URL::asset('js/jail.js') }}"></script>
                            <script type="text/javascript">countdown('<?php echo $dateNew ?>','{{ $user }}','{{ $costsPerSec }}');</script>
                        </div>

                    </div>
                </div>
            </div>
        </div>

</x-app-layout>
