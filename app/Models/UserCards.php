<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCards extends Model
{
    use HasFactory;

    protected $table = 'user_cards';

    protected $fillable = [
        'userID',
        'cardID',
        'turnedIn',
        'collection',
        'hasSold'
    ];

    public function collectionDetails()
    {
        return $this->hasMany(Collections::class, 'id','collection');
    }

    public function card()
    {
        return $this->belongsTo(CollectionCards::class,'cardID');
    }

}
