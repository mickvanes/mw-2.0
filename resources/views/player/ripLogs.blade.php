@extends('layouts.default')

@section('header')
    RIP LOGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div>
                            <div x-data="{ show: 1 }">
                                <div class="mb-4">
                                    <button  @click="show=1" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                                        Last 10 Attacks
                                    </button>
                                    <button  @click="show=2" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                                        Last 10 Defenses
                                    </button>
                                    <a href="{{ route('player.ripLogsAttacks') }}" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                                        All Attacks
                                    </a>
                                    <a  href="{{ route('player.ripLogsDefenses') }}" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                                        All Defenses
                                    </a>
                                </div>

                                <div x-show="show === 1">
                                    <p>Attack</p>
                                    <table class="table-fixed w-full mb-2">
                                        <thead>
                                        <tr>
                                            <th class="text-left">Defender</th>
                                            <th class="text-left">Cash</th>
                                            <th class="text-left">Won/Loss</th>
                                            <th class="text-left">Time</th>
                                        </tr>
                                        </thead>

                                        @foreach($ripAttack as $ripAttackItem)
                                            <tr>
                                                <td><a href="{{ route('player.show',$ripAttackItem->defender->username) }}">{{ $ripAttackItem->defender->username }}</a></td>
                                                <td>&euro; {{ number_format($ripAttackItem->cash, 0, ',', '.') }}</td>
                                                <td>
                                                    @if($ripAttackItem->result == 0)
                                                        Loss
                                                    @else
                                                        Won
                                                    @endif
                                                </td>
                                                <td>{{ $ripAttackItem->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>

                                <div x-show="show === 2">
                                    <p>Defense</p>
                                    <table class="table-fixed w-full mb-2">
                                        <thead>
                                        <tr>
                                            <th class="text-left">Attacker</th>
                                            <th class="text-left">Cash</th>
                                            <th class="text-left">Won/Loss</th>
                                            <th class="text-left">Time</th>
                                        </tr>
                                        </thead>

                                        @foreach($ripDefense as $ripDefenseItem)
                                            <tr>
                                                <td><a href="{{ route('player.show',$ripDefenseItem->attacker->username) }}">{{ $ripDefenseItem->attacker->username }}</a></td>
                                                <td>&euro; {{ number_format($ripDefenseItem->cash, 0, ',', '.') }}</td>
                                                <td>
                                                    @if($ripDefenseItem->result == 0)
                                                        Loss
                                                    @else
                                                        Won
                                                    @endif
                                                </td>
                                                <td>{{ $ripDefenseItem->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
