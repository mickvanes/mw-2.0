@extends('layouts.default')

@section('header')
    Family Heists
@stop

@section('content')
    <div class="mx-auto w-full md:mt-2 mb-4">
        <div class="max-w-7xl mx-auto ">
            <div class="shadow-sm md:w-2/4">
                <div class="text-white">
                    <div class="grid grid-cols-5 gap-2">
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('family.heist.create') }}">New</a></div>
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('family.heist.history') }}">History</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Heist</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Heist</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>

                        <span class="mb-4 text-red-600">@if($check) Wait for your family heist to complete before joining another @endif</span>

                        <div class="grid grid-cols-6">
                            <div><b>Date</b></div>
                            <div><b>Location</b></div>
                            <div><b>Starter</b></div>
                            <div><b>Cash to</b></div>
                            <div><b>Count</b></div>
                            <div><b>Join</b></div>
                        </div><hr>


                        @foreach($heists as $heist)
                            <div x-data="{ open: false, toggle() { this.open = ! this.open } }">
                                <div @click="toggle()" class="grid grid-cols-5 md:grid-cols-6 mb-2">
                                    <span>{{ $heist->created_at->format('d-m-Y') }}</span>
                                    <span>{{ $heist->heistLocation->name }}</span>
                                    <div>
                                        <a href="{{ route('player.show',$heist->player_1_name->username ) }}">{{ $heist->player_1_name->username }}</a>
                                        <div x-show="open">
                                            @if($heist->player_4_name)
                                                <a href="{{ route('player.show',$heist->player_2_name->username ) }}">{{ $heist->player_2_name->username }}</a>
                                                <a href="{{ route('player.show',$heist->player_3_name->username ) }}">{{ $heist->player_3_name->username }}</a><br>
                                                <a href="{{ route('player.show',$heist->player_4_name->username ) }}">{{ $heist->player_4_name->username }}</a>
                                            @elseif($heist->player_3_name)
                                                <a href="{{ route('player.show',$heist->player_2_name->username ) }}">{{ $heist->player_2_name->username }}</a>
                                                <a href="{{ route('player.show',$heist->player_3_name->username ) }}">{{ $heist->player_3_name->username }}</a><br>
                                            @elseif($heist->player_2_name)
                                                <a href="{{ route('player.show',$heist->player_2_name->username ) }}">{{ $heist->player_2_name->username }}</a>
                                            @endif
                                        </div>
                                    </div>
                                    <span>{{ $heist->heistMoneyLocation->name }}</span>
                                    <span>
                                    @if($heist->player_4_name)
                                            (4/5)
                                        @elseif($heist->player_3_name)
                                            (3/5)
                                        @elseif($heist->player_2_name)
                                            (2/5)
                                        @else
                                            (1/5)
                                        @endif
                                    </span>
                                    <span>@if(!$check)<a href="{{ route('family.heist.show', $heist->heist_id) }}">Join</a>@elseif($check) Joined @endif</span>
                                </div>
                            </div>
                        @endforeach




                </div>
            </div>
        </div>
    </div>
@endsection
