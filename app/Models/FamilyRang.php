<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyRang extends Model
{
    use HasFactory;

    protected $table = 'familyrangs';

    protected $fillable = [
        'name',
    ];

}
