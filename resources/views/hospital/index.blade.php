@extends('layouts.default')

@section('header')
    HOSPITAL
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @include('modules.flash-messages')

                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 relative"
                                         role="alert">
                                        <strong class="font-bold">Hospital</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 relative"
                                         role="alert">
                                        <strong class="font-bold">Hospital</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>

                        <div class="grid grid-cols-1">
                            Hospital
                            <div class="grid grid-cols-2 md:w-1/3 mt-4">
                                <div>Blood bags unit price:</div>
                                <div>&euro; {{ $bloodUnitPrice }}</div>
                                <div>Your health:</div>
                                <div>{{ $health }}% (-{{ 100 - $health }}%)</div>
                                <div>Available blood bags</div>
                                <div>{{ $bloodBags }}</div>
                            </div>
                            <form method="post" action="{{ route('hospital.store') }}">
                                @csrf
                                <div class="mt-4">
                                    <label for="hidden">Health Units: </label>
                                    <input type="number" class="text-black" id="units" name="units" min="1" step="1" value="{{ 100 - $health }}" max="{{ 100 - $health }}">
                                </div>
                                <input type="submit" name="send" value="Submit"
                                       class="cursor-pointer bg-darkButton hover:bg-darkHover dark:bg-buttonBlue mt-4 font-bold py-2 px-4">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


