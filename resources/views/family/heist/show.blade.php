@extends('layouts.default')

@section('header')
    Heist
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Heist</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Topic</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>

                        {{ $heist }}

                        {{ $heist->heistLocation->name }}

                        @if($heist->player_4_name)
                            {{ $heist->player_4_name->username }} {{ $heist->player_3_name->username }} {{ $heist->player_2_name->username }} {{ $heist->player_1_name->username }}
                        @elseif($heist->player_3_name)
                            {{ $heist->player_3_name->username }} {{ $heist->player_2_name->username }} {{ $heist->player_1_name->username }}
                        @elseif($heist->player_2_name)
                            {{ $heist->player_2_name->username }} {{ $heist->player_1_name->username }}
                        @else
                            {{ $heist->player_1_name->username }}
                        @endif

                        {{ $heist->heistMoneyLocation->name }}

                        <form method="POST" action="{{ route('family.heist.join', $heist->heist_id) }}">
                            @csrf
                            <select class="text-black" name="role" class="form-control">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="id" value="{{ $heist->heist_id }}">

                            <button type="submit" class="btn btn-primary">Join</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
