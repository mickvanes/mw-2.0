@extends('layouts.default')

@section('header')
    RACE
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                        <div class="grid grid-cols-2 md:grid-cols-5">
                            @foreach($ranks as $rank)
                                <span>{{ \Carbon\Carbon::parse($rank->created_at)->format('d-m-Y H:i:s') }}</span>
                                <span class="md:col-span-4">{{ Auth::user()->username }} is promoted to <span class="font-bold">{{ $rank->rank }}</span>.</span>
                            @endforeach
                        </div>
                        <div class="mt-4">{{ $ranks->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
@stop
