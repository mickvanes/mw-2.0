<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            $table->unsignedInteger('rankAdv');
            $table->unsignedInteger('bullets');
            $table->unsignedInteger('employees');
            $table->unsignedInteger('clicks');
            $table->unsignedInteger('rep');
            $table->unsignedInteger('honor');
            $table->unsignedInteger('power');
            $table->unsignedInteger('health');
            $table->unsignedInteger('kills');
            $table->unsignedInteger('coins');
            $table->unsignedInteger('country_id');

            $table->foreign('country_id')->references('id')->on('user_country')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
