@extends('layouts.default')

@section('header')
    FRIENDS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Friends</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Friends</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif
                    <form method="post" class="mb-4" action="{{ route('friends.add') }}">
                        @csrf
                        <div class="grid grid-rows-1">
                            <h1 class="text-sm font-semibold">Add/Del Friends</h1>
                            <div class="grid grid-cols-2 w-full md:w-5/12">
                                <input class="w-9/12 text-black" style="height: 42px" type="text" id="name" placeholder="Player" name="name">
                                <div>
                                    <input class="text-white bg-gray-500 hover:bg-gray-700 font-bold py-2 px-4 float-left" style="width: 100px" value="Add" name="submit" type="submit">
                                    <input class="text-white bg-gray-500 hover:bg-gray-700 font-bold py-2 px-4 float-left" style="width: 100px" value="Del" name="submit" type="submit">
                                </div>
                            </div>
                        </div>
                    </form>

                    <h1 class="text-lg font-semibold">Friends</h1>

                    <div class="grid grid-cols-1 md:grid-cols-1 mt-4">
                        <div class="grid grid-cols-3 md:grid-cols-5 mb-2">
                            <span><b>Player</b></span>
                            <span><b>Family</b></span>
                            <span class="hidden md:block"><b>Rank</b></span>
                            <span class="hidden md:block"><b>Since</b></span>
                            <span><b>Online</b></span>
                        </div>
                        @foreach($friends as $friend)
                            @if($friend->friend->id == Auth::id())
                                <div>
                                    <div class="grid grid-cols-3 md:grid-cols-5">
                                        <a href="{{ route('player.show', $friend->user->username) }}">{{ $friend->user->username }}</a>
                                        <span>{{ $friend->user->family->familyDetails->name ?? "" }}</span>
                                        <span class="hidden md:block">{{ $friend->user->getCurrentRank->ranks->rank }}</span>
                                        <span class="hidden md:block">@if($friend->updated_at){{ \Carbon\Carbon::createFromDate($friend->updated_at)->format('d/m/Y') }} @else @endif</span>
                                        <span class="text-center md:text-left">
                                            @if(Cache::has('user-is-online-' . $friend->user->id))
                                                <svg width="15" height="15">
                                                    <rect width="15" fill="green" height="15" rx="50"/>
                                                </svg>
                                            @else
                                                <svg width="15" height="15">
                                                    <rect width="15" fill="red" height="15" rx="50"/>
                                                </svg>
                                            @endif
                                        </span>
                                    </div>

                                </div>
                            @else
                            <div>
                                <div class="grid grid-cols-3 md:grid-cols-5">
                                    <a href="{{ route('player.show', $friend->friend->username) }}">{{ $friend->friend->username }}</a>
                                    <span>{{ $friend->friend->family->familyDetails->name ?? "" }}</span>
                                    <span class="hidden md:block">{{ $friend->friend->getCurrentRank->ranks->rank }}</span>
                                    <span class="hidden md:block">@if($friend->updated_at) {{ \Carbon\Carbon::createFromDate($friend->updated_at)->format('d/m/Y') }}@else @endif</span>
                                    <span class="text-center md:text-left">
                                        @if(Cache::has('user-is-online-' . $friend->friend->id))
                                        <svg width="15" height="15">
                                                <rect width="15" fill="green" height="15" rx="50"/>
                                            </svg>
                                        @else
                                            <svg width="15" height="15">
                                                    <rect width="15" fill="red" height="15" rx="50"/>
                                                </svg>
                                        @endif
                                        </span>
                                </div>
                            </div>
                            @endif
                        @endforeach

                        <div class="my-4">{{ $friends->links() }}</div>

                        <hr>
                        <h1 class="text-lg font-semibold mt-4">Pending Friends</h1>
                        <div class="grid grid-cols-1 md:grid-cols-2">
                            <div style="border-right:1px solid inherit;">
                                <h1 class="text-sm font-semibold mt-4">Incoming friendrequests</h1>
                                <div class="grid grid-cols-5 mt-2 mb-2">
                                    <span><b>Player</b></span>
                                    <span class="col-span-2"><b>Family</b></span>
                                </div>
                                @foreach($incoming as $key=>$friend)
                                    <div>
                                        <div class="grid grid-cols-5">
                                            <a href="{{ route('player.show', $friend->user->username) }}" class="mr-2 mt-2 mb-2">{{ $friend->user->username }}</a>
                                            <span class="mr-2 mt-2 mb-2 col-span-2">{{ $friend->user->family->familyDetails->name ?? "" }}</span>
                                            <span class="mr-2 mt-2 mb-2">
                                            <a href="{{ route('settings.friendsForm',['accept',$friend->id]) }}">Accept</a>
                                        </span>
                                            <span class="mr-2 mt-2 mb-2">
                                            <a href="{{ route('settings.friendsForm',['decline',$friend->id]) }}">Decline</a>
                                        </span>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="my-4">{{ $incoming->links() }}</div>
                            </div>
                            <div>
                                <h1 class="text-sm font-semibold mt-4">Outgoing friendrequests</h1>
                                <div class="grid grid-cols-3 mt-2 mb-2">
                                    <span><b>Player</b></span>
                                    <span><b>Family</b></span>
                                    <span><b>Pending</b></span>

                                </div>
                                @foreach($outgoing as $key=>$friend)
                                    <div>
                                        <div class="grid grid-cols-3">
                                            <a href="{{ route('player.show', $friend->friend->username) }}" class="mr-2 mt-2 mb-2">{{ $friend->friend->username }}</a>
                                            <span class="mr-2 mt-2 mb-2">{{ $friend->friend->family->familyDetails->name ?? "" }}</span>
                                            <span class="mr-2 mt-2 mb-2">
                                                Waiting for acceptation
                                            </span>
                                        </div>

                                    </div>
                                @endforeach
                                <div class="my-4">{{ $outgoing->links() }}</div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
