@extends('layouts.default')

@section('header')
    Message
@stop

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{route('systemMessage.delete', $message->id)}}">Delete</a>
                    @if($message)
                            <div class="grid grid-cols-1 w-3/4 md:w-1/5">
                                <div class="grid grid-cols-2 w-full">
                                    <p>Sender:</p>
                                    <p style="color: #51a529;">~ {{$message->sender->name}} ~</p>
                                    <div>Subject:</div>
                                    <div>{{$message->subject}}</div>
                                    <div>Time:</div>
                                    <div>{{$message->created_at}}</div>
                                </div>
                            </div>
                            <div class="mt-10">{!! nl2br($message->body) !!}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

