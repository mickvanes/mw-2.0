@extends('layouts.default')

@section('header')
    DASHBOARD
@stop

@section('content')
    <style>
        .grid-rows-11{
            grid-template-rows: repeat(11, minmax(0, 1fr));
        }
    </style>

    @if(session()->has('message'))
        <div class="mx-auto w-full mb-4">
            <div class="md:max-w-7xl mx-auto ">
                <div class="bg-lightDefault dark:bg-darkDefault shadow-sm border border-[#243c5a]">
                    <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                        <strong class="font-bold">System</strong>
                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(session()->has('error'))
        <div class="mx-auto w-full">
            <div class="md:max-w-7xl mx-auto mb-4">
                <div class="bg-lightDefault dark:bg-darkDefault shadow-sm border border-[#243c5a]">
                    <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                        <strong class="font-bold">System</strong>
                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                    </div>
                </div>
            </div>
        </div>
    @endif

            <div class="mx-auto h-full overflow-hidden w-full">
                <div class="md:max-w-7xl mx-auto ">
                    <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                        <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                        <div class="grid grid-cols-1 md:grid-cols-3">
                            <div>
                                @if(!$user->avatar == "")
                                    <img class="object-fill h-96 w-96" src="{{ $user->avatar }}">
                                @else
                                    <img class="object-fill h-96 w-96" src="{{ asset('images/stock.png') }}">
                                @endif
                            </div>

                            <div class="md:grid-cols-2 p-6 md:pt-0 pl-0 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                                <div class="grid lg:grid-rows-11 lg:col-span-3">
                                    <div>Player : {{ $user->username }}
                                    @if($user->admin)
                                            <span style="color: red">({{ $user->admin->role->adminRang }})</span>
                                    @endif
                                    </div>
                                    <div>@if($family)
                                            Family : <a href="{{route('family', ['name' => $family->name])}}">{{ $family->name }}</a>
                                        @elseif($familyRecruit)
                                            Family : <a href="{{route('family', ['name' => $familyRecruit->name])}}">{{ $familyRecruit->name }}-recruit</a>
                                        @else
                                            Family : None
                                        @endif
                                    </div>
                                    <div>Rank : {{ $rank->rank }} </div>
                                    <div>Power: {{ number_format($playerProfile->power, 0, ',', '.') }}</div>
                                    <div>Marital status:
                                        @if($marriage)
                                            @if($marriage->player1Details->username == $user->username)
                                                <a href="{{route('player.show', $marriage->player1Details->username )}}">{{ $marriage->player1Details->username }}</a> & <a href="{{route('player.show', $marriage->player2Details->username )}}">{{ $marriage->player2Details->username }}</a>
                                            @else
                                                <a href="{{route('player.show', $marriage->player2Details->username )}}">{{ $marriage->player2Details->username }}</a> & <a href="{{route('player.show', $marriage->player1Details->username )}}">{{ $marriage->player1Details->username }}</a>
                                            @endif
                                        @else
                                            Single
                                        @endif
                                    </div>
                                    <div>
                                        @if($playerProfile->health >= 1 && $playerProfile->health <= 100)
                                            Status: Alive
                                        @elseif($playerProfile->health == 0)
                                            Status: Dead
                                        @elseif($playerProfile->health == 1000)
                                            Status: Godmode
                                        @endif
                                    </div>
                                    <div>Online : @if(Cache::has('user-is-online-' . $user->id))
                                            Now<br>
                                        @else
                                            @php $time = substr($playerProfile->last_online,  0, -3) @endphp
                                            {{$time}}<br>
                                        @endif</div>
                                    <div>@php $user_created = substr($user->created_at,  0, -3) @endphp Creation Date: {{$user_created}}<br></div>
                                    <div class="grid grid-cols-1 md:grid-cols-1" >
                                        <span>Cash: &euro;{{number_format($profileCash, 0, ',', '.')}}</span>
                                        <span>Bank: &euro;{{number_format($profileBank, 0, ',', '.')}}</span>
                                    </div>
                                    <div>Clicks: {{ number_format($playerProfile->clicks, 0, ',', '.')}}</div>
                                    <div>Reputation: {{ number_format($playerProfile->rep, 0, ',', '.')}}</div>
                                    <div>Honor: {{ number_format($playerProfile->honor, 0, ',', '.')}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mx-auto h-full overflow-hidden w-full mt-2">
                    <div class="max-w-7xl mx-auto ">
                        <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                            <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                                <div class="grid grid-cols-3 md:grid-cols-6 gap-4 w-full">
                        <button class="bg-gray-500 text-white font-bold mx-2 my-2 py-2 px-2" style="font-size: 100%;">
                            Donate
                        </button>
                        <button class="bg-gray-500 text-white font-bold mx-2 my-2  py-2 px-2" style="font-size: 100%;">
                            <a href="{{route('player.click', $user->username )}}">Click</a>
                        </button>
                        <button class="bg-gray-500 text-white font-bold mx-2 my-2  py-2 px-4" style="font-size: 100%;">
                            <a href="{{route('player.attack', $user->username )}}">Attack</a>
                        </button>
                        <button class="bg-gray-500 text-white font-bold mx-2 my-2  py-2 px-4" style="font-size: 100%;">
                            Dets
                        </button>
                        <button class="bg-gray-500 text-white font-bold mx-2 my-2  py-2 px-4" style="font-size: 100%;">
                            Message
                        </button>
                        <button class="bg-gray-500 text-white font-bold mx-2 my-2  py-2 px-4" style="font-size: 100%;">
                            <a href="{{route('player.friends', $user->username )}}">Friends</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mx-auto h-full overflow-hidden w-full mt-2">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    {!! (new \App\Helpers\Helper)->makeUBB($userProfileText) !!}
                </div>
            </div>
        </div>
    </div>

@stop

