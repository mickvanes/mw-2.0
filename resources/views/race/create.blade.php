@extends('layouts.default')

@section('header')
    RACE
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Race</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Race</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                    Create Race

                    <form method="POST" action="{{ route("race.post") }}">
                        @csrf
                        <div class="grid grid-cols-1 md:grid-cols-2 mt-4 w-1/2">
                            <label for="bet" class="w-1/4" style="width: 100px;">Bet:</label>
                            <input class="text-black" type="number" id="bet" name="bet" min="10000">
                        </div>

                        <div class="flex justify-end mt-4 md:mt-10">
                            <button class="flex-shrink-0 text-sm py-1 px-2 rounded" type="submit">
                                Create
                            </button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
@stop
