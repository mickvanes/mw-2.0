@extends('layouts.default')

@section('header')
    FAMILIES
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                        <table class="table-fixed mx-auto mt-4 w-full">
                            <thead>
                            <tr>
                                <th class="w-1/5 text-left">Name</th>
                                <th class="w-1/5 text-left">Owner</th>
                                <th class="w-1/5 hidden sm:table-cell lg:table-cell text-left">Description</th>
                                <th class="w-1/5 text-left">Reputation</th>
                                @if(!Auth::user()->hasFamily())
                                    <th class="w-1/5 text-right sm:text-left md:text-left lg:text-left">Join</th>
                                @endif
                            </tr>
                            </thead>

                        @foreach($data as $family)
                                <tr onclick="window.location='{{route('family', $family->name)}}';" class="hover:bg-gray-700">
                                        <td class="w-1/5">{{ $family->name }}</td>
                                        <td class="w-1/5">{{ $family->owner }}</td>
                                        <td class="w-1/5 hidden sm:table-cell lg:table-cell">{{ $family->description }}</td>
                                        <td class="w-1/5">{{ number_format($family->famRep, 0, ',', '.')}}</td>
                                    @if(!Auth::user()->hasFamily())
                                        <td class="w-1/5 text-right sm:text-left md:text-left lg:text-left"><a href="{{route('family.join', $family->name)}}">Join</a></td>
                                    @endif
                                </tr>
                        @endforeach
                        </table>
                    <div class="mt-2">{{ $data->links() }}</div>
                </div>
            </div>
        </div>
    </div>


@stop
