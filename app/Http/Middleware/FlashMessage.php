<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\FlashMessage as Message;

class FlashMessage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle(Request $request, Closure $next)
    {
        $flash = Message::where('userID','=',Auth::id())->first();

        if($flash){
            session()->flash('flash', $flash->message);
            $flash->delete();
        }

        return $next($request);
    }
}
