@extends('layouts.default')

    @section('header')
        INBOX
    @stop

    @section('content')
        <div class="hidden md:block mx-auto w-full">
            <div class="max-w-7xl mx-auto ">
                <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                    <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                        Inbox
                    </div>
                </div>
            </div>
        </div>

        <div class="mx-auto w-full md:mt-2 mb-4">
            <div class="max-w-7xl mx-auto ">
                <div class="shadow-sm md:w-2/4">
                    <div class="text-white">
                        <div class="grid grid-cols-5 gap-2">
                            <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('message.create') }}">New</a></div>
                            <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('outbox') }}">Outbox</a></div>
                            <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault ">Blocked</div>
                            <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault ">Archive</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mx-auto h-full overflow-hidden w-full">
            <div class="max-w-7xl mx-auto">
                <div class="bg-lightDefault dark:bg-darkDefault shadow-sm text-white">
                    <div class="p-4 md:p-6 text-white dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif
                    <div id="messages" class="text-white dark:text-white">
                        <div class="flex flex-row">
                                        <span class="w-5 mr-3">
                                        </span>
                            <div class="hidden md:inline-flex md:w-full">
                                <div class="grid md:grid-cols-4 grid-cols-3 md:w-full mx-auto mb-2">
                                    <div class="font-light">Sender</div>
                                    <div class="md:col-span-2 font-semibold">Subject</div>
                                    <div class="my-auto">Date</div>
                                </div>
                            </div>
                        </div>
                        @foreach($messages as $message)
                            @if($message->systemMessage == 0)
                                <a href="{{ route('message.show', $message->id) }}">
                                    <div class="flex flex-row">
                                        <span class="w-5 mr-3">
                                        @if($message->seen == true)
                                                <img src="{{ asset('images/inbox/message_op.png') }}">
                                            @else
                                                <img src="{{ asset('images/inbox/message_un.png') }}">
                                            @endif</span>
                                        <div class="grid md:grid-cols-4 grid-cols-3 md:w-full mx-auto mb-2 hover:bg-gray-600">
                                            <div class="font-light">{{ $message->sender->username }}</div>
                                            <div class="md:col-span-2 font-semibold"><abbr title="{{ $message->subject }}">{{mb_strimwidth($message->subject, 0, 20, "...")}}</abbr></div>
                                            <div class="my-auto">{{$message->created_at}}</div>
                                        </div>
                                    </div>
                                </a>
                            @else
                                <a href="{{ route('message.show', $message->id) }}">
                                    <div class="flex flex-row">
                                        <span class="w-5 mr-3">
                                            @if($message->seen == 1)
                                                <img src="{{ asset('images/inbox/message_op.png') }}">
                                            @else
                                                <img src="{{ asset('images/inbox/message_un.png') }}">
                                            @endif
                                        </span>
                                        <div class="grid md:grid-cols-4 grid-cols-3 md:w-full mx-auto mb-2 hover:bg-gray-600">
                                            <div class="font-light">{{ $message->systemSender->name }}</div>
                                            <div class="md:col-span-2 font-semibold"><abbr title="{{ $message->subject }}">{{mb_strimwidth($message->subject, 0, 20, "...")}}</abbr></div>
                                            <div class="my-auto">{{$message->created_at}}</div>
                                        </div>
                                    </div>
                                </a>
                                @endif
                        @endforeach
                    </div>
                        <div class="mt-4">{{ $messages->links() }}</div>
                </div>

            </div>

        </div>
    </div>
{{--    <script src="{{ URL::asset('js/messages.js') }}"></script>--}}
{{--    <script>--}}
{{--        $(document).ready( function () {--}}
{{--            $('#messages').DataTable(--}}
{{--                {--}}
{{--                    "order": [[ 3, "desc" ]]--}}
{{--                }--}}
{{--            );--}}
{{--        } );--}}
{{--    </script>--}}
@stop
