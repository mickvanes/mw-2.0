@extends('layouts.default')

@section('header')
    Topics
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Topic</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Topic</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>
                        <div class="grid grid-cols-1 mb-4">
                            <div>
                                <div class="grid grid-cols-4 mb-2">
                                    <div class="col-span-2">Title</div>
                                    <div class="text-center">Topics</div>
                                    <div class="text-center">Reactions</div>
                                </div>
                            </div>
                            <div class="mb-8">
                                <hr>
                                <span class="text-center"><h1 class="font-semibold">General</h1></span>
                                <hr>
                                @if($forumCategories->where('list',1)->sortBy('updated_at')->count() == 0)
                                    <hr>
                                    This category is empty
                                @else
                                    @foreach($forumCategories->where('list',1)->sortBy('updated_at')->take(5) as $categoryOne)
                                        <a href="{{ route('category',$categoryOne->slug) }}">
                                            <div class="grid grid-cols-4 mb-2">
                                                <div class="col-span-2">{{ $categoryOne->title }}<br>
                                                    <span class="text-sm font-light">{{ $categoryOne->subTitle }}</span></div>
                                                <div class="text-center my-auto">{{ $topics->where('categoryID', $categoryOne->id)->count() }}</div>
                                                <div class="text-center my-auto">{{ $comments->where('categoryID', $categoryOne->id)->count() }}</div>
                                            </div>
                                        </a>

                                    @endforeach
                                @endif
                                <hr>
                            </div>

                            <div class="mb-8">
                                <hr>
                                <span class="text-center"><h2 class="font-semibold">Game Development</h2></span>
                                <hr>
                                @if($forumCategories->where('list',2)->sortBy('updated_at')->count() == 0)
                                    <hr>
                                    This category is empty
                                @else
                                    @foreach($forumCategories->where('list',2)->sortBy('updated_at')->take(5) as $categoryTwo)
                                        <a href="{{ route('category',$categoryTwo->slug) }}">
                                            <div class="grid grid-cols-4 mb-2">
                                                <div class="col-span-2">{{ $categoryTwo->title }}<br>
                                                    <span class="text-sm font-light">{{ $categoryTwo->subTitle }}</span></div>
                                                <div class="text-center my-auto">{{ $topics->where('categoryID', $categoryTwo->id)->count() }}</div>
                                                <div class="text-center my-auto">{{ $comments->where('categoryID', $categoryTwo->id)->count() }}</div>
                                            </div>
                                        </a>
                                        <hr>

                                    @endforeach
                                @endif
                            </div>

                            <div class="mb-8">
                                <hr>
                                <span class="text-center"><h2 class="font-semibold">Crimes</h2></span>
                                @if($forumCategories->where('list',3)->sortBy('updated_at')->count() == 0)
                                    <hr>
                                    This category is empty
                                @else
                                    @foreach($forumCategories->where('list',3)->sortBy('updated_at')->take(5) as $categoryThree)
                                        <hr>
                                            <div class="grid grid-cols-4 mb-2">
                                                <div class="col-span-2">{{ $categoryThree->title }}<br>
                                                    <span class="text-sm font-light">{{ $categoryThree->subTitle }}</span></div>
                                                <div class="text-center my-auto">{{ $topics->where('categoryID', $categoryThree->id)->count() }}</div>
                                                <div class="text-center my-auto">{{ $comments->where('categoryID', $categoryThree->id)->count() }}</div>
                                            </div>
                                    @endforeach
                                @endif
                                <hr>
                            </div>
                            {{ $forumCategories->links() }}
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
