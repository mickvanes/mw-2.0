@extends('layouts.default')

@section('header')
    FAMILIES
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if($errors)
                        @foreach ($errors->all() as $message)
                            <div class="alert alert-danger">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Message</strong>
                                    <span class="block sm:inline">{{$message}}</span>
                                </div>
                            </div><br>
                        @endforeach
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Error</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                        @if(session()->has('message'))
                            <div class="alert alert-danger">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Error</strong>
                                    <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                </div>
                            </div><br>
                        @endif


                        @if($userRank >= $minRankFam)
                            @if(!$checkAlreadyFam)
                                <form method="post" action="{{ route('family.store') }}" id="createFamilyForm"
                                      autocomplete="off">

                                    <!-- CROSS Site Request Forgery Protection -->
                                    @csrf

                                    <div class="form-group">
                                        <div class="grid grid-cols-5">
                                            <div>
                                                <label for="FamilyName">Family Name:</label>
                                            </div>
                                            <div><input type="text" id="family_name"
                                                        name="family_name" maxlength="15" required></div>
                                        </div>
                                        <div class="grid grid-cols-5 mt-2">
                                            <div>

                                            </div>
                                            <div>
                                                <input type="submit" name="send" value="Submit"
                                                       class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @else
                                Je zit al in een familie
                            @endif
                        @else
                            Je rank is nog te laag, rank snel door!
                        @endif
                </div>
            </div>
        </div>
    </div>
@stop
