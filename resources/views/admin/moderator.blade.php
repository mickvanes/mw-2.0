<x-app-layout>
    <x-slot name="header">
        <div class="grid grid-cols-6">
            <div>
                <label>
                    <a href="{{route('admin')}}">
                        {{ __('Admin Panel Moderator') }}
                    </a>
                </label>
            </div>
            <div></div>
            <div></div>
            <div></div>
            <div>
                <div class="float-right">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <x-nav-link class="float-right" :href="route('logout')"
                                    onclick="event.preventDefault();
                        this.closest('form').submit();">
                            {{ __('Log Out') }}
                        </x-nav-link>
                    </form>

                </div>
            </div>

            <div>

            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                </div>
            </div>
        </div>
</x-app-layout>

