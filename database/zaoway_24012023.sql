-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 24 jan 2023 om 21:06
-- Serverversie: 10.4.22-MariaDB
-- PHP-versie: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zaoway`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `adminrangs`
--

CREATE TABLE `adminrangs` (
  `id` int(11) NOT NULL,
  `adminRang` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `adminrangs`
--

INSERT INTO `adminrangs` (`id`, `adminRang`) VALUES
(1, 'Developer'),
(2, 'Moderator');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `airplanes`
--

CREATE TABLE `airplanes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `time` int(11) NOT NULL,
  `costs` int(11) NOT NULL,
  `flightCosts` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `airplanes`
--

INSERT INTO `airplanes` (`id`, `name`, `image`, `time`, `costs`, `flightCosts`, `created_at`, `updated_at`) VALUES
(1, 'Hangglider', 'https://miro.medium.com/max/1200/1*4MNoTuO1SPiRlX12tgXzlw.jpeg', 60, 1000, 2000, NULL, NULL),
(2, 'Balloon', 'https://cdn.britannica.com/84/158184-050-1D7ADEB5/balloon.jpg', 50, 3000, 4000, NULL, NULL),
(3, 'Glider', 'https://www.schempp-hirth.com/fileadmin/_processed_/d/e/csm_RS1373_DSC_8823_9eb2f0cda2.jpg', 45, 10000, 2000, NULL, NULL),
(4, 'Helicopter', 'http://lemonbin.com/wp-content/uploads/2021/06/airbus-h160-helicopter.jpg', 40, 20000, 5000, NULL, NULL),
(5, 'Aeroplane', 'https://upload.wikimedia.org/wikipedia/commons/f/fc/Tarom.b737-700.yr-bgg.arp.jpg', 35, 10000, 7500, NULL, NULL),
(6, 'Private Jet', 'https://wereldreis.net/wp-content/uploads/2021/06/private-jet-sharing-1024x652.jpg', 30, 30000, 10000, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `attack_logs`
--

CREATE TABLE `attack_logs` (
  `id` bigint(20) NOT NULL,
  `player1` bigint(20) UNSIGNED NOT NULL,
  `player2` bigint(20) UNSIGNED NOT NULL,
  `cash` bigint(20) NOT NULL,
  `result` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `attack_logs`
--

INSERT INTO `attack_logs` (`id`, `player1`, `player2`, `cash`, `result`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 2017, 1, '2021-11-13 21:43:49', '2021-11-13 21:43:49'),
(2, 1, 4, 3283, 1, '2021-11-13 21:43:52', '2021-11-13 21:43:52'),
(3, 1, 4, 2119, 1, '2021-11-13 21:43:53', '2021-11-13 21:43:53'),
(4, 1, 4, 1400, 1, '2021-11-13 21:43:53', '2021-11-13 21:43:53'),
(5, 1, 4, 644, 1, '2021-11-14 01:22:58', '2021-11-14 01:22:58'),
(6, 1, 4, 1006, 1, '2021-11-14 01:23:14', '2021-11-14 01:23:14'),
(7, 1, 4, 2764, 0, '2021-11-14 01:23:48', '2021-11-14 01:23:48'),
(8, 1, 4, 1470, 0, '2021-11-14 01:24:21', '2021-11-14 01:24:21'),
(9, 1, 4, 2666, 0, '2021-11-14 01:26:29', '2021-11-14 01:26:29'),
(10, 1, 4, 1983, 0, '2021-11-14 01:27:09', '2021-11-14 01:27:09'),
(11, 1, 4, 1289, 0, '2021-11-14 01:27:58', '2021-11-14 01:27:58'),
(12, 1, 4, 622, 0, '2021-11-14 01:29:48', '2021-11-14 01:29:48'),
(13, 1, 4, 301, 0, '2021-11-14 01:30:27', '2021-11-14 01:30:27'),
(14, 1, 4, 279, 0, '2021-11-14 01:30:56', '2021-11-14 01:30:56'),
(15, 1, 4, 286, 0, '2021-11-14 01:33:02', '2021-11-14 01:33:02'),
(16, 1, 4, 181, 0, '2021-11-14 01:33:28', '2021-11-14 01:33:28'),
(17, 1, 4, 123, 0, '2021-11-14 01:33:40', '2021-11-14 01:33:40'),
(18, 1, 4, 204, 0, '2021-11-14 01:38:39', '2021-11-14 01:38:39'),
(19, 1, 4, 123, 0, '2021-11-14 01:39:07', '2021-11-14 01:39:07'),
(20, 1, 4, 85, 0, '2021-11-14 02:16:13', '2021-11-14 02:16:13'),
(21, 1, 4, 28, 0, '2021-11-14 02:17:35', '2021-11-14 02:17:35'),
(22, 1, 4, 39, 0, '2021-11-14 02:20:17', '2021-11-14 02:20:17'),
(23, 1, 4, 40, 0, '2021-11-14 02:20:31', '2021-11-14 02:20:31'),
(24, 1, 4, 26, 0, '2021-11-14 02:25:06', '2021-11-14 02:25:06'),
(25, 1, 4, 15, 0, '2021-11-14 02:25:19', '2021-11-14 02:25:19'),
(26, 1, 4, 14, 0, '2021-11-14 02:28:30', '2021-11-14 02:28:30'),
(27, 1, 4, 63639, 0, '2021-11-14 02:28:48', '2021-11-14 02:28:48'),
(28, 1, 4, 57525, 0, '2021-11-14 02:41:07', '2021-11-14 02:41:07'),
(29, 1, 4, 34216, 0, '2021-11-14 13:06:33', '2021-11-14 13:06:33'),
(30, 1, 5, 0, 1, '2021-11-14 13:08:15', '2021-11-14 13:08:15'),
(31, 1, 5, 0, 1, '2021-11-14 13:08:28', '2021-11-14 13:08:28'),
(32, 1, 5, 0, 1, '2021-11-14 13:15:04', '2021-11-14 13:15:04'),
(33, 1, 5, 0, 1, '2021-11-14 13:15:15', '2021-11-14 13:15:15'),
(34, 1, 4, 35181, 0, '2021-11-14 15:46:28', '2021-11-14 15:46:28'),
(35, 1, 1, 0, 0, '2021-11-15 13:11:53', '2021-11-15 13:11:53'),
(36, 1, 5, 0, 0, '2021-11-15 13:12:20', '2021-11-15 13:12:20'),
(37, 4, 1, 0, 1, '2021-11-15 13:26:09', '2021-11-15 13:26:09'),
(38, 7, 4, 0, 0, '2022-01-19 15:42:22', '2022-01-19 15:42:22'),
(39, 1, 4, 0, 0, '2022-01-20 22:12:29', '2022-01-20 22:12:29'),
(40, 1, 7, 0, 1, '2022-01-20 22:12:40', '2022-01-20 22:12:40'),
(41, 1, 4, 749850, 0, '2022-01-20 22:13:27', '2022-01-20 22:13:27'),
(42, 1, 1, 0, 1, '2022-05-05 18:32:32', '2022-05-05 18:32:32'),
(43, 1, 7, 0, 0, '2022-05-17 18:10:11', '2022-05-17 18:10:11'),
(44, 1, 7, 0, 1, '2022-05-17 18:10:22', '2022-05-17 18:10:22'),
(45, 1, 6, 420981984406670, 1, '2023-01-12 20:28:54', '2023-01-12 20:28:54');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `backfires`
--

CREATE TABLE `backfires` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `backfires`
--

INSERT INTO `backfires` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Don\'t fire back', NULL, NULL),
(2, 'Fire the half bullets of the killer back', NULL, NULL),
(3, 'Fire the same amount bullets back to the killer', NULL, NULL),
(4, 'Fire the double amount of bullets to the killer', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `backfire_kills`
--

CREATE TABLE `backfire_kills` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `playerID` bigint(20) UNSIGNED NOT NULL,
  `bullets` int(11) NOT NULL,
  `userRank` bigint(20) UNSIGNED NOT NULL,
  `playerRank` bigint(20) UNSIGNED NOT NULL,
  `userPercent` bigint(20) UNSIGNED NOT NULL,
  `playerPercent` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `weapon` bigint(20) NOT NULL,
  `userDef` int(11) NOT NULL,
  `playerDef` int(11) NOT NULL,
  `message` varchar(6000) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `backfire_kills`
--

INSERT INTO `backfire_kills` (`id`, `userID`, `playerID`, `bullets`, `userRank`, `playerRank`, `userPercent`, `playerPercent`, `active`, `success`, `weapon`, `userDef`, `playerDef`, `message`, `created_at`, `updated_at`) VALUES
(14, 5, 1, 50000, 2, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 12:58:05', '2021-11-23 13:02:50'),
(15, 5, 1, 50000, 2, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:04:24', '2021-11-23 13:06:16'),
(16, 5, 1, 50000, 2, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:10:03', '2021-11-23 13:11:19'),
(17, 5, 1, 50000, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:12:53', '2021-11-23 13:12:53'),
(18, 5, 1, 50000, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:16:46', '2021-11-23 13:16:46'),
(19, 5, 1, 50000, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:21:09', '2021-11-23 13:21:45'),
(20, 5, 1, 50000, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:22:31', '2021-11-23 13:23:11'),
(21, 5, 1, 50000, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:23:37', '2021-11-23 13:23:52'),
(22, 5, 1, 50000, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:24:23', '2021-11-23 13:27:37'),
(23, 5, 1, 50000, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:32:34', '2021-11-23 13:32:34'),
(24, 5, 1, 50000, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:33:10', '2021-11-23 13:33:10'),
(25, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:41:59', '2021-11-23 13:41:59'),
(26, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:46:07', '2021-11-23 13:46:07'),
(27, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:51:52', '2021-11-23 13:51:52'),
(28, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:53:52', '2021-11-23 13:53:52'),
(29, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:56:08', '2021-11-23 13:56:08'),
(30, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 13:58:00', '2021-11-23 13:58:00'),
(31, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 14:04:58', '2021-11-23 14:04:58'),
(32, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 14:07:50', '2021-11-23 14:07:50'),
(33, 5, 1, 0, 1, 1, 0, 0, 0, 0, 4, 150, 150, NULL, '2021-11-23 14:16:23', '2021-11-23 14:16:23'),
(34, 5, 1, 0, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 14:43:56', '2021-11-23 15:22:00');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `burstranks`
--

CREATE TABLE `burstranks` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `percent` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `burstranks`
--

INSERT INTO `burstranks` (`id`, `name`, `percent`, `created_at`, `updated_at`) VALUES
(1, 'Newbie', 35.84, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(2, 'Novice', 17.92, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(3, 'Rookie', 8.96, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(4, 'Beginner', 4.48, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(5, 'Talented', 2.24, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(6, 'Skilled', 1.12, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(7, 'Intermediate', 0.56, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(8, 'Skillful', 0.28, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(9, 'Seasoned', 0.14, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(10, 'Proficient', 0.07, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(11, 'Experienced', 0.035, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(12, 'Advanced', 0.0175, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(13, 'Senior', 0.00875, '2022-01-20 01:03:08', '2022-01-20 01:03:08'),
(14, 'Expert', 0.004375, '2022-01-20 01:03:08', '2022-01-20 01:03:08');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `businesses`
--

CREATE TABLE `businesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `typeID` int(11) NOT NULL,
  `countryID` bigint(20) UNSIGNED NOT NULL,
  `ownerID` bigint(20) UNSIGNED DEFAULT NULL,
  `cash` bigint(20) NOT NULL,
  `codeExpire` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `businesses`
--

INSERT INTO `businesses` (`id`, `typeID`, `countryID`, `ownerID`, `cash`, `codeExpire`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 0, NULL, NULL, NULL),
(2, 1, 2, NULL, 0, NULL, NULL, NULL),
(3, 1, 3, 7, 0, '2022-01-24 21:44:17', NULL, NULL),
(4, 1, 4, NULL, 0, NULL, NULL, NULL),
(5, 1, 5, NULL, 0, NULL, NULL, NULL),
(6, 1, 6, NULL, 0, NULL, NULL, NULL),
(7, 1, 7, NULL, 0, NULL, NULL, NULL),
(8, 1, 8, NULL, 0, NULL, NULL, NULL),
(9, 1, 9, NULL, 0, NULL, NULL, NULL),
(10, 1, 10, NULL, 0, NULL, NULL, NULL),
(11, 1, 11, NULL, 0, NULL, NULL, NULL),
(12, 1, 12, NULL, 0, '2022-01-24 00:34:33', NULL, NULL),
(13, 1, 13, NULL, 0, NULL, NULL, NULL),
(14, 1, 14, NULL, 0, NULL, NULL, NULL),
(16, 2, 1, NULL, 0, NULL, NULL, NULL),
(17, 2, 2, NULL, 0, NULL, NULL, NULL),
(18, 2, 3, 7, 0, '2022-01-24 22:49:34', NULL, NULL),
(19, 2, 4, NULL, 0, NULL, NULL, NULL),
(20, 2, 5, NULL, 0, NULL, NULL, NULL),
(21, 2, 6, NULL, 0, NULL, NULL, NULL),
(22, 2, 7, NULL, 0, NULL, NULL, NULL),
(23, 2, 8, NULL, 0, NULL, NULL, NULL),
(24, 2, 9, NULL, 0, NULL, NULL, NULL),
(25, 2, 10, NULL, 0, NULL, NULL, NULL),
(26, 2, 11, NULL, 0, NULL, NULL, NULL),
(27, 2, 12, NULL, 0, NULL, NULL, NULL),
(29, 2, 13, NULL, 0, NULL, NULL, NULL),
(30, 2, 14, NULL, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `business_bank`
--

CREATE TABLE `business_bank` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bank` bigint(20) DEFAULT NULL,
  `ownerID` bigint(20) UNSIGNED DEFAULT NULL,
  `pincode` varchar(4) NOT NULL DEFAULT '0000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `business_bank`
--

INSERT INTO `business_bank` (`id`, `bank`, `ownerID`, `pincode`, `created_at`, `updated_at`) VALUES
(1, 158000, NULL, '0000', '2022-01-24 00:32:14', '2022-01-24 00:33:12'),
(3, 48000000, 7, '0000', NULL, '2022-01-23 20:49:37'),
(18, 47880000, 7, '0000', NULL, '2022-01-24 19:15:39');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `business_transactions`
--

CREATE TABLE `business_transactions` (
  `id` bigint(20) NOT NULL,
  `businessID` bigint(20) UNSIGNED NOT NULL,
  `transactionType` int(1) NOT NULL,
  `cash` bigint(20) NOT NULL,
  `owner` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `business_transactions`
--

INSERT INTO `business_transactions` (`id`, `businessID`, `transactionType`, `cash`, `owner`, `created_at`, `updated_at`) VALUES
(4, 3, 2, 438000, 7, '2022-01-20 00:07:15', '2022-01-20 00:07:15'),
(5, 3, 2, 748000, 7, '2022-01-20 00:09:48', '2022-01-20 00:09:48'),
(6, 3, 2, 638000, 7, '2022-01-20 00:10:43', '2022-01-20 00:10:43'),
(7, 3, 2, 498000, 7, '2022-01-20 00:11:53', '2022-01-20 00:11:53'),
(8, 3, 2, 345000, 7, '2022-01-20 00:29:17', '2022-01-20 00:29:17'),
(9, 3, 2, 684000, 7, '2022-01-20 23:04:38', '2022-01-20 23:04:38'),
(10, 3, 2, 281000, 7, '2022-01-20 23:05:28', '2022-01-20 23:05:28'),
(11, 3, 0, 281000, 7, '2022-01-21 23:11:28', '2022-01-21 23:11:28'),
(12, 3, 1, 281000, 7, '2022-01-21 23:11:34', '2022-01-21 23:11:34'),
(13, 3, 1, 1026590000, 7, '2022-01-21 23:18:03', '2022-01-21 23:18:03'),
(14, 3, 0, 500000000, 7, '2022-01-22 23:43:46', '2022-01-22 23:43:46'),
(15, 3, 1, 500000000, 7, '2022-01-23 01:09:20', '2022-01-23 01:09:20'),
(16, 3, 0, 1026871000, 7, '2022-01-23 01:41:01', '2022-01-23 01:41:01'),
(17, 3, 1, 1026871000, 7, '2022-01-23 01:41:04', '2022-01-23 01:41:04'),
(18, 3, 2, 384000, 7, '2022-01-23 20:43:36', '2022-01-23 20:43:36'),
(19, 3, 2, 855000, 7, '2022-01-23 20:45:27', '2022-01-23 20:45:27'),
(20, 3, 2, 752000, 7, '2022-01-23 20:47:10', '2022-01-23 20:47:10'),
(21, 3, 2, 700000, 7, '2022-01-23 20:48:02', '2022-01-23 20:48:02'),
(22, 3, 2, 605000, 7, '2022-01-23 20:49:37', '2022-01-23 20:49:37'),
(23, 1, 2, 279000, NULL, '2022-01-24 00:31:15', '2022-01-24 00:31:15'),
(24, 1, 2, 158000, NULL, '2022-01-24 00:33:12', '2022-01-24 00:33:12'),
(25, 18, 1, 48000000, 7, '2022-01-24 19:13:35', '2022-01-24 19:13:35'),
(26, 18, 4, 60000, 7, '2022-01-24 19:14:40', '2022-01-24 19:14:40'),
(27, 18, 4, 60000, 7, '2022-01-24 19:15:39', '2022-01-24 19:15:39');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `business_types`
--

CREATE TABLE `business_types` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `business_types`
--

INSERT INTO `business_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Prison', NULL, NULL),
(2, 'Hospital', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `countries`
--

INSERT INTO `countries` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'England', 1, NULL, NULL),
(2, 'Spain', 1, NULL, NULL),
(3, 'The Netherlands', 1, NULL, NULL),
(4, 'Germany', 1, NULL, NULL),
(5, 'Italy', 1, NULL, NULL),
(6, 'Luxembourg', 0, NULL, NULL),
(7, 'Belgium', 0, NULL, NULL),
(8, 'France', 0, NULL, NULL),
(9, 'Poland', 0, NULL, NULL),
(10, 'Switzerland', 0, NULL, NULL),
(11, 'Denmark', 0, NULL, NULL),
(12, 'Austria', 0, NULL, NULL),
(13, 'Czechia', 0, NULL, NULL),
(14, 'Portugal', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `crimes`
--

CREATE TABLE `crimes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `crimes`
--

INSERT INTO `crimes` (`id`, `name`, `description`) VALUES
(1, 'trade', 'Trade in homemade chemicals on the black market.'),
(2, 'sell', 'Sell ​​weapons to Russian gangsters.'),
(3, 'robbery', 'Robbery a transport of expensive drink items.'),
(4, 'deal', 'Deal with the Italian mafia in expensive drugs.');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `crime_chance`
--

CREATE TABLE `crime_chance` (
  `id` int(11) NOT NULL,
  `crimeID` int(11) NOT NULL,
  `rankID` bigint(20) UNSIGNED NOT NULL,
  `minChance` int(11) NOT NULL,
  `maxChance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `crime_chance`
--

INSERT INTO `crime_chance` (`id`, `crimeID`, `rankID`, `minChance`, `maxChance`) VALUES
(1, 1, 1, 0, 5),
(2, 2, 1, 0, 3),
(3, 3, 1, 0, 5),
(4, 4, 1, 0, 5),
(5, 1, 2, 3, 6),
(6, 2, 2, 2, 5),
(7, 3, 2, 2, 7),
(8, 4, 2, 3, 6);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `families`
--

CREATE TABLE `families` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `description` varchar(15) DEFAULT NULL,
  `profileText` text NOT NULL,
  `famClicks` bigint(20) UNSIGNED NOT NULL,
  `famPower` bigint(40) UNSIGNED NOT NULL,
  `shares` bigint(20) UNSIGNED DEFAULT NULL,
  `exitCost` bigint(20) UNSIGNED DEFAULT NULL,
  `creation_date` date NOT NULL DEFAULT current_timestamp(),
  `houses` int(10) UNSIGNED DEFAULT NULL,
  `energy` int(10) UNSIGNED DEFAULT NULL,
  `land` int(11) DEFAULT NULL,
  `walls` int(10) UNSIGNED DEFAULT NULL,
  `famRep` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `families`
--

INSERT INTO `families` (`id`, `name`, `avatar`, `description`, `profileText`, `famClicks`, `famPower`, `shares`, `exitCost`, `creation_date`, `houses`, `energy`, `land`, `walls`, `famRep`) VALUES
(1, 'Vongola', '', '', '', 19, 0, 1343, 30000, '2021-06-08', 10, 10000, 312123, 0, 19),
(8, 'Memento-Mori', 'https://i.postimg.cc/1RnD1nwz/ava.jpg', '', '[center][img]https://i.postimg.cc/jjgyjcgp/1.jpg[/img][url=https://www.mafiaway.nl/message.php?p=new&to=Quick&subject=Joinen][img]https://i.postimg.cc/63FRr3Nz/2.jpg[/img][/url][img]https://i.postimg.cc/90T9Jhm2/3.jpg[/img][url=https://www.mafiaway.nl/message.php?p=new&to=Psyken&subject=Joinen][img]https://i.postimg.cc/Y9vYLFTh/4.jpg[/img][/url]\n[img]https://i.postimg.cc/y69rMXZv/wachtlijst.jpg[/img]\n[img]https://i.imgur.com/9HIw5Wn.jpg[/img]\n[url=https://www.mafiaway.nl/click.php?clan=Memento-mori][img]https://i.postimg.cc/GtR3nZXn/deel1.jpg[/img][/url][img]https://i.postimg.cc/cHcGrxzk/kills.jpg[/img][url=https://www.mafiaway.nl/familie/mass-click.php][img]https://i.postimg.cc/6qhWWYP2/deel3.jpg[/img][/url][/center]', 2, 0, 0, 0, '2021-06-17', 0, 0, 0, 0, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `familyforum_category`
--

CREATE TABLE `familyforum_category` (
  `id` bigint(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `subtitle` varchar(100) NOT NULL,
  `familyID` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `onlyCrew` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `familyforum_category`
--

INSERT INTO `familyforum_category` (`id`, `title`, `subtitle`, `familyID`, `userID`, `onlyCrew`, `created_at`, `updated_at`) VALUES
(3, 'Familie topics', 'hoiii', 1, 6, 0, NULL, NULL),
(5, 'Handleidingen', 'hoiii', 1, 6, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `familyforum_reactions`
--

CREATE TABLE `familyforum_reactions` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `topicID` bigint(20) NOT NULL,
  `categoryID` bigint(20) DEFAULT NULL,
  `content` varchar(6000) NOT NULL,
  `level` int(11) NOT NULL,
  `reactionID` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `familyforum_topics`
--

CREATE TABLE `familyforum_topics` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `familyID` int(11) NOT NULL,
  `categoryID` bigint(20) DEFAULT NULL,
  `onlyCrew` tinyint(1) NOT NULL DEFAULT 0,
  `crewTopic` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL,
  `content` varchar(6000) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `familyforum_topics`
--

INSERT INTO `familyforum_topics` (`id`, `userID`, `familyID`, `categoryID`, `onlyCrew`, `crewTopic`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 5, 1, NULL, 0, 0, 'Test', 'Test 123', NULL, NULL),
(2, 5, 1, 3, 1, 0, 'test', 'category', NULL, NULL),
(3, 6, 1, 5, 0, 0, 'test', 'category', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `familyrangs`
--

CREATE TABLE `familyrangs` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `familyrangs`
--

INSERT INTO `familyrangs` (`id`, `name`) VALUES
(1, 'Owner'),
(2, 'Leader'),
(3, 'General'),
(4, 'Manager'),
(5, 'Recruiter'),
(6, 'Member');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `family_cash`
--

CREATE TABLE `family_cash` (
  `id` int(11) NOT NULL,
  `familyID` int(11) NOT NULL,
  `cash` bigint(20) UNSIGNED NOT NULL,
  `bank` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `family_cash`
--

INSERT INTO `family_cash` (`id`, `familyID`, `cash`, `bank`) VALUES
(1, 1, 7027785293112, 0),
(7, 8, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `family_heists`
--

CREATE TABLE `family_heists` (
  `id` bigint(20) NOT NULL,
  `heist_id` varchar(30) NOT NULL,
  `player` bigint(20) UNSIGNED NOT NULL,
  `family` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `location` varchar(30) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `family_heists`
--

INSERT INTO `family_heists` (`id`, `heist_id`, `player`, `family`, `role`, `location`, `completed`, `created_at`, `updated_at`) VALUES
(5, 'ZwMyyVdqG', 7, 1, 2, 'Barn', 0, NULL, NULL),
(6, 'ZwMyyVdqG', 6, 1, 1, 'Barn', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `family_heists_roles`
--

CREATE TABLE `family_heists_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `family_heists_roles`
--

INSERT INTO `family_heists_roles` (`id`, `name`) VALUES
(1, 'Hitter'),
(2, 'Hacker');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `family_promotion`
--

CREATE TABLE `family_promotion` (
  `id` int(11) NOT NULL,
  `familyID` int(11) NOT NULL,
  `rank` bigint(20) UNSIGNED NOT NULL,
  `cash` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `family_promotion`
--

INSERT INTO `family_promotion` (`id`, `familyID`, `rank`, `cash`) VALUES
(5, 1, 2, 150),
(6, 1, 3, 300);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `family_recruiting`
--

CREATE TABLE `family_recruiting` (
  `id` bigint(40) NOT NULL,
  `familyID` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `family_user_clicks`
--

CREATE TABLE `family_user_clicks` (
  `id` bigint(20) NOT NULL,
  `familyID` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `family_user_clicks`
--

INSERT INTO `family_user_clicks` (`id`, `familyID`, `userID`, `timestamp`) VALUES
(2, 1, 1, '2021-06-17 02:41:46'),
(3, 1, 1, '2021-11-13 03:44:34'),
(4, 1, 1, '2021-11-13 03:44:43'),
(5, 1, 1, '2021-11-13 03:44:44'),
(6, 1, 1, '2021-11-13 03:44:46'),
(7, 1, 1, '2021-11-13 03:44:48'),
(8, 1, 1, '2021-11-13 03:44:49'),
(9, 1, 1, '2021-11-13 03:44:51'),
(10, 1, 1, '2021-11-13 03:44:53'),
(11, 1, 1, '2021-11-15 04:52:59'),
(12, 1, 1, '2021-11-15 04:53:12'),
(13, 1, 1, '2021-11-16 20:45:17'),
(14, 8, 1, '2021-12-16 00:49:22'),
(15, 1, 1, '2022-01-20 01:35:36'),
(16, 1, 1, '2022-01-30 20:40:59'),
(17, 1, 1, '2022-01-30 20:41:01'),
(18, 1, 1, '2022-01-30 20:41:41'),
(19, 1, 1, '2022-01-30 20:41:44'),
(20, 1, 1, '2022-01-30 20:41:48'),
(21, 8, 1, '2022-01-31 21:37:19'),
(22, 1, 6, '2023-01-12 21:45:04'),
(23, 1, 6, '2023-01-15 20:44:06');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `family_user_rang`
--

CREATE TABLE `family_user_rang` (
  `id` int(11) NOT NULL,
  `familyID` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `rangID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `family_user_rang`
--

INSERT INTO `family_user_rang` (`id`, `familyID`, `userID`, `rangID`) VALUES
(8, 8, 4, 1),
(11, 1, 1, 1),
(13, 8, 6, 2),
(14, 8, 7, 6);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `flashmessages`
--

CREATE TABLE `flashmessages` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `message` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `forum_categories`
--

CREATE TABLE `forum_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) NOT NULL,
  `subTitle` varchar(100) NOT NULL,
  `list` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `title`, `subTitle`, `list`, `created_at`, `updated_at`) VALUES
(1, 'Zaoway Pub', 'In this category you can talk to others', 1, NULL, NULL),
(2, 'Devlogs', 'You can here find all the developments of the game', 2, NULL, NULL),
(3, 'Bugs and errors', 'You can post here all the bugs and errors', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `forum_reactions`
--

CREATE TABLE `forum_reactions` (
  `id` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `topicID` bigint(20) UNSIGNED NOT NULL,
  `categoryID` int(11) UNSIGNED NOT NULL,
  `content` varchar(6000) NOT NULL,
  `level` int(11) NOT NULL DEFAULT 0,
  `reactionID` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `forum_reactions`
--

INSERT INTO `forum_reactions` (`id`, `userID`, `topicID`, `categoryID`, `content`, `level`, `reactionID`, `updated_at`, `created_at`) VALUES
(1, 7, 1, 1, 'Hoi', 0, NULL, NULL, NULL),
(2, 6, 1, 1, 'adadasasa', 1, 1, NULL, NULL),
(3, 5, 1, 1, 'Homo\'s', 0, NULL, NULL, '2022-04-15 22:55:59');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `forum_topics`
--

CREATE TABLE `forum_topics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `categoryID` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `content` varchar(6000) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `forum_topics`
--

INSERT INTO `forum_topics` (`id`, `userID`, `categoryID`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 'Topic Test', 'wqeqweqewqewqewqewqewqeqe', '2022-04-11 22:45:10', '2022-04-12 22:45:18'),
(2, 1, 2, 'Updates', 'Here I will post my updates', NULL, NULL),
(3, 5, 1, 'Memento-Mori ftw', 'Iedereen naar de klote!', '2022-04-11 22:45:10', '2022-04-12 22:45:18');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `honorpoints_user`
--

CREATE TABLE `honorpoints_user` (
  `id` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `rankID` bigint(20) UNSIGNED NOT NULL,
  `points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `hospital`
--

CREATE TABLE `hospital` (
  `id` int(11) NOT NULL,
  `ownerID` bigint(20) UNSIGNED DEFAULT NULL,
  `businessID` bigint(20) UNSIGNED NOT NULL,
  `bloodRemaining` int(10) UNSIGNED NOT NULL,
  `price` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `hospital`
--

INSERT INTO `hospital` (`id`, `ownerID`, `businessID`, `bloodRemaining`, `price`, `created_at`, `updated_at`) VALUES
(1, 7, 18, 30, 75, NULL, '2022-01-24 21:30:23'),
(2, NULL, 16, 30, 75, NULL, '2022-01-24 21:30:23'),
(3, NULL, 17, 30, 75, NULL, '2022-01-24 21:30:23'),
(4, NULL, 19, 30, 75, NULL, '2022-01-24 21:30:23'),
(5, NULL, 20, 30, 75, NULL, '2022-01-24 21:30:23'),
(6, NULL, 21, 30, 75, NULL, '2022-01-24 21:30:23'),
(7, NULL, 22, 30, 75, NULL, '2022-01-24 21:30:23'),
(8, NULL, 23, 30, 75, NULL, '2022-01-24 21:30:23'),
(9, NULL, 24, 30, 75, NULL, '2022-01-24 21:30:23'),
(10, NULL, 25, 30, 75, NULL, '2022-01-24 21:30:23'),
(11, NULL, 26, 30, 75, NULL, '2022-01-24 21:30:23'),
(12, NULL, 27, 30, 75, NULL, '2022-01-24 21:30:23'),
(13, NULL, 29, 30, 75, NULL, '2022-01-24 21:30:23'),
(14, NULL, 30, 30, 75, NULL, '2022-01-24 21:30:23');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `hospital_blood`
--

CREATE TABLE `hospital_blood` (
  `id` bigint(20) NOT NULL,
  `businessID` bigint(20) UNSIGNED NOT NULL,
  `ownerID` bigint(20) UNSIGNED NOT NULL,
  `costs` bigint(20) UNSIGNED NOT NULL,
  `amount` smallint(5) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `houses`
--

CREATE TABLE `houses` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `slug` varchar(20) NOT NULL,
  `defense` int(11) NOT NULL,
  `cost` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `houses`
--

INSERT INTO `houses` (`id`, `name`, `slug`, `defense`, `cost`) VALUES
(2, 'Student room', 'studroom', 1, 500),
(3, 'Tiny House', 'tinyhouse', 5, 20000),
(4, 'Apartment', 'apartment', 10, 150000),
(5, 'Bungalow', 'bungalow', 15, 200000),
(6, 'Semi-detached house', 'semihouse', 20, 300000),
(7, 'Detached house', 'detachedhouse', 25, 350000),
(8, 'Mansion', 'mansion', 30, 400000),
(9, 'Penthouse', 'penthouse', 40, 1000000),
(10, 'Villa', 'villa', 45, 2000000),
(11, 'Palace', 'palace', 50, 4000000);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1637, 'default', '{\"uuid\":\"6c12550e-1a03-4bf6-917b-719adb47a57c\",\"displayName\":\"App\\\\Jobs\\\\ProcessDets\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\ProcessDets\",\"command\":\"O:20:\\\"App\\\\Jobs\\\\ProcessDets\\\":11:{s:12:\\\"\\u0000*\\u0000detective\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:20:\\\"App\\\\Models\\\\Detective\\\";s:2:\\\"id\\\";i:63;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2022-04-11 21:28:39.448581\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:16:\\\"Europe\\/Amsterdam\\\";}s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1649705319, 1649704119),
(1638, 'default', '{\"uuid\":\"436cd87f-d478-464a-adc7-2c3f39bfd08c\",\"displayName\":\"App\\\\Jobs\\\\ProcessDets\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\ProcessDets\",\"command\":\"O:20:\\\"App\\\\Jobs\\\\ProcessDets\\\":11:{s:12:\\\"\\u0000*\\u0000detective\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:20:\\\"App\\\\Models\\\\Detective\\\";s:2:\\\"id\\\";i:64;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2022-04-11 21:30:35.547287\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:16:\\\"Europe\\/Amsterdam\\\";}s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1649705435, 1649704235),
(1639, 'default', '{\"uuid\":\"a5becf3e-78e8-49d4-8eaf-c016e37bfa9d\",\"displayName\":\"App\\\\Jobs\\\\DeactivateDets\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\DeactivateDets\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\DeactivateDets\\\":11:{s:12:\\\"\\u0000*\\u0000detective\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:20:\\\"App\\\\Models\\\\Detective\\\";s:2:\\\"id\\\";i:64;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2022-04-11 22:30:35.565231\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:16:\\\"Europe\\/Amsterdam\\\";}s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1649709035, 1649704235),
(1640, 'default', '{\"uuid\":\"e8e104ac-cf46-46f0-bfc6-b06fd23dcde7\",\"displayName\":\"App\\\\Jobs\\\\ProcessDets\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\ProcessDets\",\"command\":\"O:20:\\\"App\\\\Jobs\\\\ProcessDets\\\":11:{s:12:\\\"\\u0000*\\u0000detective\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:20:\\\"App\\\\Models\\\\Detective\\\";s:2:\\\"id\\\";i:65;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2022-04-11 21:31:46.284387\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:16:\\\"Europe\\/Amsterdam\\\";}s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1649705506, 1649704306),
(1641, 'default', '{\"uuid\":\"b43f3849-6f6d-4997-9920-8d7aa2bbbb95\",\"displayName\":\"App\\\\Jobs\\\\DeactivateDets\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\DeactivateDets\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\DeactivateDets\\\":11:{s:12:\\\"\\u0000*\\u0000detective\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:20:\\\"App\\\\Models\\\\Detective\\\";s:2:\\\"id\\\";i:65;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2022-04-12 01:31:46.303388\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:16:\\\"Europe\\/Amsterdam\\\";}s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1649719906, 1649704306),
(1642, 'default', '{\"uuid\":\"56bbd38d-a0a1-4215-88ff-11f9ed2aaa71\",\"displayName\":\"App\\\\Jobs\\\\ProcessDets\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\ProcessDets\",\"command\":\"O:20:\\\"App\\\\Jobs\\\\ProcessDets\\\":11:{s:12:\\\"\\u0000*\\u0000detective\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:20:\\\"App\\\\Models\\\\Detective\\\";s:2:\\\"id\\\";i:66;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2023-01-12 21:51:57.163984\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:16:\\\"Europe\\/Amsterdam\\\";}s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1673556717, 1673555517),
(1643, 'default', '{\"uuid\":\"04e47206-abb2-4294-8f1e-f96fcf3eff06\",\"displayName\":\"App\\\\Jobs\\\\DeactivateDets\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\DeactivateDets\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\DeactivateDets\\\":11:{s:12:\\\"\\u0000*\\u0000detective\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:20:\\\"App\\\\Models\\\\Detective\\\";s:2:\\\"id\\\";i:66;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2023-01-12 22:51:57.308933\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:16:\\\"Europe\\/Amsterdam\\\";}s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1673560317, 1673555517);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `kills`
--

CREATE TABLE `kills` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `playerID` bigint(20) UNSIGNED NOT NULL,
  `bullets` int(11) NOT NULL,
  `backfire` tinyint(1) NOT NULL,
  `userRank` bigint(20) UNSIGNED NOT NULL,
  `playerRank` bigint(20) UNSIGNED NOT NULL,
  `userPercent` int(11) NOT NULL,
  `playerPercent` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `weapon` bigint(20) NOT NULL,
  `userDef` int(11) NOT NULL,
  `playerDef` int(11) NOT NULL,
  `message` varchar(6000) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `kills`
--

INSERT INTO `kills` (`id`, `userID`, `playerID`, `bullets`, `backfire`, `userRank`, `playerRank`, `userPercent`, `playerPercent`, `active`, `success`, `weapon`, `userDef`, `playerDef`, `message`, `created_at`, `updated_at`) VALUES
(25, 5, 6, 50000, 1, 1, 2, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 12:58:05', '2021-11-23 13:02:48'),
(26, 1, 5, 50000, 1, 1, 2, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:04:24', '2021-11-23 13:04:48'),
(27, 1, 5, 50000, 1, 1, 2, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:10:03', '2021-11-23 13:10:42'),
(28, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:12:53', '2021-11-23 13:15:03'),
(29, 4, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:16:46', '2021-11-23 13:17:08'),
(30, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:21:09', '2021-11-23 13:21:53'),
(31, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:22:31', '2021-11-23 13:23:17'),
(32, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:23:37', '2021-11-23 13:23:45'),
(33, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:24:23', '2021-11-23 13:28:10'),
(34, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:32:34', '2021-11-23 13:32:50'),
(35, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:33:10', '2021-11-23 13:33:20'),
(36, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:41:59', '2021-11-23 13:45:38'),
(37, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:46:07', '2021-11-23 13:51:40'),
(38, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:51:52', '2021-11-23 13:52:09'),
(39, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:53:52', '2021-11-23 13:55:49'),
(40, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:56:08', '2021-11-23 13:57:19'),
(41, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 13:58:00', '2021-11-23 14:04:32'),
(42, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 14:04:58', '2021-11-23 14:06:59'),
(43, 1, 5, 50000, 1, 1, 1, 0, 0, 0, 1, 4, 150, 150, NULL, '2021-11-23 14:07:50', '2021-11-23 14:15:35');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `market_categories`
--

CREATE TABLE `market_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(25) NOT NULL,
  `minimumProducts` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `market_categories`
--

INSERT INTO `market_categories` (`id`, `name`, `minimumProducts`) VALUES
(1, 'Bullets', 1000),
(2, 'Cars', 1),
(3, 'Credits', 500),
(4, 'Witnesses', 1),
(5, 'Emblems', 5),
(6, 'Objects', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `market_products`
--

CREATE TABLE `market_products` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `productID` bigint(20) UNSIGNED NOT NULL,
  `amount` int(10) UNSIGNED NOT NULL,
  `creation_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2021_11_16_200436_create_jobs_table', 2),
(5, '2019_08_19_000000_create_failed_jobs_table', 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `pokers`
--

CREATE TABLE `pokers` (
  `id` bigint(20) NOT NULL,
  `jackpot` bigint(20) UNSIGNED NOT NULL,
  `raiseLimit` bigint(20) DEFAULT NULL,
  `timeOut` int(11) NOT NULL,
  `player1` bigint(20) UNSIGNED NOT NULL,
  `player2` bigint(20) UNSIGNED DEFAULT NULL,
  `player3` bigint(20) UNSIGNED DEFAULT NULL,
  `player4` bigint(20) UNSIGNED DEFAULT NULL,
  `inProgress` tinyint(4) NOT NULL DEFAULT 1,
  `playing` tinyint(1) NOT NULL DEFAULT 0,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `pokers`
--

INSERT INTO `pokers` (`id`, `jackpot`, `raiseLimit`, `timeOut`, `player1`, `player2`, `player3`, `player4`, `inProgress`, `playing`, `completed`, `created_at`, `updated_at`) VALUES
(1, 10000, 0, 120, 1, NULL, NULL, NULL, 1, 0, 0, '2022-01-18 20:18:06', '2022-01-18 20:18:06');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `poker_logs`
--

CREATE TABLE `poker_logs` (
  `id` bigint(20) NOT NULL,
  `player` bigint(20) UNSIGNED NOT NULL,
  `changeCards` tinyint(1) DEFAULT NULL,
  `callBool` tinyint(1) DEFAULT NULL,
  `raiseBool` tinyint(1) DEFAULT NULL,
  `callMoney` bigint(20) DEFAULT NULL,
  `raiseMoney` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `poweritems`
--

CREATE TABLE `poweritems` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `power` int(11) NOT NULL,
  `needClicks` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `costs` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rankAdv` float UNSIGNED DEFAULT NULL,
  `burstRank` int(11) NOT NULL DEFAULT 1,
  `burstAdv` float NOT NULL DEFAULT 0,
  `bursts` bigint(20) NOT NULL DEFAULT 0,
  `bullets` int(11) UNSIGNED DEFAULT NULL,
  `employees` int(11) UNSIGNED DEFAULT NULL,
  `clicks` int(11) UNSIGNED DEFAULT NULL,
  `rep` int(11) UNSIGNED DEFAULT NULL,
  `honor` int(10) UNSIGNED NOT NULL,
  `kills` int(11) NOT NULL,
  `bfKills` int(11) NOT NULL,
  `power` bigint(20) UNSIGNED NOT NULL,
  `health` float UNSIGNED NOT NULL,
  `coins` int(10) UNSIGNED NOT NULL,
  `profileText` text NOT NULL,
  `last_crime` datetime DEFAULT NULL,
  `last_online` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `profiles`
--

INSERT INTO `profiles` (`id`, `rankAdv`, `burstRank`, `burstAdv`, `bursts`, `bullets`, `employees`, `clicks`, `rep`, `honor`, `kills`, `bfKills`, `power`, `health`, `coins`, `profileText`, `last_crime`, `last_online`, `updated_at`, `created_at`) VALUES
(1, 90, 1, 0, 0, 8845100, 0, 0, 123, 2, 0, 0, 123456789, 100, 0, '[img]https://i.postimg.cc/VkkJqCxb/jinbar.gif[/img]', '2023-01-23 21:31:23', '2023-01-24 21:02:31', '2021-11-23 16:22:00', NULL),
(4, 16, 1, 0, 0, 0, 0, 0, 234, 0, 0, 0, 323456789, 100, 0, '', '2021-11-15 16:40:31', '2022-04-18 21:18:41', '2021-06-17 22:10:54', '2021-06-17 22:10:54'),
(5, 84.4, 1, 0, 0, 0, 0, 0, 345, 0, 0, 0, 2342342, 0, 0, '', '2021-10-31 14:56:14', '2021-11-23 15:16:28', '2021-11-23 15:43:56', '2021-10-31 00:51:07'),
(6, 64, 1, 0, 0, 8845100, 0, 0, 567, 0, 0, 0, 0, 100, 0, '', '2023-01-15 19:46:05', '2023-01-15 19:44:36', '2021-11-01 16:55:12', '2021-11-01 16:55:12'),
(7, 4, 1, 71.68, 1, 0, 0, 0, 678, 0, 0, 0, 0, 100, 0, '', '2021-11-23 17:03:05', '2023-01-12 20:34:24', '2022-01-21 01:36:26', '2021-11-01 16:55:12');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `races`
--

CREATE TABLE `races` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `bet` bigint(20) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT 0,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `races`
--

INSERT INTO `races` (`id`, `userID`, `bet`, `hidden`, `updated_at`, `created_at`) VALUES
(229, 6, 10000000, 0, '2023-01-12 20:39:14', '2023-01-12 20:39:14'),
(230, 6, 1000000, 0, '2023-01-12 20:39:18', '2023-01-12 20:39:18');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `races_completed`
--

CREATE TABLE `races_completed` (
  `id` bigint(20) NOT NULL,
  `racer1_ID` bigint(20) UNSIGNED NOT NULL,
  `racer2_ID` bigint(20) UNSIGNED NOT NULL,
  `bet` bigint(20) NOT NULL,
  `winner` bigint(20) UNSIGNED NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `races_completed`
--

INSERT INTO `races_completed` (`id`, `racer1_ID`, `racer2_ID`, `bet`, `winner`, `updated_at`, `created_at`) VALUES
(1, 1, 4, 44444444, 1, '2021-10-30 20:24:18', '2021-10-30 20:24:18'),
(2, 1, 4, 44444444, 1, '2021-10-30 20:30:41', '2021-10-30 20:30:41'),
(3, 1, 4, 44444444, 4, '2021-10-30 20:32:23', '2021-10-30 20:32:23'),
(4, 1, 4, 44444444, 1, '2021-10-30 20:35:55', '2021-10-30 20:35:55'),
(5, 1, 4, 44444444, 4, '2021-10-30 20:38:19', '2021-10-30 20:38:19'),
(6, 1, 4, 44444444, 1, '2021-10-30 20:46:26', '2021-10-30 20:46:26'),
(7, 1, 4, 44444444, 4, '2021-10-30 20:48:05', '2021-10-30 20:48:05'),
(8, 1, 4, 44444444, 4, '2021-10-30 20:52:23', '2021-10-30 20:52:23'),
(9, 1, 4, 44444444, 1, '2021-10-30 21:19:42', '2021-10-30 21:19:42'),
(10, 4, 1, 20000, 4, '2021-10-30 22:31:16', '2021-10-30 22:31:16'),
(11, 4, 1, 20000, 4, '2021-10-30 22:32:59', '2021-10-30 22:32:59'),
(12, 1, 4, 20000, 4, '2021-10-30 22:34:58', '2021-10-30 22:34:58'),
(13, 4, 1, 20000, 4, '2021-10-30 22:36:19', '2021-10-30 22:36:19'),
(14, 4, 1, 20000, 1, '2021-10-30 22:37:47', '2021-10-30 22:37:47'),
(15, 4, 1, 20000, 4, '2021-10-30 22:37:48', '2021-10-30 22:37:48'),
(16, 4, 1, 20000, 1, '2021-10-30 22:41:53', '2021-10-30 22:41:53'),
(17, 4, 1, 20000, 4, '2021-10-30 22:41:57', '2021-10-30 22:41:57'),
(18, 4, 1, 20000, 1, '2021-10-30 22:43:02', '2021-10-30 22:43:02'),
(19, 4, 1, 20000, 1, '2021-10-30 22:44:26', '2021-10-30 22:44:26'),
(20, 4, 1, 20000, 1, '2021-10-30 23:05:35', '2021-10-30 23:05:35'),
(21, 4, 1, 20000, 1, '2021-10-30 23:05:36', '2021-10-30 23:05:36'),
(22, 4, 1, 133314058, 4, '2021-10-30 23:06:27', '2021-10-30 23:06:27'),
(23, 1, 7, 20000, 1, '2022-01-19 15:52:18', '2022-01-19 15:52:18'),
(24, 7, 1, 100000, 7, '2022-01-19 15:53:06', '2022-01-19 15:53:06'),
(25, 1, 7, 2000000, 1, '2022-01-19 15:54:03', '2022-01-19 15:54:03'),
(26, 7, 1, 4000000, 1, '2022-01-19 15:54:22', '2022-01-19 15:54:22'),
(27, 7, 1, 8000000, 7, '2022-01-19 15:54:39', '2022-01-19 15:54:39'),
(28, 7, 1, 100000, 1, '2022-01-19 15:56:45', '2022-01-19 15:56:45'),
(29, 7, 1, 100000, 7, '2022-01-19 15:56:46', '2022-01-19 15:56:46'),
(30, 7, 1, 100000, 7, '2022-01-19 15:56:50', '2022-01-19 15:56:50'),
(31, 7, 1, 100000, 1, '2022-01-19 15:56:52', '2022-01-19 15:56:52'),
(32, 7, 1, 100000, 1, '2022-01-19 15:56:55', '2022-01-19 15:56:55'),
(33, 7, 1, 100000, 1, '2022-01-19 15:56:56', '2022-01-19 15:56:56'),
(34, 7, 1, 100000, 7, '2022-01-19 15:56:59', '2022-01-19 15:56:59'),
(35, 7, 1, 100000, 1, '2022-01-19 15:57:08', '2022-01-19 15:57:08'),
(36, 7, 1, 100000, 7, '2022-01-19 15:57:09', '2022-01-19 15:57:09'),
(37, 7, 1, 100000, 1, '2022-01-19 15:57:10', '2022-01-19 15:57:10'),
(38, 7, 1, 100000, 1, '2022-01-19 15:57:10', '2022-01-19 15:57:10'),
(39, 7, 1, 100000, 7, '2022-01-19 15:57:13', '2022-01-19 15:57:13'),
(40, 7, 1, 100000, 7, '2022-01-19 15:57:14', '2022-01-19 15:57:14'),
(41, 7, 1, 100000, 7, '2022-01-19 15:57:15', '2022-01-19 15:57:15'),
(42, 7, 1, 100000, 1, '2022-01-19 15:57:18', '2022-01-19 15:57:18'),
(43, 7, 1, 100000, 1, '2022-01-19 15:57:21', '2022-01-19 15:57:21'),
(44, 7, 1, 100000, 1, '2022-01-19 15:57:22', '2022-01-19 15:57:22'),
(45, 7, 1, 100000, 1, '2022-01-19 15:57:23', '2022-01-19 15:57:23'),
(46, 7, 1, 100000, 1, '2022-01-19 15:57:24', '2022-01-19 15:57:24'),
(47, 7, 1, 100000, 7, '2022-01-19 15:57:27', '2022-01-19 15:57:27'),
(48, 7, 1, 1000000, 7, '2022-01-19 15:57:45', '2022-01-19 15:57:45'),
(49, 7, 1, 1000000, 7, '2022-01-19 15:57:47', '2022-01-19 15:57:47'),
(50, 7, 1, 1000000, 7, '2022-01-19 15:59:08', '2022-01-19 15:59:08'),
(51, 7, 1, 1000000, 7, '2022-01-19 15:59:09', '2022-01-19 15:59:09'),
(52, 7, 1, 1000000, 1, '2022-01-19 15:59:16', '2022-01-19 15:59:16'),
(53, 7, 1, 1000000, 7, '2022-01-19 15:59:17', '2022-01-19 15:59:17'),
(54, 7, 1, 1000000, 7, '2022-01-19 15:59:23', '2022-01-19 15:59:23'),
(55, 7, 1, 1000000, 1, '2022-01-19 15:59:24', '2022-01-19 15:59:24'),
(56, 7, 1, 1000000, 7, '2022-01-19 15:59:26', '2022-01-19 15:59:26'),
(57, 7, 1, 1000000, 7, '2022-01-19 15:59:28', '2022-01-19 15:59:28'),
(58, 7, 1, 210000, 1, '2022-01-19 16:00:22', '2022-01-19 16:00:22'),
(59, 7, 1, 210000, 7, '2022-01-19 16:00:23', '2022-01-19 16:00:23'),
(60, 7, 1, 220000, 1, '2022-01-19 16:00:36', '2022-01-19 16:00:36'),
(61, 7, 1, 700000, 1, '2022-01-19 16:00:48', '2022-01-19 16:00:48'),
(62, 7, 1, 700000, 1, '2022-01-19 16:00:49', '2022-01-19 16:00:49'),
(63, 7, 1, 3000000, 7, '2022-01-19 16:01:03', '2022-01-19 16:01:03'),
(64, 7, 1, 3000000, 7, '2022-01-19 16:01:32', '2022-01-19 16:01:32'),
(65, 7, 1, 3000000, 7, '2022-01-19 16:01:33', '2022-01-19 16:01:33'),
(66, 7, 1, 20000, 1, '2022-01-19 18:29:08', '2022-01-19 18:29:08'),
(67, 7, 1, 20000, 1, '2022-01-19 18:29:09', '2022-01-19 18:29:09'),
(68, 7, 1, 20000, 1, '2022-01-19 18:29:15', '2022-01-19 18:29:15'),
(69, 7, 1, 20000, 7, '2022-01-19 18:29:18', '2022-01-19 18:29:18'),
(70, 7, 1, 20000, 7, '2022-01-19 18:29:31', '2022-01-19 18:29:31'),
(71, 7, 1, 20000, 1, '2022-01-19 18:29:32', '2022-01-19 18:29:32'),
(72, 1, 7, 20000, 1, '2022-01-19 18:32:25', '2022-01-19 18:32:25'),
(73, 1, 7, 20000, 1, '2022-01-19 18:32:26', '2022-01-19 18:32:26'),
(74, 1, 7, 20000, 1, '2022-01-19 18:36:06', '2022-01-19 18:36:06'),
(75, 1, 7, 20000, 7, '2022-01-19 18:36:09', '2022-01-19 18:36:09'),
(76, 1, 7, 20000, 7, '2022-01-19 18:47:50', '2022-01-19 18:47:50'),
(77, 1, 7, 20000, 7, '2022-01-19 18:47:52', '2022-01-19 18:47:52'),
(78, 7, 1, 1950000, 7, '2022-01-19 19:04:29', '2022-01-19 19:04:29'),
(79, 7, 1, 450000, 7, '2022-01-20 21:15:37', '2022-01-20 21:15:37'),
(80, 7, 1, 450000, 1, '2022-01-20 21:15:43', '2022-01-20 21:15:43'),
(81, 7, 1, 450000, 1, '2022-01-20 21:15:46', '2022-01-20 21:15:46'),
(82, 7, 1, 450000, 7, '2022-01-20 21:15:50', '2022-01-20 21:15:50'),
(83, 7, 1, 450000, 1, '2022-01-20 21:15:53', '2022-01-20 21:15:53'),
(84, 7, 1, 450000, 7, '2022-01-20 21:15:55', '2022-01-20 21:15:55'),
(85, 7, 1, 450000, 7, '2022-01-20 21:15:59', '2022-01-20 21:15:59'),
(86, 7, 1, 450000, 7, '2022-01-20 21:16:06', '2022-01-20 21:16:06'),
(87, 7, 1, 450000, 7, '2022-01-20 21:16:08', '2022-01-20 21:16:08'),
(88, 7, 1, 450000, 1, '2022-01-20 21:16:14', '2022-01-20 21:16:14'),
(89, 7, 1, 450000, 7, '2022-01-20 21:16:17', '2022-01-20 21:16:17'),
(90, 7, 1, 450000, 7, '2022-01-20 21:16:19', '2022-01-20 21:16:19'),
(91, 7, 1, 450000, 1, '2022-01-20 21:16:21', '2022-01-20 21:16:21'),
(92, 7, 1, 450000, 7, '2022-01-20 21:16:22', '2022-01-20 21:16:22'),
(93, 7, 1, 450000, 1, '2022-01-20 21:16:24', '2022-01-20 21:16:24'),
(94, 7, 1, 450000, 7, '2022-01-20 21:16:28', '2022-01-20 21:16:28'),
(95, 7, 1, 450000, 1, '2022-01-20 21:16:30', '2022-01-20 21:16:30'),
(96, 7, 1, 450000, 7, '2022-01-20 21:16:31', '2022-01-20 21:16:31'),
(97, 7, 1, 450000, 7, '2022-01-20 21:16:35', '2022-01-20 21:16:35'),
(98, 7, 1, 450000, 1, '2022-01-20 21:16:36', '2022-01-20 21:16:36'),
(99, 7, 1, 450000, 1, '2022-01-20 21:16:38', '2022-01-20 21:16:38'),
(100, 7, 1, 450000, 1, '2022-01-20 21:16:39', '2022-01-20 21:16:39'),
(101, 7, 1, 450000, 7, '2022-01-20 21:16:40', '2022-01-20 21:16:40'),
(102, 7, 1, 450000, 1, '2022-01-20 21:16:43', '2022-01-20 21:16:43'),
(103, 7, 1, 450000, 1, '2022-01-20 21:16:44', '2022-01-20 21:16:44'),
(104, 7, 1, 450000, 1, '2022-01-20 21:16:46', '2022-01-20 21:16:46'),
(105, 7, 1, 450000, 7, '2022-01-20 21:16:48', '2022-01-20 21:16:48'),
(106, 7, 1, 450000, 1, '2022-01-20 21:17:00', '2022-01-20 21:17:00'),
(107, 7, 1, 450000, 1, '2022-01-20 21:17:02', '2022-01-20 21:17:02'),
(108, 7, 1, 450000, 7, '2022-01-20 21:17:13', '2022-01-20 21:17:13'),
(109, 7, 1, 450000, 7, '2022-01-20 21:17:15', '2022-01-20 21:17:15'),
(110, 7, 1, 450000, 1, '2022-01-20 21:17:17', '2022-01-20 21:17:17'),
(111, 7, 1, 450000, 7, '2022-01-20 21:17:18', '2022-01-20 21:17:18'),
(112, 7, 1, 450000, 7, '2022-01-20 21:17:19', '2022-01-20 21:17:19'),
(113, 7, 1, 450000, 7, '2022-01-20 21:17:20', '2022-01-20 21:17:20'),
(114, 7, 1, 450000, 7, '2022-01-20 21:17:23', '2022-01-20 21:17:23'),
(115, 7, 1, 450000, 7, '2022-01-20 21:17:24', '2022-01-20 21:17:24'),
(116, 7, 1, 450000, 7, '2022-01-20 21:17:26', '2022-01-20 21:17:26'),
(117, 7, 1, 450000, 7, '2022-01-20 21:17:29', '2022-01-20 21:17:29'),
(118, 7, 1, 450000, 1, '2022-01-20 21:17:32', '2022-01-20 21:17:32'),
(119, 7, 1, 20000, 1, '2022-01-20 21:18:39', '2022-01-20 21:18:39'),
(120, 7, 1, 20000, 7, '2022-01-20 21:18:40', '2022-01-20 21:18:40'),
(121, 7, 1, 20000, 7, '2022-01-20 21:18:41', '2022-01-20 21:18:41'),
(122, 7, 1, 20000, 7, '2022-01-20 21:18:45', '2022-01-20 21:18:45'),
(123, 7, 1, 20000, 7, '2022-01-20 21:18:47', '2022-01-20 21:18:47'),
(124, 7, 1, 20000, 1, '2022-01-20 21:18:49', '2022-01-20 21:18:49'),
(125, 7, 1, 20000, 7, '2022-01-20 21:18:50', '2022-01-20 21:18:50'),
(126, 7, 1, 20000, 7, '2022-01-20 21:18:53', '2022-01-20 21:18:53'),
(127, 7, 1, 20000, 7, '2022-01-20 21:18:55', '2022-01-20 21:18:55'),
(128, 7, 1, 20000, 7, '2022-01-20 21:18:56', '2022-01-20 21:18:56'),
(129, 7, 1, 20000, 7, '2022-01-20 21:18:58', '2022-01-20 21:18:58'),
(130, 7, 1, 20000, 1, '2022-01-20 21:18:59', '2022-01-20 21:18:59'),
(131, 7, 1, 20000, 1, '2022-01-20 21:19:02', '2022-01-20 21:19:02'),
(132, 7, 1, 20000, 1, '2022-01-20 21:19:03', '2022-01-20 21:19:03'),
(133, 7, 1, 20000, 1, '2022-01-20 21:19:05', '2022-01-20 21:19:05'),
(134, 7, 1, 20000, 7, '2022-01-20 21:19:06', '2022-01-20 21:19:06'),
(135, 7, 1, 20000, 1, '2022-01-20 21:19:08', '2022-01-20 21:19:08'),
(136, 7, 1, 20000, 1, '2022-01-20 21:19:09', '2022-01-20 21:19:09'),
(137, 7, 1, 20000, 1, '2022-01-20 21:19:10', '2022-01-20 21:19:10'),
(138, 7, 1, 20000, 7, '2022-01-20 21:19:12', '2022-01-20 21:19:12'),
(139, 7, 1, 20000, 7, '2022-01-20 21:19:13', '2022-01-20 21:19:13'),
(140, 7, 1, 20000, 1, '2022-01-20 21:19:16', '2022-01-20 21:19:16'),
(141, 7, 1, 20000, 7, '2022-01-20 21:19:18', '2022-01-20 21:19:18'),
(142, 7, 1, 20000, 1, '2022-01-20 21:19:20', '2022-01-20 21:19:20'),
(143, 7, 1, 20000, 1, '2022-01-20 21:19:21', '2022-01-20 21:19:21'),
(144, 7, 1, 20000, 1, '2022-01-20 21:19:22', '2022-01-20 21:19:22'),
(145, 7, 1, 20000, 7, '2022-01-20 21:19:24', '2022-01-20 21:19:24'),
(146, 7, 1, 20000, 1, '2022-01-20 21:19:26', '2022-01-20 21:19:26'),
(147, 7, 1, 20000, 7, '2022-01-20 21:19:27', '2022-01-20 21:19:27'),
(148, 7, 1, 20000, 1, '2022-01-20 21:19:29', '2022-01-20 21:19:29'),
(149, 7, 1, 20000, 7, '2022-01-20 21:19:30', '2022-01-20 21:19:30'),
(150, 7, 1, 20000, 7, '2022-01-20 21:19:32', '2022-01-20 21:19:32'),
(151, 7, 1, 20000, 7, '2022-01-20 21:19:33', '2022-01-20 21:19:33'),
(152, 7, 1, 20000, 7, '2022-01-20 21:19:36', '2022-01-20 21:19:36'),
(153, 7, 1, 20000, 7, '2022-01-20 21:19:38', '2022-01-20 21:19:38'),
(154, 7, 1, 20000, 1, '2022-01-20 21:19:39', '2022-01-20 21:19:39'),
(155, 7, 1, 20000, 1, '2022-01-20 21:19:40', '2022-01-20 21:19:40'),
(156, 7, 1, 20000, 7, '2022-01-20 21:19:42', '2022-01-20 21:19:42'),
(157, 7, 1, 20000, 7, '2022-01-20 21:19:45', '2022-01-20 21:19:45'),
(158, 7, 1, 20000, 7, '2022-01-20 21:19:46', '2022-01-20 21:19:46'),
(159, 7, 1, 20000, 1, '2022-01-20 21:19:48', '2022-01-20 21:19:48'),
(160, 7, 1, 20000, 1, '2022-01-20 21:19:50', '2022-01-20 21:19:50'),
(161, 7, 1, 20000, 7, '2022-01-20 21:19:51', '2022-01-20 21:19:51'),
(162, 7, 1, 20000, 7, '2022-01-20 21:19:53', '2022-01-20 21:19:53'),
(163, 7, 1, 20000, 7, '2022-01-20 21:19:55', '2022-01-20 21:19:55'),
(164, 7, 1, 20000, 1, '2022-01-20 21:19:57', '2022-01-20 21:19:57'),
(165, 7, 1, 20000, 1, '2022-01-20 21:19:59', '2022-01-20 21:19:59'),
(166, 7, 1, 20000, 1, '2022-01-20 21:20:01', '2022-01-20 21:20:01'),
(167, 7, 1, 20000, 1, '2022-01-20 21:20:02', '2022-01-20 21:20:02'),
(168, 7, 1, 20000, 7, '2022-01-20 21:20:04', '2022-01-20 21:20:04'),
(169, 7, 1, 20000, 1, '2022-01-20 21:20:05', '2022-01-20 21:20:05'),
(170, 7, 1, 20000, 7, '2022-01-20 21:20:06', '2022-01-20 21:20:06'),
(171, 7, 1, 20000, 7, '2022-01-20 21:20:08', '2022-01-20 21:20:08'),
(172, 7, 1, 20000, 7, '2022-01-20 21:20:09', '2022-01-20 21:20:09'),
(173, 7, 1, 20000, 1, '2022-01-20 21:20:12', '2022-01-20 21:20:12'),
(174, 7, 1, 20000, 7, '2022-01-20 21:20:14', '2022-01-20 21:20:14'),
(175, 7, 1, 20000, 7, '2022-01-20 21:20:16', '2022-01-20 21:20:16'),
(176, 7, 1, 20000, 1, '2022-01-20 21:20:17', '2022-01-20 21:20:17'),
(177, 7, 1, 20000, 7, '2022-01-20 21:20:19', '2022-01-20 21:20:19'),
(178, 7, 1, 20000, 1, '2022-01-20 21:20:21', '2022-01-20 21:20:21'),
(179, 7, 1, 20000, 7, '2022-01-20 21:20:22', '2022-01-20 21:20:22'),
(180, 7, 1, 20000, 1, '2022-01-20 21:20:24', '2022-01-20 21:20:24'),
(181, 7, 1, 20000, 1, '2022-01-20 21:20:25', '2022-01-20 21:20:25'),
(182, 7, 1, 20000, 7, '2022-01-20 21:20:27', '2022-01-20 21:20:27'),
(183, 7, 1, 20000, 1, '2022-01-20 21:20:28', '2022-01-20 21:20:28'),
(184, 7, 1, 20000, 1, '2022-01-20 21:20:30', '2022-01-20 21:20:30'),
(185, 7, 1, 20000, 1, '2022-01-20 21:20:32', '2022-01-20 21:20:32'),
(186, 7, 1, 20000, 7, '2022-01-20 21:20:34', '2022-01-20 21:20:34'),
(187, 7, 1, 20000, 1, '2022-01-20 21:20:35', '2022-01-20 21:20:35'),
(188, 7, 1, 20000, 1, '2022-01-20 21:20:37', '2022-01-20 21:20:37'),
(189, 7, 1, 20000, 1, '2022-01-20 21:20:38', '2022-01-20 21:20:38'),
(190, 7, 1, 20000, 1, '2022-01-20 21:20:40', '2022-01-20 21:20:40'),
(191, 7, 1, 20000, 7, '2022-01-20 21:20:41', '2022-01-20 21:20:41'),
(192, 7, 1, 20000, 1, '2022-01-20 21:20:43', '2022-01-20 21:20:43'),
(193, 7, 1, 20000, 1, '2022-01-20 21:20:44', '2022-01-20 21:20:44'),
(194, 7, 1, 20000, 1, '2022-01-20 21:20:46', '2022-01-20 21:20:46'),
(195, 7, 1, 20000, 1, '2022-01-20 21:20:48', '2022-01-20 21:20:48'),
(196, 7, 1, 20000, 7, '2022-01-20 21:20:49', '2022-01-20 21:20:49'),
(197, 7, 1, 20000, 7, '2022-01-20 21:20:50', '2022-01-20 21:20:50'),
(198, 7, 1, 20000, 1, '2022-01-20 21:20:53', '2022-01-20 21:20:53'),
(199, 7, 1, 20000, 1, '2022-01-20 21:20:54', '2022-01-20 21:20:54'),
(200, 7, 1, 20000, 1, '2022-01-20 21:20:56', '2022-01-20 21:20:56'),
(201, 7, 1, 20000, 7, '2022-01-20 21:20:58', '2022-01-20 21:20:58'),
(202, 7, 1, 20000, 1, '2022-01-20 21:21:00', '2022-01-20 21:21:00'),
(203, 7, 1, 20000, 1, '2022-01-20 21:21:01', '2022-01-20 21:21:01'),
(204, 7, 1, 20000, 1, '2022-01-20 21:21:02', '2022-01-20 21:21:02'),
(205, 7, 1, 20000, 1, '2022-01-20 21:21:04', '2022-01-20 21:21:04'),
(206, 7, 1, 20000, 1, '2022-01-20 21:21:05', '2022-01-20 21:21:05'),
(207, 7, 1, 20000, 1, '2022-01-20 21:21:07', '2022-01-20 21:21:07'),
(208, 7, 1, 20000, 7, '2022-01-20 21:21:09', '2022-01-20 21:21:09'),
(209, 7, 1, 20000, 1, '2022-01-20 21:21:11', '2022-01-20 21:21:11'),
(210, 7, 1, 20000, 1, '2022-01-20 21:21:12', '2022-01-20 21:21:12'),
(211, 7, 1, 20000, 1, '2022-01-20 21:21:15', '2022-01-20 21:21:15'),
(212, 7, 1, 20000, 7, '2022-01-20 21:21:17', '2022-01-20 21:21:17'),
(213, 7, 1, 20000, 7, '2022-01-20 21:21:19', '2022-01-20 21:21:19'),
(214, 7, 1, 20000, 1, '2022-01-20 21:21:22', '2022-01-20 21:21:22'),
(215, 7, 1, 20000, 1, '2022-01-20 21:21:23', '2022-01-20 21:21:23'),
(216, 7, 1, 20000, 1, '2022-01-20 21:21:24', '2022-01-20 21:21:24'),
(217, 7, 1, 20000, 1, '2022-01-20 21:21:26', '2022-01-20 21:21:26'),
(218, 7, 1, 20000, 1, '2022-01-20 21:21:28', '2022-01-20 21:21:28'),
(219, 7, 1, 20000, 1, '2022-01-20 21:21:29', '2022-01-20 21:21:29'),
(220, 7, 1, 20000, 7, '2022-01-20 21:21:36', '2022-01-20 21:21:36'),
(221, 7, 1, 20000, 7, '2022-01-20 21:21:38', '2022-01-20 21:21:38'),
(222, 7, 1, 5000000, 7, '2022-01-20 21:22:44', '2022-01-20 21:22:44'),
(223, 7, 1, 5000000, 7, '2022-01-20 21:22:46', '2022-01-20 21:22:46'),
(224, 7, 1, 29220000, 7, '2022-01-20 21:24:16', '2022-01-20 21:24:16'),
(225, 1, 6, 20000, 1, '2023-01-12 20:34:39', '2023-01-12 20:34:39');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ranks`
--

CREATE TABLE `ranks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rank` varchar(255) NOT NULL,
  `honorPoints` int(11) NOT NULL,
  `killPoints` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `ranks`
--

INSERT INTO `ranks` (`id`, `rank`, `honorPoints`, `killPoints`) VALUES
(1, 'Empty-Suit', 0, 0),
(2, 'Delivery-Boy', 0, 0),
(3, 'Picciotto', 0, 0),
(4, 'Shoplifter', 0, 0),
(5, 'Pickpocket', 0, 0),
(6, 'Thief', 0, 10),
(7, 'Associate', 20, 10),
(8, 'Mobster', 25, 30),
(9, 'Soldier', 30, 60),
(10, 'Swindler', 35, 70),
(11, 'Assassin', 40, 80),
(12, 'Local Chief', 45, 120),
(13, 'Chief', 50, 160),
(14, 'Bruglione', 55, 220),
(15, 'Godfather', 60, 300);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ranks_old`
--

CREATE TABLE `ranks_old` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `honorPoints` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ranks_old`
--

INSERT INTO `ranks_old` (`id`, `rank`, `honorPoints`) VALUES
(2, 'Empty-Suit', 0),
(3, 'Delivery-Boy', 0),
(4, 'Picciotto', 0),
(5, 'Shoplifter', 0),
(6, 'Pickpocket', 0),
(7, 'Thief', 0),
(8, 'Associate', 20),
(9, 'Mobster', 25),
(10, 'Soldier', 30),
(11, 'Swindler', 35),
(12, 'Assassin', 40),
(13, 'Local Chief', 45),
(14, 'Chief', 50),
(15, 'Bruglione', 55),
(16, 'Godfather', 60);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `rank_adv`
--

CREATE TABLE `rank_adv` (
  `id` int(11) NOT NULL,
  `rankID` bigint(20) UNSIGNED NOT NULL,
  `crimeID` int(11) NOT NULL,
  `rankAdv` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `rank_adv`
--

INSERT INTO `rank_adv` (`id`, `rankID`, `crimeID`, `rankAdv`) VALUES
(1, 1, 1, 4),
(2, 1, 2, 4),
(3, 1, 3, 4),
(4, 1, 4, 4),
(5, 2, 1, 3),
(6, 2, 2, 3),
(7, 2, 3, 3),
(8, 2, 4, 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `reaction_report`
--

CREATE TABLE `reaction_report` (
  `id` bigint(20) NOT NULL,
  `itemID` bigint(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `content` varchar(6000) NOT NULL,
  `user` bigint(20) UNSIGNED NOT NULL,
  `repUser` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `reaction_report`
--

INSERT INTO `reaction_report` (`id`, `itemID`, `type`, `content`, `user`, `repUser`, `created_at`, `updated_at`) VALUES
(3, 1, 'topicForum', 'wqeqweqewqewqewqewqewqeqe', 1, 6, '2022-04-16 09:51:35', '2022-04-16 09:51:35'),
(4, 2, 'commentForum', 'adadasasa', 1, 6, '2022-04-16 09:53:02', '2022-04-16 09:53:02'),
(5, 12, 'message', 'test', 1, 4, '2022-04-16 19:18:28', '2022-04-16 19:18:28');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `rules`
--

CREATE TABLE `rules` (
  `id` int(11) NOT NULL,
  `transMax` bigint(20) UNSIGNED NOT NULL,
  `minCrimeCash` bigint(20) UNSIGNED NOT NULL,
  `maxCrimeCash` bigint(20) UNSIGNED NOT NULL,
  `maxFamMembers` int(10) UNSIGNED NOT NULL,
  `minRankCreateFam` bigint(20) UNSIGNED NOT NULL,
  `maxFamLeaders` int(11) UNSIGNED NOT NULL,
  `maxFamGenerals` int(11) UNSIGNED NOT NULL,
  `maxFamManagers` int(11) UNSIGNED NOT NULL,
  `maxFamRecruiters` int(11) UNSIGNED NOT NULL,
  `maxExitCost` int(3) UNSIGNED NOT NULL,
  `maxSafeCash` bigint(20) UNSIGNED NOT NULL,
  `marriageCost` bigint(20) UNSIGNED NOT NULL,
  `transCostsFamLess` float NOT NULL,
  `transCostsFamGreater` float NOT NULL,
  `transCostsPlayerLess` float NOT NULL,
  `transCostsPlayerGreater` float NOT NULL,
  `jailCosts` bigint(20) NOT NULL,
  `minBloodPrice` bigint(20) UNSIGNED NOT NULL,
  `maxBloodPrice` bigint(20) UNSIGNED NOT NULL,
  `hospitalBloodCosts` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `rules`
--

INSERT INTO `rules` (`id`, `transMax`, `minCrimeCash`, `maxCrimeCash`, `maxFamMembers`, `minRankCreateFam`, `maxFamLeaders`, `maxFamGenerals`, `maxFamManagers`, `maxFamRecruiters`, `maxExitCost`, `maxSafeCash`, `marriageCost`, `transCostsFamLess`, `transCostsFamGreater`, `transCostsPlayerLess`, `transCostsPlayerGreater`, `jailCosts`, `minBloodPrice`, `maxBloodPrice`, `hospitalBloodCosts`, `created_at`, `updated_at`) VALUES
(1, 750000, 0, 10000, 50, 2, 6, 10, 10, 10, 0, 2250000, 50000, 0.95, 0.975, 0.9, 0.95, 2000, 50, 200, 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `system_messages`
--

CREATE TABLE `system_messages` (
  `id` int(11) NOT NULL,
  `senderID` bigint(20) UNSIGNED NOT NULL,
  `recipientID` bigint(20) UNSIGNED NOT NULL,
  `subject` varchar(30) NOT NULL,
  `body` varchar(6000) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT 0,
  `seenOn` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `system_messages`
--

INSERT INTO `system_messages` (`id`, `senderID`, `recipientID`, `subject`, `body`, `timestamp`, `seen`, `seenOn`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 'Divorce', 'After many fights and many appointments at couples therapy, you have found out to get a divorce. From now on you are single', '2021-06-16 01:32:26', 1, '2021-10-30 16:46:59', NULL, NULL),
(5, 1, 4, 'Race Result (id :3)', 'You LOST the race,Zaodith won 44444444 !', '2021-10-30 22:24:18', 1, '2021-10-30 22:34:22', NULL, NULL),
(6, 1, 1, 'Race Result (id :3)', 'You WON the race,Psyken lost 44444444 !', '2021-10-30 22:24:18', 0, NULL, NULL, NULL),
(7, 1, 4, 'Race Result (id :4)', 'You LOST the race,Zaodith won 44444444 !', '2021-10-30 22:30:41', 1, '2021-10-30 22:34:30', NULL, NULL),
(8, 1, 1, 'Race Result (id :4)', 'You WON the race,Psyken lost 44444444 !', '2021-10-30 22:30:41', 0, NULL, NULL, NULL),
(9, 1, 1, 'Race Result (id :5)', 'You LOST the race,Psyken won 44444444 !', '2021-10-30 22:32:23', 0, NULL, NULL, NULL),
(10, 1, 4, 'Race Result (id :5)', 'You WON the race,Zaodith lost 44444444 !', '2021-10-30 22:32:23', 1, '2021-10-30 22:34:35', NULL, NULL),
(11, 1, 4, 'Race Result (id :6)', 'You LOST the race,Zaodith won 44444444 !', '2021-10-30 22:35:55', 0, NULL, NULL, NULL),
(12, 1, 1, 'Race Result (id :6)', 'You WON the race,Psyken lost 44444444 !', '2021-10-30 22:35:55', 0, NULL, NULL, NULL),
(13, 1, 1, 'Race Result (id :7)', 'You LOST the race,Psyken won 44444444 !', '2021-10-30 22:38:19', 0, NULL, NULL, NULL),
(14, 1, 4, 'Race Result (id :7)', 'You WON the race,Zaodith lost 44444444 !', '2021-10-30 22:38:19', 1, '2021-10-30 22:46:42', NULL, NULL),
(15, 1, 1, 'Race Result (id :9)', 'You LOST the race,Psyken won 44444444 !', '2021-10-30 22:48:05', 1, '2021-10-30 22:51:23', NULL, NULL),
(16, 1, 4, 'Race Result (id :9)', 'You WON the race,Zaodith lost 44444444 !', '2021-10-30 22:48:05', 0, NULL, NULL, NULL),
(17, 1, 1, 'Race Result (id :10)', 'You LOST the race,Psyken won 44444444 !', '2021-10-30 22:52:23', 0, NULL, NULL, NULL),
(18, 1, 4, 'Race Result (id :10)', 'You WON the race,Zaodith lost 44444444 !', '2021-10-30 22:52:23', 1, '2021-11-22 04:17:57', NULL, NULL),
(19, 1, 4, 'Race Result (id :11)', 'You LOST the race,Zaodith won 44444444 !', '2021-10-30 23:19:42', 0, NULL, NULL, NULL),
(20, 1, 1, 'Race Result (id :11)', 'You WON the race,Psyken lost 44444444 !', '2021-10-30 23:19:42', 0, NULL, NULL, NULL),
(21, 1, 1, 'Race Result (id :12)', 'You LOST the race,Psyken won 20000 !', '2021-10-31 00:31:16', 0, NULL, NULL, NULL),
(22, 1, 4, 'Race Result (id :12)', 'You WON the race,Zaodith lost 20000 !', '2021-10-31 00:31:16', 0, NULL, NULL, NULL),
(23, 1, 1, 'Race Result (id :13)', 'You LOST the race,Psyken won 20000 !', '2021-10-31 00:32:59', 0, NULL, NULL, NULL),
(24, 1, 4, 'Race Result (id :13)', 'You WON the race,Zaodith lost 20000 !', '2021-10-31 00:32:59', 0, NULL, NULL, NULL),
(25, 1, 1, 'Race Result (id :14)', 'You LOST the race,Psyken won 20000 !', '2021-10-31 00:34:58', 0, NULL, NULL, NULL),
(26, 1, 4, 'Race Result (id :14)', 'You WON the race,Zaodith lost 20000 !', '2021-10-31 00:34:58', 0, NULL, NULL, NULL),
(27, 1, 1, 'Race Result (id :15)', 'You LOST the race,Psyken won 20000 !', '2021-10-31 00:36:19', 1, '2021-11-22 03:09:26', NULL, NULL),
(28, 1, 4, 'Race Result (id :15)', 'You WON the race,Zaodith lost 20000 !', '2021-10-31 00:36:19', 0, NULL, NULL, NULL),
(29, 1, 4, 'Race Result (id :16)', 'You LOST the race,Zaodith won 20000 !', '2021-10-31 00:37:47', 0, NULL, NULL, NULL),
(30, 1, 1, 'Race Result (id :16)', 'You WON the race,Psyken lost 20000 !', '2021-10-31 00:37:47', 1, '2021-10-31 00:38:00', NULL, NULL),
(31, 1, 1, 'Race Result (id :17)', 'You LOST the race,Psyken won 20000 !', '2021-10-31 00:37:48', 1, '2021-10-31 00:37:55', NULL, NULL),
(32, 1, 4, 'Race Result (id :17)', 'You WON the race,Zaodith lost 20000 !', '2021-10-31 00:37:48', 0, NULL, NULL, NULL),
(33, 1, 4, 'Race Result (id :20)', 'You LOST the race,Zaodith won 20000 !', '2021-10-31 00:41:53', 0, NULL, NULL, NULL),
(34, 1, 1, 'Race Result (id :20)', 'You WON the race,Psyken lost 20000 !', '2021-10-31 00:41:53', 0, NULL, NULL, NULL),
(35, 1, 1, 'Race Result (id :19)', 'You LOST the race,Psyken won 20000 !', '2021-10-31 00:41:57', 0, NULL, NULL, NULL),
(36, 1, 4, 'Race Result (id :19)', 'You WON the race,Zaodith lost 20000 !', '2021-10-31 00:41:57', 0, NULL, NULL, NULL),
(37, 1, 4, 'Race Result (id :21)', 'You LOST the race,Zaodith won 20000 !', '2021-10-31 00:43:02', 0, NULL, NULL, NULL),
(38, 1, 1, 'Race Result (id :21)', 'You WON the race,Psyken lost 20000 !', '2021-10-31 00:43:02', 0, NULL, NULL, NULL),
(39, 1, 4, 'Race Result (id :22)', 'You LOST the race,Zaodith won 20000 !', '2021-10-31 00:44:26', 0, NULL, NULL, NULL),
(40, 1, 1, 'Race Result (id :22)', 'You WON the race,Psyken lost 20000 !', '2021-10-31 00:44:26', 0, NULL, NULL, NULL),
(41, 1, 4, 'Race Result (id :23)', 'You LOST the race,Zaodith won 20000 !', '2021-10-31 01:05:35', 0, NULL, NULL, NULL),
(42, 1, 1, 'Race Result (id :23)', 'You WON the race,Psyken lost 20000 !', '2021-10-31 01:05:35', 0, NULL, NULL, NULL),
(43, 1, 4, 'Race Result (id :24)', 'You LOST the race,Zaodith won 20000 !', '2021-10-31 01:05:36', 0, NULL, NULL, NULL),
(44, 1, 1, 'Race Result (id :24)', 'You WON the race,Psyken lost 20000 !', '2021-10-31 01:05:36', 0, NULL, NULL, NULL),
(45, 1, 1, 'Race Result (id :25)', 'You LOST the race,Psyken won 133314058 !', '2021-10-31 01:06:27', 1, '2021-11-16 19:47:05', NULL, NULL),
(46, 1, 4, 'Race Result (id :25)', 'You WON the race,Zaodith lost 133314058 !', '2021-10-31 01:06:27', 0, NULL, NULL, NULL),
(47, 1, 4, 'Marriage proposal', 'You have had a marriage proposal from Zaodith, Go quickly to the marriage page to accept or decline', '2021-10-31 01:08:37', 1, '2021-11-15 11:22:43', NULL, NULL),
(48, 1, 1, 'Marriage', 'Your marriage proposal with Psyken has been declined.', '2021-10-31 01:10:05', 1, '2021-11-16 02:43:38', NULL, NULL),
(49, 1, 5, 'Marriage proposal', 'You have had a marriage proposal from Zaodith, Go quickly to the marriage page to accept or decline', '2021-10-31 13:22:58', 1, '2021-11-21 01:29:38', NULL, NULL),
(50, 1, 1, 'Marriage', 'Your marriage proposal with Koffiekiphas been accepted \n\n                    Ladies and gentlemen, rise with me and let\'s raise our glasses in a toast to the bride and groom. \n\n                    Koffiekip and Zaodith, you are the one connected today! \n\n                    We wish you a life full of happiness and health! \n\n                    You may now kiss the bride!', '2021-11-15 11:49:24', 1, '2021-11-16 19:46:52', NULL, NULL),
(51, 1, 5, 'Marriage', '\n                    Ladies and gentlemen, rise with me and let\'s raise our glasses in a toast to the bride and groom. \n\n                    Koffiekip and Zaodith, you are the one connected today! \n\n                    We wish you a life full of happiness and health! \n\n                    You may now kiss the bride!', '2021-11-15 11:49:24', 1, '2021-11-21 01:30:06', NULL, NULL),
(59, 1, 1, 'Detectives', 'The detectives found Koffiekip in Czechia', NULL, 1, '2021-11-22 16:15:54', '2021-11-17 14:59:01', '2021-11-22 15:15:54'),
(60, 1, 1, 'Detectives', 'The detectives found Psyken in Luxembourg', NULL, 1, '2021-11-17 16:09:13', '2021-11-17 15:09:01', '2021-11-17 15:09:01'),
(61, 1, 1, 'Detectives', 'The detectives found Psyken in Luxembourg', NULL, 1, '2021-11-17 22:23:51', '2021-11-17 20:25:01', '2021-11-17 20:25:01'),
(62, 1, 1, 'Detectives', 'The detectives found Koffiekip in Czechia', NULL, 1, '2021-11-18 21:03:37', '2021-11-17 23:48:01', '2021-11-17 23:48:01'),
(63, 1, 1, 'Detectives', 'The detectives found Koffiekip in Czechia', NULL, 1, '2021-11-18 11:05:13', '2021-11-18 09:57:01', '2021-11-18 09:57:01'),
(64, 1, 1, 'Detectives', 'The detectives found Koffiekip in Czechia', NULL, 1, '2021-11-18 21:03:25', '2021-11-18 10:38:01', '2021-11-18 10:38:01'),
(65, 1, 1, 'Detectives', 'The detectives didn\'t found Psyken in England', NULL, 1, '2021-11-18 21:03:19', '2021-11-18 11:34:02', '2021-11-18 11:34:02'),
(66, 1, 1, 'Detectives', 'The detectives found Psyken in Luxembourg', NULL, 1, '2021-11-22 03:09:24', '2021-11-20 12:45:01', '2021-11-20 12:45:01'),
(67, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 51057', NULL, 0, NULL, '2021-11-23 12:58:05', '2021-11-23 12:58:05'),
(68, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 54294', NULL, 0, NULL, '2021-11-23 13:04:24', '2021-11-23 13:04:24'),
(69, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 59771', NULL, 0, NULL, '2021-11-23 13:10:03', '2021-11-23 13:10:03'),
(70, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 59171', NULL, 0, NULL, '2021-11-23 13:12:53', '2021-11-23 13:12:53'),
(71, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 52820', NULL, 0, NULL, '2021-11-23 13:16:46', '2021-11-23 13:16:46'),
(72, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 52710', NULL, 0, NULL, '2021-11-23 13:21:09', '2021-11-23 13:21:09'),
(73, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 50875', NULL, 0, NULL, '2021-11-23 13:22:31', '2021-11-23 13:22:31'),
(74, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 50849', NULL, 0, NULL, '2021-11-23 13:23:37', '2021-11-23 13:23:37'),
(75, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 59924', NULL, 0, NULL, '2021-11-23 13:24:23', '2021-11-23 13:24:23'),
(76, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 55934', NULL, 0, NULL, '2021-11-23 13:32:34', '2021-11-23 13:32:34'),
(77, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 57485', NULL, 0, NULL, '2021-11-23 13:33:10', '2021-11-23 13:33:10'),
(78, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 58595', NULL, 0, NULL, '2021-11-23 13:41:59', '2021-11-23 13:41:59'),
(79, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 59284', NULL, 0, NULL, '2021-11-23 13:46:07', '2021-11-23 13:46:07'),
(80, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 55339', NULL, 0, NULL, '2021-11-23 13:51:52', '2021-11-23 13:51:52'),
(81, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 54880', NULL, 0, NULL, '2021-11-23 13:53:52', '2021-11-23 13:53:52'),
(82, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 51931', NULL, 0, NULL, '2021-11-23 13:56:08', '2021-11-23 13:56:08'),
(83, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 56214', NULL, 0, NULL, '2021-11-23 13:58:00', '2021-11-23 13:58:00'),
(84, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 58547', NULL, 0, NULL, '2021-11-23 14:04:58', '2021-11-23 14:04:58'),
(85, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 52949', NULL, 0, NULL, '2021-11-23 14:07:50', '2021-11-23 14:07:50'),
(86, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 51986', NULL, 0, NULL, '2021-11-23 14:16:23', '2021-11-23 14:16:23'),
(87, 1, 4, 'Witness', 'You saw how {\"id\":5,\"name\":\"Koffiekip\",\"email\":\"koffiekip@zaoway.nl\",\"email_verified_at\":null,\"created_at\":\"2021-10-30T22:49:22.000000Z\",\"updated_at\":\"2021-10-30T22:49:22.000000Z\",\"country\":{\"userID\":5,\"countryID\":\"12\",\"updated_at\":\"2021-11-21T13:58:27.000000Z\",\"created_at\":\"2021-10-30T22:52:14.000000Z\"},\"defense\":{\"id\":4,\"userID\":\"5\",\"weapon\":\"4\",\"house\":\"11\",\"totalDefense\":\"150\",\"created_at\":null,\"updated_at\":null}} was shot dead by Zaodith.\n\n You can sell these witnesses on the marketplace with code 53206', NULL, 0, NULL, '2021-11-23 14:43:56', '2021-11-23 14:43:56'),
(88, 1, 7, 'Race Result (id :26)', 'You LOST the race,Zaodith won 20000 !', '2022-01-19 16:52:18', 0, NULL, NULL, NULL),
(89, 1, 1, 'Race Result (id :26)', 'You WON the race,Kapiteinterreur lost 20000 !', '2022-01-19 16:52:18', 0, NULL, NULL, NULL),
(90, 1, 1, 'Race Result (id :27)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:53:06', 0, NULL, NULL, NULL),
(91, 1, 7, 'Race Result (id :27)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:53:06', 0, NULL, NULL, NULL),
(92, 1, 7, 'Race Result (id :28)', 'You LOST the race,Zaodith won 2000000 !', '2022-01-19 16:54:03', 0, NULL, NULL, NULL),
(93, 1, 1, 'Race Result (id :28)', 'You WON the race,Kapiteinterreur lost 2000000 !', '2022-01-19 16:54:03', 0, NULL, NULL, NULL),
(94, 1, 7, 'Race Result (id :29)', 'You LOST the race,Zaodith won 4000000 !', '2022-01-19 16:54:22', 0, NULL, NULL, NULL),
(95, 1, 1, 'Race Result (id :29)', 'You WON the race,Kapiteinterreur lost 4000000 !', '2022-01-19 16:54:22', 0, NULL, NULL, NULL),
(96, 1, 1, 'Race Result (id :30)', 'You LOST the race,Kapiteinterreur won 8000000 !', '2022-01-19 16:54:39', 0, NULL, NULL, NULL),
(97, 1, 7, 'Race Result (id :30)', 'You WON the race,Zaodith lost 8000000 !', '2022-01-19 16:54:39', 0, NULL, NULL, NULL),
(98, 1, 7, 'Race Result (id :31)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:56:45', 0, NULL, NULL, NULL),
(99, 1, 1, 'Race Result (id :31)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:56:45', 0, NULL, NULL, NULL),
(100, 1, 1, 'Race Result (id :32)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:56:46', 0, NULL, NULL, NULL),
(101, 1, 7, 'Race Result (id :32)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:56:46', 0, NULL, NULL, NULL),
(102, 1, 1, 'Race Result (id :33)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:56:50', 0, NULL, NULL, NULL),
(103, 1, 7, 'Race Result (id :33)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:56:51', 0, NULL, NULL, NULL),
(104, 1, 7, 'Race Result (id :34)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:56:52', 0, NULL, NULL, NULL),
(105, 1, 1, 'Race Result (id :34)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:56:52', 0, NULL, NULL, NULL),
(106, 1, 7, 'Race Result (id :35)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:56:55', 0, NULL, NULL, NULL),
(107, 1, 1, 'Race Result (id :35)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:56:55', 0, NULL, NULL, NULL),
(108, 1, 7, 'Race Result (id :36)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:56:56', 0, NULL, NULL, NULL),
(109, 1, 1, 'Race Result (id :36)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:56:56', 0, NULL, NULL, NULL),
(110, 1, 1, 'Race Result (id :37)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:56:59', 0, NULL, NULL, NULL),
(111, 1, 7, 'Race Result (id :37)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:56:59', 0, NULL, NULL, NULL),
(112, 1, 7, 'Race Result (id :38)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:57:08', 0, NULL, NULL, NULL),
(113, 1, 1, 'Race Result (id :38)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:57:08', 0, NULL, NULL, NULL),
(114, 1, 1, 'Race Result (id :39)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:57:09', 0, NULL, NULL, NULL),
(115, 1, 7, 'Race Result (id :39)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:57:09', 0, NULL, NULL, NULL),
(116, 1, 7, 'Race Result (id :40)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:57:10', 0, NULL, NULL, NULL),
(117, 1, 1, 'Race Result (id :40)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:57:10', 0, NULL, NULL, NULL),
(118, 1, 7, 'Race Result (id :41)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:57:10', 0, NULL, NULL, NULL),
(119, 1, 1, 'Race Result (id :41)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:57:10', 0, NULL, NULL, NULL),
(120, 1, 1, 'Race Result (id :43)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:57:13', 0, NULL, NULL, NULL),
(121, 1, 7, 'Race Result (id :43)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:57:13', 0, NULL, NULL, NULL),
(122, 1, 1, 'Race Result (id :42)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:57:14', 0, NULL, NULL, NULL),
(123, 1, 7, 'Race Result (id :42)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:57:14', 0, NULL, NULL, NULL),
(124, 1, 1, 'Race Result (id :44)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:57:15', 0, NULL, NULL, NULL),
(125, 1, 7, 'Race Result (id :44)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:57:15', 0, NULL, NULL, NULL),
(126, 1, 7, 'Race Result (id :45)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:57:18', 0, NULL, NULL, NULL),
(127, 1, 1, 'Race Result (id :45)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:57:18', 0, NULL, NULL, NULL),
(128, 1, 7, 'Race Result (id :47)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:57:21', 0, NULL, NULL, NULL),
(129, 1, 1, 'Race Result (id :47)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:57:21', 0, NULL, NULL, NULL),
(130, 1, 7, 'Race Result (id :46)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:57:22', 0, NULL, NULL, NULL),
(131, 1, 1, 'Race Result (id :46)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:57:22', 0, NULL, NULL, NULL),
(132, 1, 7, 'Race Result (id :48)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:57:23', 0, NULL, NULL, NULL),
(133, 1, 1, 'Race Result (id :48)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:57:23', 0, NULL, NULL, NULL),
(134, 1, 7, 'Race Result (id :49)', 'You LOST the race,Zaodith won 100000 !', '2022-01-19 16:57:24', 0, NULL, NULL, NULL),
(135, 1, 1, 'Race Result (id :49)', 'You WON the race,Kapiteinterreur lost 100000 !', '2022-01-19 16:57:24', 0, NULL, NULL, NULL),
(136, 1, 1, 'Race Result (id :50)', 'You LOST the race,Kapiteinterreur won 100000 !', '2022-01-19 16:57:27', 0, NULL, NULL, NULL),
(137, 1, 7, 'Race Result (id :50)', 'You WON the race,Zaodith lost 100000 !', '2022-01-19 16:57:27', 0, NULL, NULL, NULL),
(138, 1, 1, 'Race Result (id :51)', 'You LOST the race,Kapiteinterreur won 1000000 !', '2022-01-19 16:57:45', 0, NULL, NULL, NULL),
(139, 1, 7, 'Race Result (id :51)', 'You WON the race,Zaodith lost 1000000 !', '2022-01-19 16:57:45', 0, NULL, NULL, NULL),
(140, 1, 1, 'Race Result (id :52)', 'You LOST the race,Kapiteinterreur won 1000000 !', '2022-01-19 16:57:47', 0, NULL, NULL, NULL),
(141, 1, 7, 'Race Result (id :52)', 'You WON the race,Zaodith lost 1000000 !', '2022-01-19 16:57:47', 0, NULL, NULL, NULL),
(142, 1, 1, 'Race Result (id :53)', 'You LOST the race,Kapiteinterreur won 1000000 !', '2022-01-19 16:59:08', 0, NULL, NULL, NULL),
(143, 1, 7, 'Race Result (id :53)', 'You WON the race,Zaodith lost 1000000 !', '2022-01-19 16:59:08', 0, NULL, NULL, NULL),
(144, 1, 1, 'Race Result (id :54)', 'You LOST the race,Kapiteinterreur won 1000000 !', '2022-01-19 16:59:09', 0, NULL, NULL, NULL),
(145, 1, 7, 'Race Result (id :54)', 'You WON the race,Zaodith lost 1000000 !', '2022-01-19 16:59:09', 0, NULL, NULL, NULL),
(146, 1, 7, 'Race Result (id :55)', 'You LOST the race,Zaodith won 1000000 !', '2022-01-19 16:59:16', 0, NULL, NULL, NULL),
(147, 1, 1, 'Race Result (id :55)', 'You WON the race,Kapiteinterreur lost 1000000 !', '2022-01-19 16:59:16', 0, NULL, NULL, NULL),
(148, 1, 1, 'Race Result (id :56)', 'You LOST the race,Kapiteinterreur won 1000000 !', '2022-01-19 16:59:17', 0, NULL, NULL, NULL),
(149, 1, 7, 'Race Result (id :56)', 'You WON the race,Zaodith lost 1000000 !', '2022-01-19 16:59:17', 0, NULL, NULL, NULL),
(150, 1, 1, 'Race Result (id :57)', 'You LOST the race,Kapiteinterreur won 1000000 !', '2022-01-19 16:59:23', 0, NULL, NULL, NULL),
(151, 1, 7, 'Race Result (id :57)', 'You WON the race,Zaodith lost 1000000 !', '2022-01-19 16:59:23', 0, NULL, NULL, NULL),
(152, 1, 7, 'Race Result (id :58)', 'You LOST the race,Zaodith won 1000000 !', '2022-01-19 16:59:24', 0, NULL, NULL, NULL),
(153, 1, 1, 'Race Result (id :58)', 'You WON the race,Kapiteinterreur lost 1000000 !', '2022-01-19 16:59:24', 0, NULL, NULL, NULL),
(154, 1, 1, 'Race Result (id :59)', 'You LOST the race,Kapiteinterreur won 1000000 !', '2022-01-19 16:59:26', 0, NULL, NULL, NULL),
(155, 1, 7, 'Race Result (id :59)', 'You WON the race,Zaodith lost 1000000 !', '2022-01-19 16:59:26', 0, NULL, NULL, NULL),
(156, 1, 1, 'Race Result (id :60)', 'You LOST the race,Kapiteinterreur won 1000000 !', '2022-01-19 16:59:28', 0, NULL, NULL, NULL),
(157, 1, 7, 'Race Result (id :60)', 'You WON the race,Zaodith lost 1000000 !', '2022-01-19 16:59:28', 0, NULL, NULL, NULL),
(158, 1, 7, 'Race Result (id :61)', 'You LOST the race,Zaodith won 210000 !', '2022-01-19 17:00:22', 0, NULL, NULL, NULL),
(159, 1, 1, 'Race Result (id :61)', 'You WON the race,Kapiteinterreur lost 210000 !', '2022-01-19 17:00:22', 0, NULL, NULL, NULL),
(160, 1, 1, 'Race Result (id :62)', 'You LOST the race,Kapiteinterreur won 210000 !', '2022-01-19 17:00:23', 0, NULL, NULL, NULL),
(161, 1, 7, 'Race Result (id :62)', 'You WON the race,Zaodith lost 210000 !', '2022-01-19 17:00:23', 0, NULL, NULL, NULL),
(162, 1, 7, 'Race Result (id :63)', 'You LOST the race,Zaodith won 220000 !', '2022-01-19 17:00:36', 0, NULL, NULL, NULL),
(163, 1, 1, 'Race Result (id :63)', 'You WON the race,Kapiteinterreur lost 220000 !', '2022-01-19 17:00:36', 0, NULL, NULL, NULL),
(164, 1, 7, 'Race Result (id :64)', 'You LOST the race,Zaodith won 700000 !', '2022-01-19 17:00:48', 0, NULL, NULL, NULL),
(165, 1, 1, 'Race Result (id :64)', 'You WON the race,Kapiteinterreur lost 700000 !', '2022-01-19 17:00:48', 0, NULL, NULL, NULL),
(166, 1, 7, 'Race Result (id :65)', 'You LOST the race,Zaodith won 700000 !', '2022-01-19 17:00:49', 0, NULL, NULL, NULL),
(167, 1, 1, 'Race Result (id :65)', 'You WON the race,Kapiteinterreur lost 700000 !', '2022-01-19 17:00:49', 0, NULL, NULL, NULL),
(168, 1, 1, 'Race Result (id :66)', 'You LOST the race,Kapiteinterreur won 3000000 !', '2022-01-19 17:01:03', 0, NULL, NULL, NULL),
(169, 1, 7, 'Race Result (id :66)', 'You WON the race,Zaodith lost 3000000 !', '2022-01-19 17:01:03', 0, NULL, NULL, NULL),
(170, 1, 1, 'Race Result (id :67)', 'You LOST the race,Kapiteinterreur won 3000000 !', '2022-01-19 17:01:32', 0, NULL, NULL, NULL),
(171, 1, 7, 'Race Result (id :67)', 'You WON the race,Zaodith lost 3000000 !', '2022-01-19 17:01:32', 0, NULL, NULL, NULL),
(172, 1, 1, 'Race Result (id :68)', 'You LOST the race,Kapiteinterreur won 3000000 !', '2022-01-19 17:01:33', 0, NULL, NULL, NULL),
(173, 1, 7, 'Race Result (id :68)', 'You WON the race,Zaodith lost 3000000 !', '2022-01-19 17:01:33', 0, NULL, NULL, NULL),
(174, 1, 7, 'Race Result (id :69)', 'You LOST the race,Zaodith won 20000 !', '2022-01-19 19:29:08', 0, NULL, NULL, NULL),
(175, 1, 1, 'Race Result (id :69)', 'You WON the race,Kapiteinterreur lost 20000 !', '2022-01-19 19:29:08', 0, NULL, NULL, NULL),
(176, 1, 7, 'Race Result (id :70)', 'You LOST the race,Zaodith won 20000 !', '2022-01-19 19:29:09', 0, NULL, NULL, NULL),
(177, 1, 1, 'Race Result (id :70)', 'You WON the race,Kapiteinterreur lost 20000 !', '2022-01-19 19:29:09', 0, NULL, NULL, NULL),
(178, 1, 7, 'Race Result (id :71)', 'You LOST the race,Zaodith won 20000 !', '2022-01-19 19:29:15', 0, NULL, NULL, NULL),
(179, 1, 1, 'Race Result (id :71)', 'You WON the race,Kapiteinterreur lost 20000 !', '2022-01-19 19:29:15', 0, NULL, NULL, NULL),
(180, 1, 1, 'Race Result (id :72)', 'You LOST the race,Kapiteinterreur won 20000 !', '2022-01-19 19:29:18', 1, '2022-01-19 19:35:02', NULL, '2022-01-19 18:35:02'),
(181, 1, 7, 'Race Result (id :72)', 'You WON the race,Zaodith lost 20000 !', '2022-01-19 19:29:18', 0, NULL, NULL, NULL),
(182, 1, 1, 'Race Result (id :73)', 'You LOST the race,Kapiteinterreur won 20000 !', '2022-01-19 19:29:31', 0, NULL, NULL, NULL),
(183, 1, 7, 'Race Result (id :73)', 'You WON the race,Zaodith lost 20000 !', '2022-01-19 19:29:31', 0, NULL, NULL, NULL),
(184, 1, 7, 'Race Result (id :74)', 'You LOST the race,Zaodith won 20000 !', '2022-01-19 19:29:32', 0, NULL, NULL, NULL),
(185, 1, 1, 'Race Result (id :74)', 'You WON the race,Kapiteinterreur lost 20000 !', '2022-01-19 19:29:32', 0, NULL, NULL, NULL),
(186, 1, 7, 'Race Result (id :75)', 'You LOST the race,Zaodith won 20000 !', '2022-01-19 19:32:25', 0, NULL, NULL, NULL),
(187, 1, 1, 'Race Result (id :75)', 'You WON the race,Kapiteinterreur lost 20000 !', '2022-01-19 19:32:25', 0, NULL, NULL, NULL),
(188, 1, 7, 'Race Result (id :76)', 'You LOST the race,Zaodith won 20000 !', '2022-01-19 19:32:26', 0, NULL, NULL, NULL),
(189, 1, 1, 'Race Result (id :76)', 'You WON the race,Kapiteinterreur lost 20000 !', '2022-01-19 19:32:26', 0, NULL, NULL, NULL),
(190, 1, 7, 'Race Result (id :77)', 'You LOST the race,Zaodith won 20000 !', '2022-01-19 19:36:06', 0, NULL, NULL, NULL),
(191, 1, 1, 'Race Result (id :77)', 'You WON the race,Kapiteinterreur lost 20000 !', '2022-01-19 19:36:06', 0, NULL, NULL, NULL),
(192, 1, 1, 'Race Result (id :78)', 'You LOST the race,Kapiteinterreur won 20000 !', '2022-01-19 19:36:09', 1, '2022-01-19 19:36:15', '2022-01-19 18:36:09', '2022-01-19 18:36:15'),
(193, 1, 7, 'Race Result (id :78)', 'You WON the race,Zaodith lost 20000 !', '2022-01-19 19:36:09', 0, NULL, '2022-01-19 18:36:09', '2022-01-19 18:36:09'),
(194, 1, 1, 'Race Result (id :79)', 'You LOST the race,Kapiteinterreur won 20000 !', '2022-01-19 19:47:50', 0, NULL, '2022-01-19 18:47:50', '2022-01-19 18:47:50'),
(195, 1, 1, 'Race Result (id :80)', 'You LOST the race,Kapiteinterreur won 20000 !', '2022-01-19 19:47:52', 0, NULL, '2022-01-19 18:47:52', '2022-01-19 18:47:52'),
(196, 1, 1, 'Race Result (id :81)', 'You LOST the race,Kapiteinterreur won 1950000 !', '2022-01-19 20:04:29', 0, NULL, '2022-01-19 19:04:29', '2022-01-19 19:04:29'),
(197, 1, 6, 'Buy Out Prison', 'You were bought out by Zaodith for €438.000', NULL, 0, NULL, '2022-01-20 00:07:15', '2022-01-20 00:07:15'),
(198, 1, 6, 'Buy Out Prison', 'You were bought out by Zaodith for €748.000', NULL, 0, NULL, '2022-01-20 00:09:48', '2022-01-20 00:09:48'),
(199, 1, 5, 'Buy Out Prison', 'You were bought out by Zaodith for €638.000', NULL, 0, NULL, '2022-01-20 00:10:43', '2022-01-20 00:10:43'),
(200, 1, 5, 'Buy Out Prison', 'You were bought out by Zaodith for €498.000', NULL, 0, NULL, '2022-01-20 00:11:53', '2022-01-20 00:11:53'),
(201, 1, 5, 'Buy Out Prison', 'You were bought out by Zaodith for €690.000', NULL, 0, NULL, '2022-01-20 00:29:17', '2022-01-20 00:29:17'),
(202, 1, 1, 'Race Result (id :82)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:15:38', 0, NULL, '2022-01-20 21:15:38', '2022-01-20 21:15:38'),
(203, 1, 1, 'Race Result (id :83)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:15:43', 0, NULL, '2022-01-20 21:15:43', '2022-01-20 21:15:43'),
(204, 1, 1, 'Race Result (id :84)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:15:46', 0, NULL, '2022-01-20 21:15:46', '2022-01-20 21:15:46'),
(205, 1, 1, 'Race Result (id :85)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:15:50', 0, NULL, '2022-01-20 21:15:50', '2022-01-20 21:15:50'),
(206, 1, 1, 'Race Result (id :86)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:15:53', 0, NULL, '2022-01-20 21:15:53', '2022-01-20 21:15:53'),
(207, 1, 1, 'Race Result (id :87)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:15:55', 0, NULL, '2022-01-20 21:15:55', '2022-01-20 21:15:55'),
(208, 1, 1, 'Race Result (id :88)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:15:59', 0, NULL, '2022-01-20 21:15:59', '2022-01-20 21:15:59'),
(209, 1, 1, 'Race Result (id :89)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:06', 0, NULL, '2022-01-20 21:16:06', '2022-01-20 21:16:06'),
(210, 1, 1, 'Race Result (id :90)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:08', 0, NULL, '2022-01-20 21:16:08', '2022-01-20 21:16:08'),
(211, 1, 1, 'Race Result (id :91)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:14', 0, NULL, '2022-01-20 21:16:14', '2022-01-20 21:16:14'),
(212, 1, 1, 'Race Result (id :92)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:17', 0, NULL, '2022-01-20 21:16:17', '2022-01-20 21:16:17'),
(213, 1, 1, 'Race Result (id :93)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:19', 0, NULL, '2022-01-20 21:16:19', '2022-01-20 21:16:19'),
(214, 1, 1, 'Race Result (id :94)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:21', 0, NULL, '2022-01-20 21:16:21', '2022-01-20 21:16:21'),
(215, 1, 1, 'Race Result (id :95)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:22', 0, NULL, '2022-01-20 21:16:22', '2022-01-20 21:16:22'),
(216, 1, 1, 'Race Result (id :96)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:24', 0, NULL, '2022-01-20 21:16:24', '2022-01-20 21:16:24'),
(217, 1, 1, 'Race Result (id :97)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:28', 0, NULL, '2022-01-20 21:16:28', '2022-01-20 21:16:28'),
(218, 1, 1, 'Race Result (id :98)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:30', 0, NULL, '2022-01-20 21:16:30', '2022-01-20 21:16:30'),
(219, 1, 1, 'Race Result (id :99)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:31', 0, NULL, '2022-01-20 21:16:31', '2022-01-20 21:16:31'),
(220, 1, 1, 'Race Result (id :100)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:35', 0, NULL, '2022-01-20 21:16:35', '2022-01-20 21:16:35'),
(221, 1, 1, 'Race Result (id :101)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:36', 0, NULL, '2022-01-20 21:16:36', '2022-01-20 21:16:36'),
(222, 1, 1, 'Race Result (id :102)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:38', 0, NULL, '2022-01-20 21:16:38', '2022-01-20 21:16:38'),
(223, 1, 1, 'Race Result (id :103)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:39', 0, NULL, '2022-01-20 21:16:39', '2022-01-20 21:16:39'),
(224, 1, 1, 'Race Result (id :104)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:40', 0, NULL, '2022-01-20 21:16:40', '2022-01-20 21:16:40'),
(225, 1, 1, 'Race Result (id :105)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:43', 0, NULL, '2022-01-20 21:16:43', '2022-01-20 21:16:43'),
(226, 1, 1, 'Race Result (id :106)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:44', 0, NULL, '2022-01-20 21:16:44', '2022-01-20 21:16:44'),
(227, 1, 1, 'Race Result (id :107)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:16:46', 0, NULL, '2022-01-20 21:16:46', '2022-01-20 21:16:46'),
(228, 1, 1, 'Race Result (id :108)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:16:48', 0, NULL, '2022-01-20 21:16:48', '2022-01-20 21:16:48'),
(229, 1, 1, 'Race Result (id :109)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:17:00', 0, NULL, '2022-01-20 21:17:00', '2022-01-20 21:17:00'),
(230, 1, 1, 'Race Result (id :110)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:17:02', 0, NULL, '2022-01-20 21:17:02', '2022-01-20 21:17:02'),
(231, 1, 1, 'Race Result (id :111)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:13', 0, NULL, '2022-01-20 21:17:13', '2022-01-20 21:17:13'),
(232, 1, 1, 'Race Result (id :112)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:15', 0, NULL, '2022-01-20 21:17:15', '2022-01-20 21:17:15'),
(233, 1, 1, 'Race Result (id :113)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:17:17', 0, NULL, '2022-01-20 21:17:17', '2022-01-20 21:17:17'),
(234, 1, 1, 'Race Result (id :114)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:18', 0, NULL, '2022-01-20 21:17:18', '2022-01-20 21:17:18'),
(235, 1, 1, 'Race Result (id :115)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:19', 0, NULL, '2022-01-20 21:17:19', '2022-01-20 21:17:19'),
(236, 1, 1, 'Race Result (id :116)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:20', 0, NULL, '2022-01-20 21:17:20', '2022-01-20 21:17:20'),
(237, 1, 1, 'Race Result (id :117)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:23', 0, NULL, '2022-01-20 21:17:23', '2022-01-20 21:17:23'),
(238, 1, 1, 'Race Result (id :118)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:24', 0, NULL, '2022-01-20 21:17:24', '2022-01-20 21:17:24'),
(239, 1, 1, 'Race Result (id :119)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:26', 0, NULL, '2022-01-20 21:17:26', '2022-01-20 21:17:26'),
(240, 1, 1, 'Race Result (id :120)', 'You LOST the race,Kapiteinterreur won €450.000 !', '2022-01-20 22:17:29', 0, NULL, '2022-01-20 21:17:29', '2022-01-20 21:17:29'),
(241, 1, 1, 'Race Result (id :121)', 'You WON the race,Kapiteinterreur lost €450.000 !', '2022-01-20 22:17:32', 0, NULL, '2022-01-20 21:17:32', '2022-01-20 21:17:32'),
(242, 1, 1, 'Race Result (id :122)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:18:39', 0, NULL, '2022-01-20 21:18:39', '2022-01-20 21:18:39'),
(243, 1, 1, 'Race Result (id :123)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:40', 0, NULL, '2022-01-20 21:18:40', '2022-01-20 21:18:40'),
(244, 1, 1, 'Race Result (id :124)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:41', 0, NULL, '2022-01-20 21:18:41', '2022-01-20 21:18:41'),
(245, 1, 1, 'Race Result (id :125)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:45', 0, NULL, '2022-01-20 21:18:45', '2022-01-20 21:18:45'),
(246, 1, 1, 'Race Result (id :126)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:47', 0, NULL, '2022-01-20 21:18:47', '2022-01-20 21:18:47'),
(247, 1, 1, 'Race Result (id :127)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:18:49', 0, NULL, '2022-01-20 21:18:49', '2022-01-20 21:18:49'),
(248, 1, 1, 'Race Result (id :128)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:50', 0, NULL, '2022-01-20 21:18:50', '2022-01-20 21:18:50'),
(249, 1, 1, 'Race Result (id :129)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:53', 0, NULL, '2022-01-20 21:18:53', '2022-01-20 21:18:53'),
(250, 1, 1, 'Race Result (id :130)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:55', 0, NULL, '2022-01-20 21:18:55', '2022-01-20 21:18:55'),
(251, 1, 1, 'Race Result (id :131)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:56', 0, NULL, '2022-01-20 21:18:56', '2022-01-20 21:18:56'),
(252, 1, 1, 'Race Result (id :132)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:18:58', 0, NULL, '2022-01-20 21:18:58', '2022-01-20 21:18:58'),
(253, 1, 1, 'Race Result (id :133)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:18:59', 0, NULL, '2022-01-20 21:18:59', '2022-01-20 21:18:59'),
(254, 1, 1, 'Race Result (id :134)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:02', 0, NULL, '2022-01-20 21:19:02', '2022-01-20 21:19:02'),
(255, 1, 1, 'Race Result (id :135)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:03', 0, NULL, '2022-01-20 21:19:03', '2022-01-20 21:19:03'),
(256, 1, 1, 'Race Result (id :136)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:05', 0, NULL, '2022-01-20 21:19:05', '2022-01-20 21:19:05'),
(257, 1, 1, 'Race Result (id :137)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:06', 0, NULL, '2022-01-20 21:19:06', '2022-01-20 21:19:06'),
(258, 1, 1, 'Race Result (id :138)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:08', 0, NULL, '2022-01-20 21:19:08', '2022-01-20 21:19:08'),
(259, 1, 1, 'Race Result (id :139)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:09', 0, NULL, '2022-01-20 21:19:09', '2022-01-20 21:19:09'),
(260, 1, 1, 'Race Result (id :140)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:10', 0, NULL, '2022-01-20 21:19:10', '2022-01-20 21:19:10'),
(261, 1, 1, 'Race Result (id :141)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:12', 0, NULL, '2022-01-20 21:19:12', '2022-01-20 21:19:12'),
(262, 1, 1, 'Race Result (id :142)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:13', 0, NULL, '2022-01-20 21:19:13', '2022-01-20 21:19:13'),
(263, 1, 1, 'Race Result (id :143)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:16', 0, NULL, '2022-01-20 21:19:16', '2022-01-20 21:19:16'),
(264, 1, 1, 'Race Result (id :144)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:18', 0, NULL, '2022-01-20 21:19:18', '2022-01-20 21:19:18'),
(265, 1, 1, 'Race Result (id :145)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:20', 0, NULL, '2022-01-20 21:19:20', '2022-01-20 21:19:20'),
(266, 1, 1, 'Race Result (id :146)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:21', 0, NULL, '2022-01-20 21:19:21', '2022-01-20 21:19:21'),
(267, 1, 1, 'Race Result (id :147)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:22', 0, NULL, '2022-01-20 21:19:22', '2022-01-20 21:19:22'),
(268, 1, 1, 'Race Result (id :148)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:24', 0, NULL, '2022-01-20 21:19:24', '2022-01-20 21:19:24'),
(269, 1, 1, 'Race Result (id :149)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:26', 0, NULL, '2022-01-20 21:19:26', '2022-01-20 21:19:26'),
(270, 1, 1, 'Race Result (id :150)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:27', 0, NULL, '2022-01-20 21:19:27', '2022-01-20 21:19:27'),
(271, 1, 1, 'Race Result (id :151)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:29', 0, NULL, '2022-01-20 21:19:29', '2022-01-20 21:19:29'),
(272, 1, 1, 'Race Result (id :152)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:30', 0, NULL, '2022-01-20 21:19:30', '2022-01-20 21:19:30'),
(273, 1, 1, 'Race Result (id :153)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:32', 0, NULL, '2022-01-20 21:19:32', '2022-01-20 21:19:32'),
(274, 1, 1, 'Race Result (id :154)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:33', 0, NULL, '2022-01-20 21:19:33', '2022-01-20 21:19:33'),
(275, 1, 1, 'Race Result (id :155)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:36', 0, NULL, '2022-01-20 21:19:36', '2022-01-20 21:19:36'),
(276, 1, 1, 'Race Result (id :156)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:38', 0, NULL, '2022-01-20 21:19:38', '2022-01-20 21:19:38'),
(277, 1, 1, 'Race Result (id :157)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:39', 0, NULL, '2022-01-20 21:19:39', '2022-01-20 21:19:39'),
(278, 1, 1, 'Race Result (id :158)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:40', 0, NULL, '2022-01-20 21:19:40', '2022-01-20 21:19:40'),
(279, 1, 1, 'Race Result (id :159)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:42', 0, NULL, '2022-01-20 21:19:42', '2022-01-20 21:19:42'),
(280, 1, 1, 'Race Result (id :160)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:45', 0, NULL, '2022-01-20 21:19:45', '2022-01-20 21:19:45'),
(281, 1, 1, 'Race Result (id :161)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:46', 0, NULL, '2022-01-20 21:19:46', '2022-01-20 21:19:46'),
(282, 1, 1, 'Race Result (id :162)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:48', 0, NULL, '2022-01-20 21:19:48', '2022-01-20 21:19:48');
INSERT INTO `system_messages` (`id`, `senderID`, `recipientID`, `subject`, `body`, `timestamp`, `seen`, `seenOn`, `created_at`, `updated_at`) VALUES
(283, 1, 1, 'Race Result (id :163)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:50', 0, NULL, '2022-01-20 21:19:50', '2022-01-20 21:19:50'),
(284, 1, 1, 'Race Result (id :164)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:51', 0, NULL, '2022-01-20 21:19:51', '2022-01-20 21:19:51'),
(285, 1, 1, 'Race Result (id :165)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:53', 0, NULL, '2022-01-20 21:19:53', '2022-01-20 21:19:53'),
(286, 1, 1, 'Race Result (id :166)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:19:55', 0, NULL, '2022-01-20 21:19:55', '2022-01-20 21:19:55'),
(287, 1, 1, 'Race Result (id :167)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:57', 0, NULL, '2022-01-20 21:19:57', '2022-01-20 21:19:57'),
(288, 1, 1, 'Race Result (id :168)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:19:59', 0, NULL, '2022-01-20 21:19:59', '2022-01-20 21:19:59'),
(289, 1, 1, 'Race Result (id :169)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:01', 0, NULL, '2022-01-20 21:20:01', '2022-01-20 21:20:01'),
(290, 1, 1, 'Race Result (id :170)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:02', 0, NULL, '2022-01-20 21:20:02', '2022-01-20 21:20:02'),
(291, 1, 1, 'Race Result (id :171)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:04', 0, NULL, '2022-01-20 21:20:04', '2022-01-20 21:20:04'),
(292, 1, 1, 'Race Result (id :172)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:05', 0, NULL, '2022-01-20 21:20:05', '2022-01-20 21:20:05'),
(293, 1, 1, 'Race Result (id :173)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:06', 0, NULL, '2022-01-20 21:20:06', '2022-01-20 21:20:06'),
(294, 1, 1, 'Race Result (id :174)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:08', 0, NULL, '2022-01-20 21:20:08', '2022-01-20 21:20:08'),
(295, 1, 1, 'Race Result (id :175)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:09', 0, NULL, '2022-01-20 21:20:09', '2022-01-20 21:20:09'),
(296, 1, 1, 'Race Result (id :176)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:12', 0, NULL, '2022-01-20 21:20:12', '2022-01-20 21:20:12'),
(297, 1, 1, 'Race Result (id :177)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:14', 0, NULL, '2022-01-20 21:20:14', '2022-01-20 21:20:14'),
(298, 1, 1, 'Race Result (id :178)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:16', 0, NULL, '2022-01-20 21:20:16', '2022-01-20 21:20:16'),
(299, 1, 1, 'Race Result (id :179)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:17', 0, NULL, '2022-01-20 21:20:17', '2022-01-20 21:20:17'),
(300, 1, 1, 'Race Result (id :180)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:19', 0, NULL, '2022-01-20 21:20:19', '2022-01-20 21:20:19'),
(301, 1, 1, 'Race Result (id :181)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:21', 0, NULL, '2022-01-20 21:20:21', '2022-01-20 21:20:21'),
(302, 1, 1, 'Race Result (id :182)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:22', 0, NULL, '2022-01-20 21:20:22', '2022-01-20 21:20:22'),
(303, 1, 1, 'Race Result (id :183)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:24', 0, NULL, '2022-01-20 21:20:24', '2022-01-20 21:20:24'),
(304, 1, 1, 'Race Result (id :184)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:25', 0, NULL, '2022-01-20 21:20:25', '2022-01-20 21:20:25'),
(305, 1, 1, 'Race Result (id :185)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:27', 0, NULL, '2022-01-20 21:20:27', '2022-01-20 21:20:27'),
(306, 1, 1, 'Race Result (id :186)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:28', 0, NULL, '2022-01-20 21:20:28', '2022-01-20 21:20:28'),
(307, 1, 1, 'Race Result (id :187)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:30', 0, NULL, '2022-01-20 21:20:30', '2022-01-20 21:20:30'),
(308, 1, 1, 'Race Result (id :188)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:32', 0, NULL, '2022-01-20 21:20:32', '2022-01-20 21:20:32'),
(309, 1, 1, 'Race Result (id :189)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:34', 0, NULL, '2022-01-20 21:20:34', '2022-01-20 21:20:34'),
(310, 1, 1, 'Race Result (id :190)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:35', 0, NULL, '2022-01-20 21:20:35', '2022-01-20 21:20:35'),
(311, 1, 1, 'Race Result (id :191)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:37', 0, NULL, '2022-01-20 21:20:37', '2022-01-20 21:20:37'),
(312, 1, 1, 'Race Result (id :192)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:38', 0, NULL, '2022-01-20 21:20:38', '2022-01-20 21:20:38'),
(313, 1, 1, 'Race Result (id :193)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:40', 0, NULL, '2022-01-20 21:20:40', '2022-01-20 21:20:40'),
(314, 1, 1, 'Race Result (id :194)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:41', 0, NULL, '2022-01-20 21:20:41', '2022-01-20 21:20:41'),
(315, 1, 1, 'Race Result (id :195)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:43', 0, NULL, '2022-01-20 21:20:43', '2022-01-20 21:20:43'),
(316, 1, 1, 'Race Result (id :196)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:44', 0, NULL, '2022-01-20 21:20:44', '2022-01-20 21:20:44'),
(317, 1, 1, 'Race Result (id :197)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:46', 0, NULL, '2022-01-20 21:20:46', '2022-01-20 21:20:46'),
(318, 1, 1, 'Race Result (id :198)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:48', 0, NULL, '2022-01-20 21:20:48', '2022-01-20 21:20:48'),
(319, 1, 1, 'Race Result (id :199)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:49', 0, NULL, '2022-01-20 21:20:49', '2022-01-20 21:20:49'),
(320, 1, 1, 'Race Result (id :200)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:50', 0, NULL, '2022-01-20 21:20:50', '2022-01-20 21:20:50'),
(321, 1, 1, 'Race Result (id :201)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:53', 0, NULL, '2022-01-20 21:20:53', '2022-01-20 21:20:53'),
(322, 1, 1, 'Race Result (id :202)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:54', 0, NULL, '2022-01-20 21:20:54', '2022-01-20 21:20:54'),
(323, 1, 1, 'Race Result (id :203)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:20:56', 0, NULL, '2022-01-20 21:20:56', '2022-01-20 21:20:56'),
(324, 1, 1, 'Race Result (id :204)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:20:58', 0, NULL, '2022-01-20 21:20:58', '2022-01-20 21:20:58'),
(325, 1, 1, 'Race Result (id :205)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:00', 0, NULL, '2022-01-20 21:21:00', '2022-01-20 21:21:00'),
(326, 1, 1, 'Race Result (id :206)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:01', 0, NULL, '2022-01-20 21:21:01', '2022-01-20 21:21:01'),
(327, 1, 1, 'Race Result (id :207)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:02', 0, NULL, '2022-01-20 21:21:02', '2022-01-20 21:21:02'),
(328, 1, 1, 'Race Result (id :208)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:04', 0, NULL, '2022-01-20 21:21:04', '2022-01-20 21:21:04'),
(329, 1, 1, 'Race Result (id :209)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:05', 0, NULL, '2022-01-20 21:21:05', '2022-01-20 21:21:05'),
(330, 1, 1, 'Race Result (id :210)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:07', 0, NULL, '2022-01-20 21:21:07', '2022-01-20 21:21:07'),
(331, 1, 1, 'Race Result (id :211)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:21:09', 0, NULL, '2022-01-20 21:21:09', '2022-01-20 21:21:09'),
(332, 1, 1, 'Race Result (id :212)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:11', 0, NULL, '2022-01-20 21:21:11', '2022-01-20 21:21:11'),
(333, 1, 1, 'Race Result (id :213)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:12', 0, NULL, '2022-01-20 21:21:12', '2022-01-20 21:21:12'),
(334, 1, 1, 'Race Result (id :214)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:15', 0, NULL, '2022-01-20 21:21:15', '2022-01-20 21:21:15'),
(335, 1, 1, 'Race Result (id :215)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:21:17', 0, NULL, '2022-01-20 21:21:17', '2022-01-20 21:21:17'),
(336, 1, 1, 'Race Result (id :216)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:21:19', 0, NULL, '2022-01-20 21:21:19', '2022-01-20 21:21:19'),
(337, 1, 1, 'Race Result (id :217)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:22', 0, NULL, '2022-01-20 21:21:22', '2022-01-20 21:21:22'),
(338, 1, 1, 'Race Result (id :218)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:23', 0, NULL, '2022-01-20 21:21:23', '2022-01-20 21:21:23'),
(339, 1, 1, 'Race Result (id :219)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:24', 0, NULL, '2022-01-20 21:21:24', '2022-01-20 21:21:24'),
(340, 1, 1, 'Race Result (id :220)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:26', 0, NULL, '2022-01-20 21:21:26', '2022-01-20 21:21:26'),
(341, 1, 1, 'Race Result (id :221)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:28', 0, NULL, '2022-01-20 21:21:28', '2022-01-20 21:21:28'),
(342, 1, 1, 'Race Result (id :222)', 'You WON the race,Kapiteinterreur lost €20.000 !', '2022-01-20 22:21:29', 0, NULL, '2022-01-20 21:21:29', '2022-01-20 21:21:29'),
(343, 1, 1, 'Race Result (id :223)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:21:36', 0, NULL, '2022-01-20 21:21:36', '2022-01-20 21:21:36'),
(344, 1, 1, 'Race Result (id :224)', 'You LOST the race,Kapiteinterreur won €20.000 !', '2022-01-20 22:21:38', 0, NULL, '2022-01-20 21:21:38', '2022-01-20 21:21:38'),
(345, 1, 1, 'Race Result (id :225)', 'You LOST the race,Kapiteinterreur won €5.000.000 !', '2022-01-20 22:22:44', 0, NULL, '2022-01-20 21:22:44', '2022-01-20 21:22:44'),
(346, 1, 1, 'Race Result (id :226)', 'You LOST the race,Kapiteinterreur won €5.000.000 !', '2022-01-20 22:22:46', 0, NULL, '2022-01-20 21:22:46', '2022-01-20 21:22:46'),
(347, 1, 1, 'Race Result (id :227)', 'You LOST the race,Kapiteinterreur won €29.220.000 !', '2022-01-20 22:24:16', 0, NULL, '2022-01-20 21:24:16', '2022-01-20 21:24:16'),
(348, 1, 5, 'Buy Out Prison', 'You were bought out of jail by Kapiteinterreur for €700000', NULL, 0, NULL, '2022-01-20 22:59:12', '2022-01-20 22:59:12'),
(349, 1, 5, 'Buy Out Prison', 'You were bought out of jail by Kapiteinterreur for €1368000', NULL, 0, NULL, '2022-01-20 23:04:38', '2022-01-20 23:04:38'),
(350, 1, 5, 'Buy Out Prison', 'You were bought out of jail by Kapiteinterreur for €562000', NULL, 0, NULL, '2022-01-20 23:05:28', '2022-01-20 23:05:28'),
(351, 1, 7, 'Buy Out Prison', 'You were bought out of jail by Zaodith for €768000', NULL, 0, NULL, '2022-01-23 20:43:36', '2022-01-23 20:43:36'),
(352, 1, 7, 'Buy Out Prison', 'You were bought out of jail by Zaodith for €1710000', NULL, 0, NULL, '2022-01-23 20:45:27', '2022-01-23 20:45:27'),
(353, 1, 7, 'Buy Out Prison', 'You were bought out of jail by Zaodith for €1504000', NULL, 0, NULL, '2022-01-23 20:47:10', '2022-01-23 20:47:10'),
(354, 1, 7, 'Buy Out Prison', 'You were bought out of jail by Zaodith for €1400000', NULL, 0, NULL, '2022-01-23 20:48:02', '2022-01-23 20:48:02'),
(355, 1, 7, 'Buy Out Prison', 'You were bought out of jail by Zaodith for €1210000', NULL, 0, NULL, '2022-01-23 20:49:37', '2022-01-23 20:49:37'),
(356, 1, 1, 'Buy Out Prison', 'You were bought out of jail by Kapiteinterreur for €702000', NULL, 0, NULL, '2022-01-24 00:29:59', '2022-01-24 00:29:59'),
(357, 1, 1, 'Buy Out Prison', 'You were bought out of jail by Kapiteinterreur for €558000', NULL, 0, NULL, '2022-01-24 00:31:15', '2022-01-24 00:31:15'),
(358, 1, 1, 'Buy Out Prison', 'You were bought out of jail by Kapiteinterreur for €316000', NULL, 0, NULL, '2022-01-24 00:33:12', '2022-01-24 00:33:12'),
(359, 1, 1, 'Detectives', 'The detectives found Psyken in Spain', NULL, 0, NULL, '2022-04-11 17:54:01', '2022-04-11 17:54:01'),
(360, 1, 1, 'Detectives', 'The detectives found Koffiekip in Italy', NULL, 1, '2022-04-11 20:15:28', '2022-04-11 18:15:14', '2022-04-11 18:15:28'),
(361, 1, 1, 'Detectives', 'The detectives found Koffiekip in Italy', NULL, 1, '2022-04-11 20:19:40', '2022-04-11 18:19:25', '2022-04-11 18:19:40'),
(362, 1, 1, 'Detectives', 'The detectives found Koffiekip in Italy', NULL, 0, NULL, '2022-04-11 18:23:47', '2022-04-11 18:23:47'),
(363, 1, 1, 'Detectives', 'The detectives found Psyken in Spain', NULL, 0, NULL, '2022-04-11 18:26:22', '2022-04-11 18:26:22'),
(364, 1, 1, 'Detectives', 'The detectives found Koffiekip in Italy', NULL, 0, NULL, '2022-04-11 18:28:33', '2022-04-11 18:28:33'),
(365, 1, 1, 'Detectives', 'The detectives found Psyken in Spain', NULL, 0, NULL, '2022-04-11 18:32:45', '2022-04-11 18:32:45'),
(366, 1, 1, 'Detectives', 'The detectives found Koffiekip in Italy', NULL, 0, NULL, '2022-04-11 18:38:36', '2022-04-11 18:38:36'),
(367, 1, 1, 'Detectives', 'The detectives found Psyken in Spain', NULL, 0, NULL, '2022-04-11 18:54:59', '2022-04-11 18:54:59'),
(370, 1, 6, 'Race Result (id :228)', 'You LOST the race,Zaodith won €20.000 !', '2023-01-12 21:34:39', 0, NULL, '2023-01-12 20:34:39', '2023-01-12 20:34:39'),
(371, 1, 1, 'Race Result (id :228)', 'You WON the race,Akinator lost €20.000 !', '2023-01-12 21:34:39', 0, NULL, '2023-01-12 20:34:39', '2023-01-12 20:34:39');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `system_users`
--

CREATE TABLE `system_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `system_users`
--

INSERT INTO `system_users` (`id`, `name`) VALUES
(1, 'Notification'),
(2, 'Admin'),
(3, 'System');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Zaodith', 'zaodith@zaoway.nl', NULL, '$2y$10$5ZlSDSdk/6gSeHjxqRayqOYHYo.cYcV8brjkjfVRn0GTNoK9sB.3G', 'tfvyqKg9CIDl7RgZwv6kQrLL8nXGjOejOo3CzEZku9Z1QH0qExmJP1s4yKbP', 'https://i.pinimg.com/originals/91/f5/ce/91f5ce6e7be20ac41c088ef8fac9a776.gif', '2021-06-07 20:47:59', '2023-01-24 18:50:25'),
(4, 'Psyken', 'psyken@zaoway.nl', NULL, '$2y$10$hWtBcPe5bu/KBQxllY2CbuWo0cRLY39eqUn/3My5B1z7sRabR7VcG', NULL, '', '2021-06-17 20:10:54', '2021-06-17 20:10:54'),
(5, 'Koffiekip', 'koffiekip@zaoway.nl', NULL, '$2y$10$hWtBcPe5bu/KBQxllY2CbuWo0cRLY39eqUn/3My5B1z7sRabR7VcG', 'oq9uPldX78e7NqYtpDgWh39ttKAAp082jhPE0rdThzZknbD5pNmzh2OEddnx', 'https://www.mafiaway.nl/avatar/big/1605903231.Koffiekip.jpg', '2021-10-30 22:49:22', '2021-10-30 22:49:22'),
(6, 'Akinator', 'mickos@zaoway.nl', NULL, '$2y$10$hWtBcPe5bu/KBQxllY2CbuWo0cRLY39eqUn/3My5B1z7sRabR7VcG', 'eAlaD7cdtazfNHvjiJjpOF5HvStLdeEXEfSV3R1R5vz0rISWN45KD3W32VtW', 'https://www.mafiaway.nl/avatar/big/1643303109.Akinator.gif', '2021-11-23 15:54:38', '2021-11-23 15:54:38'),
(7, 'Supremacy', 'supremacy@zaoway.nl', NULL, '$2y$10$hWtBcPe5bu/KBQxllY2CbuWo0cRLY39eqUn/3My5B1z7sRabR7VcG', NULL, 'https://www.mafiaway.nl/avatar/big/1666117648.Supremacy.jpg', '2022-01-19 15:26:29', '2022-01-19 15:26:29');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_admin_adminrang`
--

CREATE TABLE `user_admin_adminrang` (
  `id` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `adminRangID` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_admin_adminrang`
--

INSERT INTO `user_admin_adminrang` (`id`, `userID`, `admin`, `adminRangID`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_burst`
--

CREATE TABLE `user_burst` (
  `id` bigint(20) NOT NULL,
  `prisoner` bigint(20) UNSIGNED NOT NULL,
  `burster` bigint(20) UNSIGNED NOT NULL,
  `buyout` tinyint(1) NOT NULL,
  `buyOutCash` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_burst`
--

INSERT INTO `user_burst` (`id`, `prisoner`, `burster`, `buyout`, `buyOutCash`, `created_at`, `updated_at`) VALUES
(1, 7, 1, 1, 1710000, '2022-01-23 20:45:27', '2022-01-23 20:45:27'),
(2, 7, 1, 1, 1504000, '2022-01-23 20:47:10', '2022-01-23 20:47:10'),
(3, 7, 1, 1, 1400000, '2022-01-23 20:48:02', '2022-01-23 20:48:02'),
(4, 7, 1, 1, 1210000, '2022-01-23 20:49:37', '2022-01-23 20:49:37'),
(5, 1, 7, 1, 558000, '2022-01-24 00:31:15', '2022-01-24 00:31:15'),
(6, 1, 7, 1, 316000, '2022-01-24 00:33:12', '2022-01-24 00:33:12');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_cash`
--

CREATE TABLE `user_cash` (
  `id` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `cash` bigint(255) UNSIGNED NOT NULL,
  `bank` bigint(255) UNSIGNED NOT NULL,
  `safe` bigint(255) UNSIGNED NOT NULL,
  `time` datetime(3) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_cash`
--

INSERT INTO `user_cash` (`id`, `userID`, `cash`, `bank`, `safe`, `time`, `updated_at`) VALUES
(1, 1, 0, 1778988385973226, 0, '2021-06-11 00:06:37.000', '2023-01-23 20:19:00'),
(3, 4, 0, 0, 0, '2021-06-17 22:10:54.000', '2022-01-20 22:13:27'),
(4, 5, 0, 0, 0, '0000-00-00 00:00:00.000', '2022-04-11 18:00:01'),
(5, 6, 0, 929816417045160, 0, '2022-01-20 23:14:48.000', '2023-01-12 20:37:14'),
(6, 7, 0, 0, 0, '2022-01-19 16:29:23.000', '2022-05-17 18:10:22');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_clicks`
--

CREATE TABLE `user_clicks` (
  `id` bigint(255) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `playerID` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_clicks`
--

INSERT INTO `user_clicks` (`id`, `userID`, `playerID`, `created_at`, `updated_at`) VALUES
(4, 1, 1, '2022-05-05 18:58:56', '2022-05-05 18:58:56'),
(5, 1, 6, '2023-01-12 19:51:52', '2023-01-12 19:51:52'),
(6, 1, 1, '2023-01-19 20:57:48', '2023-01-19 20:57:48'),
(7, 1, 1, '2023-01-20 20:03:22', '2023-01-20 20:03:22');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_country`
--

CREATE TABLE `user_country` (
  `userID` bigint(20) UNSIGNED NOT NULL,
  `countryID` bigint(20) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_country`
--

INSERT INTO `user_country` (`userID`, `countryID`, `updated_at`, `created_at`) VALUES
(1, 5, '2022-05-18 13:50:52', NULL),
(4, 2, '2021-10-31 01:11:53', '2021-06-17 22:10:54'),
(5, 5, '2021-11-21 14:58:27', '2021-10-31 00:52:14'),
(6, 3, '2021-11-23 16:56:10', '2021-11-23 16:56:10'),
(7, 3, '2022-01-24 01:44:19', '2021-11-23 16:56:10');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_country_house`
--

CREATE TABLE `user_country_house` (
  `id` int(11) NOT NULL,
  `houseID` int(11) NOT NULL,
  `countryID` bigint(20) UNSIGNED NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_country_house`
--

INSERT INTO `user_country_house` (`id`, `houseID`, `countryID`, `userID`, `created_at`, `updated_at`) VALUES
(12, 3, 6, 4, NULL, NULL),
(17, 8, 12, 1, '2021-11-19 01:56:09', '2021-11-19 01:56:09'),
(18, 11, 3, 6, '2023-01-12 20:37:14', '2023-01-12 20:37:14');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_defense`
--

CREATE TABLE `user_defense` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `weapon` bigint(20) DEFAULT NULL,
  `house` int(11) DEFAULT NULL,
  `airplane` int(11) DEFAULT NULL,
  `totalDefense` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_defense`
--

INSERT INTO `user_defense` (`id`, `userID`, `weapon`, `house`, `airplane`, `totalDefense`, `created_at`, `updated_at`) VALUES
(2, 1, 4, 11, 6, 150, NULL, '2022-05-18 14:34:55'),
(3, 4, 4, 11, NULL, 150, NULL, NULL),
(4, 5, 4, 11, NULL, 150, NULL, NULL),
(5, 6, 4, NULL, NULL, 50, NULL, '2023-01-12 20:37:14');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_dets`
--

CREATE TABLE `user_dets` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `playerID` bigint(20) UNSIGNED NOT NULL,
  `dets` int(11) NOT NULL,
  `countryID` bigint(20) UNSIGNED DEFAULT NULL,
  `detsCountry` bigint(20) UNSIGNED NOT NULL,
  `maxTime` int(11) NOT NULL,
  `endTime` timestamp NULL DEFAULT NULL,
  `found` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `ProcessDetsID` bigint(20) UNSIGNED DEFAULT NULL,
  `DeactivateDetsID` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_dets`
--

INSERT INTO `user_dets` (`id`, `userID`, `playerID`, `dets`, `countryID`, `detsCountry`, `maxTime`, `endTime`, `found`, `active`, `ProcessDetsID`, `DeactivateDetsID`, `created_at`, `updated_at`) VALUES
(52, 1, 4, 500, 2, 15, 1, '2022-04-11 19:14:01', 1, 0, NULL, NULL, '2022-04-11 17:54:01', '2022-04-11 17:54:01'),
(53, 1, 5, 480, 5, 15, 1, '2022-04-11 19:40:14', 1, 0, NULL, NULL, '2022-04-11 18:15:14', '2022-04-11 18:15:14'),
(54, 1, 5, 500, 5, 15, 1, '2022-04-11 19:39:25', 1, 0, NULL, NULL, '2022-04-11 18:19:25', '2022-04-11 18:19:25'),
(56, 1, 4, 500, 2, 15, 1, '2022-04-11 19:46:22', 1, 0, NULL, NULL, '2022-04-11 18:26:22', '2022-04-11 18:26:22'),
(57, 1, 5, 500, 5, 15, 1, '2022-04-11 19:48:33', 1, 0, NULL, NULL, '2022-04-11 18:28:33', '2022-04-11 18:28:33'),
(58, 1, 4, 500, 2, 15, 1, '2022-04-11 19:52:45', 1, 0, NULL, NULL, '2022-04-11 18:32:45', '2022-04-11 18:32:45'),
(59, 1, 5, 500, 5, 15, 1, '2022-04-11 19:58:36', 1, 0, NULL, NULL, '2022-04-11 18:38:36', '2022-04-11 18:38:36'),
(60, 1, 4, 500, 2, 15, 1, '2022-04-11 20:14:59', 1, 0, NULL, NULL, '2022-04-11 18:54:59', '2022-04-11 18:54:59'),
(61, 1, 5, 500, 5, 15, 1, '2022-04-11 20:15:13', 1, 0, NULL, NULL, '2022-04-11 18:55:13', '2022-04-11 18:55:13'),
(66, 6, 1, 500, NULL, 15, 1, '2023-01-12 21:51:57', 0, 1, NULL, NULL, '2023-01-12 20:31:57', '2023-01-12 20:31:57');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_family_transactions`
--

CREATE TABLE `user_family_transactions` (
  `id` int(11) NOT NULL,
  `familyID` int(11) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `nettoCash` bigint(20) NOT NULL,
  `brutoCash` bigint(20) NOT NULL,
  `message` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_family_transactions`
--

INSERT INTO `user_family_transactions` (`id`, `familyID`, `userID`, `nettoCash`, `brutoCash`, `message`, `created_at`, `updated_at`) VALUES
(10, 1, 1, 123, 0, '321', NULL, NULL),
(11, 1, 1, 79112, 81140, NULL, '2021-11-16 01:09:03', '2021-11-16 01:09:03'),
(12, 1, 1, 475, 500, NULL, '2022-01-23 01:55:42', '2022-01-23 01:55:42'),
(13, 1, 1, 4275, 4500, NULL, '2022-01-23 01:56:14', '2022-01-23 01:56:14'),
(14, 1, 6, 7027785209250, 7207984830000, NULL, '2023-01-12 20:40:17', '2023-01-12 20:40:17');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_friends`
--

CREATE TABLE `user_friends` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `friendID` bigint(20) UNSIGNED NOT NULL,
  `accepted` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_friends`
--

INSERT INTO `user_friends` (`id`, `userID`, `friendID`, `accepted`, `created_at`, `updated_at`) VALUES
(4, 1, 6, 1, NULL, '2022-05-08 12:52:12'),
(7, 1, 5, 1, '2023-01-24 19:54:54', '2023-01-24 19:54:54');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_hidden`
--

CREATE TABLE `user_hidden` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `beginTime` timestamp NULL DEFAULT NULL,
  `endTime` timestamp NULL DEFAULT NULL,
  `hours` int(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_hidden`
--

INSERT INTO `user_hidden` (`id`, `userID`, `beginTime`, `endTime`, `hours`, `created_at`, `updated_at`) VALUES
(12, 1, '2021-11-15 14:02:44', '2021-11-15 15:02:44', 1, '2021-11-15 14:02:44', '2021-11-15 14:02:44'),
(13, 4, '2021-11-15 14:03:48', '2021-11-15 15:03:48', 1, '2021-11-15 14:03:48', '2021-11-15 14:03:48'),
(14, 1, '2021-11-15 21:03:20', '2021-11-15 22:03:20', 1, '2021-11-15 21:03:20', '2021-11-15 21:03:20'),
(15, 1, '2021-11-20 16:20:46', '2021-11-20 17:20:46', 1, '2021-11-20 16:20:46', '2021-11-20 16:20:46'),
(16, 1, '2021-11-21 19:56:26', '2021-11-21 20:56:26', 1, '2021-11-21 19:56:26', '2021-11-21 19:56:26'),
(17, 1, '2022-01-19 20:20:36', '2022-01-19 20:20:36', 1, '2022-01-19 20:20:36', '2022-01-19 20:20:36'),
(18, 1, '2022-01-19 20:37:25', '2022-01-19 21:37:25', 1, '2022-01-19 20:37:25', '2022-01-19 20:37:25');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_jail`
--

CREATE TABLE `user_jail` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `countryID` bigint(20) UNSIGNED NOT NULL,
  `businessID` bigint(20) UNSIGNED NOT NULL,
  `reason` varchar(50) NOT NULL,
  `endTime` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_marriage`
--

CREATE TABLE `user_marriage` (
  `id` int(11) NOT NULL,
  `player1` bigint(20) UNSIGNED NOT NULL,
  `player2` bigint(20) UNSIGNED NOT NULL,
  `accepted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_marriage`
--

INSERT INTO `user_marriage` (`id`, `player1`, `player2`, `accepted`, `created_at`, `updated_at`) VALUES
(11, 1, 5, 1, '2021-10-31 12:22:58', '2021-11-15 10:49:24');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_messages`
--

CREATE TABLE `user_messages` (
  `id` int(11) NOT NULL,
  `senderID` bigint(20) UNSIGNED DEFAULT NULL,
  `systemID` bigint(20) UNSIGNED DEFAULT NULL,
  `recipientID` bigint(20) UNSIGNED NOT NULL,
  `systemMessage` tinyint(1) NOT NULL DEFAULT 0,
  `subject` varchar(30) NOT NULL,
  `body` varchar(6000) NOT NULL,
  `deletedSender` tinyint(1) DEFAULT 0,
  `deletedRecipient` tinyint(1) DEFAULT 0,
  `timestamp` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT 0,
  `seenOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_messages`
--

INSERT INTO `user_messages` (`id`, `senderID`, `systemID`, `recipientID`, `systemMessage`, `subject`, `body`, `deletedSender`, `deletedRecipient`, `timestamp`, `created_at`, `updated_at`, `seen`, `seenOn`) VALUES
(12, 4, NULL, 1, 0, 'test', 'test', NULL, 1, NULL, NULL, '2022-04-16 19:30:44', 1, '2022-05-03 22:00:48'),
(16, 4, NULL, 1, 0, 'test', 'test', 0, 1, NULL, '2021-11-22 02:51:49', '2022-04-16 19:37:14', 1, '2022-05-03 22:01:01'),
(17, 1, NULL, 1, 0, 'test', 'test', 0, 0, NULL, '2021-11-22 02:55:56', '2021-11-22 02:55:56', 1, '2023-01-05 22:22:15'),
(18, 1, NULL, 1, 0, 'test', 'test', 0, 0, NULL, '2021-11-22 02:58:07', '2021-11-22 02:58:07', 1, '2022-05-03 22:00:53'),
(19, 1, NULL, 1, 0, 'trest', 'test', 0, 0, NULL, '2021-11-22 02:59:29', '2021-11-22 02:59:29', 1, '2022-05-03 22:00:54'),
(20, 1, NULL, 1, 0, 'test', 'test', 0, 0, NULL, '2021-11-22 03:00:00', '2021-11-22 03:00:00', 1, '2022-05-03 22:00:55'),
(21, 1, NULL, 1, 0, 'test', 'sada', 0, 0, NULL, '2021-11-22 03:00:56', '2021-11-22 03:00:56', 1, '2022-05-15 17:56:10'),
(22, 5, NULL, 1, 0, 't', 't', 0, 1, NULL, NULL, '2022-04-16 19:38:45', 1, '2022-05-03 22:00:49'),
(23, 1, NULL, 4, 0, 'HOI', '<img src=\"https://i.postimg.cc/02n5s2mJ/forrest-gump.gif\" />', 0, 0, NULL, '2021-11-22 03:25:11', '2021-11-22 03:25:11', 1, '2021-11-22 12:57:09'),
(25, 1, NULL, 5, 0, 'Hoi', '<img src=\"https://i.postimg.cc/2y1PwJLT/hey-cat.gif\" />', 0, 0, NULL, '2021-11-22 12:25:04', '2021-11-22 12:25:04', 1, '2021-11-22 13:32:12'),
(26, 1, NULL, 7, 0, 'Hoi', ' <img src=\"\\images/smileys/bye.gif\" />', 0, 0, NULL, '2022-01-19 18:31:10', '2022-01-19 18:31:10', 1, '2023-01-12 20:34:08'),
(27, 1, NULL, 4, 0, 'a', '[img]https://www.mafiaway.nl/avatar/big/1637892846.Jinx.jpg[/img] \n\n\n\nik test dit even groetjes', 0, 0, NULL, '2022-04-18 19:16:01', '2022-04-18 19:16:01', 1, '2022-04-18 21:18:41'),
(28, 4, NULL, 1, 0, 'test', 'test', 0, 0, NULL, '2022-04-28 18:28:15', '2022-04-28 18:28:15', 1, '2023-01-05 22:22:14'),
(29, NULL, 1, 1, 1, 'adads', 'Test', 0, 0, NULL, '2022-04-28 19:42:04', '2022-04-28 19:42:07', 1, '2023-01-12 20:30:35'),
(30, 1, NULL, 7, 0, 'Hoi', 'Hoi gap!', 0, 0, NULL, '2023-01-12 19:31:30', '2023-01-12 19:31:30', 1, '2023-01-12 20:34:18');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_mutes`
--

CREATE TABLE `user_mutes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `player` bigint(20) UNSIGNED NOT NULL,
  `reason` text NOT NULL,
  `days` int(11) NOT NULL,
  `beginDate` timestamp NULL DEFAULT NULL,
  `endDate` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_mutes`
--

INSERT INTO `user_mutes` (`id`, `player`, `reason`, `days`, `beginDate`, `endDate`, `created_at`, `updated_at`) VALUES
(1, 1, 'Facking klootzak', 3, '2022-04-15 18:48:40', '2022-04-15 19:37:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_rank`
--

CREATE TABLE `user_rank` (
  `userID` bigint(20) UNSIGNED NOT NULL,
  `rankID` bigint(20) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_rank_logs`
--

CREATE TABLE `user_rank_logs` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `rankID` bigint(20) UNSIGNED NOT NULL,
  `adv` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_rank_logs`
--

INSERT INTO `user_rank_logs` (`id`, `userID`, `rankID`, `adv`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 0, 0, '2021-11-21 02:10:53', '2021-10-31 13:40:09'),
(3, 5, 2, 0, 0, '2021-11-20 01:47:14', '2021-10-31 13:52:47'),
(4, 4, 2, 0, 0, '2021-11-20 01:48:10', '2021-11-02 22:41:42'),
(5, 1, 1, 0, 0, '2021-11-21 02:09:04', '2021-11-17 16:10:49'),
(7, 4, 1, 0, 0, '2021-11-19 01:47:10', '2021-11-19 22:41:42'),
(8, 5, 1, 0, 0, '2021-11-19 01:46:14', '2021-10-19 12:52:47'),
(9, 1, 2, 101, 0, '2023-01-23 19:15:02', '2023-01-23 19:15:02'),
(10, 1, 1, 0, 0, '2021-11-23 04:30:00', '2021-11-23 04:30:00'),
(11, 1, 1, 0, 0, '2021-11-23 04:32:15', '2021-11-23 04:32:15'),
(12, 1, 1, 0, 0, '2021-11-23 12:58:05', '2021-11-23 12:58:05'),
(13, 1, 1, 0, 0, '2021-11-23 13:04:24', '2021-11-23 13:04:24'),
(14, 1, 1, 0, 0, '2021-11-23 13:10:03', '2021-11-23 13:10:03'),
(15, 5, 1, 0, 0, '2021-11-23 13:10:03', '2021-11-23 13:10:03'),
(16, 5, 1, 0, 0, '2021-11-23 13:12:53', '2021-11-23 13:12:53'),
(17, 5, 1, 0, 0, '2021-11-23 13:16:46', '2021-11-23 13:16:46'),
(18, 1, 1, 0, 0, '2021-11-23 13:21:09', '2021-11-23 13:21:09'),
(19, 5, 1, 0, 0, '2021-11-23 13:21:09', '2021-11-23 13:21:09'),
(20, 1, 1, 0, 0, '2021-11-23 13:22:31', '2021-11-23 13:22:31'),
(21, 5, 1, 0, 0, '2021-11-23 13:22:31', '2021-11-23 13:22:31'),
(22, 1, 1, 0, 0, '2021-11-23 13:23:37', '2021-11-23 13:23:37'),
(23, 5, 1, 0, 0, '2021-11-23 13:23:37', '2021-11-23 13:23:37'),
(24, 1, 1, 0, 0, '2021-11-23 13:24:23', '2021-11-23 13:24:23'),
(25, 5, 1, 0, 0, '2021-11-23 13:24:23', '2021-11-23 13:24:23'),
(26, 5, 1, 0, 0, '2021-11-23 13:32:34', '2021-11-23 13:32:34'),
(27, 5, 1, 0, 0, '2021-11-23 13:33:10', '2021-11-23 13:33:10'),
(28, 5, 1, 0, 0, '2021-11-23 13:41:59', '2021-11-23 13:41:59'),
(29, 5, 1, 0, 0, '2021-11-23 13:46:07', '2021-11-23 13:46:07'),
(30, 5, 1, 0, 0, '2021-11-23 13:51:52', '2021-11-23 13:51:52'),
(31, 5, 1, 0, 0, '2021-11-23 13:53:52', '2021-11-23 13:53:52'),
(32, 5, 1, 0, 0, '2021-11-23 13:56:08', '2021-11-23 13:56:08'),
(33, 5, 1, 0, 0, '2021-11-23 13:58:00', '2021-11-23 13:58:00'),
(34, 5, 1, 0, 0, '2021-11-23 14:04:58', '2021-11-23 14:04:58'),
(35, 5, 1, 0, 0, '2021-11-23 14:07:50', '2021-11-23 14:07:50'),
(36, 5, 1, 0, 0, '2021-11-23 14:16:23', '2021-11-23 14:16:23'),
(37, 1, 2, 0, 0, '2022-05-18 12:27:20', '2021-11-23 14:43:56'),
(38, 5, 2, 0, 1, '2023-01-24 19:56:45', '2021-11-23 14:43:56'),
(39, 6, 1, 0, 1, '2023-01-23 20:48:35', '0000-00-00 00:00:00'),
(40, 7, 1, 0, 0, '2021-11-21 02:10:53', '2021-10-31 13:40:09'),
(41, 1, 1, 103, 0, '2023-01-23 20:12:03', '2023-01-23 20:12:03'),
(42, 1, 2, 102, 0, '2023-01-23 20:13:40', '2023-01-23 20:13:40'),
(43, 1, 2, 102, 0, '2023-01-23 20:15:46', '2023-01-23 20:15:46'),
(44, 1, 2, 102, 0, '2023-01-23 20:19:00', '2023-01-23 20:19:00'),
(45, 1, 2, 12, 1, '2023-01-23 20:44:38', '2023-01-23 20:29:53');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_security`
--

CREATE TABLE `user_security` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `minutes` int(11) NOT NULL,
  `beginTime` datetime NOT NULL,
  `endTime` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_security`
--

INSERT INTO `user_security` (`id`, `userID`, `minutes`, `beginTime`, `endTime`, `created_at`, `updated_at`) VALUES
(1, 1, 30, '2021-11-12 17:03:32', '2021-11-12 17:33:32', NULL, NULL),
(4, 1, 10, '2021-11-12 22:55:19', '2021-11-12 23:05:19', '2021-11-12 21:45:19', '2021-11-12 21:45:19'),
(5, 4, 10, '2021-11-13 02:09:31', '2021-11-13 02:19:31', '2021-11-13 00:59:31', '2021-11-13 00:59:31'),
(8, 1, 30, '2021-11-13 02:06:19', '2021-11-13 02:36:19', '2021-11-13 01:06:19', '2021-11-13 01:06:19'),
(9, 1, 10, '2021-11-13 02:45:22', '2021-11-13 02:55:22', '2021-11-13 01:45:22', '2021-11-13 01:45:22'),
(10, 4, 30, '2021-11-19 03:01:54', '2021-11-19 03:31:54', '2021-11-19 02:01:54', '2021-11-19 02:01:54'),
(11, 1, 30, '2022-01-19 19:28:19', '2022-01-19 19:58:19', '2022-01-19 18:28:19', '2022-01-19 18:28:19'),
(12, 7, 30, '2022-01-19 19:28:34', '2022-01-19 19:58:34', '2022-01-19 18:28:34', '2022-01-19 18:28:34'),
(13, 6, 30, '2023-01-12 21:30:50', '2023-01-12 22:00:50', '2023-01-12 20:30:50', '2023-01-12 20:30:50');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_settings`
--

CREATE TABLE `user_settings` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `backfire` tinyint(1) NOT NULL DEFAULT 0,
  `backfireOption` int(11) NOT NULL DEFAULT 0,
  `defaultDets` int(11) UNSIGNED DEFAULT NULL,
  `detsCountries` int(11) DEFAULT NULL,
  `detsTime` int(11) DEFAULT NULL,
  `detsMessage` tinyint(1) DEFAULT NULL,
  `theme` varchar(4) DEFAULT NULL,
  `testament` bigint(20) UNSIGNED DEFAULT NULL,
  `raceMessages` tinyint(1) NOT NULL DEFAULT 1,
  `buyOutJail` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_settings`
--

INSERT INTO `user_settings` (`id`, `userID`, `backfire`, `backfireOption`, `defaultDets`, `detsCountries`, `detsTime`, `detsMessage`, `theme`, `testament`, `raceMessages`, `buyOutJail`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 500, NULL, NULL, NULL, NULL, 5, 1, 1, NULL, NULL),
(2, 5, 1, 2, NULL, NULL, NULL, NULL, 'dark', NULL, 1, 1, NULL, NULL),
(3, 4, 1, 2, NULL, NULL, NULL, NULL, 'dark', NULL, 1, 1, NULL, NULL),
(4, 6, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL),
(5, 7, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_stats`
--

CREATE TABLE `user_stats` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `crime` int(11) NOT NULL,
  `car` int(11) NOT NULL,
  `hostage` int(11) NOT NULL,
  `races` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_stats`
--

INSERT INTO `user_stats` (`id`, `userID`, `crime`, `car`, `hostage`, `races`, `created_at`, `updated_at`) VALUES
(1, 1, 346, 0, 0, 16, '2021-11-21 01:02:03', '2022-01-19 15:45:26'),
(2, 4, 7, 0, 0, 14, '2021-11-21 01:02:03', '2021-11-21 01:02:03'),
(3, 5, 1, 0, 0, 0, '2021-11-21 01:03:01', '2021-11-21 01:03:01'),
(4, 1, 370, 123, 343, 1, '2021-11-21 01:13:22', '2021-11-21 01:13:26'),
(5, 6, 15, 0, 0, 0, NULL, NULL),
(6, 7, 0, 0, 0, 1, '2021-11-21 01:13:22', '2021-11-21 01:13:26');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_transactions`
--

CREATE TABLE `user_transactions` (
  `id` bigint(40) NOT NULL,
  `fromUserID` bigint(20) UNSIGNED NOT NULL,
  `toUserID` bigint(20) UNSIGNED NOT NULL,
  `nettoCash` bigint(20) NOT NULL,
  `brutoCash` bigint(20) NOT NULL,
  `message` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user_transactions`
--

INSERT INTO `user_transactions` (`id`, `fromUserID`, `toUserID`, `nettoCash`, `brutoCash`, `message`, `created_at`, `updated_at`) VALUES
(20, 1, 4, 2362500, 2625000, NULL, '2022-01-18 16:29:31', '2022-01-18 16:29:35'),
(21, 1, 4, 2362500, 2623000, NULL, '2022-01-11 16:29:36', '2022-01-18 16:29:36'),
(22, 1, 4, 1800, 2000, NULL, '2022-01-18 16:31:39', '2022-01-18 16:31:39'),
(23, 1, 4, 90, 100, NULL, '2022-01-18 18:04:33', '2022-01-18 18:04:33');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `weapons`
--

CREATE TABLE `weapons` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `defense` int(11) NOT NULL,
  `costs` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `weapons`
--

INSERT INTO `weapons` (`id`, `name`, `defense`, `costs`, `created_at`, `updated_at`) VALUES
(1, 'Stanley knife', 25, 5000, '2021-11-19 00:43:28', '2021-11-19 00:43:28'),
(2, 'Shotgun', 50, 10000, '2021-11-19 00:43:28', '2021-11-19 00:43:28'),
(3, 'Chaingun', 75, 20000, '2021-11-19 00:43:28', '2021-11-19 00:43:28'),
(4, 'Sniper', 100, 40000, '2021-11-19 00:43:28', '2021-11-19 00:43:28');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `witnesses`
--

CREATE TABLE `witnesses` (
  `id` bigint(20) NOT NULL,
  `userID` bigint(20) UNSIGNED NOT NULL,
  `victimID` bigint(20) UNSIGNED NOT NULL,
  `killerID` bigint(20) UNSIGNED NOT NULL,
  `marketCode` int(11) NOT NULL,
  `validUntil` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `witnesses`
--

INSERT INTO `witnesses` (`id`, `userID`, `victimID`, `killerID`, `marketCode`, `validUntil`, `created_at`, `updated_at`) VALUES
(1, 4, 5, 1, 51057, '2021-11-24 12:58:05', '2021-11-23 12:58:05', '2021-11-23 12:58:05'),
(2, 4, 5, 1, 54294, '2021-11-24 13:04:24', '2021-11-23 13:04:24', '2021-11-23 13:04:24'),
(3, 4, 5, 1, 59771, '2021-11-24 13:10:03', '2021-11-23 13:10:03', '2021-11-23 13:10:03'),
(4, 4, 5, 1, 59171, '2021-11-24 13:12:53', '2021-11-23 13:12:53', '2021-11-23 13:12:53'),
(5, 4, 5, 1, 52820, '2021-11-24 13:16:46', '2021-11-23 13:16:46', '2021-11-23 13:16:46'),
(6, 4, 5, 1, 52710, '2021-11-24 13:21:09', '2021-11-23 13:21:09', '2021-11-23 13:21:09'),
(7, 4, 5, 1, 50875, '2021-11-24 13:22:31', '2021-11-23 13:22:31', '2021-11-23 13:22:31'),
(8, 4, 5, 1, 50849, '2021-11-24 13:23:37', '2021-11-23 13:23:37', '2021-11-23 13:23:37'),
(9, 4, 5, 1, 59924, '2021-11-24 13:24:23', '2021-11-23 13:24:23', '2021-11-23 13:24:23'),
(10, 4, 5, 1, 55934, '2021-11-24 13:32:34', '2021-11-23 13:32:34', '2021-11-23 13:32:34'),
(11, 4, 5, 1, 57485, '2021-11-24 13:33:10', '2021-11-23 13:33:10', '2021-11-23 13:33:10'),
(12, 4, 5, 1, 58595, '2021-11-24 13:41:59', '2021-11-23 13:41:59', '2021-11-23 13:41:59'),
(13, 4, 5, 1, 59284, '2021-11-24 13:46:07', '2021-11-23 13:46:07', '2021-11-23 13:46:07'),
(14, 4, 5, 1, 55339, '2021-11-24 13:51:52', '2021-11-23 13:51:52', '2021-11-23 13:51:52'),
(15, 4, 5, 1, 54880, '2021-11-24 13:53:52', '2021-11-23 13:53:52', '2021-11-23 13:53:52'),
(16, 4, 5, 1, 51931, '2021-11-24 13:56:08', '2021-11-23 13:56:08', '2021-11-23 13:56:08'),
(17, 4, 5, 1, 56214, '2021-11-24 13:58:00', '2021-11-23 13:58:00', '2021-11-23 13:58:00'),
(18, 4, 5, 1, 58547, '2021-11-24 14:04:58', '2021-11-23 14:04:58', '2021-11-23 14:04:58'),
(19, 4, 5, 1, 52949, '2021-11-24 14:07:50', '2021-11-23 14:07:50', '2021-11-23 14:07:50'),
(20, 4, 5, 1, 51986, '2021-11-24 14:16:23', '2021-11-23 14:16:23', '2021-11-23 14:16:23'),
(21, 4, 5, 1, 53206, '2021-11-24 14:43:56', '2021-11-23 14:43:56', '2021-11-23 14:43:56');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `adminrangs`
--
ALTER TABLE `adminrangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `airplanes`
--
ALTER TABLE `airplanes`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `attack_logs`
--
ALTER TABLE `attack_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player1` (`player1`),
  ADD KEY `player2` (`player2`);

--
-- Indexen voor tabel `backfires`
--
ALTER TABLE `backfires`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `backfire_kills`
--
ALTER TABLE `backfire_kills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerID` (`playerID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `weapon` (`weapon`),
  ADD KEY `playerRank` (`playerRank`),
  ADD KEY `userRank` (`userRank`);

--
-- Indexen voor tabel `burstranks`
--
ALTER TABLE `burstranks`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ownerID` (`ownerID`),
  ADD KEY `countryID` (`countryID`),
  ADD KEY `typeID` (`typeID`);

--
-- Indexen voor tabel `business_bank`
--
ALTER TABLE `business_bank`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `ownerID` (`ownerID`);

--
-- Indexen voor tabel `business_transactions`
--
ALTER TABLE `business_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `businessID` (`businessID`),
  ADD KEY `owner` (`owner`);

--
-- Indexen voor tabel `business_types`
--
ALTER TABLE `business_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `crimes`
--
ALTER TABLE `crimes`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `crime_chance`
--
ALTER TABLE `crime_chance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rankID` (`rankID`),
  ADD KEY `crimeID` (`crimeID`);

--
-- Indexen voor tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexen voor tabel `families`
--
ALTER TABLE `families`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexen voor tabel `familyforum_category`
--
ALTER TABLE `familyforum_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `familyID` (`familyID`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `familyforum_reactions`
--
ALTER TABLE `familyforum_reactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryID` (`categoryID`),
  ADD KEY `reactionID` (`reactionID`),
  ADD KEY `topicID` (`topicID`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `familyforum_topics`
--
ALTER TABLE `familyforum_topics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `familyID` (`familyID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `categoryID` (`categoryID`);

--
-- Indexen voor tabel `familyrangs`
--
ALTER TABLE `familyrangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `family_cash`
--
ALTER TABLE `family_cash`
  ADD PRIMARY KEY (`id`),
  ADD KEY `familyID` (`familyID`);

--
-- Indexen voor tabel `family_heists`
--
ALTER TABLE `family_heists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person1` (`player`),
  ADD KEY `roleP1` (`role`),
  ADD KEY `familyID` (`family`);

--
-- Indexen voor tabel `family_heists_roles`
--
ALTER TABLE `family_heists_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `family_promotion`
--
ALTER TABLE `family_promotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `familyID` (`familyID`),
  ADD KEY `rank` (`rank`);

--
-- Indexen voor tabel `family_recruiting`
--
ALTER TABLE `family_recruiting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`),
  ADD KEY `familyID` (`familyID`);

--
-- Indexen voor tabel `family_user_clicks`
--
ALTER TABLE `family_user_clicks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `familyID` (`familyID`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `family_user_rang`
--
ALTER TABLE `family_user_rang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD KEY `familyID` (`familyID`),
  ADD KEY `rangID` (`rangID`);

--
-- Indexen voor tabel `flashmessages`
--
ALTER TABLE `flashmessages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `forum_reactions`
--
ALTER TABLE `forum_reactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reactionID` (`reactionID`),
  ADD KEY `categoryID` (`categoryID`),
  ADD KEY `topicID` (`topicID`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `forum_topics`
--
ALTER TABLE `forum_topics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`),
  ADD KEY `categoryID` (`categoryID`);

--
-- Indexen voor tabel `honorpoints_user`
--
ALTER TABLE `honorpoints_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rankID` (`rankID`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `hospital`
--
ALTER TABLE `hospital`
  ADD PRIMARY KEY (`id`),
  ADD KEY `businessID` (`businessID`),
  ADD KEY `hospital_ibfk_2` (`ownerID`);

--
-- Indexen voor tabel `hospital_blood`
--
ALTER TABLE `hospital_blood`
  ADD PRIMARY KEY (`id`),
  ADD KEY `businessID` (`businessID`),
  ADD KEY `ownerID` (`ownerID`);

--
-- Indexen voor tabel `houses`
--
ALTER TABLE `houses`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexen voor tabel `kills`
--
ALTER TABLE `kills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerID` (`playerID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `weapon` (`weapon`),
  ADD KEY `playerRank` (`playerRank`),
  ADD KEY `userRank` (`userRank`);

--
-- Indexen voor tabel `market_categories`
--
ALTER TABLE `market_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `market_products`
--
ALTER TABLE `market_products`
  ADD KEY `userID` (`userID`),
  ADD KEY `productID` (`productID`);

--
-- Indexen voor tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexen voor tabel `pokers`
--
ALTER TABLE `pokers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player1` (`player1`),
  ADD KEY `player2` (`player2`),
  ADD KEY `player3` (`player3`),
  ADD KEY `player4` (`player4`);

--
-- Indexen voor tabel `poweritems`
--
ALTER TABLE `poweritems`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `burstRank` (`burstRank`);

--
-- Indexen voor tabel `races`
--
ALTER TABLE `races`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `races_completed`
--
ALTER TABLE `races_completed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `racer1_ID` (`racer1_ID`),
  ADD KEY `racer2_ID` (`racer2_ID`),
  ADD KEY `winner` (`winner`);

--
-- Indexen voor tabel `ranks`
--
ALTER TABLE `ranks`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `ranks_old`
--
ALTER TABLE `ranks_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `rank_adv`
--
ALTER TABLE `rank_adv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rankID` (`rankID`),
  ADD KEY `crimeID` (`crimeID`);

--
-- Indexen voor tabel `reaction_report`
--
ALTER TABLE `reaction_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `repUser` (`repUser`);

--
-- Indexen voor tabel `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `minRankCreateFam` (`minRankCreateFam`);

--
-- Indexen voor tabel `system_messages`
--
ALTER TABLE `system_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipientID` (`recipientID`),
  ADD KEY `senderID` (`senderID`);

--
-- Indexen voor tabel `system_users`
--
ALTER TABLE `system_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexen voor tabel `user_admin_adminrang`
--
ALTER TABLE `user_admin_adminrang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`),
  ADD KEY `adminRangID` (`adminRangID`);

--
-- Indexen voor tabel `user_burst`
--
ALTER TABLE `user_burst`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prisoner` (`prisoner`),
  ADD KEY `burster` (`burster`);

--
-- Indexen voor tabel `user_cash`
--
ALTER TABLE `user_cash`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `user_clicks`
--
ALTER TABLE `user_clicks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playerID` (`playerID`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `user_country`
--
ALTER TABLE `user_country`
  ADD UNIQUE KEY `userID` (`userID`),
  ADD KEY `countryID` (`countryID`);

--
-- Indexen voor tabel `user_country_house`
--
ALTER TABLE `user_country_house`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`),
  ADD KEY `houseID` (`houseID`),
  ADD KEY `countryID` (`countryID`);

--
-- Indexen voor tabel `user_defense`
--
ALTER TABLE `user_defense`
  ADD PRIMARY KEY (`id`),
  ADD KEY `house` (`house`),
  ADD KEY `userID` (`userID`),
  ADD KEY `weapon` (`weapon`),
  ADD KEY `airplane` (`airplane`);

--
-- Indexen voor tabel `user_dets`
--
ALTER TABLE `user_dets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`),
  ADD KEY `player` (`playerID`),
  ADD KEY `countryID` (`countryID`),
  ADD KEY `DeactivateDetsID` (`DeactivateDetsID`),
  ADD KEY `ProcessDetsID` (`ProcessDetsID`);

--
-- Indexen voor tabel `user_family_transactions`
--
ALTER TABLE `user_family_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `familyID` (`familyID`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `user_friends`
--
ALTER TABLE `user_friends`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`),
  ADD KEY `friendID` (`friendID`);

--
-- Indexen voor tabel `user_hidden`
--
ALTER TABLE `user_hidden`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `user_jail`
--
ALTER TABLE `user_jail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`),
  ADD KEY `objectID` (`businessID`),
  ADD KEY `countryID` (`countryID`);

--
-- Indexen voor tabel `user_marriage`
--
ALTER TABLE `user_marriage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player1` (`player1`),
  ADD KEY `player2` (`player2`);

--
-- Indexen voor tabel `user_messages`
--
ALTER TABLE `user_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `senderID` (`senderID`),
  ADD KEY `recipientID` (`recipientID`),
  ADD KEY `systemID` (`systemID`);

--
-- Indexen voor tabel `user_mutes`
--
ALTER TABLE `user_mutes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player` (`player`);

--
-- Indexen voor tabel `user_rank`
--
ALTER TABLE `user_rank`
  ADD UNIQUE KEY `userID` (`userID`),
  ADD KEY `rankID` (`rankID`);

--
-- Indexen voor tabel `user_rank_logs`
--
ALTER TABLE `user_rank_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`),
  ADD KEY `user_rank_logs_ibfk_2` (`rankID`);

--
-- Indexen voor tabel `user_security`
--
ALTER TABLE `user_security`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `user_settings`
--
ALTER TABLE `user_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `testament` (`testament`),
  ADD KEY `userID` (`userID`),
  ADD KEY `backfireOption` (`backfireOption`);

--
-- Indexen voor tabel `user_stats`
--
ALTER TABLE `user_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexen voor tabel `user_transactions`
--
ALTER TABLE `user_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fromUserID` (`fromUserID`),
  ADD KEY `toUserID` (`toUserID`);

--
-- Indexen voor tabel `weapons`
--
ALTER TABLE `weapons`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `witnesses`
--
ALTER TABLE `witnesses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `killerID` (`killerID`),
  ADD KEY `victimID` (`victimID`),
  ADD KEY `userID` (`userID`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `adminrangs`
--
ALTER TABLE `adminrangs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `airplanes`
--
ALTER TABLE `airplanes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `attack_logs`
--
ALTER TABLE `attack_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT voor een tabel `backfires`
--
ALTER TABLE `backfires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `backfire_kills`
--
ALTER TABLE `backfire_kills`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT voor een tabel `burstranks`
--
ALTER TABLE `burstranks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT voor een tabel `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT voor een tabel `business_transactions`
--
ALTER TABLE `business_transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT voor een tabel `business_types`
--
ALTER TABLE `business_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT voor een tabel `crimes`
--
ALTER TABLE `crimes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `crime_chance`
--
ALTER TABLE `crime_chance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT voor een tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT voor een tabel `families`
--
ALTER TABLE `families`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT voor een tabel `familyforum_category`
--
ALTER TABLE `familyforum_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `familyforum_reactions`
--
ALTER TABLE `familyforum_reactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `familyforum_topics`
--
ALTER TABLE `familyforum_topics`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `familyrangs`
--
ALTER TABLE `familyrangs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `family_cash`
--
ALTER TABLE `family_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT voor een tabel `family_heists`
--
ALTER TABLE `family_heists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `family_heists_roles`
--
ALTER TABLE `family_heists_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `family_promotion`
--
ALTER TABLE `family_promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `family_recruiting`
--
ALTER TABLE `family_recruiting`
  MODIFY `id` bigint(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT voor een tabel `family_user_clicks`
--
ALTER TABLE `family_user_clicks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT voor een tabel `family_user_rang`
--
ALTER TABLE `family_user_rang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT voor een tabel `flashmessages`
--
ALTER TABLE `flashmessages`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT voor een tabel `forum_categories`
--
ALTER TABLE `forum_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `forum_reactions`
--
ALTER TABLE `forum_reactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `forum_topics`
--
ALTER TABLE `forum_topics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `honorpoints_user`
--
ALTER TABLE `honorpoints_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `hospital`
--
ALTER TABLE `hospital`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT voor een tabel `hospital_blood`
--
ALTER TABLE `hospital_blood`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `houses`
--
ALTER TABLE `houses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT voor een tabel `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1644;

--
-- AUTO_INCREMENT voor een tabel `kills`
--
ALTER TABLE `kills`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT voor een tabel `market_categories`
--
ALTER TABLE `market_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `pokers`
--
ALTER TABLE `pokers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT voor een tabel `poweritems`
--
ALTER TABLE `poweritems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT voor een tabel `races`
--
ALTER TABLE `races`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT voor een tabel `races_completed`
--
ALTER TABLE `races_completed`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;

--
-- AUTO_INCREMENT voor een tabel `ranks`
--
ALTER TABLE `ranks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT voor een tabel `ranks_old`
--
ALTER TABLE `ranks_old`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT voor een tabel `rank_adv`
--
ALTER TABLE `rank_adv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT voor een tabel `reaction_report`
--
ALTER TABLE `reaction_report`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `rules`
--
ALTER TABLE `rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT voor een tabel `system_messages`
--
ALTER TABLE `system_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=372;

--
-- AUTO_INCREMENT voor een tabel `system_users`
--
ALTER TABLE `system_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT voor een tabel `user_admin_adminrang`
--
ALTER TABLE `user_admin_adminrang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `user_burst`
--
ALTER TABLE `user_burst`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `user_cash`
--
ALTER TABLE `user_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `user_clicks`
--
ALTER TABLE `user_clicks`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT voor een tabel `user_country_house`
--
ALTER TABLE `user_country_house`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT voor een tabel `user_defense`
--
ALTER TABLE `user_defense`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `user_dets`
--
ALTER TABLE `user_dets`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT voor een tabel `user_family_transactions`
--
ALTER TABLE `user_family_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT voor een tabel `user_friends`
--
ALTER TABLE `user_friends`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT voor een tabel `user_hidden`
--
ALTER TABLE `user_hidden`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT voor een tabel `user_jail`
--
ALTER TABLE `user_jail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT voor een tabel `user_marriage`
--
ALTER TABLE `user_marriage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT voor een tabel `user_messages`
--
ALTER TABLE `user_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT voor een tabel `user_mutes`
--
ALTER TABLE `user_mutes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT voor een tabel `user_rank_logs`
--
ALTER TABLE `user_rank_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT voor een tabel `user_security`
--
ALTER TABLE `user_security`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT voor een tabel `user_settings`
--
ALTER TABLE `user_settings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `user_stats`
--
ALTER TABLE `user_stats`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `user_transactions`
--
ALTER TABLE `user_transactions`
  MODIFY `id` bigint(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT voor een tabel `weapons`
--
ALTER TABLE `weapons`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `witnesses`
--
ALTER TABLE `witnesses`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `attack_logs`
--
ALTER TABLE `attack_logs`
  ADD CONSTRAINT `attack_logs_ibfk_1` FOREIGN KEY (`player1`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `attack_logs_ibfk_2` FOREIGN KEY (`player2`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `backfire_kills`
--
ALTER TABLE `backfire_kills`
  ADD CONSTRAINT `backfire_kills_ibfk_1` FOREIGN KEY (`playerID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `backfire_kills_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `backfire_kills_ibfk_3` FOREIGN KEY (`weapon`) REFERENCES `weapons` (`id`),
  ADD CONSTRAINT `backfire_kills_ibfk_4` FOREIGN KEY (`playerRank`) REFERENCES `ranks` (`id`),
  ADD CONSTRAINT `backfire_kills_ibfk_5` FOREIGN KEY (`userRank`) REFERENCES `ranks` (`id`);

--
-- Beperkingen voor tabel `businesses`
--
ALTER TABLE `businesses`
  ADD CONSTRAINT `businesses_ibfk_1` FOREIGN KEY (`ownerID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `businesses_ibfk_2` FOREIGN KEY (`countryID`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `businesses_ibfk_3` FOREIGN KEY (`typeID`) REFERENCES `business_types` (`id`);

--
-- Beperkingen voor tabel `business_bank`
--
ALTER TABLE `business_bank`
  ADD CONSTRAINT `business_bank_ibfk_1` FOREIGN KEY (`id`) REFERENCES `businesses` (`id`),
  ADD CONSTRAINT `business_bank_ibfk_2` FOREIGN KEY (`ownerID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `business_transactions`
--
ALTER TABLE `business_transactions`
  ADD CONSTRAINT `business_transactions_ibfk_1` FOREIGN KEY (`businessID`) REFERENCES `businesses` (`id`),
  ADD CONSTRAINT `business_transactions_ibfk_2` FOREIGN KEY (`owner`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `crime_chance`
--
ALTER TABLE `crime_chance`
  ADD CONSTRAINT `crime_chance_ibfk_2` FOREIGN KEY (`rankID`) REFERENCES `ranks` (`id`),
  ADD CONSTRAINT `crime_chance_ibfk_3` FOREIGN KEY (`crimeID`) REFERENCES `crimes` (`id`);

--
-- Beperkingen voor tabel `familyforum_category`
--
ALTER TABLE `familyforum_category`
  ADD CONSTRAINT `familyForum_category_ibfk_1` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`),
  ADD CONSTRAINT `familyForum_category_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `familyforum_reactions`
--
ALTER TABLE `familyforum_reactions`
  ADD CONSTRAINT `familyForum_reactions_ibfk_1` FOREIGN KEY (`categoryID`) REFERENCES `familyforum_category` (`id`),
  ADD CONSTRAINT `familyForum_reactions_ibfk_2` FOREIGN KEY (`reactionID`) REFERENCES `familyforum_reactions` (`id`),
  ADD CONSTRAINT `familyForum_reactions_ibfk_3` FOREIGN KEY (`topicID`) REFERENCES `familyforum_topics` (`id`),
  ADD CONSTRAINT `familyForum_reactions_ibfk_4` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `familyforum_topics`
--
ALTER TABLE `familyforum_topics`
  ADD CONSTRAINT `familyForum_topics_ibfk_1` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`),
  ADD CONSTRAINT `familyForum_topics_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `familyForum_topics_ibfk_3` FOREIGN KEY (`categoryID`) REFERENCES `familyforum_category` (`id`);

--
-- Beperkingen voor tabel `family_cash`
--
ALTER TABLE `family_cash`
  ADD CONSTRAINT `family_cash_ibfk_1` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`);

--
-- Beperkingen voor tabel `family_heists`
--
ALTER TABLE `family_heists`
  ADD CONSTRAINT `family_heists_ibfk_1` FOREIGN KEY (`player`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `family_heists_ibfk_2` FOREIGN KEY (`role`) REFERENCES `family_heists_roles` (`id`),
  ADD CONSTRAINT `family_heists_ibfk_3` FOREIGN KEY (`family`) REFERENCES `families` (`id`);

--
-- Beperkingen voor tabel `family_promotion`
--
ALTER TABLE `family_promotion`
  ADD CONSTRAINT `family_promotion_ibfk_1` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`),
  ADD CONSTRAINT `family_promotion_ibfk_2` FOREIGN KEY (`rank`) REFERENCES `ranks` (`id`);

--
-- Beperkingen voor tabel `family_recruiting`
--
ALTER TABLE `family_recruiting`
  ADD CONSTRAINT `family_recruiting_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `family_recruiting_ibfk_2` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`);

--
-- Beperkingen voor tabel `family_user_clicks`
--
ALTER TABLE `family_user_clicks`
  ADD CONSTRAINT `family_user_clicks_ibfk_1` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `family_user_clicks_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `family_user_rang`
--
ALTER TABLE `family_user_rang`
  ADD CONSTRAINT `family_user_rang_ibfk_1` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`),
  ADD CONSTRAINT `family_user_rang_ibfk_2` FOREIGN KEY (`rangID`) REFERENCES `familyrangs` (`id`),
  ADD CONSTRAINT `family_user_rang_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `flashmessages`
--
ALTER TABLE `flashmessages`
  ADD CONSTRAINT `flashMessages_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `forum_reactions`
--
ALTER TABLE `forum_reactions`
  ADD CONSTRAINT `forum_reactions_ibfk_1` FOREIGN KEY (`reactionID`) REFERENCES `forum_reactions` (`id`),
  ADD CONSTRAINT `forum_reactions_ibfk_2` FOREIGN KEY (`categoryID`) REFERENCES `forum_categories` (`id`),
  ADD CONSTRAINT `forum_reactions_ibfk_3` FOREIGN KEY (`topicID`) REFERENCES `forum_topics` (`id`),
  ADD CONSTRAINT `forum_reactions_ibfk_4` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `forum_topics`
--
ALTER TABLE `forum_topics`
  ADD CONSTRAINT `forum_topics_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `forum_topics_ibfk_2` FOREIGN KEY (`categoryID`) REFERENCES `forum_categories` (`id`);

--
-- Beperkingen voor tabel `honorpoints_user`
--
ALTER TABLE `honorpoints_user`
  ADD CONSTRAINT `honorPoints_user_ibfk_1` FOREIGN KEY (`rankID`) REFERENCES `ranks_old` (`id`),
  ADD CONSTRAINT `honorPoints_user_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `hospital`
--
ALTER TABLE `hospital`
  ADD CONSTRAINT `hospital_ibfk_1` FOREIGN KEY (`businessID`) REFERENCES `businesses` (`id`),
  ADD CONSTRAINT `hospital_ibfk_2` FOREIGN KEY (`ownerID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `hospital_blood`
--
ALTER TABLE `hospital_blood`
  ADD CONSTRAINT `hospital_blood_ibfk_1` FOREIGN KEY (`businessID`) REFERENCES `businesses` (`id`),
  ADD CONSTRAINT `hospital_blood_ibfk_2` FOREIGN KEY (`ownerID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `kills`
--
ALTER TABLE `kills`
  ADD CONSTRAINT `kills_ibfk_1` FOREIGN KEY (`playerID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `kills_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `kills_ibfk_3` FOREIGN KEY (`weapon`) REFERENCES `weapons` (`id`),
  ADD CONSTRAINT `kills_ibfk_4` FOREIGN KEY (`playerRank`) REFERENCES `ranks` (`id`),
  ADD CONSTRAINT `kills_ibfk_5` FOREIGN KEY (`userRank`) REFERENCES `ranks` (`id`);

--
-- Beperkingen voor tabel `market_products`
--
ALTER TABLE `market_products`
  ADD CONSTRAINT `market_products_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `market_products_ibfk_2` FOREIGN KEY (`productID`) REFERENCES `market_categories` (`id`);

--
-- Beperkingen voor tabel `pokers`
--
ALTER TABLE `pokers`
  ADD CONSTRAINT `pokers_ibfk_1` FOREIGN KEY (`player1`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `pokers_ibfk_2` FOREIGN KEY (`player2`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `pokers_ibfk_3` FOREIGN KEY (`player3`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `pokers_ibfk_4` FOREIGN KEY (`player4`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `profiles_ibfk_2` FOREIGN KEY (`burstRank`) REFERENCES `burstranks` (`id`);

--
-- Beperkingen voor tabel `races`
--
ALTER TABLE `races`
  ADD CONSTRAINT `races_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `races_completed`
--
ALTER TABLE `races_completed`
  ADD CONSTRAINT `races_completed_ibfk_1` FOREIGN KEY (`racer1_ID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `races_completed_ibfk_2` FOREIGN KEY (`racer2_ID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `races_completed_ibfk_3` FOREIGN KEY (`winner`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `rank_adv`
--
ALTER TABLE `rank_adv`
  ADD CONSTRAINT `rank_adv_ibfk_1` FOREIGN KEY (`rankID`) REFERENCES `ranks` (`id`),
  ADD CONSTRAINT `rank_adv_ibfk_2` FOREIGN KEY (`crimeID`) REFERENCES `crimes` (`id`);

--
-- Beperkingen voor tabel `reaction_report`
--
ALTER TABLE `reaction_report`
  ADD CONSTRAINT `reaction_report_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `reaction_report_ibfk_2` FOREIGN KEY (`repUser`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `system_messages`
--
ALTER TABLE `system_messages`
  ADD CONSTRAINT `system_messages_ibfk_1` FOREIGN KEY (`senderID`) REFERENCES `system_users` (`id`),
  ADD CONSTRAINT `system_messages_ibfk_2` FOREIGN KEY (`recipientID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_admin_adminrang`
--
ALTER TABLE `user_admin_adminrang`
  ADD CONSTRAINT `user_admin_adminrang_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_admin_adminrang_ibfk_2` FOREIGN KEY (`adminRangID`) REFERENCES `adminrangs` (`id`);

--
-- Beperkingen voor tabel `user_burst`
--
ALTER TABLE `user_burst`
  ADD CONSTRAINT `user_burst_ibfk_1` FOREIGN KEY (`prisoner`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_burst_ibfk_2` FOREIGN KEY (`burster`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_cash`
--
ALTER TABLE `user_cash`
  ADD CONSTRAINT `user_cash_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_clicks`
--
ALTER TABLE `user_clicks`
  ADD CONSTRAINT `user_clicks_ibfk_1` FOREIGN KEY (`playerID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_clicks_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_country`
--
ALTER TABLE `user_country`
  ADD CONSTRAINT `user_country_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_country_ibfk_2` FOREIGN KEY (`countryID`) REFERENCES `countries` (`id`);

--
-- Beperkingen voor tabel `user_country_house`
--
ALTER TABLE `user_country_house`
  ADD CONSTRAINT `user_country_house_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_country_house_ibfk_2` FOREIGN KEY (`houseID`) REFERENCES `houses` (`id`),
  ADD CONSTRAINT `user_country_house_ibfk_3` FOREIGN KEY (`countryID`) REFERENCES `countries` (`id`);

--
-- Beperkingen voor tabel `user_defense`
--
ALTER TABLE `user_defense`
  ADD CONSTRAINT `user_defense_ibfk_2` FOREIGN KEY (`house`) REFERENCES `houses` (`id`),
  ADD CONSTRAINT `user_defense_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_defense_ibfk_4` FOREIGN KEY (`weapon`) REFERENCES `weapons` (`id`),
  ADD CONSTRAINT `user_defense_ibfk_5` FOREIGN KEY (`airplane`) REFERENCES `airplanes` (`id`);

--
-- Beperkingen voor tabel `user_dets`
--
ALTER TABLE `user_dets`
  ADD CONSTRAINT `user_dets_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_dets_ibfk_2` FOREIGN KEY (`playerID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_dets_ibfk_3` FOREIGN KEY (`countryID`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `user_dets_ibfk_4` FOREIGN KEY (`DeactivateDetsID`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `user_dets_ibfk_5` FOREIGN KEY (`ProcessDetsID`) REFERENCES `jobs` (`id`);

--
-- Beperkingen voor tabel `user_family_transactions`
--
ALTER TABLE `user_family_transactions`
  ADD CONSTRAINT `user_family_transactions_ibfk_1` FOREIGN KEY (`familyID`) REFERENCES `families` (`id`),
  ADD CONSTRAINT `user_family_transactions_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_friends`
--
ALTER TABLE `user_friends`
  ADD CONSTRAINT `user_friends_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_friends_ibfk_2` FOREIGN KEY (`friendID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_hidden`
--
ALTER TABLE `user_hidden`
  ADD CONSTRAINT `user_hidden_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_jail`
--
ALTER TABLE `user_jail`
  ADD CONSTRAINT `user_jail_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_jail_ibfk_2` FOREIGN KEY (`businessID`) REFERENCES `businesses` (`id`),
  ADD CONSTRAINT `user_jail_ibfk_3` FOREIGN KEY (`countryID`) REFERENCES `countries` (`id`);

--
-- Beperkingen voor tabel `user_marriage`
--
ALTER TABLE `user_marriage`
  ADD CONSTRAINT `user_marriage_ibfk_1` FOREIGN KEY (`player1`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_marriage_ibfk_2` FOREIGN KEY (`player2`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_messages`
--
ALTER TABLE `user_messages`
  ADD CONSTRAINT `user_messages_ibfk_1` FOREIGN KEY (`senderID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_messages_ibfk_2` FOREIGN KEY (`recipientID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_messages_ibfk_3` FOREIGN KEY (`systemID`) REFERENCES `system_users` (`id`);

--
-- Beperkingen voor tabel `user_mutes`
--
ALTER TABLE `user_mutes`
  ADD CONSTRAINT `user_mutes_ibfk_1` FOREIGN KEY (`player`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_rank`
--
ALTER TABLE `user_rank`
  ADD CONSTRAINT `user_rank_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `profiles` (`ID`),
  ADD CONSTRAINT `user_rank_ibfk_2` FOREIGN KEY (`rankID`) REFERENCES `ranks` (`id`);

--
-- Beperkingen voor tabel `user_rank_logs`
--
ALTER TABLE `user_rank_logs`
  ADD CONSTRAINT `user_rank_logs_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_rank_logs_ibfk_2` FOREIGN KEY (`rankID`) REFERENCES `ranks` (`id`);

--
-- Beperkingen voor tabel `user_security`
--
ALTER TABLE `user_security`
  ADD CONSTRAINT `user_security_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_settings`
--
ALTER TABLE `user_settings`
  ADD CONSTRAINT `user_settings_ibfk_1` FOREIGN KEY (`testament`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_settings_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_settings_ibfk_3` FOREIGN KEY (`backfireOption`) REFERENCES `backfires` (`id`);

--
-- Beperkingen voor tabel `user_stats`
--
ALTER TABLE `user_stats`
  ADD CONSTRAINT `user_stats_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `user_transactions`
--
ALTER TABLE `user_transactions`
  ADD CONSTRAINT `user_transactions_ibfk_1` FOREIGN KEY (`fromUserID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_transactions_ibfk_2` FOREIGN KEY (`toUserID`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `witnesses`
--
ALTER TABLE `witnesses`
  ADD CONSTRAINT `witnesses_ibfk_1` FOREIGN KEY (`killerID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `witnesses_ibfk_2` FOREIGN KEY (`victimID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `witnesses_ibfk_3` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
