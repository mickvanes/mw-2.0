<!doctype html>
<html lang="en">
<html class="{{ Auth::user()->settings->theme }}" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Zaoway - Free Click-based Mafiagame')</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" nonce="{{ csp_nonce() }}">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" nonce="{{ csp_nonce() }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" nonce="{{ csp_nonce() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" nonce="{{ csp_nonce() }}" defer></script>
    <script src="{{ asset('js/menu.js') }}" nonce="{{ csp_nonce() }}" defer></script>
</head>
<body class="font-sans antialiased">
<div class="min-h-screen bg-lightSecondary dark:bg-darkSecondary">
    <!-- Page Heading -->

    <div class="flex flex-col h-screen">
        <div>
            <div class="flex space-x-4">
                <header class="bg-lightDefault text-white dark:bg-darkDefault dark:text-white shadow w-full">
                    <nav class="flex justify-between items-center bg-blue-300 p-4 text-white">
                        <div id="leftMenu" class="flex md:hidden" onclick="openLeftNav()"><i id="icon-left" class="fas fa-bars fa-lg" style="width: 20px"></i></div>
                        <div class="md:text-center md:block hidden md:w-full">Logged in as {{ Auth::user()->username }}</div>
                        <div class="md:hidden text-center text-white md:hidden text-lg font-bold leading-7" style="width: 200px;">@yield('header')</div>
                        <div class="md:hidden"><i class="fas fa-envelope md:fa-lg mr-2"></i>{{ Auth::user()->hasUnreadMessages() }}</div>
                        <div id="rightMenu" class="flex md:hidden" onclick="openRightNav()"><i id="icon-right" class="fas fa-bars fa-lg" style="width: 20px"></i></div>
                    </nav>
                    <div class="grid grid-cols-1 gap-2 md:grid-cols-3 text-center text-white pb-4 mx-auto pl-6 pr-6">

                        <div><a href="{{ route('bank') }}"><i class="fas fa-wallet  md:fa-lg mr-2"></i><span class="hidden md:inline">Cash:</span> &euro;{{ number_format(Auth::user()->cash->cash, 0, ',', '.') }} </a></div>
                        <div><a href="{{ route('bank') }}"><i class="fas fa-university  md:fa-lg mr-2"></i><span class="hidden md:inline">Bank:</span> &euro;{{ number_format(Auth::user()->cash->bank, 0, ',', '.') }}</a></div>
                        <div class="hidden md:block"><i class="fas fa-envelope md:fa-lg mr-2"></i>{{ Auth::user()->hasUnreadMessages() }} <span class="hidden md:inline">unread messages</span></div>
                    </div>
                </header>
            </div>
        </div>


        <div class="flex flex-grow">
            <div class="flex justify-between mx-auto flex-row w-full">
                <nav id="leftSideBar" class="flex basis-1/4 md:w-72 hidden md:block bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="w-3/4 md:w-full mx-auto mt-4 md:mt-0">
                        <div class="grid grid-cols-1 text-white p-0 md:p-4">
                            <form method="POST" action="{{ route('logout') }}" class="cursor-pointer">
                                @csrf
                                <a :href="route('logout')"
                                   onclick="event.preventDefault();
                                    this.closest('form').submit();">
                                    Logout
                                </a>
                            </form>
                            <hr class="mt-2 mb-2">
                            <a href="{{ route('dashboard') }}">Dashboard</a>
                            <a href="{{ route('player.allOnline') }}">All online players</a>
                            <a href="{{ route('player.all') }}">All players</a>
                            <hr class="mt-2 mb-2">
                            <a href="{{ route('bank') }}">Bank</a>
                            <a href="{{ route('logs') }}">Logs</a>
                            <a href="{{ route('forum') }}">Forum</a>
                            <hr class="mt-2 mb-2">
                            <a href="{{ route('shop') }}">Shop</a>
                            <a href="{{ route('house') }}">House</a>
                            <a href="{{ route('airport') }}">Airport</a>
                            <a href="{{ route('crime.show') }}">Crime</a>
                            <a href="{{ route('race.index') }}">Races</a>
                            <a href="{{ route('hospital') }}">Hospital</a>
                        </div>
                    </div>
                </nav>

                <main id="content" class="flex basis-1/2 flex-col w-full bg-lightSecondary dark:bg-darkSecondary overflow-x-hidden">
                    <div class="flex flex-col mx-auto mt-4 md:px-6 md:py-8 w-full">
                        @yield('content')
                    </div>
                </main>

                <nav id="rightSideBar" class="flex md:w-72 basis-1/4 hidden md:block justify-end bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="w-3/4 md:w-full mx-auto mt-4 md:mt-0">
                        <div class="grid grid-cols-1 text-white text-right p-0 md:p-4">
                            <a href="{{ route('marriage.show') }}">Marriage</a>
                            <a href="{{ route('detectives') }}">Detectives</a>
                            <a href="{{ route('kill') }}">Murder</a>
                            <a href="{{ route('inbox') }}">Inbox</a>
                            <hr class="mt-2 mb-2">
                            <a href="{{ route('settings') }}">Settings</a>
                            <hr class="mt-2 mb-2">
                            <a href="{{ route('families') }}">Family List</a>
                            @if(!Auth::user()->hasFamily())
                                <a href="{{ route('family.create') }}">Create a Family</a>
                            @else
                                <a href="{{ route('family', Auth::user()->family->familyDetails->name) }}">Family Profile</a>
                                <a href="{{ route('family.stats') }}">Family Statistics</a>
                                <a href="{{ route('family.forum') }}">Family Forum</a>
                                <a href="{{ route('family.heists') }}">Family Heist</a>
                                <a href="{{ route('family.kills') }}">Family Kills</a>
                                <a href="{{ route('family.deads') }}">Family Attacks</a>

                                @if(Auth::user()->getFamilyRang() == "Owner" || Auth::user()->getFamilyRang() == "Leader")
                                    @if(Auth::user()->getFamilyRang() == "Owner")
                                        <div class="grid grid-cols-1 mt-2">
                                            <b>OWNER MENU</b>
                                            <a href="{{ route('recruitment') }}">Recruitment</a>
                                            <a href="{{ route('familyShop') }}">Family Shop</a>
                                            <a href="{{ route('families') }}">Disband Family</a>
                                        </div>
                                    @else
                                        <div class="grid grid-cols-1 mt-2">
                                            <b>LEADER MENU</b>
                                            <a href="{{ route('recruitment') }}">Recruitment</a>
                                            <a href="{{ route('familyShop') }}">Family Shop</a>
                                            <a href="{{ route('families') }}">Leave Family</a>
                                        </div>
                                    @endif
                                @endif
                            @endif


                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


</body>
</html>
