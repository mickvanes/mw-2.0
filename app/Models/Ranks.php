<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ranks extends Model
{
    use HasFactory;

    protected $table = 'ranks';

    protected $fillable = [
        'rank',
        'rep',
        'bfRep',
        'honorPoints',
        'killPoints'
    ];

    public function ranks(){
        return $this->hasMany(Rank::class, 'rankID');
    }
}
