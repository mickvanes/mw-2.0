<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PowerLog extends Model
{
    use HasFactory;

    protected $table = 'power_log';

    protected $fillable = [
        'id',
        'userID',
        'usedClicks',
        'item',
        'addedPower',
        'amount',
        'costs',
    ];

}
