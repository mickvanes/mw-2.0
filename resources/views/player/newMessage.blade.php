@extends('layouts.default')

@section('header')
    MESSAGE
@stop

@section('content')
    <div class="mx-auto w-full md:mt-2 mb-4">
        <div class="max-w-7xl mx-auto ">
            <div class="shadow-sm md:w-2/4">
                <div class="text-white">
                    <div class="grid grid-cols-5 gap-2">
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('inbox') }}">Inbox</a></div>
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('outbox') }}">Outbox</a></div>
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault ">Blocked</div>
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault ">Archive</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                        <div x-data="{ open: false }">
                            <button class="mb-4" @click="open = ! open"> UBB's & Smileys</button>

                            <div x-show="open" class="mt-4 mb-4 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                                <div class="grid grid-rows-1 md:grid-rows-2 gap-2 rounded p-4">
                                    <div>
                                        <label class="font-semibold">UBBs:</label>
                                        <button onclick="addText('[img][/img]')">Image</button>
                                        <button onclick="addText('[B][/B]')">Bold</button>
                                        <button onclick="addText('[I][/I]')">Italic</button>
                                        <button onclick="addText('[U][/U]')">Underline</button>
                                        <button onclick="addText('[S][/S]')">Line-through</button>
                                    </div>
                                    <div>
                                        <label class="font-semibold">Smileys:</label>
                                        <button onclick="addText(':=')"><img src="{{ asset('images/smileys/applaus.gif') }}"></button>
                                        <button onclick="addText(':O')"><img src="{{ asset('images/smileys/blush.gif') }}"></button>
                                        <button onclick="addText(':$')"><img src="{{ asset('images/smileys/smiley_schamen.gif') }}"></button>
                                        <button onclick="addText(':w')"><img src="{{ asset('images/smileys/bye.gif') }}"></button>
                                        <button onclick="addText(':7')"><img src="{{ asset('images/smileys/cigar.gif') }}"></button>
                                        <button onclick="addText(':+')"><img src="{{ asset('images/smileys/clown.gif') }}"></button>
                                        <button onclick="addText(':Z')"><img src="{{ asset('images/smileys/deepsleep.gif') }}"></button>
                                        <button onclick="addText('8)7')"><img src="{{ asset('images/smileys/hammer2.gif') }}"></button>
                                        <button onclick="addText(':\')')"><img src="{{ asset('images/smileys/happytears.gif') }}"></button>
                                        <button onclick="addText(':^')"><img src="{{ asset('images/smileys/idea.gif') }}"></button>
                                        <button onclick="addText('|)')"><img src="{{ asset('images/smileys/indifferent.gif') }}"></button>
                                        <button onclick="addText('+)')"><img src="{{ asset('images/smileys/koekwaus.gif') }}"></button>
                                        <button onclick="addText(':D')"><img src="{{ asset('images/smileys/lol.gif') }}"></button>
                                        <button onclick="addText(':P')"><img src="{{ asset('images/smileys/puh.gif') }}"></button>
                                        <button onclick="addText(':r')"><img src="{{ asset('images/smileys/puke.gif') }}"></button>
                                        <button onclick="addText(':x')"><img src="{{ asset('images/smileys/silence.gif') }}"></button>
                                        <button onclick="addText('o-)')"><img src="{{ asset('images/smileys/smilebounce.gif') }}"></button>
                                        <button onclick="addText(':?')"><img src="{{ asset('images/smileys/smileconfused.gif') }}"></button>
                                        <button onclick="addText('(h)')"><img src="{{ asset('images/smileys/smilecool.gif') }}"></button>
                                        <button onclick="addText(':)')"><img src="{{ asset('images/smileys/smilehmmm.gif') }}"></button>
                                        <button onclick="addText('*)')"><img src="{{ asset('images/smileys/smilemean.gif') }}"></button>
                                        <button onclick="addText(':(')"><img src="{{ asset('images/smileys/smileredface.gif') }}"></button>
                                        <button onclick="addText(':)')"><img src="{{ asset('images/smileys/smilesmile.gif') }}"></button>
                                        <button onclick="addText('*D')"><img src="{{ asset('images/smileys/smileteeth.gif') }}"></button>
                                        <button onclick="addText(';)')"><img src="{{ asset('images/smileys/smilewink.gif') }}"></button>
                                        <button onclick="addText(')~')"><img src="{{ asset('images/smileys/smileyummie.gif') }}"></button>
                                        <button onclick="addText(':\'(')"><img src="{{ asset('images/smileys/tears.gif') }}"></button>
                                        <button onclick="addText('_O_')"><img src="{{ asset('images/smileys/worship.gif') }}"></button>
                                        <button onclick="addText('_O-')"><img src="{{ asset('images/smileys/schater.gif') }}"></button>
                                        <button onclick="addText('(L)')"><img src="{{ asset('images/smileys/smiley_love_heart.gif') }}"></button>
                                        <button onclick="addText('(K)')"><img src="{{ asset('images/smileys/smiley_love_kissing.gif') }}"></button>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <form method="post" name="form" action="{{ route('message.store') }}" id="messageForm" autocomplete="off">

                            <!-- CROSS Site Request Forgery Protection -->
                            @csrf

                            <div class="form-group">
                                <div class="grid grid-cols-5">
                                    <div>
                                        <label for="recipient">Recipient:</label>
                                    </div>
                                    <div><input type="text" id="recipient"
                                                name="recipient" required style="color: black"></div>
                                </div>
                                <div></div>
                                <div class="grid grid-cols-5 mt-2">
                                    <div>
                                        <label for="Subject">Subject:</label>
                                    </div>
                                    <div>
                                        <input type="text" id="subject" name="subject"
                                               maxlength="25" required style="color: black"></div>
                                </div>
                                <div></div>

                                <div class="grid grid-cols-5 mt-2">
                                    <div>
                                        <label for="Subject">Message:</label>
                                    </div>
                                    <div>
                                        <textarea cols="60" rows="14" style="color: black; min-width: 20vw; min-height: 20vh; max-width: 70vw; resize: both; overflow: auto;" name="message" form="messageForm"
                                                  required></textarea>
                                    </div>
                                </div>

                                <div></div>
                                <div class="grid grid-cols-5 mt-2">
                                    <div>

                                    </div>
                                    <div>
                                        <input type="submit" name="send" value="Submit"
                                               class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4">
                                    </div>
                                </div>
                            </div>

                        </form>

                </div>
            </div>
        </div>
    </div>
    <script>
        function addText(value) {
            document.form.message.value += value;
        }
    </script>
@endsection


