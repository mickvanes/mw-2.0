<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HonorPoints extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function rank()
    {
        return $this->belongsTo(Rank::class, 'rankID');
    }
}
