@extends('layouts.default')

@section('header')
    DEFENSE RIP LOGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="pt-4">
                            <table class="table-fixed w-full mb-2">
                                Defense
                                <thead>
                                <tr>
                                    <th class="text-left">Attacker</th>
                                    <th class="text-left">Cash</th>
                                    <th class="text-left">Won/Loss</th>
                                    <th class="text-left">Time</th>
                                </tr>
                                </thead>

                                @foreach($ripDefense as $ripDefenseItem)
                                    <tr>
                                        <td><a href="{{ route('player.show',$ripDefenseItem->attacker->username) }}">{{ $ripDefenseItem->attacker->username }}</a></td>
                                        <td>&euro; {{ number_format($ripDefenseItem->cash, 0, ',', '.') }}</td>
                                        <td>
                                            @if($ripDefenseItem->result == 0)
                                                Loss
                                            @else
                                                Won
                                            @endif
                                        </td>
                                        <td>{{ $ripDefenseItem->created_at }}</td>
                                    </tr>
                                @endforeach
                            </table>
                            {{ $ripDefense->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

