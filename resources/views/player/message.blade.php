@extends('layouts.default')

@section('header')
    MESSAGE
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif
                    @if($message && $message->deletedRecipient == 0)
                        @if($message->systemMessage == 0)
                            <div class="grid grid-cols-3 w-3/4 mt-4">
                                <div class="w-1/4">Sender:</div>
                                <div class="col-span-2"><a href="{{route('player.show', $message->username)}}">{{$message->username}}</a></div>
                            </div>
                            <div class="grid grid-cols-3 w-3/4">
                                <div class="w-1/4">Subject:</div>
                                <div class="col-span-2">{{$message->subject}}</div>
                            </div>
                            <div class="grid grid-cols-3 w-3/4">
                                <div class="w-1/4">Time:</div>
                                <div class="col-span-2">{{$message->created_at}}</div>
                            </div>
                        @else
                            <div class="grid grid-cols-3 md:w-3/4 mt-4">
                                <div>Sender:</div><div>{{$message->name}} <span class="text-green-700">(System Message)</span></div>
                            </div>
                            <div class="grid grid-cols-3 md:w-3/4">
                                <div>Subject:</div><div class="col-span-2">{{$message->subject}}</div>
                            </div>
                            <div class="grid grid-cols-3 md:w-3/4">
                                <div>Time:</div><div>{{$message->created_at}}</div>
                            </div>
                        @endif
                </div>
            </div>
        </div>

        <div class="mx-auto overflow-hidden w-full mt-2">
            <div class="max-w-7xl mx-auto ">
                <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                    <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white pb-14">
                        <span>{!! (new \App\Helpers\Helper)->makeUBB($message->body)  !!} </span>
                        @elseif($message->deletedRecipient)
                            <p>This message is deleted</p>
                        @else
                            <p>You don't have the rights to read this message</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="mx-auto overflow-hidden w-full mt-2">
            <div class="max-w-7xl mx-auto ">
                <div class="shadow-sm w-1/2">
                    <div class="text-white">
                        <div class="grid grid-cols-4 gap-2">
                            @if($message->systemMessage == 0)
                                <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{route('message.reply', $message->id)}}">Reply</a></div>
                                <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{route('message.reply', $message->id)}}">Block</a></div>
                                <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{route('message.report', $message->id)}}">Report</a></div>
                            @endif
                            <span class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{route('message.delete', $message->id)}}">Delete</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

