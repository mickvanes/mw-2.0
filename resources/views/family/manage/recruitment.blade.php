@extends('layouts.default')

@section('header')
    FAMILY RECRUITMENT
@stop

@section('content')
<div class="mx-auto h-full overflow-hidden w-full">
    <div class="max-w-7xl mx-auto">
        <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
            <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
    @if(session()->has('flash'))
        <div class="alert alert-success">
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                    role="alert">
                <strong class="font-bold">System</strong>
                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
            </div>
        </div><br>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success">
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                    role="alert">
                <strong class="font-bold">Message</strong>
                <span class="block sm:inline">{{ session()->pull('message') }}</span>
            </div>
        </div><br>
    @endif
    <div x-show="show === 0"></div>

    <div class="grid grid-cols-1" x-data="{ show: 0 }">
        <table>
            <thead>
            <th class="text-left">Name</th>
            <th class="text-left">Rank</th>
            <th class="text-left">Since</th>
            </thead>
            <tbody>

            @foreach($recruitMembers as $recruitMember)
                <div x-show="show === {{ $recruitMember->id }}" class="p-4 mb-4 rounded">
                    <div class="grid grid-cols-1 md:grid-cols-2">
                        <div>
                            <div>Player: {{ $recruitMember->user->username }} <a href="{{ route('player.show',$recruitMember->user->username) }}">(link)</a></div>
                            <br/>
                            <div>Power: {{ number_format($recruitMember->user->profile->power, 0, ',', '.') }}</div>
                            <div>Clicks: {{ number_format($recruitMember->user->profile->clicks, 0, ',', '.') }}</div>
                            <div>Bullets: {{ number_format($recruitMember->user->profile->bullets, 0, ',', '.') }}</div>
                            <div>Reputation: {{ number_format($recruitMember->user->profile->rep, 0, ',', '.') }}</div>
                            <br/>
                            <div>Avg rank: {{ $recruitMember->avgRank($recruitMember->user->id) }}</div>
                            <div>Highest rank: {{ $recruitMember->maxRank($recruitMember->user->id) }}</div>
                            <br/>
                            <div>Coins: {{ number_format($recruitMember->user->profile->coins, 0, ',', '.') }}</div>
                            <br/>
                            <div>Avg Crimes per life: {{ number_format($recruitMember->user->allStats->avg('crime'), 0, ',', '.')  }}</div>
                            <div>Avg Carjackings per life: {{ number_format($recruitMember->user->allStats->avg('car'), 0, ',', '.')  }}</div>
                            <div>Avg Hostages per life: {{ number_format($recruitMember->user->allStats->avg('hostage'), 0, ',', '.')  }}</div>
                            <br/>
                            <div>Kills: {{ $recruitMember->user->kills->count() }}  (Backfire {{ $recruitMember->user->backfireKills->count() }})</div>
                            <div>Avg Kill Rank: {{ $recruitMember->avgKill($recruitMember->user->id)}}</div>
                            <div>Avg Backfire Kill Rank: {{ $recruitMember->avgBackfireKill($recruitMember->user->id)}}</div>

                        </div>
                        <div class="mt-4 md:mt-0">
                            <div x-data="{ open: false }">
                                <button @click="open = ! open">> Count Account Ranks</button>

                                <div x-show="open">
                                    <div class="grid grid-cols-2">
                                        <div class="font-bold">Rank</div>
                                        <div class="font-bold">Count</div>
                                    </div>
                                    @foreach($recruitMember->getCountPerRank($recruitMember->user->id) as $count)
                                        <div class="grid grid-cols-2">
                                            <div>{{ $count[0] }}</div>
                                            <div>{{ $count[1] }}</div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                    <button class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button" @click="show=0">Close</button>
                </div>
            @endforeach
            @foreach($recruitMembers as $recruitMember)
                <tr>
                    <div class="grid grid-cols-2 grid-rows-1">
                        <div>
                            <td class="cursor-pointer" @click="show={{ $recruitMember->id }}">{{ $recruitMember->user->username }}</td>
                            <td>{{ $recruitMember->user->getCurrentRank->ranks->rank }}</td>
                            <td>{{ $recruitMember->recruitSince($recruitMember->created_at) }}</td>
                        </div>
                        <td>
                            <div class="grid grid-cols-2">
                                <div><label for="javascript">Yes</label><input type="radio" id="html" name="{{ $recruitMember->id }}" value="HTML"></div>
                                <div><label for="javascript">No</label><input type="radio" id="css" name="{{ $recruitMember->id }}" value="CSS"></div>
                            </div>

                        </td>
                    </div>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
            </div>
        </div>
    </div>
</div>
@endsection
