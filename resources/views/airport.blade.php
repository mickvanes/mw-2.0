@extends('layouts.default')

@section('header')
    AIRPORT
@stop

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success mb-4">
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                 role="alert">
                <strong class="font-bold">Airport</strong>
                <span class="block sm:inline">{{ session()->pull('message') }}</span>
            </div>
        </div><br>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger mb-4">
            <div class="alertBox px-4 py-3 rounded relative"
                 role="alert">
                <strong class="font-bold">Airport</strong>
                <span class="block sm:inline">{{ session()->pull('error') }}</span>
            </div>
        </div><br>
    @endif

    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
                                <strong class="font-bold">Fly</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif
                        @if(session()->has('error'))
                            <div class="alert alert-success">
                                <div class="alertBox px-4 py-3 rounded relative" role="alert">
                                    <strong class="font-bold">Fly</strong>
                                    <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                </div>
                            </div><br>
                        @endif

                    @if($boolean)
                    <form method="post" action="{{ route('airport.store') }}">

                        <!-- CROSS Site Request Forgery Protection -->
                        @csrf

                        <div class="form-group">
                            <div class="grid grid-cols-2">
                                <div>
                                    @foreach($countries as $index => $country)
                                        <div class="p-0.5">
                                            <input type="radio" id="{{$country->name}}" name="countryID" value="{!! $country->id !!}">
                                            <label for="{{$country->name}}">{{$country->name}} </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div>
                                    @foreach($countries as $index => $country)
                                    <div class="text-right p-0.5">
                                        @if(!array_key_exists($country->id,$countryOnlineUsers))
                                            (0 players online)
                                        @else
                                            @if($countryOnlineUsers[$country->id] == 1)
                                                ({{ $countryOnlineUsers[$country->id]}} player online)
                                            @else
                                                ({{ $countryOnlineUsers[$country->id]}} players online)
                                            @endif
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <br>

                        <input type="submit" name="send" value="Submit" class="cursor-pointer bg-darkButton hover:bg-darkHover dark:bg-buttonBlue font-bold py-2 px-4">
                    </form>
                        @else
                            <span id="airport"></span>
                            <script src="{{ URL::asset('js/timer.js') }}"></script>
                            <script type="text/javascript">countdown('<?php echo $dateNew ?>','default','airport');</script>
                    @endif

                </div>
            </div>
        </div>
    </div>
@stop
