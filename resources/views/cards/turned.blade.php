@extends('layouts.default')

@section('header')
    CARD
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                    <div class="mb-5">

                    </div>
                    <div class="mx-auto">
                        <div class="grid grid-rows-none">
                            @foreach($allCollections->groupBy('collectionID') as $key => $collection)
                                {{ $collection[$key]->collectionDetails->name }}
                                <div class="grid grid-flow-col">
                                @foreach($collection as $cardCollection)
                                    <div class="mb-4 mx-auto">
                                        <div class="grid grid-cols-1 mx-auto">
                                            @if(!$cardCollection->owned)
                                                <img src="https://upload.wikimedia.org/wikipedia/commons/c/ce/EmptyCard.svg" />
                                            @else
                                                @if($cardCollection->turnedIn)
                                                    <img src="https://tekeye.uk/playing_cards/images/svg_playing_cards/fronts/png_96_dpi/spades_ace.png">
                                                @else
                                                    <img src="https://tekeye.uk/playing_cards/images/svg_playing_cards/backs/astronaut.svg">
                                                @endif
                                            @endif
                                            <span class="text-center">{{ $cardCollection->card->cardName }}</span>
                                        </div>

                                    </div>
                                @endforeach
                                </div>
                                <br><br><br>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





