<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mute extends Model
{
    use HasFactory;

    protected $table = 'user_mutes';

    protected $fillable = [
        'player',
        'reason',
        'days',
        'beginDate',
        'endDate'
    ];

    public function player()
    {
        return $this->belongsTo(User::class, 'player');
    }

}
