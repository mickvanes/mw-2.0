@extends('layouts.default')

@section('header')
    Family Stats
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                        @if(Auth::user()->hasFamily())
                            <div class="md:col-span-2 text-center">
                                <a href="{{ route('family.stats') }}"> View your family stats</a>
                            </div>
                        @endif
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Power</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Power</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($familiesStats->sortByDesc('power')->take(5) as $statPower)
                                    <span class="border p-2"><a href="{{ route('family', $statPower->name) }}"> {{ $statPower->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statPower->power, 0, ',', '.') }} Power</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Clicks</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Clicks</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($familiesStats->sortByDesc('clicks')->take(5) as $statClicks)
                                    <span class="border p-2"><a href="{{ route('family', $statClicks->name) }}">{{ $statClicks->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statClicks->clicks, 0, ',', '.') }} Clicks</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Reputation</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Reputation</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($familiesStats->sortByDesc('rep')->take(5) as $statRep)
                                    <span class="border p-2"><a href="{{ route('family', $statRep->name) }}">{{ $statRep->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statRep->rep, 0, ',', '.') }} Reputation</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Honor</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Honor</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($familiesStats->sortByDesc('honor')->take(5) as $statHonor)
                                    <span class="border p-2"><a href="{{ route('family', $statHonor->name) }}">{{ $statHonor->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ number_format($statHonor->honor, 0, ',', '.')  }} Honor</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">AVG Rank</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Rank</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($familiesStats->sortByDesc('rankDown')->take(5) as $statRanks)
                                    <span class="border p-2"><a href="{{ route('family', $statRanks->name) }}">{{ $statRanks->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ $statRanks->rankDown->rank }}</b></span>
                                @endforeach

                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Loan</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Loan</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($familiesStats->sortByDesc('loan')->take(5) as $statLoan)
                                    <span class="border p-2"><a href="{{ route('family', $statLoan->name) }}">{{ $statLoan->name }}</a></span>
                                    <span class="border p-2 break-all"><b>&euro;{{ number_format($statLoan->loan, 0, ',', '.') }}</b></span>
                                @endforeach
                            </div>

                        </div>

                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Kills</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Kills (points)</span>
                            </div>
                            <div class="grid grid-cols-2">
                                @foreach($familiesStats->sortByDesc('killPoints')->take(5) as $statKills)
                                    <span class="border p-2"><a href="{{ route('family', $statKills->name) }}">{{ $statKills->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ $statKills->killCount }} ({{ $statKills->killPoints }})</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="md:p-4">
                            <h1 class="text-center mb-2 text-lg font-bold">Top Backfire Kills</h1>
                            <div class="grid grid-cols-2 mt-2 font-semibold text-center mb-1">
                                <span>Name</span>
                                <span>Backfire Kills (points)</span>
                            </div>
                            <div class="grid grid-cols-2 border">
                                @foreach($familiesStats->sortByDesc('bfKillPoints')->take(5) as $statBfKills)
                                    <span class="border p-2"><a href="{{ route('family', $statBfKills->name) }}">{{ $statBfKills->name }}</a></span>
                                    <span class="border p-2 break-all"><b>{{ $statBfKills->bfKillCount }} ({{ $statBfKills->bfKillPoints }})</b></span>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
