<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recruit extends Model
{
    use HasFactory;

    protected $table = 'family_recruiting';

    protected $fillable = [
        'familyID',
        'userID',
    ];

    public function family()
    {
        return $this->belongsTo(Family::class, 'familyID');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function stats()
    {
        return $this->hasMany(Stats::class, 'userID');
    }

    public function avgRank($userID){
        $checkAvgRank = round(Rank::where('userID', '=', $userID)->avg('rankID'));
        $avgRank = Ranks::find($checkAvgRank)->rank;
        return $avgRank;
    }

    public function getCountPerRank($userID){
        $ranks = Ranks::all();

        $data = [];

        foreach ($ranks as $rank){
            $value = Rank::where([
                ['userID', '=', $userID],
                ['rankID', '=', $rank->id]
            ])->count();

            $name = $rank->rank;

            if($value == null){
                $value = 0;
            }

            array_push($data, [$name,$value]);
        }
        return $data;
    }

    public function maxRank($userID){
        $checkMaxRank = round(Rank::where('userID', '=', $userID)->max('rankID'));
        $maxRank = Ranks::find($checkMaxRank)->rank;
        return $maxRank;
    }

    public function avgKill($userID){
        if(!Kill::where('userID', '=', $userID)->count() == null || Kill::where('userID', '=', $userID)->count() > 1  ){
            $checkAvgKill = round(Kill::where('userID', '=', $userID))->avg('playerRank');
            $avgKillRank = Ranks::find($checkAvgKill)->rank;
            return $avgKillRank;
        }
        else{
            $avgKillRank = "-";
            return $avgKillRank;
        }

    }

    public function avgBackfireKill($userID){
        if(!BackfireKill::where('userID', '=', $userID)->count() == null || BackfireKill::where('userID', '=', $userID)->count() > 1  ) {
            $checkAvgBackfireKill = round(BackfireKill::where('playerID', '=', $userID))->avg('playerRank');
            $avgBackfireKill = Ranks::find($checkAvgBackfireKill)->rank;
            return $avgBackfireKill;
        }
        else{
            $avgBackfireKill = "-";
            return $avgBackfireKill;
        }

    }

    public function recruitSince($date){
        $dt     = Carbon::now();

        $daysUser = $dt->diff($date)->format('%m months %d days %h hours %i minutes');

        return $daysUser;
    }
}
