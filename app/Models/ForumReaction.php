<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForumReaction extends Model
{
    use HasFactory;

    protected $table = 'forum_reactions';

    protected $fillable = [
        'userID',
        'topicID',
        'categoryID',
        'content',
        'level',
        'reactionID'
    ];

    public function topic()
    {
        return $this->belongsTo(ForumTopic::class, 'topicID');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function category()
    {
        return $this->belongsTo(ForumCategory::class, 'categoryID');
    }

    public function reaction()
    {
        return $this->belongsTo(ForumReaction::class, 'reactionID');
    }
}
