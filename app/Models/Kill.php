<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kill extends Model
{
    use HasFactory;

    protected $table = 'kills';

    protected $fillable = [
        'uniqID',
        'userID',
        'playerID',
        'bullets',
        'backfire',
        'userRank',
        'playerRank',
        'userPercent',
        'playerPercent',
        'player_familyID',
        'user_familyID',
        'playerCash',
        'playerBank',
        'active',
        'success',
        'weapon',
        'userDef',
        'playerDef',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function witnesses()
    {
        return $this->hasMany(Witness::class, 'uniqID','uniqID');
    }

    public function player()
    {
        return $this->belongsTo(User::class, 'playerID');
    }

    public function playerFamily()
    {
        return $this->belongsTo(Family::class, 'player_familyID');
    }

    public function userFamily()
    {
        return $this->belongsTo(Family::class, 'user_familyID');
    }

    public function backfire()
    {
        return $this->hasOne(BackfireKill::class, 'uniqID');
    }

    public function userRankName()
    {
        return $this->belongsTo(Rank::class, 'userRank');
    }

    public function playerRankName()
    {
        return $this->belongsTo(Ranks::class, 'playerRank');
    }





}
