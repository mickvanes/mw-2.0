@extends('layouts.default')

@section('header')
    LOGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-3">
                        @foreach($shops as $shop)
                            <div><div class="w-36 h-36"></div><a href="{{ route($shop[0]) }}">{{ $shop[1] }}</a></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
