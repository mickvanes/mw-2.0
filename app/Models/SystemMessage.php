<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemMessage extends Model
{
    use HasFactory;

    protected $table = 'system_messages';

    protected $fillable = [
        'id',
        'senderID',
        'recipientID',
        'subject',
        'body',
        'seen'
    ];

    public function sender()
    {
        return $this->belongsTo(SystemUser::class, 'senderID');
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipientID');
    }

}
