<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'id';

    public $incrementing = true;

    protected $table = 'users';

    protected $fillable = [
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_online' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class,'id');
    }

    public function cash()
    {
        return $this->hasOne(Cash::class, 'userID');
    }

    public function family()
    {
        return $this->hasOne(FamilyUserRang::class, 'userID');
    }

    public function getFamilyName(){
        if ($this->family == null) {
            return false;
        }
        else{
            return $this->family->familyDetails->name;
        }
    }

    public function isMarriedPlayer1(){
        return $this->hasOne(Marriage::class,'player1');
    }

    public function isMarriedPlayer2(){
        return $this->hasOne(Marriage::class,'player2');
    }

    public function isMarried(){
        return Marriage::where(function($q) {
            $q->where('player1',$this->id)
                ->orWhere('player2',$this->id);
        });
    }

    public function settings()
    {
        return $this->hasOne(Settings::class, 'userID');
    }

    public function hasRang($rangName) {
        if ($this->family == null || $this->family->rang == null) {
            return false;
        }
        if ($this->family->rang->name == $rangName) {
            return true;
        }
        return false;
    }

    public function hasFamily() {
        if ($this->family == null) {
            return False;
        }

        return True;
    }

    public function isRecruit()
    {
        if($this->family == null){
            return $this->hasOne(Recruit::class,'userID');
        }
        else{
            return False;
        }
    }

    public function getFamilyRang() {
        if ($this->family == null || $this->family->rang == null) {
            return null;
        }

        return $this->family->rang->name;
    }

    public function hasUnreadMessages(){
        $messages = Message::where([['recipientID',Auth::id()],['seen',0]])->count();

        return $messages;
    }

    public function attacks()
    {
        return $this->hasMany(Attack::class, 'id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'id');
    }

    public function hidden()
    {
        return $this->hasMany(Hidden::class, 'userID');
    }

    public function rank()
    {
        return $this->hasMany(Rank::class, 'userID');
    }

    public function getCurrentRank()
    {
        return $this->hasOne(Rank::class, 'userID')->latest();
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'userID');
    }

    public function stats()
    {
        return $this->hasOne(Stats::class, 'userID')->latest();
    }

    public function allStats()
    {
        return $this->hasMany(Stats::class, 'userID');
    }

    public function races()
    {
        return $this->hasMany(Race::class);
    }

    public function defense()
    {
        return $this->belongsTo(Defense::class, 'id','userID');
    }

    public function kills(){
        return $this->hasMany(Kill::class,'userID');
    }

    public function backfireKills(){
        return $this->hasMany(BackfireKill::class, 'playerID');
    }

    public function friends($userID){
        $friends = Friends::where('userID', $userID)->orWhere([
            ['friendID',$userID],
        ]);
        return $friends;
    }

    public function pendingFriends($userID){
        $friends = Friends::where([
            'userID' => $userID,
            'accepted' => 0
        ])->orWhere([
            'friendID' => $userID,
            'accepted' => 0
        ],
        );
        return $friends;
    }

    public function admin()
    {
        return $this->hasOne(Admin::class,'userID');
    }

    public function getCountPerRank(){
        $ranks = Ranks::all();

        $data = [];

        foreach ($ranks as $rank){
            $value = Rank::where([
                ['userID', '=', Auth::id()],
                ['rankID', '=', $rank->id]
            ])->count();

            $name = $rank->rank;

            if($value == null){
                $value = 0;
            }

            array_push($data, [$name,$value]);
        }
        return $data;
    }


}
