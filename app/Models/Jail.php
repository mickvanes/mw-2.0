<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jail extends Model
{
    use HasFactory;

    protected $table = 'user_jail';

    protected $fillable = [
        'userID',
        'countryID',
        'reason',
        'businessID',
        'endTime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'userID');
    }

    public function business()
    {
        return $this->belongsTo(Business::class,'businessID');
    }

}
