@extends('layouts.default')

@section('header')
    SHOP POWER
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                        <div class="mb-5">
                            You can choose a house below that suits you.
                            At the bottom of this page you will find several pages with even more houses.<br><br>

                            For example, a home offers extra protection.
                            Your house will be in the country you are in, in addition, a house also offers a safe in which you can save money.
                            If someone kills you, you can't lose that money that's in your safe.<br><br>

                            The more expensive the home, the more protection it will provide!
                        </div>
                    <div class="mx-auto w-2/3">
                        <table class="table">
                            <tr>
                                <th class="text-left w-1/3">Name</th>
                                <th class="text-left w-1/3">Cost</th>
                                <th class="text-left w-1/3">defense</th>
                                <th class="text-left w-1/3"></th>
                            </tr>
                    @foreach($houses as $key => $house)

                                <tr>
                                <td class="w-1/3">{{$house->name}}</td>
                                <td class="w-1/3">{{$house->cost}}</td>
                                <td class="w-1/3">{{$house->defense}}</td>
                                <td class="w-1/3"><a href="{{route('house.buyForm', $house->slug)}}">Buy</a></td>
                                </tr>
                        @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





