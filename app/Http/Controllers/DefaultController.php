<?php

namespace App\Http\Controllers;

use App\Models\Cash;
use App\Models\FamilyPromotion;
use App\Models\Kill;
use App\Models\Rank;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DefaultController extends Controller
{
    public function allStats(){
        $stats = User::selectRaw('
            users.username as username,
            profiles.power as power,
            profiles.rep as rep,
            profiles.kills as kills,
            profiles.bfKills as bfKills,
            profiles.bursts as bursts,
            profiles.killPoints as killPoints,
            profiles.bfKillPoints as bfKillPoints,
            (user_cash.cash + user_cash.bank) as funds,
            profiles.clicks as clicks,
            profiles.honor as honor
        ')
            ->join('profiles', 'profiles.id', '=', 'users.id')
            ->join('user_cash', 'user_cash.userID', '=', 'users.id')
            ->take(10)
            ->get();

        $highestRanks = Rank::where('active','=',1)
            ->join('users', 'users.id' , '=', 'user_rank_logs.userID')
            ->join('ranks', 'ranks.id' , '=', 'user_rank_logs.rankID' )
            ->orderByDesc('adv')->take(10)->get();

        return view('stats', compact('stats','highestRanks'));
    }
}

