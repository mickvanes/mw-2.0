<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyTransaction extends Model
{
    use HasFactory;

    protected $table = 'user_family_transactions';

    protected $fillable = [
        'familyID',
        'userID',
        'nettoCash',
        'brutoCash',
        'message',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function family()
    {
        return $this->belongsTo(Family::class, 'familyID');
    }
}
