<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyHeistRole extends Model
{
    use HasFactory;

    protected $table = 'family_heists_roles';

    protected $fillable = [
        'id',
        'name',
        'description',
    ];
}
