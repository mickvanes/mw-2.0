@extends('layouts.default')

@section('header')
    SHOP POWER
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif

                    Hire Security <br><br>

                    <a href="{{route('security.logs')}}">[Security Logs]</a>

                    @if(!$security)
                        <form method="POST" action="{{ route("security.post") }}">
                            @csrf
                            <table class="table-fixed mt-4 text-white">
                                <tbody>
                                    <tr>
                                        <td><input type="radio" name="security" value="10"><label for="security"> 10 minutes of money security (&euro;{{ number_format(3000, 0, ',', '.') }})</label></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="security" value="20"><label for="security"> 20 minutes of money security (&euro;{{ number_format(4000, 0, ',', '.') }})</label></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="security" value="30"><label for="security"> 30 minutes of money security (&euro;{{ number_format(5000, 0, ',', '.') }})</label></td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="flex justify-end mt-4 md:mt-10">
                                <button class="flex-shrink-0 text-sm py-1 px-2 rounded" type="submit">
                                    Hire Security
                                </button>
                            </div>
                        </form>
                        @else
                            <div id="demo"></div>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    var date = <?= isset($security->endTime) ? json_encode($security->endTime) : "" ?>;

    var countDownDate = new Date(date).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for hours, minutes and seconds
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML = hours + "h " + minutes + "m " + seconds + "s ";

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            window.location.reload()
        }
    }, 1000);
</script>

