
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white mx-auto overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200 w-3/4">

                        @if(session()->has('message'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Prison</strong>
                                    <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                </div>
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-success mb-4">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Prison</strong>
                                    <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                </div>
                            </div>
                        @endif
                            Revenue last week: &euro;{{ number_format($revenueLastWeek , 0, ',', '.') }} <br>
                            @if($revenue >= 0)
                                Revenue this week: <span class="text-green-600">&euro;{{ number_format($revenue , 0, ',', '.') }} </span><br><br>
                            @else
                                Revenue this week: <span class="text-red-600">&euro;{{ number_format($revenue , 0, ',', '.') }} </span><br><br>
                            @endif

                            <div class="grid grid-cols-1 md:grid-cols-2">
                                <div>Player Cash: &euro;{{ number_format($userCash , 0, ',', '.') }}<br>
                                    Player Bank: &euro;{{ number_format($userBank , 0, ',', '.') }}<br><br></div>
                                <div>Business Bank: &euro;{{ number_format($bank , 0, ',', '.') }}<br><br></div>
                            </div>

                            <form method="post" action="{{ route('businessBank.store',$businessID) }}">
                                @csrf
                                <div class="grid grid-cols-3">
                                </div>
                                <span class="text-red-600"><b>You need this night &euro; {{ number_format($costsNight , 0, ',', '.') }} to clean the jails and pay the guards </b></span>
                                <div class="grid grid-cols-3 mt-4 mb-4">
                                    <div><label for="safe">Transaction Type</label></div>
                                    <div class="grid grid-cols-1 md:grid-cols-2">
                                        <div><input type="radio" id="withdraw"
                                                    name="transaction_type"
                                                    value="0">
                                            <label for="withdraw"> Withdraw</label></div>
                                        <div>
                                            <input type="radio" id="deposit"
                                                   name="transaction_type"
                                                   value="1" checked>
                                            <label for="deposit"> Deposit</label>
                                        </div>
                                    </div>
                                    <div></div>
                                </div>
                                <div></div>
                                <div class="grid grid-cols-3">
                                    <div class="py-2"><label for="transaction">Cash &euro;</label></div>
                                    <div><input type="number" id="transaction"
                                                name="transaction" min="0" value=""></div><a onclick="transAll()">[all]</a>
                                </div>

                                <input type="submit" value="Submit"
                                       class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4">
                                <button class="bg-gray-500 hover:bg-gray-700 mt-4 text-white font-bold py-2 px-4" type="button">
                                    <a href="{{ route('businessBank.logs',$businessID) }}">Bank Logs</a>
                                </button>
                                <script>
                                    function transAll(){
                                        var type = document.querySelector('input[name="transaction_type"]:checked').value

                                        if(type == 0){
                                            document.getElementById('transaction').value=<?= $bank ?>;
                                        }
                                        else if(type == 1) {
                                            document.getElementById('transaction').value=<?= $userCash ?>;
                                        }
                                        else{
                                            console.log('error');
                                        }
                                    }
                                </script>
                            </form>
                    </div>
                </div>
        </div>
    </div>
</x-app-layout>

