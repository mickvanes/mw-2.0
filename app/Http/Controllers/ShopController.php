<?php

namespace App\Http\Controllers;

use App\Models\Airplane;
use App\Models\Cash;
use App\Models\Defense;
use App\Models\PowerItems;
use App\Models\PowerLog;
use App\Models\Profile;
use App\Models\Security;
use App\Models\User;
use App\Models\Weapon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    public function index(){
        $shops = [
            ['shop.security','Security'],
            ['shop.airplanes','Airplanes'],
            ['shop.weapons','Weapons'],
            ['shop.power','Power'],
            ['house.buy', 'House']
        ];

        return view('shop', compact('shops'));
    }


    public function security(){
        $user = User::with('cash')->find(Auth::id());

        $security = Security::where([
            ['userID', '=', Auth::id()],
            ['endTime', '>=', Carbon::now('Europe/Amsterdam')]
        ])->first();

        return view('shop.security', compact('user','security'));
    }

    public function securityForm(Request $request){
        $this->validate($request, [
            'security' => 'required',
        ]);

        $security = Security::where([
            ['userID', '=', Auth::id()],
            ['endTime', '>=', Carbon::now('Europe/Amsterdam')]
        ])->first();

        if(!$security){
            $userCash = DB::table('user_cash')
                ->select('cash')
                ->where('user_cash.userID', '=', Auth::id())
                ->pluck('cash')
                ->first();

            $securityCost = [
                30 => 5000,
                20 => 4000,
                10 => 3000
            ];

            if ($securityCost[$request->input('security')] <= $userCash) {
                $beginTime = Carbon::now('Europe/Amsterdam');
                $endTime = Carbon::now('Europe/Amsterdam')->addMinutes(intval($request->input('security')));

                DB::table('user_security')->insert([
                    'userID' => Auth::id(),
                    'minutes' => $request->input('security'),
                    'beginTime' => $beginTime,
                    'endTime' => $endTime,
                    'created_at' => Carbon::now('Europe/Amsterdam'),
                    'updated_at' => Carbon::now('Europe/Amsterdam')
                ]);

                DB::table('user_cash')
                    ->where('userID', Auth::id())
                    ->update(['cash' => $userCash - $securityCost[$request->input('security')]]);

                return redirect()->route('shop.security')->with('message', "You bought security for" . $request->input('security') . " minutes");
            }
            else{
                return redirect()->route('shop.security')->with('error', "You don't have enough cash");
            }
        }
        else{
            return redirect()->route('shop.security')->with('error', "You have already security");
        }
    }

    public function airplanes(){
        $airplanes = Airplane::all();
        $defenses = Defense::with('plane')->where('userID',Auth::id())->first();

        return view('shop.airplanes', compact('defenses','airplanes'));
    }

    public function buyAirplane($id){
        $defenses = Defense::with('plane')->where('userID',Auth::id())->first();
        $buyAirplane = Airplane::find($id);

        if(!$defenses->airplane){
            if(Auth::user()->cash->cash >= $buyAirplane->costs){
                Cash::where('userID',Auth::id())->update([
                    'cash' => Auth::user()->cash->cash - $buyAirplane->costs
                ]);

                $defenses->airplane = $id;
                $defenses->save();

                return redirect()->route('shop.airplanes')->with('message',"You bought your airplane for " . $buyAirplane->costs);
            }
            else{
                return redirect()->route('shop.airplanes')->with('error',"You don't have enough cash for buying this plane");
            }
        }
        else{
            return redirect()->route('shop.airplanes')->with('error',"You have already an airplane, sell it first to buy a new one");
        }
    }

    public function sellAirplane(){
        $defenses = Defense::with('plane')->where('userID',Auth::id())->first();

        if($defenses){
            $sellPrice = $defenses->airplane->costs * 0.5;

            Cash::where('userID',Auth::id())->update([
                'cash' => Auth::user()->cash->cash + $sellPrice
            ]);

            $defenses->airplane = NULL;
            $defenses->save();

            return redirect()->route('shop.airplanes')->with('error',"You sold your airplane for " . $sellPrice);
        }
        else{
            return redirect()->route('shop.airplanes')->with('error',"You don't have an airplane to sell.");
        }
    }

    public function weapons(){

        $weapons = Weapon::all();
        $defenses = Defense::with('gun')->where('userID',Auth::id())->first();

        return view('shop.weapons',compact('weapons','defenses'));
    }

    public function power(){
        $powerItems = PowerItems::all();
        $remClicks = Auth::user()->profile->remClicks;
        $userData = Auth::user()->profile;

        return view('shop.power',compact('powerItems','userData','remClicks'));
    }

    public function buyPower(Request $request){
        $this->validate($request, [
            'item' => 'required|exists:powerItems,id',
            'amount' => 'required|min:1|numeric'
        ]);

        $item = PowerItems::where('id','=', $request->input('item'))->firstOrFail('name');
        $remClicks = Auth::user()->profile->remClicks;
        $cash = Auth::user()->cash->cash;

        $powerItem = PowerItems::where('id','=', $request->input('item'))->firstOrFail();
        $needClicks = $request->input('amount') * $powerItem->needClicks;
        $clicks = $remClicks - $needClicks;

        if($cash > 1000){
            if($remClicks >= 1){
                if($clicks >= 0){
                    if($cash >= $powerItem->costs * $request->input('amount')){
                        $power = Auth::user()->profile->power;
                        $costs = $powerItem->costs * $request->input('amount');
                        $usedClicks = $powerItem->needClicks * $request->input('amount');
                        $addedPower = $powerItem->power * $request->input('amount');

                        Cash::where('userID', '=', Auth::id())->update([
                            'cash' => $cash - $costs
                        ]);

                        Profile::where('id','=', Auth::id())->update([
                            'power' => $power + $addedPower,
                            'remClicks' => $remClicks - $usedClicks
                        ]);

                        PowerLog::create([
                            'userID' => Auth::id(),
                            'item' => $request->input('item'),
                            'amount' => $request->input('amount'),
                            'addedPower' => $addedPower,
                            'usedClicks' => $usedClicks,
                            'costs' => $costs
                        ]);

                        return back()->with('message', "You bought " . number_format($request->input('amount')) . "x ". $item->name . " (Added power: ". number_format($addedPower) . ")" );
                    }
                }
                else{
                    return back()->with('error', "Not enough clicks idiot.");
                }
            }
            else{
                return back()->with('error', "No clicks idiot.");
            }
        }else{
            return back()->with('error', "Not enough cash.");
        }
    }

}
