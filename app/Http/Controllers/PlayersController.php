<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Attack;
use App\Models\BackfireKill;
use App\Models\Business;
use App\Models\BusinessTransaction;
use App\Models\Cash;
use App\Models\Click;
use App\Models\Country;
use App\Models\Family;
use App\Models\FamilyTransaction;
use App\Models\FamilyUserRang;
use App\Models\Friends;
use App\Models\Hidden;
use App\Models\Kill;
use App\Models\Marriage;
use App\Models\Message;
use App\Models\Rank;
use App\Models\Ranks;
use App\Models\Recruit;
use App\Models\Rule;
use App\Models\Settings;
use App\Models\Transaction;

use App\Rules\ValidHCaptcha;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use App\Models\Profile;
use App\Models\User;
use App\Models\Stats;
use App\Models\Security;
use Illuminate\Support\Facades\Redirect;
use function App\Http\Controllers\attack;

class PlayersController extends Controller
{
    public function dashboard()
    {
        // Get user data
        $user = User::with('cash', 'country', 'stats', 'family', 'isRecruit', 'isMarriedPlayer1', 'isMarriedPlayer2')->find(Auth::id());
        $killCount = Profile::where('id','=',Auth::id())->first()->kills;
        $backfireKillCount = Profile::where('id','=',Auth::id())->first()->bfKills;
        $killPoints = Profile::where('id','=',Auth::id())->first()->killPoints;
        $bfKillPoints = Profile::where('id','=',Auth::id())->first()->bfKillPoints;

        // Set default data
        $userMarriage = "Single";

        // Check if user is married and return data to variable
        if ($user->isMarriedPlayer1) {
            $userMarriage = [
                $user->isMarriedPlayer1->player1Details->name,
                $user->isMarriedPlayer1->updated_at
            ];
        } else if ($user->isMarriedPlayer2) {
            $userMarriage = [
                $user->isMarriedPlayer2->player2Details->name,
                $user->isMarriedPlayer2->updated_at
            ];
        }

        return view('dashboard', compact('user', 'killCount', 'backfireKillCount', 'killPoints', 'bfKillPoints', 'userMarriage'));
    }

    public function player($name)
    {
        $user = User::where('username', '=', $name)->with('profile','admin','rank')->firstOrFail();
        $playerProfile = $user->profile;

        $marriage = Marriage::with('player1Details', 'player2Details')
            ->where('accepted','=', 1)
            ->where(function ($query) use ($user){
                $query->orWhere([
                    'player1' => $user->id,
                    'player2' => $user->id
                ]);
            })->first();

        $rank = Rank::where(['userID' => $user->id , 'active' => 1])
            ->join('ranks','ranks.id','=','user_rank_logs.rankID')
            ->first('rank');

        $family = FamilyUserRang::where('family_user_rang.userID', '=', $user->id)
            ->join('families', 'family_user_rang.familyID', '=', 'families.id')
            ->select('families.name')
            ->first();

        $familyRecruit = Recruit::where('userID', '=', $user->id)
            ->join('families', 'family_recruiting.familyID', '=', 'families.id')
            ->first('families.name');

        $profileCash = $user->cash->cash;
        $profileBank = $user->cash->bank;
        $userProfileText = nl2br($user->profile->profileText);

        return view('player.show', compact('user','playerProfile', 'rank', 'family', 'marriage', 'profileCash', 'profileBank', 'familyRecruit', 'userProfileText'));
    }

    public function bank()
    {
        $rules = Rule::latest()->first();
        $lastTransactionsToPlayer = Transaction::with('receiver')->where('fromUserID', '=', Auth::id())->orderByDesc('created_at')->take(5)->get();
        $lastTransactionsFromPlayer = Transaction::with('sender')->where('toUserID', '=', Auth::id())->orderByDesc('created_at')->take(5)->get();
        $lastTransactionsToFamily = FamilyTransaction::with('family')->where('userID', '=', Auth::id())->orderByDesc('created_at')->take(5)->get();

        $friends = Friends::with('user', 'friend')
            ->where('accepted','=', 1)
            ->where(function ($query){
                $query->orWhere([
                    'userID' => Auth::id(),
                    'friendID' => Auth::id()
                ]);
            })->get();

        $funds = Auth::user()->cash;
        $userCash = $funds->cash;
        $userBank = $funds->bank;
        $userSafe = $funds->safe;

        $userTotal = $userCash + $userBank + $userSafe;

        $hasFamily = FamilyUserRang::where('userID', '=', Auth::id())->first();

        if ($hasFamily) {
            $familyName = Family::where('id', '=', $hasFamily->familyID)->first()->name;
        } else {
            $familyName = null;
        }

        return view('player.bank', compact('lastTransactionsFromPlayer', 'lastTransactionsToPlayer', 'lastTransactionsToFamily', 'rules', 'friends', 'hasFamily', 'familyName','userBank','userSafe','userCash','userTotal'));
    }

    public function friends($name)
    {
        $player = User::where('username', $name)->first();
        $playerID = $player->id;

        $friends = Friends::with('user', 'friend')
            ->where('accepted','=', 1)
            ->where(function ($query) use ($playerID){
                $query->orWhere([
                    'userID' => $playerID,
                    'friendID' => $playerID
                ]);
            })->paginate(15);

        return view('player.friends', compact('playerID', 'friends'));
    }

    public function click($name)
    {
        $now = Carbon::now();
        $hour = Carbon::now()->addHour();

        $player = User::where('username', $name)->first();

        $checkTime = DB::table('user_clicks')
            ->where('userID', '=', Auth::id())
            ->where('playerID', '=', $player->id)
            ->pluck('timestamp')
            ->first();

        if ($checkTime == null || $checkTime < $now) {
            if(!$checkTime){
                Click::create([
                    'userID' => Auth::id(),
                    'playerID' => $player->id,
                    'timestamp' => $hour
                ]);
            }
            else{
                Click::where([
                    'userID' => Auth::id(),
                    'playerID' => $player->id,
                ])->update(['timestamp' => $hour]);
            }

            $playerProfile = Profile::where('id', '=', $player->id)->first();
            $playerProfile->increment('clicks');

            return redirect()->route('player.show', $name)->with('message', 'You clicked on ' . $name);
        } else {
            return redirect()->route('player.show', $name)->with('error', 'You can only click once per hour');
        }
    }

    public function bankForm(Request $request)
    {
        $cash = Auth::user()->cash->cash;
        $bank = Auth::user()->cash->bank;

        if ($request->input('action') == "transaction") {
            $this->validate($request, [
                'transaction_type' => 'required',
                'transaction' => 'required',
            ]);

            #transaction type { 0 = withdraw, 1 = deposit }

            if ($request->input('transaction_type') == 0) {

                $trans = intval($request->input('transaction'));
                $bankMinTrans = $bank - $trans;
                $cashPlusTrans = $cash + $trans;

                if ($trans <= $bank) {
                    Cash::where('userID', Auth::id())
                        ->update([
                            'cash' => $cashPlusTrans,
                            'bank' => $bankMinTrans,
                        ]);
                    return Redirect::back()->with('message', "You successfully withdraw €" . number_format($request->input('transaction'), 0, ',', '.') . ' of your bank.');
                } else {
                    return Redirect::back()->with('error', "You don't have €" . number_format($request->input('transaction'), 0, ',', '.') . ' .');
                }
            } elseif ($request->input('transaction_type') == 1) {
                $trans = $request->input('transaction');
                if ($trans <= $cash) {
                    DB::table('user_cash')
                        ->where('userID', Auth::id())
                        ->update(['cash' => $cash - $request->input('transaction')]);
                    DB::table('user_cash')
                        ->where('userID', Auth::id())
                        ->update(['bank' => $bank + $request->input('transaction')]);
                    return Redirect::back()->with('message', "You successfully deposit €" . number_format($request->input('transaction'), 0, ',', '.') . ' to your bank.');
                } else {
                    return Redirect::back()->with('error', "You don't have €" . number_format($request->input('transaction'), 0, ',', '.') . ' cash.');
                }
            }
        }

        if ($request->input('action') == "send") {
            $this->validate($request, [
                'trans_type' => 'required',
            ]);

            if ($request->input('trans_type') == "player") {
                $this->validate($request, [
                    'player' => 'required|exists:users,username',
                    'text' => 'regex:/^[a-zA-Z0-9\s]*$/|max:20|nullable',
                    'trans' => 'required|numeric',
                ]);

                $trans = $request->input('trans');

                $playerID = DB::table('users')
                    ->select('id')
                    ->where('users.username', '=', $request->input('player'))
                    ->pluck('id')
                    ->first();

                $rules = Rule::latest()->first();
                $transRule = $rules->transMax;

                if ($playerID) {
                    $transCash = DB::table('user_cash')
                        ->select('cash')
                        ->where('user_cash.userID', '=', $playerID)
                        ->pluck('cash')
                        ->first();

                    if ($cash >= $trans) {
                        if ($trans <= $transRule) {
                            $allTransactions = Transaction::where([
                                'fromUserID' => Auth::id(),
                                'toUserID' => $playerID,
                            ])->where('created_at', '>', Carbon::now()->subDays(7))->sum('brutoCash');

                            if ($allTransactions + $trans > (7 * $transRule)) {
                                return redirect()->route('bank')->with('error', "In the last 7 days you have already sent $" . number_format($allTransactions, 0, ',', '.') . " to " . ucfirst($request->input('player')) . " , the max is $" . number_format((7 * $transRule), 0, ',', '.'));
                            } else {
                                if ($trans >= ($transRule * 0.5)) {
                                    Cash::where('userID', Auth::id())->update(['cash' => $cash - $request->input('trans')]);
                                    Cash::where('userID', $playerID)->update(['cash' => $transCash + ($request->input('trans') * $rules->transCostsPlayerGreater)]);

                                    Transaction::create([
                                        'fromUserID' => Auth::id(),
                                        'toUserID' => $playerID,
                                        'message' => $request->input('text'),
                                        'nettoCash' => $request->input('transaction') * $rules->transCostsPlayerGreater,
                                        'brutoCash' => $request->input('transaction'),
                                    ]);
                                } else {
                                    Cash::where('userID', Auth::id())->update(['cash' => $cash - $request->input('trans')]);
                                    Cash::where('userID', $playerID)->update(['cash' => $transCash + ($request->input('trans') * $rules->transCostsPlayerLess)]);

                                    Transaction::create([
                                        'fromUserID' => Auth::id(),
                                        'toUserID' => $playerID,
                                        'message' => $request->input('text'),
                                        'nettoCash' => $request->input('trans') * $rules->transCostsPlayerLess,
                                        'brutoCash' => $request->input('trans'),
                                    ]);
                                }
                                return redirect()->route('bank')->with('message', "Successfully transferred money to " . $request->input('player') . " All transactions last 7 days " . $allTransactions);
                            }
                        } else {
                            return redirect()->route('bank')->with('error', "Your transaction €" . number_format($request->input('trans'), 0, ',', '.') . " is higher than the max transaction €" . number_format($transRule, 0, ',', '.') . '.');
                        }
                    } else {
                        return redirect()->route('bank')->with('error', "Your transaction is higher than your cash");
                    }
                } else {
                    return redirect()->route('bank')->with('error', "This player does not exist: " . $request->input('player') . ' .');
                }
            }
            if ($request->input('trans_type') == "family") {
                $this->validate($request, [
                    'text' => 'regex:/^[a-zA-Z0-9\s]*$/|max:20|nullable',
                    'trans' => 'required|numeric',
                ]);

                $trans = $request->input('trans');

                $familyID = DB::table('family_user_rang')
                    ->select('familyID')
                    ->where('family_user_rang.userID', '=', Auth::id())
                    ->pluck('familyID')
                    ->first();

                if ($familyID) {
                    $transCash = DB::table('family_cash')
                        ->select('cash')
                        ->where('family_cash.familyID', '=', $familyID)
                        ->pluck('cash')
                        ->first();

                    if ($trans <= $cash) {
                        $transMoney = $request->input('trans');

                        $rules = Rule::latest()->first();
                        $maxTrans = $rules->transMax;
                        $toFamilyCostsLess = $rules->transCostsFamLess;
                        $toFamilyCostsGreater = $rules->transCostsFamGreater;

                        if ($transMoney >= ($maxTrans * 0.5)) {
                            DB::table('user_cash')
                                ->where('userID', Auth::id())
                                ->update(['cash' => $cash - $request->input('trans')]);
                            DB::table('family_cash')
                                ->where('familyID', $familyID)
                                ->update(['cash' => $transCash + (round($transMoney * $toFamilyCostsGreater))]);

                            FamilyTransaction::create([
                                'userID' => Auth::id(),
                                'familyID' => $familyID,
                                'message' => $request->input('text'),
                                'nettoCash' => round($transMoney * $toFamilyCostsGreater),
                                'brutoCash' => $transMoney
                            ]);

                        } elseif ($transMoney < ($maxTrans * 0.5)) {
                            $trans = $transMoney * $toFamilyCostsLess;

                            DB::table('user_cash')
                                ->where('userID', Auth::id())
                                ->update(['cash' => $cash - $request->input('trans')]);
                            DB::table('family_cash')
                                ->where('familyID', $familyID)
                                ->update(['cash' => $transCash + round($trans)]);

                            FamilyTransaction::create([
                                'userID' => Auth::id(),
                                'familyID' => $familyID,
                                'message' => $request->input('text'),
                                'nettoCash' => round($trans),
                                'brutoCash' => $transMoney
                            ]);
                        }
                        return redirect()->route('bank')->with('message', "You successfully transferred €" . number_format($request->input('trans'), 0, ',', '.') . ' to your family.');
                    } else {
                        return redirect()->route('bank')->with('error', "Your transaction (€" . number_format($request->input('trans'), 0, ',', '.') . ") is higher than the amount of your cash money €" . number_format($userCash, 0, ',', '.') . '.');
                    }
                } else {
                    return redirect()->route('bank')->with('error', "You don't have a family");
                }
            }
        } else {
            return redirect()->route('bank')->with('error', "Error: No action given");
        }
    }

    public function onlinePlayerList()
    {
        $players = User::with('cash')->paginate(30);

        return view('onlinePlayerList', compact('players'));
    }

    public function playerList()
    {
        $players = User::with('cash')->paginate(30);

        return view('playerList', compact('players'));
    }

    public function marriage(){

        $userMarriagePlayer1 = DB::table('user_marriage')
            ->join('users','user_marriage.player2', '=', 'users.id')
            ->select('users.username','user_marriage.accepted')
            ->where('user_marriage.player1', '=', Auth::id())
            ->get();

        $userMarriagePlayer2 = DB::table('user_marriage')
            ->join('users','user_marriage.player1', '=', 'users.id')
            ->select('users.username','user_marriage.accepted')
            ->where('user_marriage.player2', '=', Auth::id())
            ->get();

        return view('player.marriage', compact('userMarriagePlayer1','userMarriagePlayer2'));
    }

    public function marriageForm(Request $request)
    {
        $this->validate($request, [
            'player' => 'required|alpha_num',
        ]);

        $userMarriage = DB::table('user_marriage')
            ->where('user_marriage.player1', '=', Auth::id())
            ->orWhere('user_marriage.player2', '=', Auth::id())
            ->first();

        $player = $request->input('player');

        $userCash = Auth::user()->funds->cash;

        $marriageCost = DB::table('rules')
            ->select('marriageCost')
            ->pluck('marriageCost')
            ->first();

        $playerMarriage = DB::table('users')
            ->select('id')
            ->where('users.name', '=', $player)
            ->pluck('id')
            ->first();

        User::where('username', '=' , $player)->first()->pluck('id');

        $playerMarriageCheck = DB::table('user_marriage')
            ->where('user_marriage.player1', '=', $playerMarriage)
            ->orWhere('user_marriage.player2', '=', $playerMarriage)
            ->first();

        if (!$userMarriage) {
            if ($playerMarriage) {
                if (!$playerMarriageCheck) {
                    if ($marriageCost <= $userCash) {
                        Marriage::create([
                            'player1' => Auth::id(),
                            'player2' => $playerMarriage,
                            'accepted' => 0,
                        ]);

                        DB::table('user_cash')
                            ->where('userID', Auth::id())
                            ->update(['cash' => $userCash - $marriageCost]);

                        Message::create([
                            'systemID' => 1,
                            'systemMessage' => 1,
                            'recipientID' => $playerMarriage,
                            'subject' => "Marriage proposal",
                            'body' => "You have had a marriage proposal from " . Auth::user()->name . ", Go quickly to the marriage page to accept or decline",
                            'timestamp' => Carbon::now(),
                            'seen' => 0
                        ]);

                        return redirect()->route('marriage.show')->with('message', 'You made a marriage proposal to ' . $player . "\n" . ' Wait for your proposal to be accepted');
                    } else {
                        return redirect()->route('marriage.show')->with('error', "You don't have enough cash to do this. Cash:" . $userCash . " Costs:" . $marriageCost);
                    }
                } else {
                    return redirect()->route('marriage.show')->with('error', "Unfortunately your marriage proposal cannot be made because this person is already married");
                }
            } else {
                return redirect()->route('marriage.show')->with('error', "This person does not exist");
            }
        } else {
            return redirect()->route('marriage.show')->with('error', "You are already married");
        }
    }

    public function marriageAction($action){
        $userMarriage = DB::table('user_marriage')
            ->join('users', 'users.id', '=', 'user_marriage.player1')
            ->where('user_marriage.player2', '=', Auth::id())
            ->select('users.name','user_marriage.*')
            ->first();

        if($action == "accept"){
            if($userMarriage){

                Marriage::where('player2', Auth::id())->update([
                    'accepted' => 1
                ]);

                Message::create([
                    'systemID' => 1,
                    'systemMessage' => 1,
                    'recipientID' => $userMarriage->player1,
                    'subject' => "Marriage",
                    'body' => "Your marriage proposal with " . Auth::user()->name . "has been accepted \n
                    Ladies and gentlemen, rise with me and let's raise our glasses in a toast to the bride and groom. \n
                    " . Auth::user()->name . " and " . $userMarriage->name . ", you are the one connected today! \n
                    We wish you a life full of happiness and health! \n
                    You may now kiss the bride!",
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'timestamp' => Carbon::now(),
                    'seen' => 0
                ]);

                Message::create([
                    'systemID' => 1,
                    'systemMessage' => 1,
                    'recipientID' => $userMarriage->player2,
                    'subject' => "Marriage",
                    'body' => "Your marriage proposal with " . Auth::user()->name . "has been accepted \n
                    Ladies and gentlemen, rise with me and let's raise our glasses in a toast to the bride and groom. \n
                    " . Auth::user()->name . " and " . $userMarriage->name . ", you are the one connected today! \n
                    We wish you a life full of happiness and health! \n
                    You may now kiss the bride!",
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'timestamp' => Carbon::now(),
                    'seen' => 0
                ]);

                return redirect()->route('marriage.show')->with('message', "You accepted the marriage proposal with " . $userMarriage->name);
            }
            else{
                return redirect()->route('marriage.show')->with('error', "Can't find the marriage proposal");
            }
        }
        elseif($action == "decline"){
            if($userMarriage){
                DB::table('user_marriage')
                    ->where('player2', Auth::id())
                    ->delete();

                Message::create([
                    'systemID' => 1,
                    'systemMessage' => 1,
                    'recipientID' => $userMarriage->player1,
                    'subject' => "Marriage",
                    'body' => "Your marriage proposal with " . Auth::user()->name . " has been declined.",
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'timestamp' => Carbon::now(),
                    'seen' => 0
                ]);

                return redirect()->route('marriage.show')->with('message', "You declined the marriage proposal with " . $userMarriage->name);
            }
            else{
                return redirect()->route('marriage.show')->with('error', "Can't find the marriage proposal");
            }
        }
        elseif($action == "divorce"){
            $userMarriageCheck = DB::table('user_marriage')
                ->where('user_marriage.player2', '=', Auth::id())
                ->orWhere('user_marriage.player1', '=', Auth::id())
                ->first();

            if($userMarriageCheck){

                if(Auth::id() == $userMarriageCheck->player1){
                    DB::table('user_marriage')
                        ->where('player1', Auth::id())
                        ->delete();

                    Message::create([
                        'systemID' => 1,
                        'systemMessage' => 1,
                        'recipientID' => $userMarriageCheck->player2,
                        'subject' => "Divorce",
                        'body' => "After many fights and many appointments at couples therapy, you have found out to get a divorce. From now on you are single",
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'timestamp' => Carbon::now(),
                        'seen' => 0
                    ]);

                    return redirect()->route('marriage.show')->with('message', "You are now divorced!");
                }
                if(Auth::id() == $userMarriageCheck->player2){
                    DB::table('user_marriage')
                        ->where('player2', Auth::id())
                        ->delete();

                    Message::create([
                        'systemID' => 1,
                        'systemMessage' => 1,
                        'recipientID' => $userMarriageCheck->player1,
                        'subject' => "Divorce",
                        'body' => "After many fights and many appointments at couples therapy, you have found out to get a divorce. From now on you are single",
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'timestamp' => Carbon::now(),
                        'seen' => 0
                    ]);

                    return redirect()->route('marriage.show')->with('message', "You are now divorced!");
                }
            }
            else{
                return redirect()->route('marriage.show')->with('error', "You are single, you can't divorce!");
            }
        }
        elseif($action == "cancel"){
            $userMarriageCheck = DB::table('user_marriage')
                ->where([
                    ['user_marriage.player1', '=', Auth::id()],
                    ['user_marriage.accepted', '=', 0]
                ])
                ->first();

            if($userMarriageCheck){
                DB::table('user_marriage')
                    ->where('player1', Auth::id())
                    ->delete();

                return redirect()->route('marriage.show')->with('message', "You cancelled your marriage proposal!");

            }
            else{
                return redirect()->route('marriage.show')->with('error', "Your marriage proposal is already accepted or you don't have a marriage proposal");
            }
        }
        else{
            return redirect()->route('marriage.show')->with('error', "Error");
        }
    }

    public function moneyAttack($name){
        $playerHidden = User::with('hidden')->find(Auth::id());
        $hidden = $playerHidden->hidden->sortByDesc('endTime')->first()->endTime;

        $dateParse = Carbon::parse($hidden)->format('Y/m/d H:i:s');
        $dateNew = Carbon::createFromFormat('Y/m/d H:i:s',  $dateParse);
        $hidden = $dateNew->isPast();

        $playerLastAttack = Attack::orderBy('created_at', 'desc')->where('player1','=',Auth::id())->first();

        function attack($playerLastAttack,$name){
            $player2 = User::where('username', '=' , $name)->first();
            $securityCheck = Security::orderBy('endTime', 'desc')->where('userID','=',$player2->id)->first();

            if(empty($securityCheck->endTime) || Carbon::now('Europe/Amsterdam') >= $securityCheck->endTime){
                if(empty($playerLastAttack->created_at) || $playerLastAttack->created_at <= Carbon::now('Europe/Amsterdam')->subSeconds(10)){
                    $profileAttacker = Profile::find(Auth::id());

                    $player2 = User::where('username', '=' , $name)->first();
                    $profileDefender = Profile::findOrFail($player2->id);

                    $combinedStatsAttacker = $profileAttacker->clicks + $profileAttacker->power;  // 60% chance
                    $combinedStatsDefender = $profileDefender->clicks + $profileDefender->power;
                    $rank = Rank::find(Auth::id())->rankID - 1; // max 15% chance

                    if($combinedStatsAttacker == 0){
                        $divide = 1 / $combinedStatsDefender;
                    }
                    elseif($combinedStatsDefender == 0){
                        $divide = $combinedStatsAttacker / 1;
                    }
                    else{
                        $divide = $combinedStatsAttacker / $combinedStatsDefender;
                    }

                    function attacking($defender,$result){
                        $minPercent = 15;
                        $maxPercent = 35;

                        if($result === 1){
                            $defenderCash = Cash::where('userID', '=', $defender->id)->first();
                            $attackerCash = Cash::where('userID', '=', Auth::id())->first();

                            $cash = round($defenderCash->cash / 100 * mt_rand($minPercent,$maxPercent));

                            $cashMin = $defenderCash->cash - $cash;
                            $cashPlus = $attackerCash->cash + $cash;

                            Cash::where('userID', '=', $defender->id)->update(['cash' => $cashMin]);
                            Cash::where('userID', '=', Auth::id())->update(['cash' => $cashPlus]);

                            Attack::create([
                                'player1' => Auth::id(),
                                'player2' => $defender->id,
                                'cash' => $cash,
                                'result' => $result,
                                'created_at' => Carbon::now('Europe/Amsterdam'),
                                'updated_at' => Carbon::now('Europe/Amsterdam')
                            ]);

                            return $cash;
                        }
                        elseif($result === 0){
                            $defenderCash = Cash::where('userID', '=', $defender->id)->first();
                            $attackerCash = Cash::where('userID', '=', Auth::id())->first();

                            $cash = round($attackerCash->cash / 100 * mt_rand($minPercent,$maxPercent));

                            $cashMin = $attackerCash->cash - $cash;
                            $cashPlus = $defenderCash->cash + $cash;

                            Cash::where('userID', '=', Auth::id())->update(['cash' => $cashMin]);
                            Cash::where('userID', '=', $defender->id)->update(['cash' => $cashPlus]);

                            Attack::create([
                                'player1' => Auth::id(),
                                'player2' => $defender->id,
                                'cash' => $cash,
                                'result' => $result,
                                'created_at' => Carbon::now('Europe/Amsterdam'),
                                'updated_at' => Carbon::now('Europe/Amsterdam')
                            ]);

                            return $cash;
                        }
                    }

                    if($divide >= 0.9){
                        $chance = 60 + $rank;
                        #$random = mt_rand(1, (1 / $chance) * 100);
                        $random = random_int(0,100);
                        $player = User::where('username', '=' , $name)->first();

                        if($random <= $chance){
                            $attack = attacking($player,1);
                            $message = "You attacked ". ucfirst($name) ." and you won \xE2\x82\xAc " . number_format($attack, 0, ',', '.');
                        }
                        else{
                            $attack = attacking($player,0);
                            $message = "You attacked" .ucfirst($name). " but you lost, you lost \xE2\x82\xAc " . number_format($attack, 0, ',', '.');
                        }
                        return view('player.attack',compact('message'));
                    }
                    else{
                        $player = User::where('username', '=' , $name)->first();
                        $attack = attacking($player,0);
                        $message = "You attacked " . ucfirst($name) . " but you had no chance, you lost \xE2\x82\xAc" . number_format($attack, 0, ',', '.');

                        return view('player.attack',compact('message'));
                    }
                }
                else{
                    $message = "You already attacked a player in the last 10 seconds";
                    return view('player.attack',compact('message'));
                }
            }
            else{
                $message = ucfirst($name) . " has currently security, you can't attack him/her now";
                return view('player.attack',compact('message'));
            }
        }

        if($hidden){
            return attack($playerLastAttack,$name);
        }
        else{
            $message = "You are hidden, you can't attack now.";
            return view('player.attack',compact('message'));
        }
    }

    public function hidden(){
        $user = User::with('cash','hidden')->findOrFail(Auth::id());

        $userHidden = $user->hidden->sortByDesc('endTime')->first();

        if(!empty($userHidden)){
            $dateParse = Carbon::parse($userHidden->endTime)->format('d/m/Y H:i:s');
            $dateNew = Carbon::createFromFormat('d/m/Y H:i:s',  $dateParse);

            if($dateNew->isPast()){
                $isHidden = FALSE;
                $lastHidden = null;
            }
            else{
                $isHidden = TRUE;
                $lastHidden = Carbon::createFromFormat('d/m/Y H:i:s',  $dateParse)->format('d/m/Y H:i:s');
            }
        }
        else{
            $isHidden = FALSE;
            $lastHidden = null;
        }

        return view('player.hide', compact('isHidden','lastHidden'));
    }

    public function hiddenForm(Request $request){
        $user = User::with('family','cash','hidden')->findOrFail(Auth::id());
        $userHidden = $user->hidden->sortByDesc('endTime')->first();

        $hours = $request->input('hours');

        if($user->family){
            $familyShares = $user->family->shares;
        }
        else{
            $familyShares = null;
        }

        if($hours >= 1 && $hours <= 12){
            if(!empty($familyShares) || $familyShares >= 1000){
                $hiddenCost = $familyShares * 10 * $hours;
            }
            else{
                $hiddenCost = 1000 * $hours;
            }
            return $this->hide($userHidden, $user, $hiddenCost, $hours);
        }
        else{
            return redirect()->route('hidden')->with('error', "Invalid Hours");
        }
    }

    /**
     * @param $userHidden
     * @param $user
     * @param $hiddenCost
     * @param $hours
     * @return \Illuminate\Http\RedirectResponse|void
     */

    public function hide($userHidden, $user, $hiddenCost, $hours)
    {
        if (empty($userHidden) || Carbon::now('Europe/Amsterdam') >= $userHidden->endTime) {
            if ($user->funds->cash >= $hiddenCost) {
                Cash::where('userID', '=', Auth::id())->update(['cash' => $user->funds->cash - $hiddenCost]);
                Hidden::create([
                    'userID' => Auth::id(),
                    'beginTime' => Carbon::now('Europe/Amsterdam'),
                    'endTime' => Carbon::now('Europe/Amsterdam')->addHours($hours),
                    'hours' => $hours
                ]);

                return redirect()->route('hidden')->with('message', "You are hidden");
            } else {
                return redirect()->route('hidden')->with('error', "Not Enough Cash");
            }
        } else {
            return redirect()->route('hidden')->with('error', "You are already hidden, last time:" . $userHidden->endTime . " time now" . Carbon::now('Europe/Amsterdam'));
        }
    }


    public function test(){
        $killer = User::with('country','defense','profile','getCurrentRank')->where('username', '=', Auth::user()->username)->firstOrFail(); // Find killer
        $getRep = Ranks::where('id','=',$killer->getCurrentRank->rankID)->first('rep')->rep;

        ddd($getRep);
    }

}

