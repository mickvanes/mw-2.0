<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Friends extends Model
{
    use HasFactory;

    protected $table = 'user_friends';

    protected $fillable = [
        'userID',
        'friendID',
        'accepted',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function friend()
    {
        return $this->belongsTo(User::class, 'friendID');
    }
}
