<?php

namespace App\Http\Controllers;

use App\Models\Cash;
use App\Models\Defense;
use App\Models\House;
use App\Models\User;
use App\Models\UserHouse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use function Illuminate\Events\queueable;

class HouseController extends Controller
{

    public function house()
    {
        $user = auth()->user();

        $userName = $user->name;

        $userCash = DB::table('user_cash')
            ->select('cash')
            ->where('user_cash.userID', '=', $user->id)
            ->pluck('cash')
            ->first();

        $userBank = DB::table('user_cash')
            ->select('bank')
            ->where('user_cash.userID', '=', $user->id)
            ->pluck('bank')
            ->first();

        $housesCheck = DB::table('user_country_house')
            ->join('users','user_country_house.userID', '=', 'users.id')
            ->join('houses','user_country_house.houseID', '=', 'houses.id')
            ->join('countries','user_country_house.countryID', '=', 'countries.id')
            ->select('users.username as userName','countries.name as countryName','houses.name as houseName','houses.defense as defense')
            ->where('user_country_house.userID', '=', $user->id)
            ->first();

        $houses = DB::table('houses')
            ->select('*')
            ->get();

        $currentCountryUser = DB::table('user_country')
            ->join('countries','user_country.countryID', '=', 'countries.id')
            ->select('countries.name as currentCountry')
            ->where('user_country.userID', '=', $user->id)
            ->first();

        if($housesCheck) {
            if($housesCheck->countryName == $currentCountryUser->currentCountry){
                $currentCountryBoolean = true;
                $text = nl2br("Welcome home in your ". $housesCheck->houseName. " in ". $housesCheck->countryName . ",\n you can use now your safe!");
            }
            else{
                $currentCountryBoolean = false;
                $text = $currentCountryUser->currentCountry . "To use your safe you need to fly to " . $housesCheck->countryName . "";
            }
            return view('house.house', compact('text','currentCountryBoolean','userCash','userBank','userName'));
        }
        else{
            return view('house.buyHouse', compact('currentCountryUser','houses','userCash','userBank','userName'));
        }
    }

    public function safe()
    {
        $userID = Auth::id();

        $userName = Auth::user()->username;

        $userCash = DB::table('user_cash')
            ->select('cash')
            ->where('user_cash.userID', '=', $userID)
            ->pluck('cash')
            ->first();

        $userBank = DB::table('user_cash')
            ->select('bank')
            ->where('user_cash.userID', '=', $userID)
            ->pluck('bank')
            ->first();

        $userSafe = DB::table('user_cash')
            ->select('safe')
            ->where('user_cash.userID', '=', $userID)
            ->pluck('safe')
            ->first();

        $housesCheck = DB::table('user_country_house')
            ->join('users','user_country_house.userID', '=', 'users.id')
            ->join('houses','user_country_house.houseID', '=', 'houses.id')
            ->join('countries','user_country_house.countryID', '=', 'countries.id')
            ->select('users.username as userName','countries.name as countryName','houses.name as houseName','houses.defense as defense')
            ->where('user_country_house.userID', '=', $userID)
            ->first();

        $houses = DB::table('houses')
            ->select('*')
            ->get();

        $currentCountryUser = DB::table('user_country')
            ->join('countries','user_country.countryID', '=', 'countries.id')
            ->select('countries.name as currentCountry')
            ->where('user_country.userID', '=', $userID)
            ->first();

        if($housesCheck) {
            if($housesCheck->countryName == $currentCountryUser->currentCountry){
                $currentCountryBoolean = true;
                return view('house.safe', compact('housesCheck','currentCountryUser','currentCountryBoolean','userCash','userSafe','userBank','userName'));
            }
            else{
                $currentCountryBoolean = false;
                return view('house.house', compact('housesCheck','currentCountryUser','currentCountryBoolean','userCash','userSafe','userBank','userName'));
            }
        }
        else{
            return view('house.buyHouse', compact('currentCountryUser','houses','userCash','userBank','userName'));
        }
    }

    public function safeForm(Request $request)
    {
        $user = auth()->user();

        $currentCountryUser = DB::table('user_country')
            ->join('countries', 'user_country.countryID', '=', 'countries.id')
            ->select('countries.name as currentCountry')
            ->where('user_country.userID', '=', $user->id)
            ->first();

        $userCash = DB::table('user_cash')
            ->select('cash')
            ->where('user_cash.userID', '=', $user->id)
            ->pluck('cash')
            ->first();

        $userSafe = DB::table('user_cash')
            ->select('safe')
            ->where('user_cash.userID', '=', $user->id)
            ->pluck('safe')
            ->first();

        $housesCheck = DB::table('user_country_house')
            ->join('users', 'user_country_house.userID', '=', 'users.id')
            ->join('houses', 'user_country_house.houseID', '=', 'houses.id')
            ->join('countries', 'user_country_house.countryID', '=', 'countries.id')
            ->select('users.name as userName', 'countries.name as countryName', 'houses.name as houseName', 'houses.defense as defense')
            ->where('user_country_house.userID', '=', $user->id)
            ->first();

        if ($housesCheck) {
            if ($housesCheck->countryName == $currentCountryUser->currentCountry) {

                $this->validate($request, [
                    'safe_type' => 'required',
                    'transaction' => 'required|numeric',
                ]);

                $trans = $request->input('transaction');

                $transRule = DB::table('rules')
                    ->select('maxSafeCash')
                    ->pluck('maxSafeCash')
                    ->first();

                if ($request->input('safe_type') == 0) {
                    if ($userSafe > 0) {
                        if ($trans <= $userSafe) {
                            DB::table('user_cash')
                                ->where('userID', $user->id)
                                ->update(['safe' => $userSafe - $request->input('transaction')]);
                            DB::table('user_cash')
                                ->where('userID', $user->id)
                                ->update(['cash' => $userCash + $request->input('transaction')]);
                            return Redirect::back()->with('message', "You successfully withdraw €" . number_format($request->input('transaction'), 0, ',', '.') . ' of your safe.');
                        } else {
                            return Redirect::back()->with('error', "Your transaction (€" . number_format($request->input('transaction'), 0, ',', '.') . ") is higher than the money in your safe (€" . number_format($userSafe, 0, ',', '.') . ').');
                        }
                    }
                    else{
                        return Redirect::back()->with('error', "Your safe is empty");
                    }
                }
                if ($request->input('safe_type') == 1) {
                    if ($userCash > 0) {
                        if ($trans <= $userCash) {
                            if ($userSafe + $trans <= $transRule) {
                                DB::table('user_cash')
                                    ->where('userID', $user->id)
                                    ->update(['safe' => $userSafe + $request->input('transaction')]);
                                DB::table('user_cash')
                                    ->where('userID', $user->id)
                                    ->update(['cash' => $userCash - $request->input('transaction')]);
                                return Redirect::back()->with('message', "You successfully deposit €" . number_format($request->input('transaction'), 0, ',', '.') . ' to your safe.');
                            } else {
                                $total = $trans + $userSafe;
                                return Redirect::back()->with('error', "Your transaction (€" . number_format($request->input('transaction'), 0, ',', '.') . ") is higher in total (€" . number_format($total, 0, ',', '.') . ")" . " than the maximum amount of money allowed in your safe (€" . number_format($transRule, 0, ',', '.') . ').');
                            }
                        } else {
                            return Redirect::back()->with('error', "Your cash (€" . number_format($userCash, 0, ',', '.') . ") is €" . number_format(0, 0, ',', '.') . '.');
                        }
                    }
                    else{
                        return Redirect::back()->with('error', "You have no cash!");
                    }
                }
            }
            else {
                return redirect()->route('house',)->with('error', 'You are not in the same country of your house');
            }
        }
        else{
            return redirect()->route('house',)->with('error', "You don't have a house");
        }
    }

    public function buyHouse(){
        $check = UserHouse::where('userID', '=', Auth::id())->first();

        if(!$check){
            $houses = DB::table('houses')
                ->select('*')
                ->get();

            $user = Auth::user();

            $userName = $user->username;

            $userCash = DB::table('user_cash')
                ->select('cash')
                ->where('user_cash.userID', '=', $user->id)
                ->pluck('cash')
                ->first();

            $userBank = DB::table('user_cash')
                ->select('bank')
                ->where('user_cash.userID', '=', $user->id)
                ->pluck('bank')
                ->first();

            return view('house.buyHouse', compact('userCash','houses','userBank','userName'));
        }
        else{
            return redirect()->route('house')->with('message', "You have already a house");
        }

    }

    public function buyHouseForm($name){
        $user = User::with('cash','country')->findOrFail(Auth::id());
        $housesCheck = UserHouse::where('userID', '=', Auth::id())->first();
        $currentDefense = Defense::where('userID', '=',Auth::id())->first();

        if(!$housesCheck){
            $house = House::where('slug','=', $name)->first();

            if ($house->cost <= $user->cash->cash){
                Cash::where('userID','=',Auth::id())->update([
                    'cash' => $user->cash->cash - $house->cost
                ]);

                UserHouse::create([
                    'houseID' => $house->id,
                    'countryID' => $user->country->countryID,
                    'userID' => Auth::id(),
                ]);

                Defense::where('userID','=',Auth::id())->update([
                    'totalDefense' => $currentDefense->totalDefense + $house->defense
                ]);

                return redirect()->route('dashboard')->with('message', "You bought a ". $house->name . " in " . $user->country->country->name);
            }
            else{
                return redirect()->route('house.buy')->with('error', "You don't have enough cash for buying this house");
            }

        }
    }

    public function sellHouseForm(){
        $userID = Auth::id();

        $housesCheck = DB::table('user_country_house')
            ->join('users','user_country_house.userID', '=', 'users.id')
            ->join('houses','user_country_house.houseID', '=', 'houses.id')
            ->join('countries','user_country_house.countryID', '=', 'countries.id')
            ->select('users.username as userName','countries.name as countryName','houses.name as houseName','houses.defense as defense', 'user_country_house.houseID as houseID', 'houses.defense as defense')
            ->where('user_country_house.userID', '=', $userID)
            ->first();

        $currentDefense = Defense::where('userID', '=',Auth::id())->firstOrFail();

        $house = DB::table('houses')
            ->select('houses.cost as cost', 'houses.name as name')
            ->where('houses.id', '=', $housesCheck->houseID)
            ->first();

        $currentCountryUser = DB::table('user_country')
            ->join('countries', 'user_country.countryID', '=', 'countries.id')
            ->select('countries.name as currentCountry')
            ->where('user_country.userID', '=', $userID)
            ->first();

        if ($housesCheck->countryName == $currentCountryUser->currentCountry){
            if($housesCheck){
                $userCash = Cash::where('userID', '=' , Auth::id())->firstOrFail()->cash;
                $sellPrice = $house->cost * 0.75;
                UserHouse::where('userID','=', Auth::id())->delete();
                Cash::where('userID', '=' , Auth::id())->update([
                   'cash' => $userCash + $sellPrice,
                   'safe' => 0
                ]);
                Defense::where('userID','=',Auth::id())->update([
                   'totalDefense' =>  $currentDefense->totalDefense - $housesCheck->defense,
                   'house' => null
                ]);
                return redirect()->route('dashboard')->with('message', "You sold your " . $housesCheck->houseName . " in " . $housesCheck->countryName . " \n If you had money in your safe, you lost that money" );
            }
            return redirect()->route('dashboard')->with('error', "You don't own a house, you can't sell nothing" );
        }
        return redirect()->route('dashboard')->with('error', "You are not in the same country of your house or you don't own a house" );
    }


}
