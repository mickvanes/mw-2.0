<?php

namespace App\Http\Controllers;

use App\Models\Cash;
use App\Models\Poker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PokerController extends Controller
{
    public function index(){
        $allPokers = Poker::where([['inProgress', '=', 1],['playing','=', 0]])->get();

        dd($allPokers);
    }

    public function create(Request $request){
        $playerMoney = Cash::where('userID', Auth::id())->pluck('cash')->first();

        $timeOuts = [60,90,120];

        $this->validate($request, [
            'ante' => 'required|numeric|min:10000',
            'raiseLimit' => 'numeric|nullable',
            'timeOut' => 'required|in_array:timeOuts',
            'playerCount' => 'required|integer|between:2,4'
        ]);

        $pokerAlreadyCreated = Poker::where(
            ['player1', '=', Auth::id()],
            ['inProgress', '=' , 1],
            ['playing', '=' , 0],
            ['completed', '=', 0]
        )->get();

        $pokerAlreadyInGame = Poker::where(
            ['player1' , '=' , Auth::id()],
            ['playing', '=', 1]
        )->orWhere(
            ['player2' , '=' , Auth::id()],
            ['playing', '=', 1]
        )->orWhere(
            ['player3' , '=' , Auth::id()],
            ['playing', '=', 1]
        )->orWhere(
            ['player4' , '=' , Auth::id()],
            ['playing', '=', 1]
        )->get();

        if(!$pokerAlreadyCreated || !$pokerAlreadyInGame){
            if(!$request->ante > $playerMoney){
                Poker::create([
                    'jackpot' => $request->ante,
                    'raiseLimit' => $request->raiseLimit,
                    'timeOut' => $request->timeOut,
                    'player1' => Auth::id(),
                    'playerCount' => $request->playerCount,
                    'inProgress' => TRUE,
                    'playing' => FALSE,
                    'completed' => FALSE
                ]);
            }
            else{
                return "Je hebt te weinig contante geld!";
            }
        }
        else{
            return "Je zit al in een poker!";
        }
    }

    public function raise(){

    }

    public function allIn(){

    }

    public function changeCards(){

    }

    public function fold(){

    }

    public function leave(){

    }




}
