<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageReport extends Model
{
    use HasFactory;

    protected $table = 'message_report';

    protected $fillable = [
        'itemID',
        'content',
        'user',
        'repUser',
        'treatedBy',
        'active',
        'approved',
        'declined',
    ];

    public function message()
    {
        return $this->belongsTo(Message::class, 'itemID','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id','user');
    }

    public function treatedBy()
    {
        return $this->belongsTo(User::class, 'id','treatedBy');
    }

    public function repUser()
    {
        return $this->belongsTo(User::class, 'id','repUser');
    }

}
