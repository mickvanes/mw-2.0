<?php

namespace App\Http\Controllers;

use App\Models\FlashMessage;
use App\Models\Burst;
use App\Models\BurstRank;
use App\Models\Business;
use App\Models\BusinessBank;
use App\Models\BusinessTransaction;
use App\Models\Cash;
use App\Models\Country;
use App\Models\Jail;
use App\Models\Message;
use App\Models\Profile;
use App\Models\Rule;
use App\Models\Settings;
use App\Models\SystemMessage;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class JailController extends Controller
{
    public function index(){
        $userCountry = Auth::user()->country->countryID;

        $jails = Jail::where([
            ['countryID', '=' , $userCountry],
            ['endTime', '>=' , Carbon::now('Europe/Amsterdam')]
        ])->get();

        $country = Auth::user()->country->countryDetails->name;
        $prisonOwner = Business::where([
            ['typeID','=',1] ,
            ['countryID', '=', Auth::user()->country->countryID]
        ])->first();

        if($prisonOwner->ownerID == null){
            $prisonOwner = "Government";
            $businessID = null;
            $owner = false;
        }
        else{
            if($prisonOwner->ownerID == Auth::id()){
                $businessID = $prisonOwner->id;
                $prisonOwner = $prisonOwner->owner->name;
                $owner = true;
            }
            else{
                $prisonOwner = $prisonOwner->owner->name;
                $businessID = null;
                $owner = false;
            }
        }

        $burstRank = Profile::with('rank')->find(Auth::id());

        $costsPerSec = Rule::latest()->first()->jailCosts;

        return view('prison.index', compact('jails','costsPerSec','burstRank','country','prisonOwner','owner','businessID'));
    }

    public function owner(){
        $country = Country::where('userID','=', Auth::id())->first()->countryID;
        $business = Business::where([ ['ownerID', '=', Auth::id()] , ['countryID','=', $country] , ['typeID','=', 1]])->first();

        if($business){
            $businessID = $business->id;
            $now = Carbon::today();

            $start = $now->copy()->startOfWeek();
            $end = $now->copy()->endOfWeek();

            $startLastWeek = $now->copy()->startOfWeek()->subweek();
            $endLastWeek = $now->copy()->endOfWeek()->subweek();

            $firstDayCosts = $now->copy()->subDays(7);
            $lastDayCosts = $now->copy()->subDay(1);

            $costsNight = (BusinessTransaction::where([
                ['transactionType','=',2],
                ['businessID','=',$businessID],
            ])->whereBetween('created_at', [$firstDayCosts, $lastDayCosts])->avg('cash') * 25);

            $bank = BusinessBank::where([
                ['id','=',$businessID],
                ['ownerID','=', Auth::id()]
            ])->first()->bank;

            $revenue = BusinessTransaction::where([
                ['transactionType','=',2],
                ['owner','=',Auth::id()],
                ['businessID','=',$businessID],
            ])->whereBetween('created_at', [$start, $end])->sum('cash');

            $revenueLastWeek = BusinessTransaction::where([
                ['transactionType','=',2],
                ['owner','=',Auth::id()],
                ['businessID','=',$businessID],
            ])->whereBetween('created_at', [$startLastWeek, $endLastWeek])->sum('cash');

            $userCash = Auth::user()->cash->cash;
            $userBank = Auth::user()->cash->bank;

            return view('prison.manage',compact('businessID','bank','revenue','revenueLastWeek','userCash','userBank','costsNight'));
        }
        else{
            return Redirect::back()->with('error', "The prison is not in the same country as you are in or you don't own a prison");
        }
    }

    public function buyOut($id){
        $user = User::with('cash')->find(Auth::id());
        $cash = $user->cash->cash;

        $jail = Jail::find($id);

        $now = Carbon::now();
        $endTime = Carbon::parse($jail->endTime);

        $totalDuration = $endTime->diffInSeconds($now);

        $costsPerSec = Rule::latest()->first()->jailCosts;
        $costs = $totalDuration * $costsPerSec;

        if($costs > $cash && $totalDuration >= 1){
            return redirect()->action([JailController::class, 'index'])->with('error', "You don't have €" . number_format($costs, 0, ',', '.') . ' cash.');
        }
        elseif($costs <= $cash && $totalDuration >= 1){
            $country = Country::where('userID', '=', Auth::id())->pluck('countryID')->first();

            if($country == $jail->countryID){
                Cash::where('userID', Auth::id())->update([ 'cash' => $cash - $costs ]);
                $jail->delete();
                if(Settings::where('userID','=',$jail->userID)->get()->pluck('buyOutJail')->first()) {
                    Message::create([
                        'systemID' => 1,
                        'systemMessage' => 1,
                        'recipientID' => $jail->userID,
                        'subject' => "Buy Out Prison",
                        'body' => "You were bought out of jail by " . ucfirst(Auth::user()->name) . " for €" . $costs,
                        'seen' => 0,
                        'timestamp' => Carbon::now(),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
                }

                $business = Business::where([
                    ['countryID', '=', $country],
                    ['typeID', '=', 1]
                ])->first();

                BusinessTransaction::create([
                    'businessID' => $business->id,
                    'transactionType' => 2,
                    'cash' => $costs / 2,
                    'owner' => $business->ownerID,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                Burst::create([
                    'prisoner' => $jail->userID,
                    'burster' => Auth::id(),
                    'buyout' => true,
                    'buyOutCash' => $costs
                ]);

                FlashMessage::create([
                    'userID' => $jail->userID,
                    'message' => "You were bought out of prison by " . ucfirst(Auth::user()->name) . " for €". number_format($costs, 0, ',', '.')
                ]);

                $bank = BusinessBank::where('id', $business->id)->first()->bank;

                BusinessBank::where('id', $business->id)
                    ->update(['bank' => $bank + ($costs / 2)]);

                return redirect()->action([JailController::class, 'index'])->with('message', "You bought " . $jail->user->name . " out of jail for €" . number_format($costs, 0, ',', '.'));
            }
            else{
                return Redirect::back()->with('error', "The prison is not in the same country as you are in ");
            }
        }
        elseif ($totalDuration <= 0){
            return Redirect::back()->with('error', "This person is already out of jail ");
        }
        else{
            return Redirect::back()->with('error', "Report back to an Admin - FP-01"); // Fault Prison-01
        }
    }

    public function burst($id){
        $jail = Jail::find($id);

        $now = Carbon::now();
        $endTime = Carbon::parse($jail->endTime);

        $totalDuration = $endTime->diffInSeconds($now);
        $country = Country::where('userID', '=', Auth::id())->pluck('countryID')->first();

        if($jail->countryID == $country){
            if($totalDuration >= 1){
                $burstRank = Profile::where('id','=', Auth::id())->first()->burstRank;
                $burstAdv = Profile::where('id','=', Auth::id())->first()->burstAdv;
                $bursts = Profile::find(Auth::id())->pluck('bursts')->first();
                $percent = BurstRank::find($burstRank)->percent;
                $progress = floatval($burstAdv) + floatval($percent);
                $chance = (bool)rand(0, 1);

                if($chance){
                    if($burstRank == 14){
                        DB::table('profiles')
                            ->where('id', Auth::id())
                            ->update([
                                'burstAdv' => $progress,
                                'bursts' => $bursts + 1
                            ]);

                        Burst::create([
                            'prisoner' => $jail->userID,
                            'burster' => Auth::id(),
                            'buyout' => false,
                        ]);

                        FlashMessage::create([
                            'userID' => $jail->userID,
                            'message' => "You were broken out of prison by " . ucfirst(Auth::user()->name)
                        ]);

                        return redirect()->action([JailController::class, 'index'])->with('message', "You burst " . ucfirst($jail->user->name) . " out of jail");
                    }
                    else{
                        if($progress >= 100){
                            DB::table('profiles')
                                ->where('id', Auth::id())
                                ->update([
                                    'burstRank' => $burstRank + 1,
                                    'burstAdv' => 0,
                                    'bursts' => $bursts + 1
                                ]);

                            Burst::create([
                                'prisoner' => $jail->userID,
                                'burster' => Auth::id(),
                                'buyout' => false,
                            ]);

                            FlashMessage::create([
                                'userID' => $jail->userID,
                                'message' => "You were broken out of prison by " . ucfirst(Auth::user()->name)
                            ]);

                            $jail->delete();

                            return redirect()->action([JailController::class, 'index'])->with('message',"You burst " . ucfirst($jail->user->name) . " out of jail");
                        }
                        else{
                            DB::table('profiles')
                                ->where('id', Auth::id())
                                ->update([
                                    'burstAdv' => $progress,
                                    'bursts' => $bursts + 1
                                ]);

                            Burst::create([
                                'prisoner' => $jail->userID,
                                'burster' => Auth::id(),
                                'buyout' => false,
                            ]);

                            FlashMessage::create([
                                'userID' => $jail->userID,
                                'message' => "You were broken out of prison by " . ucfirst(Auth::user()->name)
                            ]);

                            $jail->delete();

                            return redirect()->action([JailController::class, 'index'])->with('message',"You burst " . ucfirst($jail->user->name) . " out of jail");
                        }
                    }
                }
                else{
                    $business = Business::where([
                        ['countryID', '=', $country],
                        ['typeID', '=', 1]
                    ])->first();

                    Jail::create([
                        'userID' => Auth::id(),
                        'countryID' => $country,
                        'reason' => "Failed Burst",
                        'businessID' => $business->id,
                        'endTime' => Carbon::now()->addSeconds(15)
                    ]);

                    return redirect()->action([JailController::class, 'jailed'])->with('error', "You failed to burst " . ucfirst($jail->user->name) . " out of jail, the police arrested you");
                }
            }
            else{
                return Redirect::back()->with('error', "This person is already out of jail ");
            }
        }
        else{
            return Redirect::back()->with('error', "The prison is not in the same country as you are in ");
        }
    }

    public function jailed(){
        $jailed = Jail::orderBy('endTime', 'desc')->where('userID','=', Auth::id())->first();

        $dateParse = Carbon::parse($jailed->endTime)->format('Y/m/d H:i:s');
        $dateNew = Carbon::createFromFormat('Y/m/d H:i:s',  $dateParse);
        $jailed = $dateNew->isPast();

        if(!$jailed){

            $jailed = Jail::orderBy('endTime', 'desc')->where('userID','=', Auth::id())->first();
            $id = $jailed->id;

            $dateNew = Carbon::createFromFormat('Y/m/d H:i:s',  $dateParse)->format('d/m/Y H:i:s');
            $costsPerSec = Rule::latest()->first()->jailCosts * 1.5;
            $user = strtolower(Auth::user()->name);

            return view('prison.jailed', compact('costsPerSec','dateNew','id','user'));
        }
        else{
            return redirect()->route('dashboard');
        }
    }

    public function jailedForm($id)
    {
        $jail = Jail::find($id);

        $dateParse = Carbon::parse($jail->endTime)->format('Y/m/d H:i:s');
        $dateNew = Carbon::createFromFormat('Y/m/d H:i:s', $dateParse);
        $jailed = $dateNew->isPast();

        if (!$jailed) {
            $cash = Auth::user()->cash->cash;

            $now = Carbon::now();
            $endTime = Carbon::parse($jail->endTime);
            $totalDuration = $endTime->diffInSeconds($now);

            $costsPerSec = Rule::latest()->first()->jailCosts * 2;
            $costs = $totalDuration * $costsPerSec;

            if ($costs > $cash && $totalDuration >= 1) {
                return redirect()->action([JailController::class, 'jailed'])->with('error', "You don't have €" . number_format($costs, 0, ',', '.') . ' cash.');
            } elseif ($costs <= $cash && $totalDuration >= 1) {

                Cash::where('userID', Auth::id())->update(['cash' => $cash - $costs]);

                $bank = BusinessBank::where('id', $jail->businessID)->first()->bank;

                BusinessBank::where('id', $jail->businessID)
                    ->update(
                        ['bank' => $bank + ($costs / 4)]
                    );

                $ownerID = Business::where([
                    ['typeID','=',1],
                    ['businessID','=',$jail->businessID]
                ])->first()->ownerID;

                BusinessTransaction::create([
                    'businessID' => $jail->businessID,
                    'transactionType' => 2,
                    'cash' => $costs / 4,
                    'owner' => $ownerID,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                $jail->delete();
            } elseif ($totalDuration <= 0) {
                return Redirect::back()->with('error', "You are already out of prison");
            }
        }
        else{
            return redirect()->action([JailController::class, 'prison'])->with('message', "You are already out of prison");
        }
    }
}
