<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Witness extends Model
{
    use HasFactory;

    protected $table = 'witnesses';

    protected $fillable = [
        'userID',
        'victimID',
        'killerID',
        'marketCode',
        'validUntil'
    ];

    public function witness()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function victim()
    {
        return $this->belongsTo(User::class, 'victimID');
    }

    public function killer()
    {
        return $this->belongsTo(User::class, 'killerID');
    }
}
