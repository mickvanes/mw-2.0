@extends('layouts.default')

@section('header')
    SHOP WEAPONS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-success">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Shop</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif
                    @if(!$defenses->weapon)
                        Buy a weapon<br><br>

                        <form method="POST" action="{{ route("weapons.post") }}">
                            @csrf
                            <div class="grid grid-cols-1 md:grid-cols-2">
                            @foreach($weapons as $weapon)
                                    <div class="grid grid-cols-1 mb-4 w-75">
                                        <img src="{{ $weapon->image }}" class="w-75">
                                        <span>{{ $weapon->name }}</span>
                                        <span>{{ $weapon->defense }} Defense</span>
                                        <span>Costs: &euro;{{ number_format($weapon->costs, 0, ',', '.')  }}</span>
                                        <a href="{{ route('shop.buyAirplane', $weapon->id) }}">Buy this weapon</a>
                                    </div>
                            @endforeach
                            </div>
                        </form>
                        @else
                            <div class="p-2">
                                <div class="grid grid-cols-1 mb-4 md:w-6/12 md:w-9/12 mx-auto">
                                    <span class="text-center font-semibold text-xl my-2 ">Your weapon</span>
                                    <img src="{{ $defenses->gun->image }}" class="w-25">
                                    <div class="grid grid-cols-1 mt-2 text-center">
                                        <span>{{ $defenses->gun->name }}</span>
                                        <span>{{ $defenses->gun->defense }} Defense</span>
                                        <span>Costs: &euro;{{ number_format($defenses->gun->costs, 0, ',', '.')  }}</span>
                                    </div>
                                    <span class="mt-4 text-center"><a href="{{ route('shop.sellAirplane') }}">Sell your weapon for &euro;{{ number_format($defenses->gun->costs * 0.50, 0, ',', '.') }}</a></span>
                                </div>
                            </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
@endsection


