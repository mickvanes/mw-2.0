<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marriage extends Model
{
    use HasFactory;

    protected $table = 'user_marriage';

    protected $fillable = [
        'player1',
        'player2',
        'accepted',
    ];

    public function player1Details()
    {
        return $this->belongsTo(User::class, 'player1');
    }

    public function player2Details()
    {
        return $this->belongsTo(User::class, 'player2');
    }

}
