@extends('layouts.default')

@section('header')
    LOGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1 w-3/4 m-auto">
                    @foreach($businesses as $businessType)
                        <div class="mt-2">
                            <div class="grid grid-cols-3">
                                <span class="col-span-3 text-center mb-2"><b>{{ $businessType->name }}s</b></span>
                                <span>Country</span>
                                <span>Owner</span>
                                <span>Revenue</span>
                            </div>
                            <div class="grid grid-cols-3">
                                @foreach($businessType->business as $business)
                                    @if($business->country->active == 1)
                                        <span>{{ $business->country->name }}</span>
                                        <span>
                                            @if($business->ownerID == null)
                                                Government
                                            @else
                                                <a href="{{ route('player.show', $business->owner->username) }}"> {{ $business->owner->username }}</a>
                                            @endif</span>
                                        <span>
                                            @if($business->revenue->sum('cash') == 0)
                                                <span class="text-yellow-600">&euro;{{ number_format($business->revenue->sum('cash'), 0, ',', '.') }}</span>
                                            @elseif($business->revenue->sum('cash') > 1)
                                                <span class="text-green-600">&euro;{{ number_format($business->revenue->sum('cash'), 0, ',', '.') }}</span>
                                            @else
                                                <span class="text-red-600">&euro;{{ number_format($business->revenue->sum('cash'), 0, ',', '.') }}</span>
                                            @endif</span>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                            @endforeach
                        <span class="mt-4">{{ $businesses->links() }}</span>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
