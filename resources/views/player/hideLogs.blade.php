@extends('layouts.default')

@section('header')
    RACE
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div>
                            <table class="table-fixed w-full md:w-3/4">
                                Hide Logs
                                <thead>
                                <tr>
                                    <th class="text-left">Begin Time</th>
                                    <th class="text-left">End Time</th>
                                    <th class="text-left">Hours</th>
                                </tr>
                                </thead>

                                @foreach($hiddenLogs as $hiddenLogsItem)
                                    <tr>
                                        <td>{{ $hiddenLogsItem->beginTime  }}</td>
                                        <td>{{ $hiddenLogsItem->endTime  }}</td>
                                        <td>{{ $hiddenLogsItem->hours }}</td>
                                    </tr>
                                @endforeach
                            </table>
                            {{ $hiddenLogs->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

