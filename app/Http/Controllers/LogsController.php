<?php

namespace App\Http\Controllers;

use App\Models\Attack;
use App\Models\BusinessTransaction;
use App\Models\CompletedRace;
use App\Models\FamilyTransaction;
use App\Models\Hidden;
use App\Models\Race;
use App\Models\Security;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LogsController extends Controller
{
    public function index(){
        $logs = [
            ['security.logs','Security'],
            ['player.transactions','Transactions'],
            ['player.ranks','Rank logs'],
            ['race.logs','Race logs'],
            ['player.hideLogs','Hide logs'],
            ['player.ripLogs','Rip logs']
        ];

        return view('logs', compact('logs'));
    }


    public function security(){
        $securityLogs = Security::where('userID','=', Auth::id())->paginate('15');

        return view('player.securityLogs', compact('securityLogs'));
    }

    public function rip(){

        $ripAttack = Attack::with('defender')->where('player1','=', Auth::id())->orderByDesc('created_at')->take(10)->get();
        $ripDefense = Attack::with('attacker')->where('player2','=', Auth::id())->orderByDesc('created_at')->take(10)->get();

        return view('player.ripLogs', compact('ripDefense','ripAttack'));
    }

    public function attacks()
    {
        $ripAttack = Attack::with('defender')->where('player1', '=', Auth::id())->orderByDesc('created_at')->paginate(15);

        return view('player.ripAttacks', compact('ripAttack'));
    }

    public function defenses()
    {
        $ripDefense = Attack::with('attacker')->where('player2', '=', Auth::id())->orderByDesc('created_at')->paginate(15);

        return view('player.ripDefenses', compact('ripDefense'));
    }

    public function hide(){
        $hiddenLogs = Hidden::with('user')->where('userID', '=', Auth::id())->orderByDesc('endTime')->paginate(15);

        return view('player.hideLogs', compact('hiddenLogs'));
    }

    public function transactions(){
        $fromUserTransactions = Transaction::with('sender')->where('toUserID','=', Auth::id())->orderByDesc('created_at')->paginate(15, ['*'], 'fromUser');
        $toUserTransactions = Transaction::with('receiver')->where('fromUserID','=', Auth::id())->orderByDesc('created_at')->paginate(15, ['*'], 'toUser');
        $toFamilyTransactions = FamilyTransaction::with('family')->where('userID', '=' , Auth::id())->orderByDesc('created_at')->paginate(15, ['*'], 'toFamily');

        return view('player.transactions', compact( 'fromUserTransactions', 'toUserTransactions','toFamilyTransactions'));
    }

    public function ranks(){
        $ranks =    DB::table('user_rank_logs')
            ->join('ranks', 'user_rank_logs.rankID', '=' , 'ranks.id')
            ->orderByDesc('created_at')
            ->where('user_rank_logs.userID', '=', Auth::id())
            ->paginate(15);

        return view('player.ranks', compact( 'ranks'));
    }

    public function races(){
        $races = CompletedRace::with('racer1','racer2')->orderBy('created_at','desc')->where('racer1_ID', Auth::id())->orWhere('racer2_ID', Auth::id())->paginate(15);

        $winRates = CompletedRace::with('racer1','racer2')->orderBy('created_at','desc')->where('racer1_ID', Auth::id())->orWhere('racer2_ID', Auth::id())->get();

        $winsPage = 0;
        $lostsPage = 0;

        $wins = 0;
        $losts = 0;

        foreach ($races as $race){
            if($race->winner == Auth::id()){
                $winsPage = $winsPage + 1;
            }
            else{
                $lostsPage = $lostsPage + 1;
            }
        }

        foreach ($winRates as $winRate){
            if($winRate->winner == Auth::id()){
                $wins = $wins + 1;
            }
            else{
                $losts = $losts + 1;
            }
        }

        return view('race.logs', compact('races','winsPage','lostsPage','wins','losts'));
    }

    public function businessLogs($id){
        $bankLogsDeposit = BusinessTransaction::where([['businessID','=',$id], ['owner', '=' , Auth::id()], ['transactionType','=', 1]])->orderByDesc('created_at')->paginate(15);
        $bankLogsWithdraw = BusinessTransaction::where([['businessID','=',$id], ['owner', '=' , Auth::id()], ['transactionType','=', 0]])->orderByDesc('created_at')->paginate(15);

        return view('businesses.bankLogs', compact('bankLogsDeposit','bankLogsWithdraw'));
    }

}
