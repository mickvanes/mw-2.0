@extends('layouts.default')

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <a class="block px-4 py-4 mt-2 text-sm font-semibold text-white-900 dark-mode:text-gray-200" href="{{ route('settings.user') }}"><i class="fas fa-user-cog fa-lg pr-2"></i>Settings</a>
                    <a class="block px-4 py-4 mt-2 text-sm font-semibold text-white-900 dark-mode:text-gray-200" href="{{ route('settings.profile') }}"><i class="fas fa-user-edit fa-lg pr-2"></i>Edit Profile</a>
                    <a class="block px-4 py-4 mt-2 text-sm font-semibold text-white-900 dark-mode:text-gray-200" href="{{ route('settings.password') }}"><i class="fas fa-user-lock fa-lg pr-2"></i>Change Password</a>
                    <a class="block px-4 py-4 mt-2 text-sm font-semibold text-white-900 dark-mode:text-gray-200" href="{{ route('settings.referrals') }}"><i class="fas fa-users fa-lg pr-2"></i>Refferals</a>
                    <a class="block px-4 py-4 mt-2 text-sm font-semibold text-white-900 dark-mode:text-gray-200" href="{{ route('settings.friends') }}"><i class="fas fa-user-friends fa-lg pr-2"></i>Friends</a>
                </div>
            </div>
        </div>
    </div>
@endsection

