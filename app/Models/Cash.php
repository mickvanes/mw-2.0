<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    use HasFactory;

    protected $table = 'user_cash';

    protected $fillable = [
        'cash',
        'bank',
        'safe',
        'time'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'userID');
    }


}
