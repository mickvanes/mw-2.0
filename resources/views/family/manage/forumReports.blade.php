@extends('layouts.default')

@section('header')
    FAMILY RECRUITMENT
@stop

@section('content')
<div class="mx-auto h-full overflow-hidden w-full">
    <div class="max-w-7xl mx-auto">
        <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
            <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
    @if(session()->has('flash'))
        <div class="alert alert-success">
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                    role="alert">
                <strong class="font-bold">System</strong>
                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
            </div>
        </div><br>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success">
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                    role="alert">
                <strong class="font-bold">Message</strong>
                <span class="block sm:inline">{{ session()->pull('message') }}</span>
            </div>
        </div><br>
    @endif
    <div x-show="show === 0"></div>

    <div class="grid grid-cols-1" x-data="{ show: 0 }">
        <table>
            <thead>
            <th class="text-left">Reported User</th>
            <th class="text-left">User</th>
            <th class="text-left">Date</th>
            <th class="text-left">Count</th>
            </thead>
            <tbody>

            @foreach($recruitMembers as $recruitMember)
                <tr>
                    <div class="grid grid-cols-2 grid-rows-1">
                        <div>
                            <td class="cursor-pointer" @click="show={{ $recruitMember->id }}">{{ $recruitMember->user->username }}</td>
                            <td>{{ $recruitMember->user->getCurrentRank->ranks->rank }}</td>
                            <td>{{ $recruitMember->recruitSince($recruitMember->created_at) }}</td>
                        </div>
                        <td>
                            <div class="grid grid-cols-2">
                                <div><label for="javascript">Yes</label><input type="radio" id="html" name="{{ $recruitMember->id }}" value="HTML"></div>
                                <div><label for="javascript">No</label><input type="radio" id="css" name="{{ $recruitMember->id }}" value="CSS"></div>
                            </div>

                        </td>
                    </div>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
            </div>
        </div>
    </div>
</div>
@endsection
