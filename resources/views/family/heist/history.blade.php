@extends('layouts.default')

@section('header')
    Family Heists History
@stop

@section('content')
    <div class="mx-auto w-full md:mt-2 mb-4">
        <div class="max-w-7xl mx-auto ">
            <div class="shadow-sm md:w-2/4">
                <div class="text-white">
                    <div class="grid grid-cols-5 gap-2">
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('family.heist.create') }}">New</a></div>
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('family.heists') }}">All</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1 mb-4">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Heist</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Heist</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>

                        <div class="grid grid-cols-5 md:grid-cols-6">
                            <div class="hidden md:block">#</div>
                            <div><b>Date</b></div>
                            <div><b>Location</b></div>
                            <div><b>Cash to</b></div>
                            <div><b>Starter (Players)</b></div>
                            <div><b>Total cash</b></div>
                        </div><hr>

                        @foreach($heists as $key => $heist)
                            <div x-data="{ open: false, toggle() { this.open = ! this.open } }">
                                <div @click="toggle()" class="grid grid-cols-5 md:grid-cols-6 mb-2">
                                <span class="hidden md:block">{{ $key + 1 }}</span>
                                <span>{{ $heist->created_at->format('d-m-Y h:i:s') }}</span>
                                <span>{{ $heist->heistLocation->name }}</span>

                                <span>{{ $heist->heistMoneyLocation->name }}</span>
                                <div>
                                    <a href="{{ route('player.show',$heist->player_1_name->username ) }}">{{ $heist->player_1_name->username }}</a>
                                    <div x-show="open">
                                        <a href="{{ route('player.show',$heist->player_2_name->username ) }}">{{ $heist->player_2_name->username }}</a>
                                        <a href="{{ route('player.show',$heist->player_3_name->username ) }}">{{ $heist->player_3_name->username }}</a><br>
                                        <a href="{{ route('player.show',$heist->player_4_name->username ) }}">{{ $heist->player_4_name->username }}</a>
                                        <a href="{{ route('player.show',$heist->player_5_name->username ) }}">{{ $heist->player_5_name->username }}</a>
                                    </div>
                                </div>
                                <span>{{ $heist->money }}</span>
                                </div>
                            </div>
                            <hr>
                        @endforeach

                    </div>
                    {{ $heists->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
