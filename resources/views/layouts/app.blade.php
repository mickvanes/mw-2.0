<!DOCTYPE html>

<html class="{{ Auth::user()->settings->theme }}" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', 'Zaoway - Free Click-based Mafiagame')</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/all.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>

    <body class="font-sans antialiased">
        <div class="min-h-screen bg-lightSecondary dark:bg-darkSecondary">
            <!-- Page Heading -->
            <header class="bg-lightDefault text-white dark:bg-darkDefault dark:text-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    <div class="grid grid-cols-5">
                        <div><a href="{{route('dashboard')}}">{{ Auth::user()->name }}</a></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div class="col-span-2"><a href="{{route('bank')}}"><div>Cash: &euro;{{ number_format(Auth::user()->cash->cash, 0, ',', '.') }}</div></a></div>
                        <div class="col-span-2"><a href="{{route('bank')}}"><div>Bank: &euro;{{ number_format(Auth::user()->cash->bank, 0, ',', '.') }}</div></a></div>
                        <div>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <a :href="route('logout')"
                                    onclick="event.preventDefault();
                                    this.closest('form').submit();">
                                    <i class="fas fa-sign-out-alt float-right cursor-pointer text-white"></i>
                                </a>
                            </form>
                        </div>


                    </div>
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
    </body>
</html>

