@extends('layouts.default')

@section('header')
    Topic
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="mx-auto w-full mb-4">
                                    <div class="md:max-w-7xl mx-auto ">
                                        <div class="bg-lightDefault dark:bg-darkDefault shadow-sm border border-[#243c5a]">
                                            <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                                                <strong class="font-bold">System</strong>
                                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(session()->has('error'))
                                <div class="mx-auto w-full">
                                    <div class="md:max-w-7xl mx-auto mb-4">
                                        <div class="bg-lightDefault dark:bg-darkDefault shadow-sm border border-[#243c5a]">
                                            <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                                                <strong class="font-bold">System</strong>
                                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="grid grid-cols-1">
                            <div class="w-full">
                                <div class="">{{ $forumTopic->name }} - {{ $forumTopic->created_at }} | Latest Reaction: {{ $forumTopic->updated_at }}</div>
                                <div class="flex">
                                    <div class="w-2/5 md:w-2/12">
                                        <a href="{{ route('player.show', $forumTopic->user->username) }}">
                                            <div class="w-full py-4">
                                                <img class="mx-auto mb-2" src="{{ $forumTopic->user->avatar }}">
                                                <div class="text-center">{{ $forumTopic->user->username }}</div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="w-3/5 md:w-10/12 py-4 h-50 break-words flex flex-col">
                                        <div class="ml-4">{!! (new \App\Helpers\Helper)->makeUBB($forumTopic->content)  !!}</div>
                                        <div id="message_footer" class="ml-4 mb-2 mt-auto">Created on {{ $forumTopic->created_at }} @if( $forumTopic->content_updated != NULL ) Edited: {{ $forumTopic->content_updated }} @endif</div>
                                    </div>
                                    <div class="mb-4 text-right float-end">
                                        @if($forumTopic->user->id != \Illuminate\Support\Facades\Auth::id())<a href="{{ route('reportItem', ["forumTopic",$forumTopic->id]) }}" )><img src="https://www.mafiaway.nl/images/icons/error.png"></a>@endif
                                    </div>
                                </div>
                            </div>

                            <div>
                                @foreach($comments as $comment)
                                    <div class="flex mt-4">
                                        <div class="w-3/5 md:w-2/12">
                                            <a href="{{ route('player.show', $comment->user->username) }}">
                                                <div class="w-full py-4">
                                                    <div class="p-4"><img class="mx-auto mb-2 block" src="{{ $comment->user->avatar }}" style="max-width: 100px; max-height: 100px"></div>
                                                    <div class="text-center break-words">{{ $comment->user->username }}</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="w-3/5 md:w-10/12 pt-4 h-50 bg-gray-600 text-white rounded-xl flex flex-col">
                                            <div id="message_{{ $comment->id }}" class="ml-4">{!! (new \App\Helpers\Helper)->makeUBB($comment->content)  !!}</div>
                                            <div id="message_footer" class="ml-4 mb-2 mt-auto">Created on {{ $comment->created_at }} @if($comment->updated_at != $comment->created_at) Edited: {{ $comment->updated_at }} @endif</div>
                                        </div>
                                        <div class="mb-4 text-right float-end">
                                            <a id="comment_{{ $comment->id }}" ><img src="{{ asset('images/forum/comment.png')  }}"></a>
                                            <a href="{{ route('reportItem', ["forumComment",$comment->id]) }}">
                                                <img src="{{ asset('images/forum/error.png')  }}">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if($mute)
                                <span class="font-bold mb-4 text-center" style="color: #970000;">You are muted because "{{ $mute->reason }}", you have a {{ $mute->days }} days mute, it ends on {{ $mute->endDate }} </span>
                            @else
                                <form action="" id="form" method="post">
                                    <!-- CROSS Site Request Forgery Protection -->
                                    @csrf
                                    <div class="flex mt-4">
                                        <div class="w-3/5 md:w-2/12">
                                            <div class="w-full py-4">
                                                <div class="p-4"><img class="mx-auto mb-2 block" src="{{ \Illuminate\Support\Facades\Auth::user()->avatar }}" style="max-width: 100px; max-height: 100px"></div>

                                                <div class="text-center break-words">{{ \Illuminate\Support\Facades\Auth::user()->username }}</div>
                                            </div>
                                        </div>
                                        <div class="w-full py-4 bg-gray-800 text-white rounded-xl">
                                            <textarea class="bg-gray-800 mx-auto border-0 ml-4 mr-4 w-11/12 min-h-full " name="message" id="message" cols="70" rows="10" ></textarea>
                                        </div>
                                    </div>
                                    <div class="mb-4 text-right">
                                        <input type="hidden" id="comment" name="comment" value="">
                                        <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
                                    </div>
                                </form>
                            @endif
                            <div class="w-full">
                                {{ $comments->links() }}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
