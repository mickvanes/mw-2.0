<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessTransaction extends Model
{
    use HasFactory;

    protected $table = 'business_transactions';

    protected $fillable = [
        'businessID',
        'owner',
        'transactionType',
        'cash',
    ];


}
