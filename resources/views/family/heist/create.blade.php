@extends('layouts.default')

@section('header')
    Family Heists
@stop

@section('content')
    <div class="mx-auto w-full md:mt-2 mb-4">
        <div class="max-w-7xl mx-auto ">
            <div class="shadow-sm md:w-2/4">
                <div class="text-white">
                    <div class="grid grid-cols-5 gap-2">
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('message.create') }}">All family heists</a></div>
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('message.create') }}">History</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Heist</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Heist</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>

                       <form action="{{ route('family.heist.store') }}" method="post"> {{--  Change the action attribute to the route that will be used to create a new heist--}}
                            @csrf
                            <div class="grid grid-cols-2 md:w-2/4 text-black">
                                <label for="location" class="text-white">Location</label>
                                <div>
                                    <select name="location">
                                        @foreach($locations as $location)
                                            <option value="{{ $location->id }}">{{ $location->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="moneyLocation" class="text-white">Money Location</label>
                                <div><select id="moneyLocation" name="moneyLocation">
                                        @foreach($moneyLocations as $moneyLocation)
                                            <option value="{{ $moneyLocation->id }}">{{ $moneyLocation->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="role" class="text-white">Role</label>
                                <div>

                                    <select id="role" name="role">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div><button type="submit" class="bg-lightDefault dark:bg-darkDefault text-white dark:text-white p-2">Create</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
