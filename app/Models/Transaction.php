<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'user_transactions';

    protected $fillable = [
        'fromUserID',
        'toUserID',
        'nettoCash',
        'brutoCash',
        'message'
    ];

    public function receiver()
    {
        return $this->belongsTo(User::class, 'toUserID');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'fromUserID');
    }
}
