<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BackfireKill extends Model
{
    use HasFactory;

    protected $table = 'backfire_kills';

    protected $fillable = [
        'uniqID',
        'userID',
        'playerID',
        'bullets',
        'userRank',
        'playerRank',
        'userPercent',
        'playerPercent',
        'playerCash',
        'playerBank',
        'active',
        'success',
        'weapon',
        'userDef',
        'playerDef',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function uniqid()
    {
        return $this->hasOne(Kill::class, 'uniqID');
    }

    public function player()
    {
        return $this->belongsTo(User::class, 'playerID');
    }
}
