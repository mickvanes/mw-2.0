<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyUserRang extends Model
{
    use HasFactory;

    protected $table = 'family_user_rang';

    protected $fillable = [
        'familyID',
        'userID',
        'rangID',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function familyDetails()
    {
        return $this->belongsTo(Family::class, 'familyID');
    }

//    public function familyMembers()
//    {
//        return $this->hasMany(User::class, 'id');
//    }

    public function rang()
    {
        return $this->hasOne(FamilyRang::class, 'id','rangID');
    }
}
