<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rank extends Model
{
    use HasFactory;

    protected $table = 'user_rank_logs';

    protected $fillable = [
        'userID',
        'rankID',
        'adv',
        'active'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function ranks(){
        return $this->belongsTo(Ranks::class, 'rankID');
    }

}
