<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    use HasFactory;

    protected $table = 'hospital';

    protected $fillable = [
        'ownerID',
        'businessID',
        'price',
        'bloodRemaining',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'ownerID');
    }

    public function business()
    {
        return $this->belongsTo(Business::class, 'businessID');
    }

}
