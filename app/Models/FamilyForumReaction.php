<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyForumReaction extends Model
{
    use HasFactory;

    protected $table = 'familyforum_reactions';

    protected $fillable = [
        'userID',
        'topicID',
        'categoryID',
        'content',
        'level',
        'reactionID'
    ];

    public function topic()
    {
        return $this->belongsTo(FamilyForumTopic::class, 'topicID');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function category()
    {
        return $this->belongsTo(FamilyForumCategory::class, 'categoryID');
    }

    public function reaction()
    {
        return $this->belongsTo(FamilyForumReaction::class, 'reactionID');
    }
}
