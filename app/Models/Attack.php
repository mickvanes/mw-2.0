<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attack extends Model
{
    use HasFactory;

    protected $table = 'attack_logs';

    protected $fillable = [
        'player1',
        'player2',
        'cash',
        'result'
    ];

    public function attacker()
    {
        return $this->belongsTo(User::class, 'player1');
    }

    public function defender()
    {
        return $this->belongsTo(User::class, 'player2');
    }


}
