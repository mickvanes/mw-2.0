@if(session()->has('flash'))
    <div class="alert alert-success">
        <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
             role="alert">
            <strong class="font-bold">System</strong>
            <span class="block sm:inline">{{ session()->pull('flash') }}</span>
        </div>
    </div><br>
@endif
