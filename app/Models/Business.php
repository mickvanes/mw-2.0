<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    use HasFactory;

    protected $table = 'businesses';

    protected $fillable = [
        'typeID',
        'countryID',
        'ownerID',
        'cash'
    ];

    public function type()
    {
        return $this->belongsTo(BusinessType::class,'typeID');
    }

    public function country()
    {
        return $this->belongsTo(Countries::class,'countryID');
    }

    public function owner()
    {
        return $this->belongsTo(User::class,'ownerID');
    }

    public function revenue()
    {
        return $this->hasMany(BusinessTransaction::class,'businessID','id')->where('transactionType', '=', 2);
    }

    public function jailCount($businessID)
    {
        return Jail::where([
            ['businessID', '=' , $businessID],
            ['endTime','>=', Carbon::now('Europe/Amsterdam')]
        ])->count();
    }
}
