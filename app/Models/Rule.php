<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    use HasFactory;

    protected $table = 'rules';

    protected $fillable = [
        'transMax',
        'minCrimeCash',
        'maxCrimeCash',
        'maxFamMembers',
        'minRankCreateFam',
        'maxFamLeaders',
        'maxFamGenerals',
        'maxFamManagers',
        'maxFamRecruiters',
        'maxExitCost',
        'maxSafeCash',
        'marriageCost',
        'transCostsFamLess',
        'transCostsFamGreater',
        'transCostsPlayerLess',
        'transCostsPlayerGreater',
        'heistMoneyMin',
        'heistMoneyMax',
    ];
}
