<?php

namespace App\Helpers;

class Helper
{
    public function makeUBB($message)
    {
        // HTML verbieden
                $message = htmlspecialchars($message);
        // Enters maken
                $message = nl2br($message);

                $message = preg_replace("_\[br\]_", '</br>', $message);
        // List maken
                $message = preg_replace("_\[pointList](.*)\[/pointList\]_si", '<ul>$1</ul>', $message);
                $message = preg_replace("_\[numberList](.*)\[/numberList\]_si", '<ol>$1</ol>', $message);
                $message = preg_replace("_\[list](.*)\[/list\]_si", '<li>$1</li>', $message);
        // Links maken
                $message = preg_replace("_\[link]http://(.*)\[/url\]_si", '<a href="$1" target="_new">$1</a>', $message);
                $message = preg_replace("_\[link](.*)\[/link\]_si", '<a href="http://$1" target="_new">$1</a>', $message);
                $message = preg_replace("_\[url=(.*?)\](.*?)\[/url\]_si", '<a href="$1" target="_new">$2</a>', $message);
        // Afbeeldingen
                $message = preg_replace('_\[img](https://i.postimg.cc/.*?)\[/img\]_si', '<img src="$1">', $message);
                $message = preg_replace("_\[img](https://i.imgur.com/.*?)\[/img\]_si", '<img src="$1">', $message);
        // Youtube video
                $message = preg_replace('_\[youtube\].*?(v=|v/)(.+?)(&.*?|/.*?)?\[/youtube\]_is', '[youtube]$2[/youtube]', $message);
                $message = preg_replace('_\[youtube\]([a-z0-9-]+?)\[/youtube\]_is', '<iframe class="youtube-player" type="text/html" src="https://www.youtube.com/embed/$1" allowfullscreen="" width="318" height="196" frameborder="0"></iframe>', $message);
        // Uitlijnen
                $message = preg_replace("_\[left](.*)\[/left\]_si", '<div class="text-left">$1</div>', $message);
                $message = preg_replace("_\[right](.*)\[/right\]_si", '<div class="text-right mr-5">$1</div>', $message);
                $message = preg_replace("_\[center](.*)\[/center\]_si", '<div class="text-center mx-auto">$1</div>', $message);
        // Kleuren
                $message = preg_replace("_\[color=(.*)\](.*?)\[/color\]_si", '<span style="color: $1">$2</span>', $message);
        // Tekstgrootte
                $message = preg_replace("_\[size=(.*)\](.*?)\[/size\]_si", '<span style="font-size: $1">$2</span>', $message);
        // Vetgedrukt
                $message = preg_replace("_\[b\](.*?)\[/b\]_si", '<b>$1</b>', $message);
        // Cursief
                $message = preg_replace("_\[i\](.*?)\[/i\]_si", '<i>$1</i>', $message);
        // Onderstrepen
                $message = preg_replace("_\[u\](.*?)\[/u\]_si", '<u>$1</u>', $message);
        // Doorstrepen
                $message = preg_replace("_\[s\](.*?)\[/s\]_si", '<s>$1</s>', $message);
        // Knipperen
                $message = preg_replace("_\[blink\](.*?)\[/blink\]_si", '<blink>$1</blink>', $message);
        // Superscript
                $message = preg_replace("_\[sup\](.*?)\[/sup\]_si", '<sup>$1</sup>', $message);
        // Subscript
                $message = preg_replace("_\[sub\](.*?)\[/sub\]_si", '<sub>$1</sub>', $message);
        // All caps
                $message = preg_replace("_\[ac\](.*?)\[/ac\]_si", '<span class="uppercase">$1</span>', $message);
        // Small caps
                $message = preg_replace("_\[sc\](.*?)\[/sc\]_si", '<span class="lowercase">$1</span>', $message);
        // Marquee, standaard "scroll"
                $message = preg_replace("_\[slide\](.*?)\[/slide\]_si", '<marquee>$1</marquee>', $message);
        // Smilies
                $message = str_replace(":="," <img src=\"\images/smileys/applaus.gif\" />", $message);
                $message = str_replace(":O"," <img src=\"\images/smileys/blush.gif\" />", $message);
                $message = str_replace(":o"," <img src=\"\images/smileys/blush.gif\" />", $message);
                $message = str_replace(":$"," <img src=\"\images/smileys/smiley_schamen.gif\" />", $message);
                $message = str_replace(":w"," <img src=\"\images/smileys/bye.gif\" />", $message);
                $message = str_replace(":W"," <img src=\"\images/smileys/bye.gif\" />", $message);
                $message = str_replace(":7"," <img src=\"\images/smileys/cigar.gif\" />", $message);
                $message = str_replace(":+"," <img src=\"\images/smileys/clown.gif\" />", $message);
                $message = str_replace(":Z"," <img src=\"\images/smileys/deepsleep.gif\" />", $message);
                $message = str_replace("8)7"," <img src=\"\images/smileys/hammer2.gif\" />", $message);
                $message = str_replace(":')"," <img src=\"\images/smileys/happytears.gif\" />", $message);
                $message = str_replace(":^"," <img src=\"\images/smileys/idea.gif\" />", $message);
                $message = str_replace("|)"," <img src=\"\images/smileys/indifferent.gif\" />", $message);
                $message = str_replace("+)"," <img src=\"\images/smileys/koekwaus.gif\" />", $message);
                $message = str_replace(":D"," <img src=\"\images/smileys/lol.gif\" />", $message);
                $message = str_replace(":d"," <img src=\"\images/smileys/lol.gif\" />", $message);
                $message = str_replace(":P"," <img src=\"\images/smileys/puh.gif\" />", $message);
                $message = str_replace(":p"," <img src=\"\images/smileys/puh.gif\" />", $message);
                $message = str_replace(":r"," <img src=\"\images/smileys/puke.gif\" />", $message);
                $message = str_replace(":x"," <img src=\"\images/smileys/silence.gif\" />", $message);
                $message = str_replace(":X"," <img src=\"\images/smileys/silence.gif\" />", $message);
                $message = str_replace("o-)"," <img src=\"\images/smileys/smilebounce.gif\" />", $message);
                $message = str_replace(":?"," <img src=\"\images/smileys/smileconfused.gif\" />", $message);
                $message = str_replace("(h)"," <img src=\"\images/smileys/smilecool.gif\" />", $message);
                $message = str_replace("(H)"," <img src=\"\images/smileys/smilecool.gif\" />", $message);
                $message = str_replace(":)"," <img src=\"\images/smileys/smilehmmm.gif\" />", $message);
                $message = str_replace("*)"," <img src=\"\images/smileys/smilemean.gif\" />", $message);
                $message = str_replace(":("," <img src=\"\images/smileys/smileredface.gif\" />", $message);
                $message = str_replace(":)"," <img src=\"\images/smileys/smilesmile.gif\" />", $message);
                $message = str_replace("*D"," <img src=\"\images/smileys/smileteeth.gif\" />", $message);
                $message = str_replace(";)"," <img src=\"\images/smileys/smilewink.gif\" />", $message);
                $message = str_replace(")~"," <img src=\"\images/smileys/smileyummie.gif\" />", $message);
                $message = str_replace(":'("," <img src=\"\images/smileys/tears.gif\" />", $message);
                $message = str_replace("_O_"," <img src=\"\images/smileys/worship.gif\" />", $message);
                $message = str_replace("_o_"," <img src=\"\images/smileys/worship.gif\" />", $message);
                $message = str_replace("_o-"," <img src=\"\images/smileys/schater.gif\" />", $message);
                $message = str_replace("_O-"," <img src=\"\images/smileys/schater.gif\" />", $message);
                $message = str_replace("(K)"," <img src=\"\images/smileys/smiley_love_kissing.gif\" />", $message);
                $message = str_replace("(L)"," <img src=\"\images/smileys/smiley_love_heart.gif\" />", $message);

        // Quotes
                while(preg_match("((\[quote=(.+?)\](.+?)\[\/quote\])|(\[quote\](.+?)\[\/quote]))is", $message)) {
                    $message = preg_replace("(\[quote=(.+?)\](.+?)\[\/quote\])is",'<fieldset><legend> <b>$1 wrote:</b> </legend><br />$2<br /></fieldset><br />' ,$message);
                    $message = preg_replace("(\[quote\](.+?)\[\/quote])is",'<fieldset><legend> <b>Quote</b> </legend><br />$1<br /></fieldset><br />' ,$message);
                }

        return $message;
    }
//
//    public function decryptUBB($message){
//        // List maken /<h1>(.*?)<\/h1>/s
//        $message = preg_replace( "/<ul>(.*?)<\/ul>/s",'[pointList]$1[/pointList]', $message);
//        $message = preg_replace( "/<ol>(.*?)<\/ol>/s",'[numberList]$1[/numberList]', $message);
//        $message = preg_replace( "/<li>(.*?)<\/li>/s","[list]$1[/list]", $message);
//        // Links maken
//        $message = preg_replace( '/<a href="(.*?)" target="_blank">(.*?)<\/a>/s','[link]http://$1[/link]', $message);
//        #$message = preg_replace( '/<a href="http://(.*?)" target="_blank">(.*?)<\/a>/s','[link]$1[/link]', $message);
//        $message = preg_replace( '/<a href="(.*?)" target="_blank">(.*?)<\/a>/s',"[url=http://$1]$2[/url]", $message);
//        #$message = preg_replace( '/<a href="http://(.*?)" target="_blank">(.*?)<\/a>/s',"[url=$1]$2[/url]", $message);
//        // Afbeeldingen
//        $message = preg_replace( '/<img src="(\/\/www\.i.postimg.cc\/.*)" \/>/s',"[img]https://i.postimg.cc/$1[/img]", $message);
//        $message = preg_replace( "/<img src='(\/\/www\.i.imgur.com\/.*)' \/>/s",'[img]https://i.imgur.com/$1[/img]', $message);
//        $message = preg_replace( "/<img src='(.*?)' \/>/s","[p] Upload on Imgur or Postimages please[/p]", $message);
//        // Youtube video
//        $message = preg_replace( '/<iframe class="youtube-player" src="(\/\/www\.youtube.com\/embed\/.*)" allowfullscreen="" width="318" height="196" frameborder="0"><\/iframe>/s','[youtube]$1[/youtube]', $message);
//        // Uitlijnen
//        $message = preg_replace( '/<div class="text-left">(.*?)<\/div>/s',"[left]$2[/left]", $message);
//        $message = preg_replace( '/<div class="text-right mr-5">(.*?)<\/div>/s',"[right]$1[/right]", $message);
//        $message = preg_replace( '/<div class="text-center">(.*?)<\/div>/s',"[center]$1[/center]", $message);
//        // Kleuren
//        $message = preg_replace( '/<span style="color: (.*?)">(.*?)<\/span>/s',"[color=$1]$2[/color]", $message);
//        // Tekstgrootte
//        $message = preg_replace( '/<span style="font-size: (.*?)">(.*?)<\/span>/s',"[size=$1]$2[/size]", $message);
//        // Vetgedrukt
//        $message = preg_replace( '/<b>(.*?)<\/b>/s',"[b]$1[/b]", $message);
//        // Cursief
//        $message = preg_replace( '/<i>(.*?)<\/i>/s',"[i]$1[/i]", $message);
//        // Onderstrepen
//        $message = preg_replace( '/<u>(.*?)<\/u>/s',"[u]$1[/u]", $message);
//        // Doorstrepen
//        $message = preg_replace( '/<s>(.*?)<\/s>/s',"[s]$1[/s]", $message);
//        // Knipperen
//        $message = preg_replace( '/<blink>(.*?)<\/blink>/s',"[blink]$1[/blink]", $message);
//        // Superscript
//        $message = preg_replace( '/<sup>(.*?)<\/sup>/s',"[sup]$1[/sup]", $message);
//        // Subscript
//        $message = preg_replace( '/<sub>(.*?)<\/sub>/s',"[sub]$1[/sub]", $message);
//        // All caps
//        $message = preg_replace( '/<span class="uppercase">(.*?)<\/span>/s',"[ac]$1[/ac]", $message);
//        // Small caps
//        $message = preg_replace( '/<span class="lowercase">(.*?)<\/span>/s',"[sc]$1\[/sc]", $message);
//        // Marquee, standaard "scroll"
//        $message = preg_replace( '/<marquee>(.*?)<\/marquee>/s',"[slide]$1[/slide]", $message);
//        // Smilies
//        $message = str_replace(" <img src=\"\images/smileys/applaus.gif\" />",":=",$message);
//        $message = str_replace(" <img src=\"\images/smileys/blush.gif\" />",":O", $message);
//        $message = str_replace(" <img src=\"\images/smileys/blush.gif\" />",":o", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smiley_schamen.gif\" />",":$", $message);
//        $message = str_replace(" <img src=\"\images/smileys/bye.gif\" />",":w", $message);
//        $message = str_replace(" <img src=\"\images/smileys/bye.gif\" />",":W", $message);
//        $message = str_replace(" <img src=\"\images/smileys/cigar.gif\" />",":7", $message);
//        $message = str_replace(" <img src=\"\images/smileys/clown.gif\" />",":+", $message);
//        $message = str_replace(" <img src=\"\images/smileys/deepsleep.gif\" />",":Z", $message);
//        $message = str_replace(" <img src=\"\images/smileys/hammer2.gif\" />","8)7", $message);
//        $message = str_replace(" <img src=\"\images/smileys/happytears.gif\" />",":')", $message);
//        $message = str_replace(" <img src=\"\images/smileys/idea.gif\" />",":^", $message);
//        $message = str_replace(" <img src=\"\images/smileys/indifferent.gif\" />","|)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/koekwaus.gif\" />","+)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/lol.gif\" />",":D", $message);
//        $message = str_replace(" <img src=\"\images/smileys/lol.gif\" />",":d", $message);
//        $message = str_replace(" <img src=\"\images/smileys/puh.gif\" />",":P", $message);
//        $message = str_replace(" <img src=\"\images/smileys/puh.gif\" />",":p", $message);
//        $message = str_replace(" <img src=\"\images/smileys/puke.gif\" />",":r", $message);
//        $message = str_replace(" <img src=\"\images/smileys/silence.gif\" />",":x", $message);
//        $message = str_replace(" <img src=\"\images/smileys/silence.gif\" />",":X", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smilebounce.gif\" />","o-)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smileconfused.gif\" />",":?", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smilecool.gif\" />","(h)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smilecool.gif\" />","(H)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smilehmmm.gif\" />",":)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smilemean.gif\" />","*)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smileredface.gif\" />",":(", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smilesmile.gif\" />",":)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smileteeth.gif\" />","*D", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smilewink.gif\" />",";)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smileyummie.gif\" />",")~", $message);
//        $message = str_replace(" <img src=\"\images/smileys/tears.gif\" />",":'(", $message);
//        $message = str_replace(" <img src=\"\images/smileys/worship.gif\" />","_O_", $message);
//        $message = str_replace(" <img src=\"\images/smileys/worship.gif\" />","_o_", $message);
//        $message = str_replace(" <img src=\"\images/smileys/schater.gif\" />","_o-", $message);
//        $message = str_replace(" <img src=\"\images/smileys/schater.gif\" />","_O-", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smiley_love_kissing.gif\" />","(K)", $message);
//        $message = str_replace(" <img src=\"\images/smileys/smiley_love_heart.gif\" />","(L)", $message);
//
//        // Quotes
//        while(preg_match("((\[quote=(.+?)\](.+?)\[\/quote\])|(\[quote\](.+?)\[\/quote]))is", $message)) {
//            $message = preg_replace('<fieldset><legend> <b>(.+?) wrote:</b> </legend><br />(.+?)<br /></fieldset><br />' ,"[quote=$1]$2[/quote]",$message);
//            $message = preg_replace('<fieldset><legend> <b>Quote</b> </legend><br />(.+?)<br /></fieldset><br />',"[quote]$1[/quote]" ,$message);
//        }
//
//        return $message;
//
//    }

}
