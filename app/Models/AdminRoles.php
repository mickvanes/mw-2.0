<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminRoles extends Model
{
    use HasFactory;

    protected $table = 'adminrangs';

    public function admin()
    {
        return $this->hasMany(Admin::class, 'id');
    }
}
