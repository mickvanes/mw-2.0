<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyHeistMessages extends Model
{
    use HasFactory;

    protected $table = 'family_heist_messages';

    protected $fillable = [
        'id',
        'body',
        'location',
    ];
}
