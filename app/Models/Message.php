<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $table = 'user_messages';

    protected $fillable = [
        'id',
        'senderID',
        'systemID',
        'recipientID',
        'systemMessage',
        'deletedSender',
        'deletedRecipient',
        'subject',
        'body',
        'seen'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'senderID');
    }

    public function systemSender()
    {
        return $this->belongsTo(SystemUser::class, 'systemID');
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipientID');
    }

}
