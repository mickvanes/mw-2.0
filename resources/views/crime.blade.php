@extends('layouts.default')

@section('header')
    CRIME
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Crime</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if($dateNew == null || $diff)
                        <form method="post" action="{{ route('crime.store') }}">
                            @csrf

                            <div class="form-group">
                                <div class="grid grid-cols-2 gap-4">
                                    @foreach($crimes as $key => $crime)
                                        <div>{{$crime->description}}</div>
                                        <div><label for="{{$crime->name}}" class="float-right">
                                                <input type="radio"
                                                       id="{{$crime->name}}"
                                                       name="crimeID"
                                                       value="{{ $crime->id }} ">
                                                <?php
                                                $chance = rand($crimeChance[$key]->minChance, $crimeChance[$key]->maxChance);
                                                session([$crime->id => $chance]); print(" " . $chance . "%" . " chance")
                                                ?>
                                            </label><input type="hidden" id="chance" name="chance"
                                                           value="{!! $chance !!}"></div>
                                    @endforeach
                                </div>
                            </div>
                            <br>
                            <div class="grid grid-cols-1 md:grid-cols-2">
                                <div id="{{ $hcaptcha }}"></div>
                                <div>
                                    <input type="submit" name="send" value="Submit"
                                           class="cursor-pointer bg-darkButton hover:bg-darkHover dark:bg-buttonBlue font-bold mt-4 py-2 px-4">
                                </div>

                            </div>
                        </form>
                            <script>
                                function onloadCallback() {
                                    turnstile.render('<?= $hcaptcha ?>', {
                                        sitekey: "0x4AAAAAAAW-CfwbK0phKTM6"
                                    })
                                }
                            </script>
                            <script src="https://challenges.cloudflare.com/turnstile/v0/api.js" async defer></script>
                    @else
                        <span id="default"></span>
                        <script src="{{ URL::asset('js/timer.js') }}"></script>
                        <script type="text/javascript">countdown('<?php echo $dateNew ?>','crime','default');</script>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop


