<?php

namespace App\Jobs;

use App\Models\Detective;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeactivateDets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $detective;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(Detective $detective)
    {
        $this->detective = $detective;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle()
    {
        $detective = Detective::findOrFail($this->detective->id);
        if($detective){
            $detective->active = 0;
            $detective->save();
        }
    }
}
