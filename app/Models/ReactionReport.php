<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReactionReport extends Model
{
    use HasFactory;

    protected $table = 'reaction_report';

    protected $fillable = [
        'type',
        'itemID',
        'content',
        'user',
        'repUser',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id','user');
    }

    public function repUser()
    {
        return $this->belongsTo(User::class, 'id','repUser');
    }
}
