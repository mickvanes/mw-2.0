<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    use HasFactory;

    protected $table = 'user_clicks';

    protected $fillable = [
        'userID',
        'playerID',
        'timestamp'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'userID');
    }

    public function player()
    {
        return $this->belongsTo(User::class, 'id', 'playerID');
    }
}
