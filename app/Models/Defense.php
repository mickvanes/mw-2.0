<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Defense extends Model
{
    use HasFactory;

    protected $table = 'user_defense';

    protected $fillable = [
        'userID',
        'weapon',
        'house',
        'airplane'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userID');
    }

    public function gun()
    {
        return $this->belongsTo(Weapon::class, 'weapon');
    }

    public function house()
    {
        return $this->belongsTo(UserHouse::class, 'house');
    }

    public function plane()
    {
        return $this->belongsTo(Airplane::class, 'airplane');
    }
}
