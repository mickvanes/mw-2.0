@extends('layouts.default')

@section('header')
    Settings
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto ">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                                @if(session()->has('flash'))
                                    <div class="alert alert-success">
                                        <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 relative"
                                             role="alert">
                                            <strong class="font-bold">System</strong>
                                            <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                                        </div>
                                    </div><br>
                                @endif

                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                             role="alert">
                                            <strong class="font-bold">Settings</strong>
                                            <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                        </div>
                                    </div><br>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-danger">
                                        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                             role="alert">
                                            <strong class="font-bold">Settings</strong>
                                            <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                        </div>
                                    </div><br>
                                @endif

                                DOEI

                            </div>
                        </div>
                    </div>
                </div>
@endsection

