<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserHouse extends Model
{
    use HasFactory;

    protected $table = 'user_country_house';

    protected $fillable = [
        'houseID',
        'countryID',
        'userID',
    ];

    public function house()
    {
        return $this->belongsTo(House::class, 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function country()
    {
        return $this->belongsTo(Countries::class, 'id');
    }
}
