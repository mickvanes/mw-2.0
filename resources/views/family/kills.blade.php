@extends('layouts.default')

@section('header')
    Family Kills
@stop

@section('content')
    <div class="mx-auto w-full md:mt-2 mb-4">
        <div class="max-w-7xl mx-auto ">
            <div class="shadow-sm md:w-2/4">
                <div class="text-white">
                    <div class="grid grid-cols-5 gap-2">
                        <div class="text-center p-2 bg-lightDefault dark:bg-darkDefault "><a href="{{ route('family.killsToday') }}">Today's Kills</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-3 md:grid-cols-6">
                        @foreach($kills as $kill)
                            <div> +{{ $kill->playerRankName->rep }} family rep</div>
                                <div class="col-span-2 md:col-span-5 mb-1">
                                    @if($kill->backfire)
                                        <a href="{{ route('player.show', $kill->user->username) }} "><b>{{ $kill->user->username }}</b></a> (BF) killed
                                        <a href="{{ route('player.show', $kill->player->username) }}"><b>{{ $kill->player->username }}</b></a> <i style="font-size: small">{{ $kill->playerFamily->name }}</i> ({{ $kill->playerPercent }}%
                                        {{ $kill->playerRankName->rank }}), <b><a href="{{ route('player.show', $kill->player->username ) }}">{{ $kill->player->username }}</a></b> @if($kill->success) <span style="color: green;"><b>died</b></span> from the shots @else <span style="color: red;"><b>survived</b></span> the shots @endif
                                    @else
                                        @if(!$kill->hiddenFam)
                                            <a href="{{ route('player.show', $kill->user->username) }}"><b>{{ $kill->user->username }}</b></a> killed
                                            <a href="{{ route('player.show', $kill->player->username) }}"><b>{{ $kill->player->username }}</b></a> <i style="font-size: small">{{ $kill->playerFamily->name }}</i> ({{ $kill->playerPercent }}%
                                            {{ $kill->playerRankName->rank }}), <b><a href="{{ route('player.show', $kill->player->username ) }}">{{ $kill->player->username }}</a></b> @if($kill->success) <span style="color: green;"><b>died</b></span> from the shots @else <span style="color: red;"><b>survived</b></span> the shots @endif
                                        @else
                                            A family member killed
                                            <a href="{{ route('player.show', $kill->player->username) }}"><b>{{ $kill->player->username }}</b></a> <i style="font-size: small">{{ $kill->playerFamily->name }}</i> ({{ $kill->playerPercent }}%
                                            {{ $kill->playerRankName->rank }}), <b>{{ $kill->player->username }}</b> @if($kill->success) <span style="color: green;"><b>died</b></span> from the shots @else <span style="color: red;"><b>survived</b></span> the shots @endif

                                        @endif
                                    @endif
                                </div>
                        @endforeach


                        </div>
                    <div class="w-full text-center mt-4">{{ $killCountDay }} @if($killCountDay >= 2)kills @else kill @endif  today ({{ \Carbon\Carbon::today()->format('d-m-Y') }})</div>
                    <div class="mt-4">{{ $kills->links()}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

