<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.3/r-2.2.9/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/r-2.2.9/datatables.min.js"></script>
<script src="{{ URL::asset('js/timer.js') }}"></script>

@extends('layouts.default')

@section('header')
    Detectives
@stop

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-lightDefault dark:bg-darkDefault text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Detectives</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Detectives</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>
                        <div class="grid grid-cols-1">
                            <div>
                                <form method="post" action="{{ route('detectives.store') }}">
                                    @csrf
                                    <div class="grid grid-rows-1">
                                        <div class="grid grid-cols-1 md:grid-cols-6">
                                            <div class="grid grid-rows-1 md:grid-rows-2">
                                                <label for="player">Player: </label>
                                                <input class="w-11/12 text-black" type="text" id="player" name="player">
                                            </div>
                                            <div class="grid grid-rows-1 md:grid-rows-2">
                                                <label for="dets">Dets: </label>
                                                <input class="w-11/12 text-black" type="number" min="1" max="500" id="dets" name="dets" value="{{ $settings->defaultDets ?? 1 }}" onchange="costsDets(this.value,document.getElementById('detsCountry').value,document.getElementById('maxTime').value)">
                                            </div>
                                            <div class="grid grid-rows-1 md:grid-rows-2">
                                                <label for="detsCountry">Country: </label>
                                                <select class="w-11/12 text-black" id="detsCountry" name="detsCountry" onchange="costsDets(document.getElementById('dets').value,this.value,document.getElementById('maxTime').value)">
                                                    <option value="{{ $allCountries }}">All Countries</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->id}}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="grid grid-rows-1 md:grid-rows-2">
                                                <label for="maxTime">Time: </label>
                                                <select class="w-11/12 text-black" id="maxTime" name="maxTime" onchange="costsDets(document.getElementById('dets').value,document.getElementById('detsCountry').value,this.value)">
                                                    <option value="1">1 Hour</option>
                                                    <option value="2">2 Hours</option>
                                                    <option value="3">3 Hours</option>
                                                    <option value="4">4 Hours</option>
                                                </select>
                                            </div>
                                            <div class="grid grid-rows-1 md:grid-rows-2">
                                                <label for="message">Message: </label>
                                                <select class="w-11/12 text-black" id="message" name="message">
                                                    <option value="0">Off</option>
                                                    <option value="1" selected>On</option>
                                                </select>
                                            </div>
                                            <div class="grid grid-rows-1 md:grid-rows-2">
                                                <label for="submit">Submit: </label>
                                                <input type="submit" name="send" value="Submit"
                                                       class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 w-11/12">
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <label for="Costs">Costs: </label>
                                            <span id="costs"></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            <div class="mt-4 p-4 w-full text-white">
                                <table id="detectives" class="mx-auto">
                                    <thead>
                                    <tr>
                                        <th class="text-left" data-priority="1"><b>Player</b></th>
                                        <th class="text-left" data-priority="2"><b>Rank</b></th>
                                        <th class="text-left"><b>Dets</b></th>
                                        <th class="text-left"><b>Searching in</b></th>
                                        <th class="text-left"><b>Found In</b></th>
                                        <th class="text-left" data-priority="3"><b>Status</b></th>
                                        <th class="text-left" data-priority="4"><b>Time Remaining</b></th>
                                        <th class="text-left"><b>Restart Dets</b></th>
                                        <th class="text-left"><b>Delete Dets</b></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dets as $det)
                                            <tr>
                                                <td><a href="{{ route('player.show',$det->player->username) }}">{{$det->player->username}}</a></td>
                                                <td>{{ $det->player->getCurrentRank->ranks->rank }}</td>
                                                <td>{{ $det->dets }}</td>
                                                <td>
                                                    @if($det->detsCountry <= $countries->count())
                                                        {{ $det->searchInCountry->name }}
                                                    @elseif($det->detsCountry == $countries->count() + 1)
                                                        All
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!empty($det->country->name))
                                                        {{$det->country->name}}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($det->found == 1)
                                                        Found
                                                    @else
                                                        Searching
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($det->found == 1)
                                                        <div id="{{ $det->id }}"></div>
                                                        <script type="text/javascript">countdown('<?php echo \Carbon\Carbon::parse($det->endTime)->format('d/m/Y H:i:s') ?>','dets',<?php echo $det->id ?>);</script>
                                                    @else
                                                        Searching
                                                    @endif
                                                </td>
                                                <td><a href="{{ route('detective.restart', $det->id) }}">Restart</a></td>
                                                <td><a href="{{ route('detective.delete', $det->id) }}">Delete</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready( function () {
            $('#detectives').DataTable({
                responsive: true,
                columnDefs: [
                    { orderable: false, targets: [7,8] }
                ]
            });
        } );

        function costsDets(dets,countries,time) {
            function formatMoney(number) {
                return number.toLocaleString('nl-NL', {
                    style: 'currency', currency: 'EUR', minimumFractionDigits: 0,
                    maximumFractionDigits: 0
                });
            }

            if (<?= $allCountries ?> == countries){
                const cashNetto = dets * 15 * time * 30;
                document.getElementById("costs").innerHTML = formatMoney(cashNetto);
            }
            else if(countries < <?= $allCountries ?>){
                const cashNetto = dets * 1 * time * 30;
                document.getElementById("costs").innerHTML = formatMoney(cashNetto);
            }
            else{
                document.getElementById("costs").innerHTML = "Error";
            }
        }
    </script>

@endsection

