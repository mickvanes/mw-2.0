
<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">



                    <div x-data="{ show: 1 }">
                        <div class="mb-4">
                            <button  @click="show=1" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                                Withdraw
                            </button>
                            <button  @click="show=2" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4" type="button">
                                Deposit
                            </button>
                        </div>

                        <div x-show="show === 1">
                            <p>Withdraw Transactions</p>
                            <table class="table-fixed m-auto w-full mb-4">
                                <thead>
                                <tr>
                                    <th class="text-left">Cash</th>
                                    <th class="text-left">Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bankLogsWithdraw as $withdrawItem)
                                    <tr>
                                        <td class="text-left">&euro; -{{ number_format($withdrawItem->cash, 0, ',', '.') }}</td>
                                        <td class="text-left">{{ $withdrawItem->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>{{ $bankLogsWithdraw->links() }}</div>
                        </div>

                        <div x-show="show === 2">
                            <p>Deposit Transactions</p>
                            <table class="table-fixed m-auto w-full mb-4">
                                <thead>
                                <tr>
                                    <th class="text-left">Cash</th>
                                    <th class="text-left">Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bankLogsDeposit as $depositItem)
                                    <tr>
                                        <td class="text-left">&euro; +{{ number_format($depositItem->cash, 0, ',', '.') }}</td>
                                        <td class="text-left">{{ $depositItem->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>{{ $bankLogsDeposit->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
