<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidHCaptcha implements Rule
{
    public function __construct()
    {
        //
    }

    public function passes( $attribute, $value )
    {
        $data = array(
            'secret' => env('H_CAPTCHA_SECRET'),
            'response' => $value
        );

        $verify = curl_init();

        curl_setopt($verify, CURLOPT_URL, "https://challenges.cloudflare.com/turnstile/v0/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($verify);
        $responseData = json_decode( $response );

        if($responseData->success) {
            return true;
        } else {
            return false;
        }
    }

    public function message()
    {
        return 'Invalid CAPTCHA. You need to prove you are human.';
    }
}
