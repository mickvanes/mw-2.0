@extends('layouts.default')

@section('header')
    Forum
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <div class="grid grid-cols-1">
                        <div class="md:mt-0">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Topic</strong>
                                        <span class="block sm:inline">{{ session()->pull('message') }}</span>
                                    </div>
                                </div><br>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                         role="alert">
                                        <strong class="font-bold">Topic</strong>
                                        <span class="block sm:inline">{{ session()->pull('error') }}</span>
                                    </div>
                                </div><br>
                            @endif
                        </div>
                        <div class="grid grid-cols-1 mb-4">
                            <a href="{{ route('family.forum') }}">Back</a>
                            @foreach($forumCategory as $category)
                                {{ $category->title }}<br>
                            <div>
                                <div class="grid grid-cols-5 mb-2">
                                    <div class="col-span-2"><b>Topic</b></div>
                                    <div class="text-center"><b>User</b></div>
                                    <div class="text-center"><b>Reactions</b></div>
                                    <div class="text-center"><b>Latest reaction</b></div>
                                </div>
                            </div>

                            <div class="mb-4">
                                    @foreach($category->topics as $topic)
                                        <a href="{{ route('family.topic',$topic->id) }}">
                                            <div class="grid grid-cols-5 mb-2">
                                                <div class="col-span-2">
                                                    {{ $topic->name }}
                                                </div>
                                                <div class="text-center">{{ $topic->user->username }}</div>
                                                <div class="text-center">{{ $topic->reactions->count() }}</div>
                                                <div class="text-center">{{ $topic->reactions->sortByDesc('id')->first()->created_at ?? "No reactions"}}</div>
                                            </div>
                                        </a>
                                    @endforeach
                                @endforeach

                            </div>
                                {{ $forumCategory->links() }}

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
