<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Cash;
use App\Models\Country;
use App\Models\Defense;
use App\Models\Profile;
use App\Models\Rank;
use App\Models\Security;
use App\Models\Settings;
use App\Models\Stats;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\DB;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register'); // auth.register
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     *
     * @throws \Illuminate\Validation\ValidationException
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|alpha|max:20|unique:users,username',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

            $user = User::create([
                'username' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            Profile::create([
                'id' => $user->id,
                'rankAdv' => '0',
                'bullets' => '0',
                'employees' => '0',
                'clicks' => '0',
                'rep' => '0',
                'honor' => '0',
                'power' => '0',
                'health' => '100',
                'kills' => '0',
                'coins' => '0',
                'profileText' => '',
            ]);

            Defense::create([
               'userID' => $user->id,
               'totalDefense' => 0,
            ]);

            Rank::create([
                'userID' => $user->id,
                'rankID' => 1,
            ]);

            Security::create([
                'userID' => $user->id,
                'minutes' => 120,
                'beginTime' => Carbon::now('Europe/Amsterdam'),
                'endTime' => Carbon::now('Europe/Amsterdam')->addHours(2)
            ]);

            Settings::create([

            ]);

            $allCountries = Country::all()->count();

            Country::create([
                'userID' => $user->id,
                'countryID' => mt_rand(1,$allCountries),
            ]);

            Stats::create([
                'id' => $user->id,
                'crime' => 0,
                'car' => 0,
                'hostage' => 0,
                'races' => 0,
                'created_at' => Carbon::now('Europe/Amsterdam'),
                'updated_at' => Carbon::now('Europe/Amsterdam'),
            ]);

            Cash::create([
                'userID' => $user->id,
                'cash' => 0,
                'bank' => 0,
                'safe' => 0,
                'time' => Carbon::now('Europe/Amsterdam')
            ]);

            event(new Registered($user));

            Auth::login($user);

            return redirect(RouteServiceProvider::HOME);
        }
}
