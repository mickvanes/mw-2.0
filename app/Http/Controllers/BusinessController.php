<?php

namespace App\Http\Controllers;

use App\Models\BackfireKill;
use App\Models\Business;
use App\Models\BusinessBank;
use App\Models\BusinessTransaction;
use App\Models\BusinessType;
use App\Models\Cash;
use App\Models\Country;
use App\Models\Hospital;
use App\Models\HospitalBlood;
use App\Models\Kill;
use App\Models\Profile;
use App\Models\Rule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class BusinessController extends Controller
{
    public function index(){
        $businesses = BusinessType::with('business')->paginate(3);

        return view('businesses.index',compact('businesses'));
    }

    public function pincode($id){
        $country = Country::where('userID','=', Auth::id())->first();
        $business = Business::where('id', '=', $id)->first();

        if($business->countryID == $country->countryID){
            if($business->ownerID == Auth::id()){
                return view('businesses.pincode', compact('business','country'));
            }
            else{
                return Redirect::back()->with('error', "You don't own this business");
            }
        }
        else{
            return Redirect::back()->with('error', "You are not in the same country as this business");
        }
    }

    public function pincodeForm(Request $request){
        $this->validate($request, [
            'pincode' => 'required|digits_between:4,4|numeric',
        ]);

        $pincode = BusinessBank::where([
            ['id','=',$request->id],
            ['ownerID','=',Auth::id()]
        ])->firstOrFail()->pincode;

        $businessType = Business::where('id','=',$request->id)->first()->typeID;

        if($request->pincode == $pincode){
            DB::table("businesses")
                ->where([
                    ['id','=',$request->id],
                    ['ownerID','=',Auth::id()]
                ])->update(["codeExpire" => Carbon::now()->addMinutes(15)]);

            if($businessType == 1){
                return redirect()->route('prison.owner',$request->id);
            }
            elseif ($businessType == 2){
                return redirect()->route('hospital.owner',$request->id);
            }
            else{
                return redirect()->route('dashboard')->with('message', 'Goodboii');
            }
        }
        else{
            return Redirect::back()->with('error', 'Invalid Code');
        }


    }

    public function businessBank(Request $request){

        $this->validate($request, [
            'transaction_type' => 'required',
            'transaction' => 'required|numeric',
        ]);

        $userCash = DB::table('user_cash')
            ->select('cash')
            ->where('user_cash.userID', '=', Auth::id())
            ->pluck('cash')
            ->first();

        $businessBank = DB::table('business_bank')
            ->select('bank')
            ->where([
                ['business_bank.ownerID', '=', Auth::id()],
                ['business_bank.id', '=', $request->id]
            ])
            ->pluck('bank')
            ->first();

        if ($request->input('transaction_type') == 0) {
            $trans = $request->input('transaction');
            if ($trans <= $businessBank) {
                DB::table('user_cash')
                    ->where('userID', Auth::id())
                    ->update(['cash' => $userCash + $request->input('transaction')]);
                DB::table('business_bank')
                    ->where('ownerID', Auth::id())
                    ->update(['bank' => $businessBank - $request->input('transaction')]);
                BusinessTransaction::create(
                    [
                        'businessID' => $request->id,
                        'transactionType' => 0,
                        'cash' => $request->input('transaction'),
                        'owner' => Auth::id(),
                    ]
                );
                return Redirect::back()->with('message', "You successfully withdraw €" . number_format($request->input('transaction'), 0, ',', '.') . ' of your Business bank.');
            } else {
                return Redirect::back()->with('error', "You don't have €" . number_format($request->input('transaction'), 0, ',', '.') . ' .');
            }
        } elseif ($request->input('transaction_type') == 1) {
            $trans = $request->input('transaction');
            if ($trans <= $userCash) {
                DB::table('user_cash')
                    ->where('userID', Auth::id())
                    ->update(['cash' => $userCash - $request->input('transaction')]);
                DB::table('business_bank')
                    ->where('ownerID', Auth::id())
                    ->update(['bank' => $businessBank + $request->input('transaction')]);
                BusinessTransaction::create(
                    [
                        'businessID' => $request->id,
                        'transactionType' => 1,
                        'cash' => $request->input('transaction'),
                        'owner' => Auth::id(),
                    ]
                );
                return Redirect::back()->with('message', "You successfully deposit €" . number_format($request->input('transaction'), 0, ',', '.') . ' to your Business bank.');
            } else {
                return Redirect::back()->with('error', "You don't have €" . number_format($request->input('transaction'), 0, ',', '.') . ' cash.');
            }
        }
    }



}
