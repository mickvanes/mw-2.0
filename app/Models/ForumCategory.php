<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForumCategory extends Model
{
    use HasFactory;

    protected $table = 'forum_categories';

    protected $fillable = [
        'title',
        'subTitle',
        'list',
    ];

    public function topics()
    {
        return $this->hasMany(ForumTopic::class, 'categoryID');
    }

}
