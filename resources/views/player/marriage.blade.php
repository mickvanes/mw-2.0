@extends('layouts.default')

@section('header')
    RACE
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    @if($errors)
                        @foreach ($errors->all() as $message)
                            <div class="alert alert-danger">
                                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                     role="alert">
                                    <strong class="font-bold">Message</strong>
                                    <span class="block sm:inline">{{$message}}</span>
                                </div>
                            </div><br>
                        @endforeach
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Error</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif
                            @if($userMarriagePlayer1)
                                @foreach($userMarriagePlayer1 as $marriage)
                                    @if($marriage->accepted)
                                        You are married with {{$marriage->username}}
                                        <br/><br/>
                                        <a href="{{ route('marriage.action','divorce') }}" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4">Divorce</a>
                                    @else
                                        {{$marriage->username}} hasn't said yes to your proposal yet.
                                        <br/><br/>
                                        <a href="{{ route('marriage.action','cancel') }}" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4">Cancel</a>


                                    @endif
                                @endforeach
                            @endif
                            @if($userMarriagePlayer2)
                                @foreach($userMarriagePlayer2 as $marriage)
                                    @if($marriage->accepted)
                                        You are married with {{$marriage->username}}
                                        <br/><br/>
                                        <a href="{{ route('marriage.action','divorce') }}" class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4">Divorce</a>
                                    @else
                                        Neem het huwelijksverzoek aan of wijs het huwelijksverzoek af van {{$marriage->username}} <br><br>
                                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4"><a href="{{route('marriage.action', "accept")}}">Neem het huwelijksverzoek aan</a></button> <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4"><a href="{{route('marriage.action', "decline")}}">Wijs het huwelijksverzoek af</a></button>
                                @endif
                                @endforeach
                            @endif
                        @if($userMarriagePlayer1->isEmpty() && $userMarriagePlayer2->isEmpty())
                            <form method="post" action="{{ route('marriage.store') }}" id="marriageForm" autocomplete="off">

                                <!-- CROSS Site Request Forgery Protection -->
                                @csrf

                                <div class="form-group">
                                    <div class="grid grid-cols-5">
                                        <div>
                                            <label for="player">Player:</label>
                                        </div>
                                        <div><input type="text" id="player"
                                                    name="player" required></div>
                                    </div>
                                    <div></div>
                                    <div class="grid grid-cols-5 mt-2">
                                        <div>

                                        </div>
                                        <div>
                                            <input type="submit" name="send" value="Submit"
                                                   class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4">
                                        </div>
                                    </div>
                                </div>

                            </form>
                        @endif


                </div>
            </div>
        </div>
    </div>
@stop
