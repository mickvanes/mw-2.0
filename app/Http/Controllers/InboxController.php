<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Ban;
use App\Models\BlockedUser;
use App\Models\Message;
use App\Models\MessageReport;
use App\Models\SystemMessage;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class InboxController extends Controller
{
    public function inbox()
    {
        $messages = Message::with('sender','systemSender')
            ->where([
                'recipientID' => Auth::id(),
                'deletedRecipient' => 0
            ])
            ->orderBy('created_at','desc')
            ->paginate(15);

        return view('player.inbox', compact('messages'));
    }

    public function outbox()
    {
        $messages = Message::with('recipient')
            ->where([
                'senderID' => Auth::id(),
                'deletedSender' => 0
            ])
            ->orderBy('created_at','desc')
            ->paginate(15);

        return view('player.outbox', compact('messages'));
    }

    public function userMessage($id)
    {
        $message = Message::findOrFail($id);

        if(Auth::id() == $message->recipientID && $message->deletedRecipient){
            return redirect()->route('inbox')->with('error', "This message doesn't exists.");
        }
        else{
            if ($message->systemMessage == 0) {
                $checkUser = DB::table('user_messages')
                    ->where('user_messages.id', '=', $id)
                    ->select('user_messages.recipientID', 'user_messages.senderID')
                    ->get();

                if ($checkUser[0]->recipientID == Auth::id()) {
                    $message = DB::table('user_messages')
                        ->join('users', 'users.id', '=', 'user_messages.senderID')
                        ->where([
                            'user_messages.recipientID' => Auth::id(),
                            'user_messages.id' => $id
                        ])
                        ->select('user_messages.*', 'users.username')
                        ->first();

                    DB::table('user_messages')
                        ->where([
                            ['user_messages.id', '=', $id],
                            ['recipientID', Auth::id()]
                        ])
                        ->update(['seen' => 1, 'seenOn' => Carbon::now()]);

                    return view('player.message', compact('message'));

                } elseif (Auth::id() == $checkUser[0]->senderID) {

                    $message = DB::table('user_messages')
                        ->join('users', 'users.id', '=', 'user_messages.senderID')
                        ->where([
                            ['user_messages.senderID', '=', Auth::id()],
                            ['user_messages.id', '=', $id]
                        ])
                        ->select('user_messages.*', 'users.username')
                        ->first();

                    return view('player.message', compact('message'));
                } else {
                    return redirect()->route('inbox')->with('error', "You don't have the rights to read this message");
                }
            }
            else {
                $checkUser = DB::table('user_messages')
                    ->where('user_messages.id', '=', $id)
                    ->select('user_messages.recipientID', 'user_messages.systemID')
                    ->get();

                if ($checkUser[0]->recipientID == Auth::id()) {
                    $message = DB::table('user_messages')
                        ->join('system_users', 'system_users.id', '=', 'user_messages.systemID')
                        ->where('user_messages.recipientID', '=', Auth::id())
                        ->where('user_messages.id', '=', $id)
                        ->select('user_messages.*', 'system_users.name')
                        ->first();

                    DB::table('user_messages')
                        ->where([
                            ['user_messages.id', '=', $id],
                            ['recipientID', Auth::id()]
                        ])
                        ->update(['seen' => 1, 'seenOn' => Carbon::now()]);

                    return view('player.message', compact('message'));
                }
            }
        }

    }

    public function newMessage()
    {
        return view('player.newMessage');
    }

    public function replyMessage($id)
    {
        $message = Message::findOrFail($id);

        $recipient = User::where('id', '=', $message->senderID)->pluck('username')->first();
        $subject = "Re: " . $message->subject;
        if(strlen($subject) >= 50){
            $subject = substr($subject, 0, 50);
        }

        return view('player.replyMessage', compact('recipient', 'subject'));
    }

    public function reportMessage($id)
    {
        $message = Message::findOrFail($id);

        if($message){
            if($message->recipientID == Auth::id()){
                $check = MessageReport::where([
                    ['itemID', '=', $message->id],
                    ['user', '=', Auth::id()]
                ])->first();

                if(!$check){
                    MessageReport::create([
                        'itemID' => $message->id,
                        'content' => $message->body,
                        'user' => Auth::id(),
                        'repUser' => $message->senderID,
                        'active' => 0,
                        'approved' => 0,
                        'declined' => 0,
                    ]);
                }
                else{
                    return redirect()->route('inbox')->with('error', "You have already reported this message");
                }

                return redirect()->route('inbox')->with('message', "You have reported this message");
            }
            else{
                return redirect()->route('inbox')->with('error', "You have no rights on this message");
            }
        }
    }

    public function deleteMessage($id){
        $message = Message::findOrFail($id);

        if($message){
            if($message->senderID == Auth::id() || $message->recipientID == Auth::id()){
                if($message->senderID == Auth::id()){
                    $message->deletedSender = 1;
                    $message->save();
                    return redirect()->route('inbox')->with('message', "Deleted Message");
                }
                elseif($message->recipientID == Auth::id()){
                    $message->deletedRecipient = 1;
                    $message->save();
                    return redirect()->route('inbox')->with('message', "Deleted Message");
                }
            }
        }
    }

    public function deleteSystemMessage($id){
        $message = systemMessage::findOrFail($id);

        if($message){
            if($message->recipientID == Auth::id()){
                $message->delete();
                return redirect()->route('inbox')->with('message', "Deleted Message");
            }
            else{
                return redirect()->route('inbox')->with('error', "You have no rights on this message");
            }
        }
    }

    public function formMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required|max:25|regex:/^[a-zA-Z0-9!@#$%^&*()_\-+=?,.]+/',
            'message' => "required|max:6000",
            'recipient' => 'required|exists:users,username',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('message.create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $checkOwnName = User::where('id','=' , Auth::id())->pluck('username')->first();
            $recipient = ucfirst(strtolower($request->input('recipient')));

            if($recipient == $checkOwnName){
                return redirect()->route('message.create')->with('error', "You can't yourself messages");
            }

            $recipient = DB::table("users")
                ->select('*')
                ->where('users.username', '=', $request->input('recipient'))
                ->pluck('users.id')
                ->first();

            if($recipient){
                $blocked = BlockedUser::where([
                    ['userID', '=', $recipient],
                    ['blockedUserID', '=', Auth::id()]
                ])->first();

                $hasBlocked = BlockedUser::where([
                    ['userID', '=', Auth::id()],
                    ['blockedUserID', '=', $recipient]
                ])->first();

                $mutedRecipient = Ban::where([
                    ['userID', '=', $recipient],
                    ['type', '=', 0],
                ])->first();

                $isMuted = Ban::where([
                    ['userID', '=', Auth::id()],
                    ['type', '=', 0],
                ])->first();

                if($mutedRecipient){
                    $dt     = Carbon::now();

                    $daysRecipient = $dt->diff($mutedRecipient->expires)->format('%d');
                    $hoursRecipient = $dt->diff($mutedRecipient->expires)->format('%H');
                    $minutesRecipient = $dt->diff($mutedRecipient->expires)->format('%I');

                    $betweenRecipient = $dt->between($mutedRecipient->since, $mutedRecipient->expires);
                }
                else{
                    $betweenRecipient = false;
                }

                if($isMuted){
                    $dt     = Carbon::now();

                    $daysUser = $dt->diff($isMuted->expires)->format('%d');
                    $hoursUser = $dt->diff($isMuted->expires)->format('%H');
                    $minutesUser = $dt->diff($isMuted->expires)->format('%I');

                    $betweenUser = $dt->between($isMuted->since, $isMuted->expires);
                }
                else{
                    $betweenUser = false;
                }

                if($blocked){
                    return redirect()->route('message.create')->with('error', 'This user has blocked you');
                }
                elseif($hasBlocked){
                    return redirect()->route('message.create')->with('error', 'You have blocked this user');
                }
                elseif($betweenUser){
                    return redirect()->route('message.create')->with('error', "You are muted for $daysUser day(s) $hoursUser hour(s) $minutesUser minute(s)");
                }
                elseif ($betweenRecipient){
                    $message = htmlspecialchars($request->input('message'));

                    Message::create([
                        'senderID' => Auth::id(),
                        'recipientID' => $recipient,
                        'subject' => $request->input('subject'),
                        'body' => $message,
                        'seen' => 0
                    ]);

                    return redirect()->route('message.create')->with('message', "Message sent, this user is muted for $daysRecipient day(s) $hoursRecipient hour(s) $minutesRecipient minute(s)");
                }
                else{
                    $message = htmlspecialchars($request->input('message'));

                    Message::create([
                        'senderID' => Auth::id(),
                        'recipientID' => $recipient,
                        'subject' => $request->input('subject'),
                        'body' => $message,
                        'seen' => 0
                    ]);

                    return redirect()->route('inbox')->with('message', 'Message sent');
                }
            }
            else{
                return redirect()->route('message.create')->with('error', 'This recipient does not exist');
            }
        }
    }
}
