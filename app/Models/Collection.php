<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Collection extends Model
{
    use HasFactory;

    protected $table = 'collection';

    protected $fillable = [
        'collectionID',
        'cardID'
    ];

    public function card()
    {
        return $this->belongsTo(CollectionCards::class, 'cardID');
    }

    public function collectionDetails()
    {
        return $this->belongsTo(Collections::class, 'collectionID');
    }
}
