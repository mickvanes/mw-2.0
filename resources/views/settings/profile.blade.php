@extends('layouts.default')

@section('header')
    USER SETTINGS
@stop

@section('content')
    <div class="mx-auto h-full overflow-hidden w-full">
        <div class="max-w-7xl mx-auto">
            <div class="bg-lightDefault dark:bg-darkDefault shadow-sm">
                <div class="p-6 bg-lightDefault text-white dark:bg-darkDefault dark:text-white">
                    <form action="" id="form" method="post">
                        {!! (new \App\Helpers\Helper)->makeUBB($profile->profileText) !!}
                        <textarea id="profile" class="text-black">{{ $profile->profileText }}</textarea>
                    </form>
                </div>
                <input type="submit" name="send" value="Submit" class="bg-gray-500 hover:bg-gray-700 mt-4 text-white font-bold py-2 px-4">
            </div>
        </div>
    </div>
@endsection

