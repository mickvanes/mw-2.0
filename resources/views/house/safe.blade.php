<x-app-layout>
    <x-slot name="header">
        <div class="grid grid-cols-6">
            <div>
                <label>
                    <a href="{{route('dashboard')}}">
                        {{ __('Player') }} {{$userName}}
                    </a>
                </label>
            </div>
            <a href="{{route('bank')}}"><div>Cash: &euro;{{ number_format($userCash, 0, ',', '.') }}</div></a>
            <a href="{{route('bank')}}"><div>Bank: &euro;{{ number_format($userBank, 0, ',', '.') }}</div></a>
            <div></div>
            <div>
                <div class="float-right">
                </div>
            </div>

            <div>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <x-nav-link class="float-right" :href="route('logout')"
                                onclick="event.preventDefault();
                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-nav-link>
                </form>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('error') }}</span>
                            </div>
                        </div><br>
                    @endif
                    @if(session()->has('flash'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">System</strong>
                                <span class="block sm:inline">{{ session()->pull('flash') }}</span>
                            </div>
                        </div><br>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                                 role="alert">
                                <strong class="font-bold">Message</strong>
                                <span class="block sm:inline">{{ session()->pull('message') }}</span>
                            </div>
                        </div><br>
                    @endif

                        @if(!$currentCountryBoolean)
                            <br><br>
                            <button class="bg-gray-500 text-white font-bold py-2 px-2" style="font-size: 100%;">
                                <a href="{{ route('airport') }}">Ga naar het vliegveld!</a>
                            </button>
                        @else

                            <div class="grid grid-cols-3">
                                <div>
                                    <div><b>Cash:</b> &euro;{{ number_format($userCash, 0, ',', '.') }}</div>
                                </div>
                                <div>
                                    <div><b>Bank:</b> &euro;{{ number_format($userBank, 0, ',', '.') }}</div>
                                </div>
                                <div>
                                    <div><b>Safe:</b> &euro;{{ number_format($userSafe, 0, ',', '.') }}</div>
                                </div>
                            </div>
                            <form method="post" action="{{ route('safe.store') }}" class="mt-4 lg:mt-0"> {{--  --}}
                                @csrf

                                <div class="grid grid-cols-3 mt-4 mb-4">
                                    <div><label for="safe">Safe Transaction Type</label></div>
                                    <div><input type="radio" id="withdraw"
                                                name="safe_type"
                                                value="0"><label for="withdraw"> Withdraw</label></div>
                                    <div><input type="radio" id="deposit"
                                                name="safe_type"
                                                value="1"><label for="deposit"> Deposit</label></div>
                                </div>
                                <div class="grid grid-cols-3 mt-2">
                                    <div class="py-2"><label for="transaction">Cash &euro;</label></div>
                                    <div><input type="number" id="transaction"
                                                name="transaction" min="0"></div>
                                </div>
                                <input type="hidden" name="action" value="send">
                                <input type="submit" value="Submit"
                                       class="bg-blue-500 hover:bg-blue-700 mt-4 text-white font-bold py-2 px-4">
                            </form>
                        @endif
                </div>
            </div>
        </div>
</x-app-layout>





